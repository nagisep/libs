#include "strnum.h"

const unsigned char *table = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

int isEqualStr(char *src, char *dest, int keta)
{
  int i;
  for (i = 0; i < keta; i++)
    if (src[i] != dest[i]) return 0;
  return 1;
}

unsigned char toValueA(unsigned char c)
{
  unsigned char i;
  for (i = 0; i < 36; i++)
    if (table[i] == c) return i;
  return 2;
}

void toStr(unsigned long v, char *buff, int keta, unsigned int radix)
{
   int i;

   for(i = keta - 1; i >= 0; i--)
   {
     buff[i] = table[v % radix];
     v /= radix;
   }
}

void zeroSuppress(char *buff)
{
  
  while(*buff == '0')
  {
     if (*(buff + 1) == '.') break;
     if (*(buff + 1) == '\0') break;
      *buff = ' ';
      buff++;
  }
}

unsigned long toNum(char *buff, int keta, int radix)
{
  int i;
  unsigned long wk = 0;

  for(i = 0; i < keta; i++)
  {
     wk *= radix;
     wk += toValueA(buff[i]);
  }
  return wk;

}

void makeString(char *src, char *dest, int keta)
{
  int pSrc  = 0;
  int pDest = 0;
  int i;

  for(i = 0; i < keta; i++)
  {
    dest[pDest] = toNum(&src[pSrc], 2, 16);
    pSrc += 2;
    pDest++;
  }
}

void setStr(char *src, char *dest)
{
   int i;
   for(i = 0; src[i]; i++)
     dest[i] = src[i];

   dest[i] = '\0';
}

void setStrN(char *src, char *dest, int keta)
{
   int i;
   for(i = 0; src[i] && (i < keta) ; i++)
     dest[i] = src[i];

   dest[i] = '\0';
}

