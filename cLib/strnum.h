#if !defined(_BSL_3664_STRNUM)
#define      _BSL_3664_STRNUM

int isEqualStr(char *src, char *dest, int keta);
/* keta の文字を比較して結果を返す Equal の時 1, not equal の時 0 */

void toStr(unsigned long v, char *buff, int keta, unsigned int radix);
/* v を keta 桁の数値と見なして、buff の先頭から展開する */

void zeroSuppress(char *buff);
/* buff を走査して先頭の０をスペースに置き換える */

unsigned long toNum(char *buff, int keta, int radix);
/* buff の文字を、keta 桁の数値と見なして、数値化する */

void makeString(char *src, char *dest, int keta);
/* 16進文字列として表現された、src の keta 文字分（変換後の文字数で）を文字コードと見なして、dest に ASCII 展開する */

void setStr(char *src, char *dest);
/* src の文字列の '\0' までを （'\0' を含めて）dest にコピーする。 */

void setStrN(char *src, char *dest, int keta);
/* src の文字列の '\0' までを 最大 keta 文字の範囲で（'\0' を含めて）dest にコピーする。 */

#endif

;