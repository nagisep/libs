
#include "libFloat.h"
#include "strnum.h"

const long exp10[] = {1, 10, 100, 1000, 10000, 100000, 1000000};

void toStrFloat(double value, char *buff, int up, int down)
{
  char buff[16];
  long upValue;
  long downValue;

  upValue = value;
  value -= upValue;
  downValue = value * exp10[down];
  toStr(upValue, buff, up, 10);
  buff[up] = '.';
  toStr(downValue, buff + up + 1, down, 10);
  zeroSuppress(buff);
}

int isNum(char c)
{
   return ( ('0' <= c) && (c <= '9') || (c == ' '));
}

int setNum(char *src, char *dest)
{
  int i;
  for (i = 0; isNum(src[i]); i++)
  {
    dest[i] = src[i];
    if (dest[i] == ' ') dest[i] = '0';
  }
  return i;
}

double toNumFloat(char *buff)
{
  char up[16];
  char down[16];
  int  ketaUp   = 0i;
  int  ketaDown = 0;
  double dUp   = 0;
  double dDown = 0;

  ketUp = setNum(buff, up);
  if (buff[i] == '.')
    ketaDown = setNum(buff + i + 1, down);

  dUp = toNum(up, ketaUp, 10);
  dDown = toNum(down, ketaDown, 10);
  dDown /= (double)exp10[ketaDown];
  return dUp + dDown;
}

