#if !defined(_BCL_LIB_FLAOT)
#define      _BCL_LIB_FLAOT

void toStrFloat(double value, char *buff, int up, int down);
/* value を整数部分 up 桁、小数点以下 down 桁に展開する */

double toNumFloat(char *buff);
/* buff の文字列を数値と見なして、double 型に変換する */

#endif

