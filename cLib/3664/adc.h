#if !defined(_BCL_3664_ADC)
#define      _BCL_3664_ADC


unsigned int getADC(int ch);
/* ch で指定されたAD変換値を、16bit（有効幅は 10bit）で返却する */

#endif


