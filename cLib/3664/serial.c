#include "serial.h"

#define SMR	 (unsigned char *)(0xffa8)
#define BRR	 (unsigned char *)(0xffa9)
#define SCR3 (unsigned char *)(0xffaa)
#define TDR	 (unsigned char *)(0xffab)
#define SSR	 (unsigned char *)(0xffac)
#define RDR	 (unsigned char *)(0xffad)

unsigned char v; /* dummy read を実際に実行するためにグローバル変数にする */

void seriInit(int bps)
{

 unsigned int  i;

 *SCR3 = 0x00;
 *SMR  = 0x00;  /* no parity */
 
 switch(bps)
 {  case  4800: *BRR =  95; break;
    case  9600: *BRR =  47; break;
    case 19800: *BRR =  23; break;
    case 38400: *BRR =  11; break;
    default:    *BRR =  47; break;
 }

 for(i = 0; i < 1000; i++); /* レジスタセットまでのあいだ待ち */

 *SCR3 = 0x30; /* = 0011 0000 */
 v = *SSR;
 *SSR = 0x00;
}

void seriSend(unsigned char a)
{
  *TDR = a;
  while ((*SSR & 0x80) == 0);
}

void seriSendString(unsigned char *str)
{
  while(*str)
      seriSend(*(str++));
   seriSend('\x0d');

}



int loc()
{
  return ((*SSR & 0x40) != 0);
}

unsigned char seriGet()
{
   unsigned char v;
   v = *RDR;
   *SSR = ~0x40;
   return v;
}

void seriGetString(unsigned char *str)
{
   unsigned char c;
   int index = 0;

   while(1)
   {
       while(! loc());
       c = seriGet();
       if (c == 0x0d) 
       {
          str[index] = '\0';
          break;
       }
       str[index++] = c;
   }

}

