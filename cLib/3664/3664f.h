
/************************************************/
/*						*/
/*		H8/3664 ADDRESS			*/
/*						*/
/************************************************/

#ifndef	_H_3664
#define	_H_3664

struct portDef
{
  unsigned char b7 : 1;
  unsigned char b6 : 1;
  unsigned char b5 : 1;
  unsigned char b4 : 1;

  unsigned char b3 : 1;
  unsigned char b2 : 1;
  unsigned char b1 : 1;
  unsigned char b0 : 1;
};  

#define	TMRW	(volatile char*)(0xff80)
#define	TCRW	(volatile char*)(0xff81)
#define	TIERW	(volatile char*)(0xff82)

#define	TSRW	(volatile char*)(0xff83)
#define	TIOR0	(volatile char*)(0xff84)
#define	TIOR1	(volatile char*)(0xff85)
#define	TCNT	(volatile short*)(0xff86)

#define	GRA	(volatile short*)(0xff88)
#define	GRB	(volatile short*)(0xff8a)
#define	GRC	(volatile short*)(0xff8c)
#define	GRD	(volatile short*)(0xff8e)

/****************/
/*	ROM	*/
/****************/
#define	FLMCR1	(volatile char*)(0xff90)
#define	FLMCR2	(volatile char*)(0xff91)
#define	FLPWCR	(volatile char*)(0xff92)
#define	EBR1	(volatile char*)(0xff93)
#define	FENR	(volatile char*)(0xff9b)

/****************/
/*		*/
/****************/
#define	TCRV0	(volatile char*)(0xffa0)
#define	TCSRV	(volatile char*)(0xffa1)
#define	TCORA	(volatile char*)(0xffa2)
#define	TCORB	(volatile char*)(0xffa3)
#define	TCNTV	(volatile char*)(0xffa4)
#define	TCRV1	(volatile char*)(0xffa5)


/****************/
/*		*/
/****************/
#define	TMA	(volatile char*)(0xffa6)
#define	TCA	(volatile char*)(0xffa7)


/**********************************/
/* SIRIAL COMMUNICATION INTERFACE */
/**********************************/
#define	SMR	(volatile char*)(0xffa8)	/* ｼﾘｱﾙﾓｰﾄﾞﾚｼﾞｽﾀ */
#define	BRR	(volatile char*)(0xffa9)	/* ﾋﾞｯﾄﾚｰﾄﾚｼﾞｽﾀ */
#define	SCR3	(volatile char*)(0xffaa)	/* ｼﾘｱﾙｺﾝﾄﾛｰﾙﾚｼﾞｽﾀ3 */
#define	TDR	(volatile char*)(0xffab)	/* ﾄﾗﾝｽﾐｯﾄﾃﾞｰﾀﾚｼﾞｽﾀ */
#define	SSR	(volatile char*)(0xffac)	/* ｼﾘｱﾙｽﾃｰﾀｽﾚｼﾞｽﾀ */
#define	RDR	(volatile char*)(0xffad)	/* ﾚｼｰﾌﾞﾃﾞｰﾀﾚｼﾞｽﾀ */

/****************/
/* A/D			*/
/****************/
#define	ADDRA	(volatile short*)(0xffb0)
#define	ADDRB	(volatile short*)(0xffb2)
#define	ADDRC	(volatile short*)(0xffb4)
#define	ADDRD	(volatile short*)(0xffb6)
#define	ADCSR	(volatile char*)(0xffb8)
#define	ADCR	(volatile char*)(0xffb9)



/****************/
/* WDT		*/
/****************/
#define	TCSRWD	(volatile char*)(0xffC0)
#define	TCWD	(volatile char*)(0xffC1)
#define	TMWD	(volatile char*)(0xffC2)

/****************/
/* IIC		*/
/****************/
#define	ICCR	(volatile char*)(0xffC4)
#define	ICSR	(volatile char*)(0xffC5)
#define	ICDR	(volatile char*)(0xffC6)
#define	SARX	(volatile char*)(0xffC6)
#define	ICMR	(volatile char*)(0xffC7)
#define	SAR	(volatile char*)(0xffC7)
#define	TSCR	(volatile char*)(0xfffc)

/****************/
/* ｱﾄﾞﾚｽﾌﾞﾚｰｸ	*/
/****************/
#define	ABRKCR	(volatile char*)(0xffc8)	/* ｱﾄﾞﾚｽﾌﾞﾚｰｸｺﾝﾄﾛｰﾙﾚｼﾞｽﾀ */
#define	ABRKSR	(volatile char*)(0xffc9)	/* ｱﾄﾞﾚｽﾌﾞﾚｰｸｽﾃｰﾀｽﾚｼﾞｽﾀ	*/
#define BAR	(volatile short*)(0xffca)
#define	BARH	(volatile char*)(0xffca)	/* ﾌﾞﾚｰｸｱﾄﾞﾚｽﾚｼﾞｽﾀ(H) */
#define	BARL	(volatile char*)(0xffcb)	/* ﾌﾞﾚｰｸｱﾄﾞﾚｽﾚｼﾞｽﾀ(L) */
#define BDR	(volatile short*)(0xffcc)
#define	BDRH	(volatile char*)(0xffcc)	/* ﾌﾞﾚｰｸﾃﾞｰﾀﾚｼﾞｽﾀ(H) */
#define	BDRL	(volatile char*)(0xffcd)	/* ﾌﾞﾚｰｸﾃﾞｰﾀﾚｼﾞｽﾀ(L) */

/************/
/* I/O PORT */
/************/
#define	PUCR1	(volatile char*)(0xffd0)
#define	PUCR5	(volatile char*)(0xffd1)
#define	PDR1	(volatile char*)(0xffd4)
#define	PDR2	(volatile char*)(0xffd5)
#define	PDR5	(volatile char*)(0xffd8)
#define	PDR7	(volatile char*)(0xffda)
#define	PDR8	(volatile char*)(0xffdb)
#define	PDRB	(volatile char*)(0xffdd)
#define	PMR1	(volatile char*)(0xffe0)
#define	PMR5	(volatile char*)(0xffe1)
#define	PCR1	(volatile char*)(0xffe4)
#define	PCR2	(volatile char*)(0xffe5)
#define	PCR5	(volatile char*)(0xffe8)
#define	PCR7	(volatile char*)(0xffea)
#define	PCR8	(volatile char*)(0xffeb)


/**************/
/* 低消費電力 */
/**************/
#define	SYSCR1	(volatile char*)(0xfff0)
#define	SYSCR2	(volatile char*)(0xfff1)

#define	MSTCR1	(volatile char*)(0xfff9)


/**********/
/* 割込み */
/**********/
#define	IEGR1	(volatile char*)(0xfff2)
#define	IEGR2	(volatile char*)(0xfff3)
#define	IENR1	(volatile char*)(0xfff4)
#define	IRR1	(volatile char*)(0xfff6)
#define	IWPR	(volatile char*)(0xfff8)


#endif /* _H_3664 */