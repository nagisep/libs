#if !defined(_BCL_SERIAL_01)
#define      _BCL_SERIAL_01

void seriInit(int bps);
void seriSend(unsigned char a);
void seriSendString(unsigned char *str);
int loc();
unsigned char seriGet();
void seriGetString(unsigned char *str);
#endif

