
#include "3664f.h"
#include "adc.h"

unsigned int getADC(int ch)
{
  volatile unsigned char wk;
  unsigned int result;

  /* 準備 */
  wk = ADCSR;
  wk = (ch & 0x07);
  ADCSR = wk;

  /* スタート */
  wk |= 0x20;
  ADCSR = wk;

  /* 変換待ち */
  do
  {
   wk = ADCSR;
  }
  while(!(wk & 0x80));

  switch(ch)
  {
    case 0:
    case 4: result = ADDRA; break;

    case 1:
    case 5: result = ADDRB; break;

    case 2:
    case 6: result = ADDRC; break;

    case 3:
    case 7: result = ADDRD; break;

    default: result = 0; break;

  }

  reslut >>= 6;
  return result;
}


  