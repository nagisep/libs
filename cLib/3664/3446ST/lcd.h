#if !defined(_BCL_3664ST_LCD)
#define      _BCL_3664ST_LCD

void lcd_init( void );
void lcd_cls( void );
void lcd_print( char *data );
void lcd_locate( unsigned char x, unsigned char y );

#endif
