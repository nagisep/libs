#include "3664f.h"
#include "lcd.h"

#define LCD_CTLPORT_DR	PDR1
#define LCD_CTLPORT_CR	PCR1
#define LCD_E		0x01						/* bit0 */
#define LCD_RW		0x02						/* bit1 */
#define LCD_RS		0x04						/* bit2 */
#define LCD_DATPORT_DR	PDR5
#define LCD_DATPORT_CR	PCR5

/*************************************************
 �t���\������
*************************************************/

static void lcd_clock( unsigned char data )
{
	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR | LCD_E;
	*LCD_DATPORT_DR = ( *LCD_DATPORT_DR & 0xf0 ) | ( data & 0x0f );
	wait( 1 );
	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR & ~LCD_E;
	wait( 1 );
	*LCD_DATPORT_DR = ( *LCD_CTLPORT_DR & 0xf0 );
}


static void lcd_ir_write( unsigned char data )
{
	unsigned char i;

	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR & ~LCD_RS;
	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR & ~LCD_RW;

	i = ( data & 0xf0 ) >> 4;
	lcd_clock( i );

	i = data & 0xf;
	lcd_clock( i );
}

static void lcd_dr_write( unsigned char data )
{
	unsigned char i;

	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR | LCD_RS;
	*LCD_CTLPORT_DR = *LCD_CTLPORT_DR & ~LCD_RW;

	i = ( data & 0xf0 ) >> 4;
	lcd_clock( i );

	i = data & 0xf;
	lcd_clock( i );
}

void lcd_cls( void )
{
	lcd_ir_write( 0x01 );
	wait( 2 );
}

void lcd_locate( unsigned char x, unsigned char y )
{
	unsigned char i;
	i = y * 0x40 + x;
	i = i | 0x80;
	lcd_ir_write( i );
}

void lcd_print( char *data )
{
	while( *data != '\0' ){
		lcd_dr_write( *data );
		data++;
	}
}

void lcd_init( void )
{
	*LCD_CTLPORT_DR = 0x08;
	*LCD_DATPORT_DR = 0x00;
	*LCD_CTLPORT_CR = 0x77;
	*LCD_DATPORT_CR = 0x0f;

	wait( 20 );
	lcd_clock( 3 );							/* function set */
	wait( 5 );
	lcd_clock( 3 );							/* function set */
	lcd_clock( 3 );							/* function set */
	lcd_clock( 2 );							/* function set */

	lcd_ir_write( 0x28 );						/* function set */
	lcd_ir_write( 0x08 );						/* on/off control */
	lcd_cls();
	lcd_ir_write( 0x06 );						/* entry mode set */
	lcd_ir_write( 0x0e );						/* on/off control */
}

