
#if !defined(_BCL_B6T_WORKBENCH)
#define      _BCL_B6T_WORKBENCH

#include <J:/DEV2/IFLINE/RSLINE/winrs.h>

class workBench
{
   private:
     WinRS *myPort;

   public:
     workBench(WinRS *port);
     unsigned short read(unsigned char addr);
     void write(unsigned char addr, unsigned short data);
     void writeFix();
     void buffClear();

 // buffClear() を持つ Workbenchi.cpp が残っていない。
 // このヘッダは、obj ファイルとともに使用すること。

};

#endif
