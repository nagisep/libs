
#include "workBench.h"

workBench::workBench(WinRS *port)
{
   myPort = port;

   
   myPort->putc1(0xff);
   myPort->putc1(0x00);

   myPort->putc1(0x53);
   myPort->putc1(0x55);

   myPort->putc1(0x00);
   myPort->putc1(0x00);

   Sleep(100);

   static_cast<void>(port->getc1());
   static_cast<void>(port->getc1());
   static_cast<void>(port->getc1());
}

unsigned short workBench::read(unsigned char addr)
{
    unsigned char page = (addr >= 0x80) ? 0x80 : 0x00;
	addr &= 0x7f;

    myPort->putc1(addr);
    myPort->putc1(page);

    unsigned int s1, s2;
    s1 = myPort->getc1();
    s2 = myPort->getc1();

    s1 &= 0x00ff;
    s2 &= 0x00ff;

    Sleep(100);
    return s1 * 0x100 + s2;
}

void workBench::write(unsigned char addr, unsigned short data)
{
    unsigned char page = (addr >= 0x80) ? 0x80 : 0x00;
    addr &= 0x7f;

    myPort->putc1(addr | 0x80);
    myPort->putc1(page);
    myPort->putc1(data / 0x100);
    myPort->putc1(data % 0x100);

    Sleep(100);

}

void workBench::writeFix()
{
   write(0x3f, 0x5354);
}


