//DpHigh.h --- copyright Urban Reflection
#ifndef __DPHIGH_H__
#define __DPHIGH_H__
#include <assert.h>
#include <d3drm.h>
#include "DpLow.h"

typedef enum{
	CT_REPLACE,
	CT_AFTER,
	CT_BEFORE,
} ECombineType;


//CBox ----------------------------------------------------------------------
class CBox {
protected:
	BOOL m_bEmpty;
	CVector	m_vctrMin;
	CVector m_vctrMax;
public:
	CBox():m_bEmpty(TRUE){}
	CBox(CVector const& vctr):m_bEmpty(FALSE),m_vctrMin(vctr),m_vctrMax(vctr){}
	~CBox(){}
	CVector& GetMin() const;
	CVector& GetMax() const;
	void Empty(){m_bEmpty=TRUE;}
	BOOL IsEmpty() const{return m_bEmpty;}
	void Update(CVector const& vctr);
};

//CLight --------------------------------------------------------------------
class CMesh;
class CFrame;
class CViewport;
class CLight {
	friend CMesh;
	friend CFrame;
	friend CViewport;
public:
	enum ELightType{
		ambient,
		directional,
		parallelpoint,
		point,
		spot,
	};
protected:
	ELightType m_type;
	CColor m_color;
	CVector m_vctrTempDir;//for render
	CVector m_vctrTempDir2;//for render
	CVector m_vctrTempSpecular;//for render
	CVector m_vctrTempSpecular2;//for render
public:
	CLight(ELightType type=directional,CColor color=CColor(1.f, 1.f, 1.f)){
		m_type=type;
		m_color=color;
	}
	~CLight(){}
	void SetLightType(ELightType type){
		m_type=type;
	}
	ELightType GetLightType() const{
		return m_type;
	}
	void SetColor(CColor c){
		m_color=c;
	}
	CColor const& GetColor() const{
		return m_color;
	}
};

//CMaterial -----------------------------------------------------------------
class CMaterial {
protected:
	CColor m_color;
	real m_rSpecular;
	real m_rPower;
	CTexture const* m_pTexture;
public:
	CMaterial():m_color(0.5f,0.5f,0.5f){
		m_rSpecular=real(1);
		m_rPower=real(50);
		m_pTexture=NULL;
	}
	CMaterial(CColor const& color):m_color(color){
		m_rSpecular=real(1);
		m_rPower=real(50);
		m_pTexture=NULL;
	}
	~CMaterial(){}
	void SetColor(CColor const& c){m_color=c;}
	CColor const& GetColor() const{return m_color;}
	void SetSpecular(real r){m_rSpecular=r;}
	real GetSpecular() const{return m_rSpecular;}
	void SetPower(real r){m_rPower=r;}
	real GetPower() const{return m_rPower;}
	void SetTexture(CTexture const* pTexture){m_pTexture=pTexture;}
	CTexture const* GetTexture() const{return m_pTexture;}
	CMaterial& operator=(CMaterial const& m){
		memcpy(this,&m,sizeof(CMaterial));
		return *this;
	}
};

//CFace ----------------------------------------------------------------------
class CFace {
protected:
	int m_nVertex;
	int* m_pnVertices;
	int* m_pnNormals;
	int* m_pnTextureCoords;
	int m_nMaterial;
	CVector m_vctrNormal;
public:
	CFace(){
		m_nVertex=0;
		m_pnVertices=NULL;
		m_pnNormals=NULL;
		m_pnTextureCoords=NULL;
	}
	~CFace(){
		if(m_nVertex>0){
			delete [] m_pnVertices;
			delete [] m_pnNormals;
			delete [] m_pnTextureCoords;
		}
	}
	void Create(int nVertex,int* pnVertices,int* pnNormals
		,int* pnTextureCoords,int nMaterial);
	int GetVertexCount() const{return m_nVertex;}
	int* GetVertex() const{return m_pnVertices;}
	int* GetNormal() const{return m_pnNormals;}
	int* GetTextureCoord() const{return m_pnTextureCoords;}
	int GetMaterial() const{return m_nMaterial;}
	void SetMaterial(int n){m_nMaterial=n;}
	CVector const& GetFaceNormal() const{return m_vctrNormal;}
	void SetFaceNormal(CVector& v){m_vctrNormal=v;}
};

//CViewport ------------------------------------------------------------------
class CFrame;
class CMesh;
class CViewport {
	friend CFrame;
	friend CMesh;
protected:
	//data
	RECT* m_pRect;
	real m_rAngle;
	real m_rFront;
	real m_rBack;
	CMatrix m_matrix;
	CFrame* m_pCamera;
	TArray<CLight*> m_aLight;//for render
	CColor m_colAmbient;//for render
	int m_nAmbient;//for render
	TArray<CTLVertex> m_aTLVertices;//for sort
	TArray<CPolygon> m_aPolygons;//for sort
	//functions
	void SetMatrix(CCanvas const& canvas);
public:
	CViewport(){
		m_pCamera=NULL;
		m_pRect=NULL;
		m_rAngle=deg2rad(45.f);
		m_rFront=1.f;
		m_rBack=100.f;
	}
	~CViewport(){
		if(m_pRect){delete m_pRect;}
	}
	void SetFront(real r){m_rFront=r;}
	real GetFront()const{return m_rFront;}
	void SetBack(real r){m_rBack=r;}
	real GetBack()const{return m_rBack;}
	void SetAngle(real r){m_rAngle=r;}
	real GetAngle()const{return m_rAngle;}
	void SetCamera(CFrame* pCamera){
		assert(pCamera);
		m_pCamera=pCamera;
	}
	BOOL Render(CCanvas const& canvas,CFrame* pFrame);
	static int SortBackToFront(const void* a,const void* b){
		real r=(*(CPolygon**)b)->m_rReserved - (*(CPolygon**)a)->m_rReserved;
		if(r>0.f){
			return 1;
		} else {
			return -1;
		}
	}
};

//CMesh ----------------------------------------------------------------------
class CMesh {
public:
	enum EWrapType{
		wrapFlat,
		//wrapCylinder,
		//wrapSphere,
	};
protected:
	int m_nVertex;
	CVector* m_pVertices;
	int* m_pnVertexReference;//for render
	int m_nNormal;
	CVector* m_pNormals;
	CColor* m_pTempDiffuse;//for render
	int m_nFace;
	CFace* m_pFaces;
	BOOL* m_pbFaceVisible;//for render
	int m_nMaterial;
	CMaterial* m_pMaterials;
	int m_nTextureCoord;
	real* m_pUs;
	real* m_pVs;
	BOOL m_bDirtyFaceNormal;
	CBox m_box;
public:
	CMesh();
	~CMesh();
	int GetVertexCount() const{return m_nVertex;}
	int GetNormalCount() const{return m_nNormal;}
	int GetFaceCount() const{return m_nFace;}
	int GetMaterialCount() const{return m_nMaterial;}
	int GetTextureCoordCount() const{return m_nTextureCoord;}
	void AddTransform(CMatrix& matrix);
	void Render(CCanvas const& canvas,CMatrix const& mtrxWorld,CMatrix const& mtrxView
		,CViewport* pViewport);
	BOOL Load(LPCTSTR szFilename);
	void CreateCylinder(real rRadius,real rLength,CMaterial const& material,int nDiv);
	void CreateSphere(real rRadius, CMaterial const& material,int nDivR,int nDivH);
	void Wrap(EWrapType type
		,CVector const& vctrOrign
		,CVector const& vctrDir
		,CVector const& vctrUp
		,real ou,real ov
		,real su,real sv
		,CTexture const& texture);
	void CalcFaceNormals();
	void CalcBoundingBox();
};


//CFrame --------------------------------------------------------------------
class CFrame {
	friend BOOL CViewport::Render(CCanvas const& canvas,CFrame* pFrame);
	friend 	void CMesh::Render(CCanvas const& canvas,CMatrix const& mtrxWorld,CMatrix const& mtrxView
		,CViewport* pViewport);
protected:
	//member data
	CFrame* m_pParent;
	TArray<CFrame*> m_aChildren;
	CMatrix m_matrix;
	CMatrix m_mtrxWorld;
	CMesh* m_pMesh;
	TArray<CLight*> m_aLight;
	//function
	void PrepareRendering(CViewport* pViewport);
	void Render(CCanvas const & canvas,CMatrix const& mtrxView,CViewport* pViewport);
	CMatrix const& GetWorldMatrix(){return m_mtrxWorld;}
public:
	CFrame(){
		m_pParent=NULL;
		m_matrix.SetUnit();
		m_pMesh=NULL;
	}
	CFrame(CFrame* pParent){
		assert(pParent);
		m_pParent=pParent;
		m_pParent->m_aChildren.Add(this);
		m_matrix.SetUnit();
		m_pMesh=NULL;
	}
	~CFrame(){}
	void SetParent(CFrame* pParent){
		assert(pParent);
		if(m_pParent==NULL){
			m_pParent=pParent;
			m_pParent->m_aChildren.Add(this);
		} else {
			if(m_pParent!=pParent){
				m_pParent->m_aChildren.Remove(this);
				m_pParent=pParent;
				m_pParent->m_aChildren.Add(this);
			}
		}
	}
	CFrame* GetParent() const{return m_pParent;}
	void AddMesh(CMesh* pMesh){
		assert(pMesh);
		m_pMesh=pMesh;
	}
	void RemoveMesh(){
		m_pMesh=NULL;
	}
	CMesh* GetMesh() const{return m_pMesh;}
	void AddLight(CLight* pLight){
		assert(pLight);
		if(m_aLight.Find(0,pLight)==m_aLight.End()){
			m_aLight.Add(pLight);
		}
	}
	void RemoveLight(CLight* pLight){
		m_aLight.Remove(pLight);
	}
	void AddTransform(CMatrix const& matrix,ECombineType type){
		if(type==CT_REPLACE){
			m_matrix=matrix;
		} else if(type==CT_AFTER){
			m_matrix*=matrix;
		} else if(type==CT_BEFORE){
			m_matrix=matrix*m_matrix;
		} else {
			assert(FALSE);
		}
	}
	CMatrix const& GetTransform(){return m_matrix;}
};


#endif//__DPHIGH_H__
