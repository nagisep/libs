#include "DpLow.h"
#include "resource.h"

//global variables===========================================================
HWND		 		hwndApp;			//ウインドウハンドル
BOOL				g_bModeChanged=TRUE;//モード変更フラグ
CPrimaryCanvas		g_primary;			//プライマリバッファ

//local variables============================================================
static char			szAppName[]="DpDemo1";	//アプリケーション名
static HINSTANCE	hInstApp;				//インスタンスハンドル
static BOOL 		fAppActive;				//アクティブフラグ
static BOOL 		fAppPaused;				//ポーズフラグ


//external funtion declarations==============================================
BOOL AppInit(void);
void OnPaint(void);
void OnLButtonDown(int x,int y);

//local funtion declarations=================================================
LONG CALLBACK AppWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam);




//code ======================================================================
int PASCAL WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR szCmdLine, int sw)
{
	MSG		msg;
	WNDCLASS cls;
	hInstApp=hInst;
	if(!hPrev){
		cls.hCursor 	   = LoadCursor(0,IDC_ARROW);
		cls.hIcon		   = LoadIcon(hInst, "AppIcon");
		cls.lpszMenuName   = MAKEINTRESOURCE(COMMANDS);
		cls.lpszClassName  = szAppName;
		cls.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
		cls.hInstance	   = hInst;
		cls.style		   = 0;
		cls.lpfnWndProc    = (WNDPROC)AppWndProc;
		cls.cbClsExtra	   = 0;
		cls.cbWndExtra	   = 0;
		if (!::RegisterClass(&cls)){
			::MessageBox(NULL,"!RegisterClass()","ERROR",MB_OK);
			return FALSE;
		}
	}
	hwndApp = ::CreateWindowEx(
				  WS_EX_APPWINDOW,
				  szAppName,		   // Class name
				  szAppName,		   // Caption
				  WS_OVERLAPPEDWINDOW,
				  0, 0, 320, 270,	   // Position
				  NULL,				   // Parent window (no parent)
				  NULL,				   // use class menu
				  hInst,			   // handle to window instance
				  0 				   // no params to pass on
				  );
	//DPLowのオープン
	OpenDPLow(hwndApp);
	//ディスプレイモードの列挙
	int n;
	CDisplayMode* pDM;
	g_primary.EnumDisplayModes(&n,&pDM);
	//メニューにディスプレイモード一覧を追加
	for(int i=0;i<n;i++){
		if(pDM[i].m_nHeight<240) continue;//240ライン未満は追加しない
		char szItem[256];
		HMENU hmenu=::GetSubMenu(GetMenu(hwndApp),1);//左から１番目のアイテムに追加
		::wsprintf(szItem,"%dx%dx%d",pDM[i].m_nWidth,pDM[i].m_nHeight,pDM[i].m_nBpp);
		::AppendMenu(hmenu,MF_STRING,MENU_MODE+i,szItem);
	}
	::DrawMenuBar(hwndApp);//メニューの再描画
	//SetErrorReportType(DP_ERT_LOGFILE2);
	BOOL b;
#if 0 //フルスクリーンで起動の場合(#if 1)
	if(n>0){//フルスクリーンデバイスがあるとき
		PushErt(DP_ERT_LOGFILE);//エラーをログに残す。SetDisplayMode()のエラーはメッセージボックスに出してはいけない。
		b=g_primary.SetDisplayMode(pDM[0],DP_ST_FULLSCREEN);//フルスクリーンモード
		PopErt();
		if(!b){//フルスクリーンが失敗したら
			b=g_primary.SetDisplayMode(320,240,16,DP_ST_WINDOW);//ウインドウモード
			if(!b) return FALSE;//ウインドウモードも失敗したら諦める。
		}
	} else {//フルスクリーンデバイスがないとき
		b=g_primary.SetDisplayMode(320,240,16,DP_ST_WINDOW);//ウインドウモード
		if(!b) return FALSE;//ウインドウモードも失敗したら諦める
	}
#else //ウインドウモードで起動の場合(#if 0)
	PushErt(DP_ERT_LOGFILE);//エラーをログに残す。SetDisplayMode()のエラーはメッセージボックスに出してはいけない。
	b=g_primary.SetDisplayMode(320,240,16,DP_ST_WINDOW);//ウインドウモード
	PopErt();
	if(!b){//ウインドウモードに失敗したとき(恐らく画面の設定が16bpp以外のとき)
		if(n>0){//フルスクリーンデバイスがあるとき
			b=g_primary.SetDisplayMode(pDM[0],DP_ST_FULLSCREEN);//フルスクリーンモード
			if(!b) return FALSE;//フルスクリーンも失敗したら、諦める
		} else {//フルスクリーンデバイスがないとき
			SetError(0,"Display mode must be 16bpp/pixel");
			return FALSE;
		}
	}
#endif
	if(!AppInit()) return FALSE;
	::ShowWindow(hwndApp,sw);
	::UpdateWindow(hwndApp);
	HACCEL haccel=::LoadAccelerators(hInst,MAKEINTRESOURCE(IDR_ACCELERATOR));
	while(1){
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE)){
			if(msg.message == WM_QUIT) break;
			if(TranslateAccelerator(hwndApp,haccel,&msg)){
				TranslateMessage(&msg);
			}
			DispatchMessage(&msg);
		} else {
			if (fAppActive && !fAppPaused && GetTopWindow(hwndApp)==NULL){
				OnPaint();//Idleイベント
			}else{
				WaitMessage();
			}
		}
	}
	//プライマリバッファのクローズ
	g_primary.Release();
	//ライブラリのクローズ
	CloseDPLow();
	return msg.wParam;
}

void AppPause(BOOL f)
{
	if (f){
		g_primary.RedrawWindowFrame();
		fAppPaused = TRUE;
	}else{
		fAppPaused = FALSE;
	}
}


LONG CALLBACK AppWndProc(HWND hwnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	RECT Rect;

	switch(msg){
	case WM_KEYDOWN:
		if(wParam==VK_ESCAPE){//ESCキーで
			DestroyWindow(hwnd);//終了
		}
		break;
	case WM_LBUTTONDOWN:
		OnLButtonDown(LOWORD(lParam),HIWORD(lParam));
		break;
	case WM_RBUTTONDOWN:
		AppPause(!fAppPaused);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam)){
		case ID_HELP_ABOUT://アバウトメニュー
			AppPause(TRUE);
			MessageBox(hwnd,
				"DpLow Demo program\r\n"
				"copyright Urban Reflection\r\n"
				"http://www.asahi-net.or.jp/~qs7e-kmy/"
				,szAppName,MB_OK);
			AppPause(FALSE);
			break;
		case ID_FILE_EXIT://EXITメニュー
			PostMessage(hwnd, WM_CLOSE, 0, 0L);
			break;
		case ID_MODE_FULLSCREEN://FULLSCREENメニュー
			if(g_primary.GetScreenType()==DP_ST_FULLSCREEN){//フルスクリーンなら
				PushErt(DP_ERT_LOGFILE);//エラーをログに残す。SetDisplayMode()のエラーはメッセージボックスに出してはいけない。
				BOOL b=g_primary.SetDisplayMode(320,240,16,DP_ST_WINDOW);//ウインドウモードへ
				PopErt();
				if(!b){//失敗したら、フルスクリーン（最も面積の小さい画面へ）
					SendMessage(hwnd,WM_COMMAND,MENU_MODE,0);
				}
				g_bModeChanged=TRUE;
			} else if(g_primary.GetScreenType()==DP_ST_WINDOW){//ウインドウモードなら
				SendMessage(hwnd,WM_COMMAND,MENU_MODE,0);//フルスクリーン（最も面積の小さい画面へ）
				g_bModeChanged=TRUE;
			}
			break;
		}
		//MENU_MODEが選択されたとき
		if (LOWORD(wParam) >= MENU_MODE && LOWORD(wParam) < MENU_MODE+100){
			int n,i=LOWORD(wParam)-MENU_MODE;
			CDisplayMode* pDM;
			g_primary.EnumDisplayModes(&n,&pDM);
			PushErt(DP_ERT_LOGFILE);
			BOOL b=g_primary.SetDisplayMode(pDM[i],DP_ST_FULLSCREEN);
			PopErt();
			if(!b){
				DestroyWindow(hwnd);
			}
			g_bModeChanged=TRUE;
		}
		return 0L;
	case WM_INITMENUPOPUP://メニューのチェックマーク
		if(g_primary.GetScreenType()==DP_ST_WINDOW){
			CheckMenuItem(GetMenu(hwnd),ID_MODE_FULLSCREEN,MF_UNCHECKED);
		} else {
			CheckMenuItem(GetMenu(hwnd),ID_MODE_FULLSCREEN,MF_CHECKED);
		}
		break;
	case WM_PAINT:
		OnPaint();
		break;
	case WM_ACTIVATEAPP:
		fAppActive = (BOOL)wParam;
		break;
	case WM_SETCURSOR:
		//カーソルを消す
		if (fAppActive && !fAppPaused && g_primary.GetScreenType()!=DP_ST_WINDOW){
			SetCursor(NULL);
			return 1;
		}
		break;
	case WM_ENTERMENULOOP:
		AppPause(TRUE);
		break;
	case WM_EXITMENULOOP:
		AppPause(FALSE);
		break;
	case WM_DESTROY:
		hwndApp = NULL;
		PostQuitMessage(0);
		break;
	case WM_SIZE:
		InvalidateRect(hwnd,NULL,FALSE);
	case WM_MOVE:
	case WM_DISPLAYCHANGE:
		//フルスクリーン中にウインドウを移動させられたときは、戻す
		if (fAppActive && !IsIconic(hwnd) && g_primary.GetScreenType()!=DP_ST_WINDOW){
			SetRect(&Rect, 0, GetSystemMetrics(SM_CYCAPTION), GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
			AdjustWindowRectEx(&Rect, WS_POPUP | WS_CAPTION, FALSE, 0);
			SetWindowPos(hwnd, NULL, Rect.left, Rect.top, Rect.right-Rect.left, Rect.bottom-Rect.top, SWP_NOACTIVATE | SWP_NOZORDER);
		}
		break;
	}
	return DefWindowProc(hwnd,msg,wParam,lParam);
}
