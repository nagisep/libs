//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DpDemo1.rc
//
#define COMMANDS                        101
#define IDR_ACCELERATOR                 102
#define MENU_MODE                       500
#define ID_FILE_EXIT                    40001
#define ID_HELP_ABOUT                   40002
#define ID_MODE_FULLSCREEN              40003
#define ID_MODE_FAKE320X240             40004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
