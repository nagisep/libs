#include <math.h>
#include <assert.h>
#include "DpLow.h"
#include "DpHigh.h"


//#define DXMUSIC_ON//ぷよーんさんのライブラリDxMusicを使う場合は定義
#ifdef DXMUSIC_ON
#include "DMusic.h"
#endif

//winmm.libをリンクしてください。

//defines----------------------------------------------------------
//externs----------------------------------------------------------
extern HWND		 			hwndApp;
extern BOOL					g_bModeChanged;
extern CPrimaryCanvas		g_primary;

//local variables----------------------------------------------------------
static int			g_nDemo=11;				//デモの数
static int			g_nDemoType=0;//g_nDemo-1;	//現在表示中のデモ番号
static CTexture		g_texStone;
static CTexture		g_texTitle;
static CTexture		g_texFire;
static CTexture		g_texEnding;
static CMesh		g_mesh;
static char*		g_szTitle="no title";	//デモ名
#ifdef DXMUSIC_ON
static CDxMusic		g_music;
#endif
//local function declarations----------------------------------------------------------

//codes----------------------------------------------------------
//初期化
BOOL AppInit()
{
	//テクスチャのロード
	if(!g_texStone.Load("TitleBG.bmp")) return FALSE;
	if(!g_texTitle.Load("Title.bmp",DP_TEX_LEFTTOP)) return FALSE;
	if(!g_texTitle.LoadAlpha("TitleM.bmp")) return FALSE;
	if(!g_texEnding.Load("ByKamiyan.bmp")) return FALSE;
	if(!g_texFire.Load("Box.bmp")) return FALSE;
	//メッシュのロード
	if(!g_mesh.Load("Test.x")) return FALSE;
	g_mesh.Wrap(CMesh::wrapFlat,CVector(),CVector(),CVector(),0.f,0.f,1.f,1.f,g_texStone);
	//MIDIの再生
#ifdef DXMUSIC_ON
	g_music.OpenFile("BGM.mid");
	g_music.SetLoop(TRUE);
	g_music.Play();
#endif
	return TRUE;
}

//左クリック デモの切り替え
void OnLButtonDown(int x,int y)
{
	g_nDemoType++;
	g_nDemoType%=g_nDemo;
	g_bModeChanged=TRUE;
}

/*
//小三角形
void OnDrawMiniTriangle(CCanvas& canvas,BOOL bFirst)
{
	g_szTitle="Mini Gouraud Triangle Demo 1000 polygons/frame";
	CPolygon poly;
	CTLVertex ver[3];
	for(int i=0;i<1000;i++){
		real x=(real)(rand()%canvas.m_nWidth);//2+canvas.m_nWidth/4;
		real y=(real)(rand()%canvas.m_nHeight);//2+canvas.m_nHeight/4;
		ver[0].m_vctrPos.x=x;
		ver[0].m_vctrPos.y=y-3.f;
		ver[1].m_vctrPos.x=x+4.f;
		ver[1].m_vctrPos.y=y+3.f;
		ver[2].m_vctrPos.x=x-4.f;
		ver[2].m_vctrPos.y=y+4.f;
		for(int j=0;j<3;j++){//ランダム
			ver[j].m_color.r=(real)(rand()&0x7fff)/32767.f;
			ver[j].m_color.g=(real)(rand()&0x7fff)/32767.f;
			ver[j].m_color.b=(real)(rand()&0x7fff)/32767.f;
			ver[j].m_color.a=(real)(rand()&0x7fff)/32767.f;
			ver[j].m_rU=(real)(rand()&0x7fff)/32767.f;
			ver[j].m_rV=(real)(rand()&0x7fff)/32767.f;
		}
		poly.m_dwFlags=DP_TEXTURE_ON|DP_COLOR_MONO|DP_SHADE_GOURAUD|DP_ALPHA_OFF;;
		poly.m_nVertices=3;
		poly.m_pVertices=ver;
		poly.m_pTexture=&g_texture;
		DrawPolygon(canvas,poly);
	}
}


//FakeShine
void OnDrawFakeShine(CCanvas& canvas,BOOL bBlur,BOOL bFirst)
{
	static const int MAX_STAR=100;
	static real dirx[]={1.f,0.f,-1.f,0.f};
	static real diry[]={0.f,-1.f,0.f,1.f};
	static struct Star{
		real x,y,dx,dy,t;
		real r,g,b;
	} star[MAX_STAR];
	if(bFirst){//初期化処理
		//背景を消す
		CPattern pat;
		pat.m_color.r=pat.m_color.g=pat.m_color.b=0.f;
		pat.m_color.a=1.f;
		PatBlt(canvas,pat);
		for(int i=0;i<MAX_STAR;i++){
			star[i].x=(real)(rand()%canvas.m_nWidth);
			star[i].y=(real)(rand()%canvas.m_nHeight);
			star[i].dx=4.f*canvas.m_nWidth/512.f*(real)(rand()%2000-1000)/1000.f;
			star[i].dy=8.f*canvas.m_nWidth/512.f*(real)(rand()%500+300)/800.f;
			star[i].t=(real)(rand()%628)/100.f;
			star[i].r=(real)(rand()%0x7fff)/32767.f;
			star[i].g=(real)(rand()%0x7fff)/32767.f;
			star[i].b=(real)(rand()%0x7fff)/32767.f;
		}
	}
	int nStar;
	if(bBlur){
		nStar=30;
		g_szTitle="Fake Shine Blur Demo 30 stars";
	} else {
		nStar=50;
		g_szTitle="Fake Shine Demo 50 stars";
	}
	real radius=20.f*(real)canvas.m_nWidth/512.f;
	//背景を消す
	CPattern pat;
	pat.m_color.r=pat.m_color.g=pat.m_color.b=0;
	pat.m_color.a=(bBlur ? 0.05f : 1.f);
	PatBlt(canvas,pat);
	//星の描画
	CTLVertex ver[3];
	CPolygon poly;
	poly.m_dwFlags=0;
	poly.m_nVertices=3;
	poly.m_pVertices=ver;
	poly.m_pTexture=NULL;//&g_texture;
	ver[0].m_color.r=1.f;
	ver[0].m_color.g=1.f;
	ver[0].m_color.b=1.f;
	ver[0].m_color.a=1.f;
	ver[0].m_rU=0.5f;
	ver[0].m_rV=0.5f;
	real nTexRadius=0.5;
	for(int i=0;i<nStar;i++){
		for(int j=0;j<4;j++){
			ver[0].m_vctrPos.x=star[i].x;
			ver[0].m_vctrPos.y=star[i].y;
			for(int k=0;k<2;k++){
				ver[1+k].m_vctrPos.x=star[i].x+radius*dirx[(j+k)%4];
				ver[1+k].m_vctrPos.y=star[i].y+radius*diry[(j+k)%4];
				ver[1+k].m_color.r=star[i].r;
				ver[1+k].m_color.g=star[i].g;
				ver[1+k].m_color.b=star[i].b;
				ver[1+k].m_color.a=0.f;
				ver[1+k].m_rU=nTexRadius+nTexRadius*dirx[(j+k)%4];
				ver[1+k].m_rV=nTexRadius+nTexRadius*diry[(j+k)%4];
			}
			DrawPolygon(canvas,poly);
		}
		star[i].x+=star[i].dx;
		if(star[i].x<-radius) star[i].x=(real)canvas.m_nWidth+radius;
		if(star[i].x>(real)canvas.m_nWidth+radius) star[i].x=-radius;
		star[i].dx+=(real)cos(star[i].t);
		star[i].t+=3.1415f/16.f;
		star[i].y+=star[i].dy;
		if(star[i].y>(real)canvas.m_nHeight+radius) star[i].y=-radius;
	}
}

//GoroundDemo
void OnDrawGroundDemo(CCanvas& canvas,BOOL bFirst)
{
	g_szTitle="Fogged Ground Demo(MOVE MOUSE!!!)";
	static const int nDepth=10;
	static real x,y,z,speed,dir;
	int rSx=canvas.m_nWidth,rSy=canvas.m_nHeight;
	if(bFirst){//初期化処理
		x=0;
		y=0;
		z=1.0;
		dir=0.0;
		speed=0.0;
		//背景を消す
		CPattern pat;
		pat.m_color.r=pat.m_color.g=pat.m_color.b=0.1f;
		pat.m_color.a=1.f;
		PatBlt(canvas,pat);
	}
	CTLVertex ver[4];
	CPolygon poly;
	poly.m_dwFlags=DP_TEXTURE_ON|DP_COLOR_MONO|DP_SHADE_GOURAUD|DP_ALPHA_OFF;
	poly.m_nVertices=4;
	poly.m_pVertices=ver;
	poly.m_pTexture=&g_texture;
	for(int j=0;j<4;j++){
		ver[j].m_vctrPos.x=(j<2 ? 0.f : (real)(rSx-1));
		ver[j].m_color.a=1.f;
	}
	for(int i=0;i<nDepth-1;i++){
		real x1,y1,z1,x2,y2,t;
		for(j=0;j<4;j++){
			int k=(j==0 || j==3 ? i+1 : i);
			ver[j].m_vctrPos.y=(real)rSy-(real)(rSy/2)*(real)k/(real)nDepth;
			x1=2.0;
			y1=(real)(rSx/2)/(real)(rSy/2)*(j<2 ? -1.0f : 1.0f);
			z1=(real)k/(real)nDepth;
			x2=x1*(real)cos(dir)-y1*(real)sin(dir);
			y2=x1*(real)sin(dir)+y1*(real)cos(dir);
			t=-z/(z1-z);
			x2=x2*t+x;
			y2=y2*t+y;
			ver[j].m_rU=30.f*x2/(real)g_texture.m_nWidth;
			ver[j].m_rV=30.f*y2/(real)g_texture.m_nHeight;
			real c=0.5f-0.5f*(real)k/(real)nDepth;
			if(c>1.f) c=1.f;
			if(c<0.f) c=0.f;
			ver[j].m_color.r=ver[j].m_color.g=ver[j].m_color.b=c;
		}
		if(x2>32700.0 || x2<-32700.0 || y2>32700.0 || y2<-32000.0) break;
		DrawPolygon(canvas,poly);
	}
	x+=speed*(real)cos(dir);
	y+=speed*(real)sin(dir);
	DWORD dw=GetMessagePos();
	RECT wr;
	GetWindowRect(hwndApp,&wr);
	dir+=3.1615f/140.0f*((real)LOWORD(dw)-(real)((wr.left+wr.right)/2))/(real)((wr.left+wr.right)/2);
	speed=(((real)(wr.bottom)-(real)HIWORD(dw))/(real)(wr.right-wr.left))*0.2f;
}

//FullPaintDemo
void OnDrawFullPaint(CCanvas& canvas,BOOL bFirst)
{
	g_szTitle="Full Paint";
	int rSx=canvas.m_nWidth,rSy=canvas.m_nHeight;
	static real tx=0.f;
	static CColor col[4];
	static CColor dcol[4];
	if(bFirst){//初期化処理
		for(int j=0;j<4;j++){
			col[j].r=(real)(rand()&0x7fff)/32767.f;
			col[j].g=(real)(rand()&0x7fff)/32767.f;
			col[j].b=(real)(rand()&0x7fff)/32767.f;
			col[j].a=(real)(rand()&0x7fff)/32767.f;
			dcol[j].r=0.2f*((real)(rand()&0x7fff)/32767.f-0.5f);
			dcol[j].g=0.2f*((real)(rand()&0x7fff)/32767.f-0.5f);
			dcol[j].b=0.2f*((real)(rand()&0x7fff)/32767.f-0.5f);
			dcol[j].a=0.2f*((real)(rand()&0x7fff)/32767.f-0.5f);
		}
	}
	CTLVertex ver[4];
	CPolygon poly;
	poly.m_dwFlags=DP_TEXTURE_ON|DP_COLOR_MONO|DP_SHADE_GOURAUD|DP_ALPHA_OFF;
	poly.m_nVertices=4;
	poly.m_pVertices=ver;
	poly.m_pTexture=&g_texture;
	for(int j=0;j<4;j++){
		ver[j].m_color=col[j];
	}
	ver[0].m_vctrPos.x=0;
	ver[0].m_vctrPos.y=0;
	ver[0].m_rU=0+tx;
	ver[0].m_rV=0;
	ver[1].m_vctrPos.x=(real)(rSx-1);
	ver[1].m_vctrPos.y=0;
	ver[1].m_rU=1+tx;
	ver[1].m_rV=0;
	ver[2].m_vctrPos.x=(real)(rSx-1);
	ver[2].m_vctrPos.y=(real)(rSy-1);
	ver[2].m_rU=1+tx;
	ver[2].m_rV=1;
	ver[3].m_vctrPos.x=0;
	ver[3].m_vctrPos.y=(real)(rSy-1);
	ver[3].m_rU=0+tx;
	ver[3].m_rV=1;
	DrawPolygon(canvas,poly);
	tx+=0.05f;
	for(j=0;j<4;j++){
		col[j].r+=dcol[j].r;
		if(col[j].r>1.f){dcol[j].r=-dcol[j].r;col[j].r=1.0f;}
		if(col[j].r<0.f){dcol[j].r=-dcol[j].r;col[j].r=0.0f;}
		col[j].g+=dcol[j].g;
		if(col[j].g>1.f){dcol[j].g=-dcol[j].g;col[j].g=1.0f;}
		if(col[j].g<0.f){dcol[j].g=-dcol[j].g;col[j].g=0.0f;}
		col[j].b+=dcol[j].b;
		if(col[j].b>1.f){dcol[j].b=-dcol[j].b;col[j].b=1.0f;}
		if(col[j].b<0.f){dcol[j].b=-dcol[j].b;col[j].b=0.0f;}
		col[j].a+=dcol[j].a;
		if(col[j].a>1.f){dcol[j].a=-dcol[j].a;col[j].a=1.0f;}
		if(col[j].a<0.f){dcol[j].a=-dcol[j].a;col[j].a=0.0f;}
	}
}

*/
//ProjectDPのオープニング
void OnDrawTitle(CCanvas& canvas,BOOL bFirst,real rTime)
{
	if(bFirst){//初期化処理
		g_szTitle="Title";
	}
	real rSx=(real)canvas.m_nWidth-1,rSy=(real)canvas.m_nHeight-1;
	real tx;
	CColor colBack,colTitle;
	real r;
	//アニメーション
	if(rTime<5){//背景白、タイトルフェードイン
		r=1-rTime/10;
		colBack.Set(1,1,1);
		colTitle.Set(r,r,r);
	} else if(rTime<10){//背景フェードイン、タイトル固定
		r=1-(rTime-5)/10;
		colBack.Set(r,r,r);
		colTitle.Set(0.5f,0.5f,0.5f);
	} else if(rTime<15){//固定
		colBack.Set(0.5f,0.5f,0.5f);
		colTitle.Set(0.5f,0.5f,0.5f);
	} else if(rTime<17){//フェードアウト
		r=0.5f-(rTime-15)/4;
		colBack.Set(r,r,r);
		colTitle.Set(r,r,r);
	} else {//次のシーンへ
		colBack.Set(0,0,0);
		colTitle.Set(0,0,0);
		OnLButtonDown(0,0);
	}
	tx=0.1f*rTime;//10秒で１画面
	//背景描画
	CTLVertex ver[4];
	CPolygon poly;
	poly.m_dwFlags=DP_TEXTURE_ON|DP_COLOR_MONO|DP_SHADE_FLAT|DP_ALPHA_OFF;
	poly.m_nVertices=4;
	poly.m_pVertices=ver;
	poly.m_pTexture=&g_texStone;
	ver[0].Set(CVector(0,0,1),colBack,tx,0);
	ver[1].Set(CVector(rSx,0,1),colBack,1+tx,0);
	ver[2].Set(CVector(rSx,rSy,1),colBack,1+tx,1);
	ver[3].Set(CVector(0,rSy,1),colBack,tx,1.f);
	DrawPolygon(canvas,poly);
	//タイトル描画
	poly.m_dwFlags=DP_TEXTURE_ALPHA|DP_COLOR_MONO|DP_SHADE_FLAT|DP_ALPHA_OFF;
	poly.m_nVertices=4;
	poly.m_pVertices=ver;
	poly.m_pTexture=&g_texTitle;
	ver[0].Set(CVector(0,rSy/4,1),colTitle,0,0);
	ver[1].Set(CVector(rSx,rSy/4,1),colTitle,1,0);
	ver[2].Set(CVector(rSx,rSy/2,1),colTitle,1,1);
	ver[3].Set(CVector(0,rSy/2,1),colTitle,0,1);
	DrawPolygon(canvas,poly);
}

//Box demo
void OnDrawBox(CCanvas& canvas,BOOL bFirst,int nDemoType,real rTime)
{
	g_szTitle="Box Demo";
	static const int nVertices=8;
	static const CVector vctrBox[nVertices]={
		CVector( 1.f, 1.f,-1.f),
		CVector(-1.f, 1.f,-1.f),
		CVector(-1.f,-1.f,-1.f),
		CVector( 1.f,-1.f,-1.f),
		CVector( 1.f, 1.f, 1.f),
		CVector(-1.f, 1.f, 1.f),
		CVector(-1.f,-1.f, 1.f),
		CVector( 1.f,-1.f, 1.f),
	};
	static const int nFaces=6;
	static const int nFaceVertices=4;
	static const int faceBox[nFaces][nFaceVertices]={
		{0,1,2,3},
		{7,6,5,4},
		{4,5,1,0},
		{3,2,6,7},
		{0,3,7,4},
		{5,6,2,1},
	};
	static const real uvBox[nFaceVertices][2]={
		{0.f,0.f},{0.f,1.f},{1.f,1.f},{1.f,0.f}
	};
	static const int nBox=8;
	static const real rBoxZ=30.f;
	static const real rViewAngle=45.f;
	static const real rViewFront=1.f;//rBoxZ-2.f;
	static const real rViewBack=rBoxZ+2.f;
	struct Box {
		CVector pos;
		CVector vero;
		CColor	col;
		CColor	vCol;
		CVector angle;
		CVector vAngle;
	};
	static Box boxies[nBox];
	static CMatrix mtrxView;
	static CVector vctrBoxNormals[nFaces];
	static real rLastTime;
	if(bFirst){//初期化処理
		//箱フレームの設定
		for(int i=0;i<nBox;i++){
			boxies[i].pos.Set(RAND()*10.f-5.f,3.f,30.f);
			boxies[i].vero.Set(0.f,0.f,0.f);
//			boxies[i].col.Set(0.5f,0.5f,0.5f,0.5f);
			boxies[i].col.Set(RAND(),RAND(),RAND(),0.5f);
			boxies[i].vCol.Set(0.f,0.f,0.f,0.f);
//			boxies[i].vCol.Set(RAND()/50.f,RAND()/50.f,RAND()/50.f,0.f);
			boxies[i].angle.Set(0.f,0.f,0.f);
			boxies[i].vAngle.Set(60.f*RAND()/DP_PI/4.f,60.f*RAND()/DP_PI/4.f,60.f*RAND()/DP_PI/4.f);
		}
		//視野変換行列の作成(viewport.cppよりコピー)
		CMatrix trans,scale;
		real rHeight=rViewFront * (real)sin(deg2rad(rViewAngle));
		real rLength=rViewBack- rViewFront;
		real rScale=(real)canvas.m_nWidth/rHeight;
		trans.SetElement(
			2, 2, rHeight*rLength / (rViewFront * (rLength-rViewFront)));
		trans.SetElement(
			3, 2, rHeight / rViewFront);
		trans.SetElement(
			2, 3, -rHeight*rLength / (rLength-rViewFront));
		trans.SetElement(
			3, 3, real(0.0));
		scale.SetElement(0, 0, rScale);
		scale.SetElement(1, 1, -rScale);
		scale.SetElement(0, 3, (real)canvas.m_nWidth/2);
		scale.SetElement(1, 3, (real)canvas.m_nHeight/2);
		mtrxView=trans*scale;
		//箱メッシュの法線を求める
		for(i=0;i<nFaces;i++){
			vctrBoxNormals[i]=
				(vctrBox[faceBox[i][1]]-vctrBox[faceBox[i][0]])
				.CrossProduct(vctrBox[faceBox[i][2]]-vctrBox[faceBox[i][1]]);
			vctrBoxNormals[i].Normalize();
		}
		rLastTime=rTime;
	}
	//背景の消去
	CPattern pat;
	if(nDemoType!=7){
		pat.m_color.Set(0.0f,0.0f,0.0f);
	} else {
		pat.m_color.Set(0.0f,0.0f,0.0f,0.001f);
	}
	PatBlt(canvas,pat);
	for(int i=0;i<nBox;i++){
		//オブジェクト座標
		CMatrix mtrx;
		CVector vctr[nVertices];
		CVector vctrNormals[nFaces];
		mtrx.SetUnit();
		mtrx.AddXRotation(boxies[i].angle.x);
		mtrx.AddYRotation(boxies[i].angle.y);
		mtrx.AddZRotation(boxies[i].angle.z);
		for(int j=0;j<nFaces;j++){
			vctrNormals[j]=vctrBoxNormals[j]*mtrx;
		}
		mtrx.AddTranslation(boxies[i].pos);
		//視野座標
		mtrx=mtrx*mtrxView;
		//スクリーン座標
		for(j=0;j<nVertices;j++){
			vctr[j]=vctrBox[j]*mtrx;
			vctr[j].Homogenize();
		}
		//描画
		CTLVertex tlv[nFaceVertices];
		CPolygon poly;
		if(nDemoType==1){//flat shading
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_OFF|DP_ALPHA_OFF|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else if(nDemoType==2){//alpha blending
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_OFF|DP_ALPHA_FLAT|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else if(nDemoType==3){//add mode
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_OFF|DP_ALPHA_FLAT|DP_BLEND_ALPHA_ONE|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else if(nDemoType==4){//texture mapping
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_ON|DP_ALPHA_OFF|DP_COLOR_MONO|DP_SHADE_FLAT;
		} else if(nDemoType==5){//texture+alpha
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_ON|DP_ALPHA_FLAT|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else if(nDemoType==6){//texture+add mode
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_ON|DP_ALPHA_FLAT|DP_BLEND_ALPHA_ONE|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else if(nDemoType==7){//motion blur
			poly.m_dwFlags=DP_CULL_CCW|DP_TEXTURE_ON|DP_ALPHA_OFF|DP_COLOR_RGB|DP_SHADE_FLAT;
		} else {
			assert(FALSE);
		}
	
		poly.m_nVertices=nFaceVertices;
		poly.m_pTexture=&g_texFire;
		poly.m_pVertices=tlv;
		for(j=0;j<nFaces;j++){
			real x1=vctr[faceBox[j][1]].x-vctr[faceBox[j][0]].x;
			real y1=vctr[faceBox[j][1]].y-vctr[faceBox[j][0]].y;
			real x2=vctr[faceBox[j][2]].x-vctr[faceBox[j][1]].x;
			real y2=vctr[faceBox[j][2]].y-vctr[faceBox[j][1]].y;
			if(x1*y2-x2*y1<0.f){
				for(int k=0;k<nFaceVertices;k++){
					if(vctrNormals[j].z>0.f){//光が当たっているとき
						tlv[k].m_color.r=vctrNormals[j].z*boxies[i].col.r;
						tlv[k].m_color.g=vctrNormals[j].z*boxies[i].col.g;
						tlv[k].m_color.b=vctrNormals[j].z*boxies[i].col.b;
					} else {//光が当たっていないとき
						tlv[k].m_color.r=0.f;
						tlv[k].m_color.g=0.f;
						tlv[k].m_color.b=0.f;
					}
					tlv[k].m_color.a=boxies[i].col.a;
					tlv[k].m_rU=uvBox[k][0];
					tlv[k].m_rV=uvBox[k][1];
					tlv[k].m_vctrPos=vctr[faceBox[j][k]];
				}
				DrawPolygon(canvas,poly);
			}
		}
		//アニメーション
		real rTick=rTime-rLastTime;
		boxies[i].angle+=boxies[i].vAngle*rTick;
		boxies[i].vero.y-=96.f*rTick;
		boxies[i].pos+=boxies[i].vero*rTick;
		if(boxies[i].pos.y<-4.5f){
			boxies[i].vero.y=40.f;
			boxies[i].pos.y=-4.5f;
			boxies[i].vero.x=120.f*(RAND()-0.5f)/5.f;
			boxies[i].vAngle.Set(60.f*RAND()/DP_PI/4.f,60.f*RAND()/DP_PI/4.f,60.f*RAND()/DP_PI/4.f);
		}
		if(boxies[i].pos.x<-6.f && boxies[i].vero.x<0.f){
			boxies[i].vero.x*=-1.f;
		}
		if(boxies[i].pos.x>6.f && boxies[i].vero.x>0.f){
			boxies[i].vero.x*=-1.f;
		}
		boxies[i].col.r+=boxies[i].vCol.r;
		if(boxies[i].col.r>1.f){
			boxies[i].col.r=1.f;
			boxies[i].vCol.r*=-1.f;
		}
		if(boxies[i].col.r<0.f){
			boxies[i].col.r=0.f;
			boxies[i].vCol.r*=-1.f;
		}
		boxies[i].col.g+=boxies[i].vCol.g;
		if(boxies[i].col.g>1.f){
			boxies[i].col.g=1.f;
			boxies[i].vCol.g*=-1.f;
		}
		if(boxies[i].col.g<0.f){
			boxies[i].col.g=0.f;
			boxies[i].vCol.g*=-1.f;
		}
		boxies[i].col.b+=boxies[i].vCol.b;
		if(boxies[i].col.b>1.f){
			boxies[i].col.b=1.f;
			boxies[i].vCol.b*=-1.f;
		}
		if(boxies[i].col.b<0.f){
			boxies[i].col.b=0.f;
			boxies[i].vCol.b*=-1.f;
		}
		boxies[i].col.a+=boxies[i].vCol.a;
		if(boxies[i].col.a>1.f){
			boxies[i].col.a=1.f;
			boxies[i].vCol.a*=-1.f;
		}
		if(boxies[i].col.a<0.f){
			boxies[i].col.a=0.f;
			boxies[i].vCol.a*=-1.f;
		}
		if(GetAsyncKeyState('J')<0){
			boxies[i].pos.z-=1.f;
		}
		if(GetAsyncKeyState('K')<0){
			boxies[i].pos.z+=1.f;
		}
	}
	//最初のフラッシュ
	if(rTime<0.5f){
		real r=(0.5f-rTime)*2.f;
		CPattern pat;
		pat.m_color.Set(1.0f,r);
		PatBlt(canvas,pat);
	} else if(rTime>8.f){
		OnLButtonDown(0,0);
	}
	rLastTime=rTime;
}


void OnDrawDpHigh(CCanvas& canvas,BOOL bFirst,int nDemoType,real rTime)
{
	const int nNum=1;//オブジェクトの数
	int nNumLight;//ライトの数
	if(nDemoType==8){
		nNumLight=1;
	} else {
		nNumLight=3;
	}
	const real rLength=5.f;
	static CFrame scene,camera,frame[nNum],light[3];
	static CMesh mesh;
	static CViewport viewport;
	static CLight ambient(CLight::ambient,CColor(0.3f));
	static CLight directional[3];
	static char szTitle[256];
	CMatrix m,m2;
	if(bFirst){
		//とりあえず白で消しとく
		CPattern pat;
		pat.m_color.Set(1.0f);
		PatBlt(canvas,pat);
		//camera
		camera.SetParent(&scene);
		m.SetTranslation(0.f,0.f,-3.f);
		camera.AddTransform(m,CT_REPLACE);
		viewport.SetCamera(&camera);
		//mesh
/*		if(mesh.GetVertexCount()==0){
//			mesh.CreateCylinder(1.f,rLength,CMaterial(CColor(1,0,0)),8);
//			mesh.CreateSphere(1.f,CMaterial(CColor(1,0,0)),8,5);
			BOOL b=mesh.Load("test.x");
			if(!b) return;
			mesh.Wrap(CMesh::wrapFlat,CVector(),CVector(),CVector(),0.f,0.f,1.f,1.f,g_texStone);
//			m.SetTranslation(0.f,0.f,-rLength/2.f);
//			mesh.AddTransform(m);
		}*/
		//title
		wsprintf(szTitle,"X file demo %d polygon",mesh.GetFaceCount()*nNum);
		g_szTitle=szTitle;
		//frame
//		m.SetXRotation(-DP_PI/4.f);
//		frame[0].AddTransform(m,CT_REPLACE);
		for(int i=0;i<nNum;i++){
			if(i==0){
				frame[0].SetParent(&scene);
			} else {
				frame[i].SetParent(&frame[i-1]);
				m.SetTranslation(0.f,0.f,rLength);
				frame[i].AddTransform(m,CT_REPLACE);
			}
			frame[i].AddMesh(&g_mesh);
		}
		//light
		scene.AddLight(&ambient);
		if(nDemoType==8){//シングルライトのとき
			directional[0].SetColor(CColor(1,1,1));
		} else {
			directional[0].SetColor(CColor(1,0,0));
			directional[1].SetColor(CColor(0,1,0));
			directional[2].SetColor(CColor(0,0,1));
		}
		for(i=0;i<nNumLight;i++){
			light[i].SetParent(&scene);
			light[i].AddLight(&directional[i]);
		}
//		m.SetXRotation(DP_PI/4.f);
//		m.AddZRotation(DP_PI/8.f);
//		light.AddTransform(m,CT_REPLACE);
	}
	//背景の消去
	CPattern pat;
	pat.m_color.Set(0);
	PatBlt(canvas,pat);
	//描画
	viewport.Render(canvas,&scene);
	//アニメーション
	for(int i=0;i<nNum;i++){
//		m.SetZRotation(DP_PI/50.f);
//		m.SetYRotation(DP_PI/10.f*(real)pow(0.99,(double)(nNum-i)));
//		frame[i].AddTransform(m,CT_AFTER);
	}
	//Light
	for(i=0;i<nNumLight;i++){
		m.SetXRotation(DP_PI/50.f);
		m.AddYRotation(DP_PI/25.f*(real)i);
		light[i].AddTransform(m,CT_AFTER);
	}
	//キー操作（カメラ）
/*	if(::GetAsyncKeyState('J')<0){
		m.SetTranslation(0.f, 0.f, 1.f);
		camera.AddTransform(m,CT_AFTER);
	}
	if(::GetAsyncKeyState('K')<0){
		m.SetTranslation(0.f, 0.f, -1.f);
		camera.AddTransform(m,CT_AFTER);
	}
	if(::GetAsyncKeyState('H')<0){
		m.SetTranslation(0.f, 1.f, 0.f);
		camera.AddTransform(m,CT_AFTER);
	}
	if(::GetAsyncKeyState('L')<0){
		m.SetTranslation(0.f, -1.f, 0.f);
		camera.AddTransform(m,CT_AFTER);
	}
	if(::GetAsyncKeyState('U')<0){
		m.SetTranslation(1.f, 0.f, 0.f);
		camera.AddTransform(m,CT_AFTER);
	}
	if(::GetAsyncKeyState('I')<0){
		m.SetTranslation(-1.f, 0.f, 0.f);
		camera.AddTransform(m,CT_AFTER);
	}*/
	//最初のフラッシュ
	if(rTime<0.5f){
		real r=(0.5f-rTime)*2.f;
		CPattern pat;
		pat.m_color.Set(1.0f,r);
		PatBlt(canvas,pat);
	} else if(rTime>15.f){
		OnLButtonDown(0,0);
	}
}

void OnDrawEnding(CCanvas& canvas,BOOL bFirst,real rTime){
	g_szTitle="Ending";
	if(bFirst){//初期化処理
#ifdef DXMUSIC_ON
		g_music.FadeOut(8000);
#endif
	}
	CPattern pat;
	pat.m_matrix.SetScale(
		(256.f/(real)canvas.m_nHeight)*((real)canvas.m_nHeight/(real)canvas.m_nWidth),
		256.f/(real)canvas.m_nHeight,1);
	pat.m_pTexture=&g_texEnding;
	if(rTime<0.5f){//白からフェードイン
		pat.m_color.Set(1.f-rTime);
	} else if(rTime<3.f){//固定
		pat.m_color.Set(0.5f);
	} else if(rTime<8.f){//フェードアウト
		pat.m_color.Set((5.f-(rTime-3.f))/10.f);
	} else if(rTime<10.f){//黒固定
		pat.m_color.Set(0.f);
	} else {//終了
		::PostQuitMessage(0);
	}
	PatBlt(canvas,pat);
}

//裏画面のポインタへ直接描画
void OnDraw(CCanvas& canvas)
{
	static DWORD dwTime=::GetTickCount();
	static DWORD dwLastTime=0;
	BOOL bFirst=FALSE;
	if(g_bModeChanged){
		bFirst=TRUE;
		g_bModeChanged=FALSE;
		dwTime=::GetTickCount();
	}
	DWORD dwNow=::GetTickCount();
	real rTime=(real)(dwNow-dwTime)/1000.f;
	if(g_nDemoType==0){//タイトル
		OnDrawTitle(canvas,bFirst,rTime);
	} else if(g_nDemoType>=1 && g_nDemoType<=7){//BoxDemo
		OnDrawBox(canvas,bFirst,g_nDemoType,rTime);
	} else if(g_nDemoType==8){//DpHighデモ
		OnDrawDpHigh(canvas,bFirst,g_nDemoType,rTime);
	} else if(g_nDemoType==9){
		OnDrawDpHigh(canvas,bFirst,g_nDemoType,rTime);
	} else if(g_nDemoType==10){
		OnDrawEnding(canvas,bFirst,rTime);
	} else {
		assert(FALSE);
	}
}

void OnPaint()
{
	//仮想画面へ直接描画
	CBackCanvas* pCanvas=g_primary.GetBackCanvas();
	if(pCanvas->Lock()){
		OnDraw((CCanvas&)*pCanvas);
		pCanvas->Unlock();
	}
/*	{
		//FPSの表示
		static int FrameRate,FrameCount,FrameCount0;
		static DWORD FrameTime,FrameTime0,FrameTimePrev;
		FrameCount++;
		FrameTime = ::timeGetTime();
		if(FrameTime - FrameTime0 > 1000){
			FrameRate = (FrameCount - FrameCount0) * 10000 / (FrameTime - FrameTime0);
			FrameTime0 = FrameTime;
			FrameCount0 = FrameCount;
		}
		HDC hdc;
		char str[256];
		if(pCanvas->GetDC(&hdc)){
			SetBkMode(hdc,OPAQUE);
			SetBkColor(hdc,RGB(0,0,0));
			SelectObject(hdc,GetStockObject(ANSI_VAR_FONT));
			SetTextColor(hdc, RGB(255,255,0));
			wsprintf(str,"%3d.%1d FPS : %s",FrameRate/10,FrameRate%10,g_szTitle);
//			wsprintf(str,"%s",g_szTitle);
			TextOut(hdc,0,0, str, lstrlen(str));
			pCanvas->ReleaseDC(hdc);
		}
		FrameTimePrev=FrameTime;
	}
*/
	//表画面へ転送！
	pCanvas->Flip();
}

