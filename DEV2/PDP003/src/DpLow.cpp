#include <stdio.h>
#include <io.h>
#include <assert.h>
#include <math.h>
#include "DPLow.h"

//define======================================================
#define DPTCI_NONE			65535
#define DPTCI_TRANSPARENT	0
#define MAX_NUM_TEXTURE_COLOR 65534
//PF=5-6-5
#define RGB16_565(r,g,b) (((r)&0xF800)|(((g)&0xF800)>>(10-5))|(((b)&0xF800)>>(11)))
#define RGB8_565(r,g,b)  ((((r)&0xF8  )<<8)|(((g)&0xF8  )<<3)|(((b)&0xF8  )>> 3))
#define RGBA16_565(back,r,g,b,a,na) \
	RGB16_565((((((back)&0xF800))*(na))>>16)+(((r)*(a))>>16),\
	(((((back)&0x07E0)<<(11- 6))*(na))>>16)+(((g)*(a))>>16),\
	(((((back)&0x001F)<<(11   ))*(na))>>16)+(((b)*(a))>>16))
#define GetRValue16_565(n) ((n)&0xF800)
#define GetGValue16_565(n) (((n)&0x07E0)<<5)
#define GetBValue16_565(n) (((n)&0x001F)<<11)
//PF=5-5-5
#define RGB16_555(r,g,b) ((((r)&0xF800)>>(11-10))|(((g)&0xF800)>>(11-5))|(((b)&0xF800)>>(11)))
#define RGB8_555(r,g,b)  ((((r)&0xF8  )<<7)|(((g)&0xF8  )<<2)|(((b)&0xF8  )>> 3))
#define RGBA16_555(back,r,g,b,a,na) \
	RGB16_555((((((back)&0x7C00)<<(11-10))*(na))>>16)+(((r)*(a))>>16),\
	(((((back)&0x03E0)<<(11- 5))*(na))>>16)+(((g)*(a))>>16),\
	(((((back)&0x001F)<<(11   ))*(na))>>16)+(((b)*(a))>>16))
#define GetRValue16_555(n) (((n)&0x7C00)<<1)
#define GetGValue16_555(n) (((n)&0x03E0)<<6)
#define GetBValue16_555(n) (((n)&0x001F)<<11)

//local functions prototype declaration=======================
int GetTextureColorIndex(CColorRef col);
void ConvertTextureColorTable(EPixelFormat pf);
CPolygon* ClipPolygon(CCanvas const& canvas,CPolygon const& poly);
static void DrawPolygon0000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon0341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon1341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon2341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);
static void DrawPolygon3341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY);

//system variables==============================================
static BOOL g_bOpened=FALSE;								//ライブラリをオープンしているか
static LPDIRECTDRAW g_lpDD=NULL;							//DirectDraw
//for DPoly
static int g_nVertClip=36;											//ポリゴンの最大角数(可変長配列用)
static CTLVertex* g_pVertClipX=NULL;								//クリッピング用バッファ(可変長配列)
static CTLVertex* g_pVertClipY=NULL;								//クリッピング用バッファ(可変長配列)
//for ErrTool
static EErrorReportType g_errReportType=DP_ERT_MESSAGEBOX;			//エラーレポートタイプ
static DWORD g_dwErrCode=0;										//エラーコード保持用
static int g_nErrMsgLen=0;											//エラーメッセージ長(可変長配列用)
static char* g_szErrMsg=NULL;										//エラーメッセージ保持用(可変長配列)
static FILE* g_fpErrLog=NULL;										//エラーログ用
static int g_nErtStackIndex=0;
static int g_nErtStackSize=0;
static EErrorReportType* g_pErtStack=NULL;
static HWND g_hwndMain=NULL;								//メッセージボックス用HWND
//for DplTexHelper
static WORD* g_pTexColUsed=NULL;							//テクスチャのインデックス作成用：色が登録されているかどうかのフラグ
static int g_nTexColUsed=1;									//テクスチャのインデックス作成用：登録された色数
static TArray<CTexture*> g_apTexture;							//テクスチャ保持
static WORD** g_ppTexShade=NULL;									//カラーテーブル：テクスチャのインデックスとシェードからテクスチャのカラーを求めるテーブル
static EPixelFormat g_pfTexture;								//カラーテーブルのピクセルフォーマット
static void (*g_pDrawPolygon[160])(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)={
    DrawPolygon0000,
    DrawPolygon0001,
    DrawPolygon0010,
    DrawPolygon0011,
    DrawPolygon0020,
    DrawPolygon0021,
    DrawPolygon0030,
    DrawPolygon0031,
    DrawPolygon0040,
    DrawPolygon0041,
    DrawPolygon0100,
    DrawPolygon0101,
    DrawPolygon0110,
    DrawPolygon0111,
    DrawPolygon0120,
    DrawPolygon0121,
    DrawPolygon0130,
    DrawPolygon0131,
    DrawPolygon0140,
    DrawPolygon0141,
    DrawPolygon0200,
    DrawPolygon0201,
    DrawPolygon0210,
    DrawPolygon0211,
    DrawPolygon0220,
    DrawPolygon0221,
    DrawPolygon0230,
    DrawPolygon0231,
    DrawPolygon0240,
    DrawPolygon0241,
    DrawPolygon0300,
    DrawPolygon0301,
    DrawPolygon0310,
    DrawPolygon0311,
    DrawPolygon0320,
    DrawPolygon0321,
    DrawPolygon0330,
    DrawPolygon0331,
    DrawPolygon0340,
    DrawPolygon0341,
    DrawPolygon1000,
    DrawPolygon1001,
    DrawPolygon1010,
    DrawPolygon1011,
    DrawPolygon1020,
    DrawPolygon1021,
    DrawPolygon1030,
    DrawPolygon1031,
    DrawPolygon1040,
    DrawPolygon1041,
    DrawPolygon1100,
    DrawPolygon1101,
    DrawPolygon1110,
    DrawPolygon1111,
    DrawPolygon1120,
    DrawPolygon1121,
    DrawPolygon1130,
    DrawPolygon1131,
    DrawPolygon1140,
    DrawPolygon1141,
    DrawPolygon1200,
    DrawPolygon1201,
    DrawPolygon1210,
    DrawPolygon1211,
    DrawPolygon1220,
    DrawPolygon1221,
    DrawPolygon1230,
    DrawPolygon1231,
    DrawPolygon1240,
    DrawPolygon1241,
    DrawPolygon1300,
    DrawPolygon1301,
    DrawPolygon1310,
    DrawPolygon1311,
    DrawPolygon1320,
    DrawPolygon1321,
    DrawPolygon1330,
    DrawPolygon1331,
    DrawPolygon1340,
    DrawPolygon1341,
    DrawPolygon2000,
    DrawPolygon2001,
    DrawPolygon2010,
    DrawPolygon2011,
    DrawPolygon2020,
    DrawPolygon2021,
    DrawPolygon2030,
    DrawPolygon2031,
    DrawPolygon2040,
    DrawPolygon2041,
    DrawPolygon2100,
    DrawPolygon2101,
    DrawPolygon2110,
    DrawPolygon2111,
    DrawPolygon2120,
    DrawPolygon2121,
    DrawPolygon2130,
    DrawPolygon2131,
    DrawPolygon2140,
    DrawPolygon2141,
    DrawPolygon2200,
    DrawPolygon2201,
    DrawPolygon2210,
    DrawPolygon2211,
    DrawPolygon2220,
    DrawPolygon2221,
    DrawPolygon2230,
    DrawPolygon2231,
    DrawPolygon2240,
    DrawPolygon2241,
    DrawPolygon2300,
    DrawPolygon2301,
    DrawPolygon2310,
    DrawPolygon2311,
    DrawPolygon2320,
    DrawPolygon2321,
    DrawPolygon2330,
    DrawPolygon2331,
    DrawPolygon2340,
    DrawPolygon2341,
    DrawPolygon3000,
    DrawPolygon3001,
    DrawPolygon3010,
    DrawPolygon3011,
    DrawPolygon3020,
    DrawPolygon3021,
    DrawPolygon3030,
    DrawPolygon3031,
    DrawPolygon3040,
    DrawPolygon3041,
    DrawPolygon3100,
    DrawPolygon3101,
    DrawPolygon3110,
    DrawPolygon3111,
    DrawPolygon3120,
    DrawPolygon3121,
    DrawPolygon3130,
    DrawPolygon3131,
    DrawPolygon3140,
    DrawPolygon3141,
    DrawPolygon3200,
    DrawPolygon3201,
    DrawPolygon3210,
    DrawPolygon3211,
    DrawPolygon3220,
    DrawPolygon3221,
    DrawPolygon3230,
    DrawPolygon3231,
    DrawPolygon3240,
    DrawPolygon3241,
    DrawPolygon3300,
    DrawPolygon3301,
    DrawPolygon3310,
    DrawPolygon3311,
    DrawPolygon3320,
    DrawPolygon3321,
    DrawPolygon3330,
    DrawPolygon3331,
    DrawPolygon3340,
    DrawPolygon3341,
};



//code==========================================================
//システム関数系====================================================
//ライブラリのオープン
BOOL OpenDPLow(HWND hwnd)
{
	if(g_bOpened==TRUE) return TRUE;
	if(g_pVertClipX==NULL) g_pVertClipX=new CTLVertex[g_nVertClip+4];
	if(g_pVertClipY==NULL) g_pVertClipY=new CTLVertex[g_nVertClip+4];
	if(g_pTexColUsed==NULL){
		g_pTexColUsed=new WORD[65536];//128Kbyte
		memset(g_pTexColUsed,DPTCI_NONE,sizeof(WORD)*65536);
	}
	if(g_ppTexShade==NULL){
		g_ppTexShade=new WORD* [256];
		memset(g_ppTexShade,0,sizeof(WORD*)*256);
	}
	if(!(g_hwndMain!=NULL && hwnd==NULL)){
		g_hwndMain=hwnd;
	}
	g_bOpened=TRUE;
	return TRUE;
}

//ライブラリのクローズ
void CloseDPLow()
{
	if(g_bOpened==FALSE) return;
	if(g_pVertClipX){
		delete [] g_pVertClipX;
		g_pVertClipX=NULL;
	}
	if(g_pVertClipY){
		delete [] g_pVertClipY;
		g_pVertClipY=NULL;
	}
	if(g_szErrMsg){
		delete [] g_szErrMsg;
		g_szErrMsg=NULL;
	}
	if(g_pTexColUsed){
		delete [] g_pTexColUsed;
		g_pTexColUsed=NULL;
	}
	if(g_ppTexShade){
		for(int i=0;i<256;i++){
			if(g_ppTexShade[i]){
				delete [] g_ppTexShade[i];
				g_ppTexShade[i]=NULL;
			}
		}
		delete [] g_ppTexShade;
		g_ppTexShade=NULL;
	}
	if(g_pErtStack){
		delete [] g_pErtStack;
	}
	for(int i=0;i<g_apTexture.GetSize();i++){
		delete g_apTexture.GetAt(i);
	}
	g_apTexture.RemoveAll();
	ReleaseDD();
	if(g_fpErrLog){
		fclose(g_fpErrLog);
		if(g_errReportType==DP_ERT_LOGFILE2){
			MessageBox(NULL,"Error occured. Check " DP_ERR_LOG_FILE_NAME
				,"Error Report",MB_OK|MB_ICONEXCLAMATION);
		}
	}
	g_bOpened=FALSE;
}
//HWNDの設定
void SetMainHWND(HWND hwnd)
{
	g_hwndMain=hwnd;
}

//HWNDの取得
HWND GetMainHWND()
{
	return g_hwndMain;
}

//テクスチャの色の登録とインデックスの取得
int GetTextureColorIndex(CColorRef col)
{
	assert(g_pTexColUsed);//OpenDPLow()し忘れ
	if(col.a!=0){
		WORD n=((col.r&0xF8)<<8)|((col.g&0xFC)<<3)|((col.b&0xF8)>>3);
		if(g_pTexColUsed[n]!=DPTCI_NONE){
			return g_pTexColUsed[n];
		} else {
			int r,g,b;
			g_pTexColUsed[n]=g_nTexColUsed;
			for(int i=0;i<256;i++){
				if((g_nTexColUsed%16)==0 || g_ppTexShade[i]==NULL){//16色追加される毎に１回、realloc
					WORD* temp=new WORD[g_nTexColUsed+16];
					if(g_ppTexShade[i]){
						memcpy(temp,g_ppTexShade[i],sizeof(WORD)*g_nTexColUsed);
						delete [] g_ppTexShade[i];
					}
					g_ppTexShade[i]=temp;
				}
				if(i<128){
					r=(col.r*i)>>7;
					g=(col.g*i)>>7;
					b=(col.b*i)>>7;
				} else {
					r=col.r+(((255-col.r)*(i-128))>>7);
					g=col.g+(((255-col.g)*(i-128))>>7);
					b=col.b+(((255-col.b)*(i-128))>>7);
				}
				if(g_pfTexture==DP_PF_RGB555){
					g_ppTexShade[i][g_nTexColUsed]=RGB8_555(r,g,b);
				} else if(g_pfTexture==DP_PF_RGB565){
					g_ppTexShade[i][g_nTexColUsed]=RGB8_565(r,g,b);
				} else {
					assert(FALSE);
				}
				if(g_nTexColUsed==1){//nTexCulUsedが0のとき
					if(i<128){//透明色が不透明として描画されるときの色を黒に設定
						g_ppTexShade[i][0]=0;
					} else {
						if(g_pfTexture==DP_PF_RGB555){
							g_ppTexShade[i][0]=RGB8_555((255*(i-128))>>7
								,(255*(i-128))>>7,(255*(i-128))>>7);
						} else if(g_pfTexture==DP_PF_RGB565){
							g_ppTexShade[i][0]=RGB8_565((255*(i-128))>>7
								,(255*(i-128))>>7,(255*(i-128))>>7);
						} else {
							assert(FALSE);
						}
					}
				}
			}
			g_nTexColUsed++;
			if(g_nTexColUsed>=MAX_NUM_TEXTURE_COLOR){
				SetError(DP_ERR_TOO_MANY_TEXTURE_COLOR
					,"Too many texture color in GetTextureColorIndex()");
			}
		}
		return g_nTexColUsed-1;
	} else {//透明色のとき
		return DPTCI_TRANSPARENT;
	}
}

//テクスチャカラーテーブルのピクセルフォーマットの変換
void ConvertTextureColorTable(EPixelFormat pf)
{
	if(g_nTexColUsed==1){
		g_pfTexture=pf;
		return;
	}
//	if(g_pfTexture==pf) return;//変換の必要なし
	int r,g,b;
	for(int i=0;i<256;i++){
		for(int j=0;j<g_nTexColUsed;j++){
			WORD old=g_ppTexShade[i][j];
			if(g_pfTexture==DP_PF_RGB555){
				r=GetRValue16_555(old);
				g=GetGValue16_555(old);
				b=GetBValue16_555(old);
			} else if(g_pfTexture==DP_PF_RGB565){
				r=GetRValue16_565(old);
				g=GetGValue16_565(old);
				b=GetBValue16_565(old);
			} else {
				assert(FALSE);
			}
			if(pf==DP_PF_RGB555){
				g_ppTexShade[i][j]=RGB16_555(r,g,b);
			} else if(pf==DP_PF_RGB565){
				g_ppTexShade[i][j]=RGB16_565(r,g,b);
			} else {
				assert(FALSE);
			}
		}
	}
	g_pfTexture=pf;
}

//DDrawの取得
LPDIRECTDRAW GetDD()
{
	if(g_lpDD){
		return g_lpDD;
	} else {
		HRESULT err=DirectDrawCreate(NULL,&g_lpDD,NULL);
		if (err!=DD_OK){
			SetError(DP_ERR_DIRECTDRAWCREATE
				,"Can't create direct draw object in GetDD().");
			return NULL;
		}
	}
	return g_lpDD;
}

//DirectDraw関係の破壊
void ReleaseDD()
{
#ifdef _NDEBUG
	RELEASE(g_lpDD);
#else
	if(g_lpDD){
//		SetError(0,"release DD");
		g_lpDD->Release();
		g_lpDD=NULL;
//		SetError(0,"released DD");
	}
#endif
}

//エラー処理系===========================================================
//エラーレポートタイプの設定
void SetErrorReportType(EErrorReportType type)
{
	g_errReportType=type;
}

//エラーレポートタイプの取得
EErrorReportType GetErrorReportType()
{
	return g_errReportType;
}

//エラーレポートタイプのプッシュ
void PushErt(EErrorReportType ert/*=DP_ERT_NONE*/)
{
	if(g_nErtStackIndex>=g_nErtStackSize){
		EErrorReportType* temp=new EErrorReportType[g_nErtStackSize+10];
		memcpy(temp,g_pErtStack,sizeof(EErrorReportType)*g_nErtStackSize);
		delete [] g_pErtStack;
		g_pErtStack=temp;
		g_nErtStackSize+=10;
	}
	g_pErtStack[g_nErtStackIndex++]=GetErrorReportType();
	SetErrorReportType(ert);
}

//エラーレポートタイプのポップ
void PopErt()
{
	if(g_nErtStackIndex>0){
		g_nErtStackIndex--;
		SetErrorReportType(g_pErtStack[g_nErtStackIndex]);
	}
}

//エラーのレポート
void SetError(DWORD dwErrCode,char* szErrMsg,...)
{
	g_dwErrCode=dwErrCode;
	//可変長引数の展開
	va_list args;
    #if !(defined(__BORLANDC__) || defined(__TURBOC__))
	int l=-1;
	do{
		if(g_szErrMsg){
			va_start(args,szErrMsg);
			l=_vsnprintf(g_szErrMsg,g_nErrMsgLen,szErrMsg,args);
			va_end(args);
		}
		if(l<0){//バッファが足りないとき
			if(g_szErrMsg){
				delete [] g_szErrMsg;
				g_szErrMsg=NULL;
			}
			g_nErrMsgLen+=256;//バッファを広くする
			g_szErrMsg=new char[g_nErrMsgLen];
		}
	}while(l<0);
    #else
    if(g_szErrMsg==NULL){
    	g_nErrMsgLen=4096;
    	g_szErrMsg=new char[g_nErrMsgLen];
    }
   	va_start(args,szErrMsg);
	vsprintf(g_szErrMsg,szErrMsg,args);
	va_end(args);
	#endif
	//出力
	char* p;
	switch(g_errReportType){
	case DP_ERT_NONE:
		break;
	case DP_ERT_MESSAGEBOX:
		MessageBox(g_hwndMain,g_szErrMsg,"ERROR",MB_ICONEXCLAMATION|MB_OK);
		break;
	case DP_ERT_LOGFILE:
	case DP_ERT_LOGFILE2:
		if(g_fpErrLog==NULL){
			g_fpErrLog=fopen(DP_ERR_LOG_FILE_NAME,"wt");
		}
		if(g_fpErrLog){//ファイルがオープンできなかったら、何もしない。
			fprintf(g_fpErrLog,"%s\n",g_szErrMsg);
			fflush(g_fpErrLog);
		}
		break;
	case DP_ERT_DEBUGGER:
		p=new char[strlen(g_szErrMsg)+3];
		strcpy(p,g_szErrMsg);
		strcat(p,"\r\n");
		OutputDebugString(p);
		delete [] p;
		break;
	default:
		assert(FALSE);
		break;
	}
}


//エラーコードの取得
DWORD GetError()
{
	return g_dwErrCode;
}

//エラーメッセージの取得
char* GetErrorMessage()
{
	return g_szErrMsg;
}

//テクスチャのロード
CTexture* GetTexture(LPCTSTR szName)
{
	for(int i=0;i<g_apTexture.GetSize();i++){
		CTexture* p=g_apTexture.GetAt(i);
		if(stricmp(szName,p->GetName())==0){//すでにロードされているとき
			return p;
		}
	}
	//ロードされていなかったとき
	CTexture* pTexture=new CTexture;
	if(pTexture->Load(szName)){
		g_apTexture.Add(pTexture);
		return pTexture;
	} else {
		return FALSE;
	}
}


//CVectorクラス


//CMatrixクラス
#ifdef _DEBUG
//#define _ASSERT_ADD_X_ROTATION
//#define _ASSERT_ADD_Y_ROTATION
//#define _ASSERT_ADD_Z_ROTATION
//#define _ASSERT_ADD_TRANSLATION
//#define _ASSERT_ADD_SCALE
//#define _ASSERT_INVERSE_MATRIX
//#define _ASSERT_INVERSE_MATRIX2	// InverseMatrix()の別の算出方法をアサートする
#endif

CMatrix::CMatrix()
{
	for(int col=0; col<4; col++){
		for(int row=0; row<4; row++){
			if(row==col){
				m_rElement[row][col] = real(1.0);
			}
			else{
				m_rElement[row][col] = real(0.0);
			}
		}
	}
}

CMatrix::CMatrix(CMatrix const& mtrx)
{
	*this = mtrx;
}



#ifdef _DEBUG
real CMatrix::GetElement(int row, int col) const
{
	assert(0<=row && row<4);
	assert(0<=col && col<4);

	return m_rElement[row][col];
}
#else
//inline real CMatrix::GetElement(int row, int col) const
real CMatrix::GetElement(int row, int col) const
{
	return m_rElement[row][col];
}
#endif


#ifdef _DEBUG
CMatrix& CMatrix::SetElement(int row, int col, real val)
{
	assert(0<=row && row<4);
	assert(0<=col && col<4);

	m_rElement[row][col] = val;

	return *this;
}
#else
//inline CMatrix& CMatrix::SetElement(int row, int col, real val)
CMatrix& CMatrix::SetElement(int row, int col, real val)
{
	m_rElement[row][col] = val;

	return *this;
}
#endif


real& CMatrix::operator()(int row, int col)
{
	assert(0<=row && row<4);
	assert(0<=col && col<4);

	return m_rElement[row][col];
}

CMatrix& CMatrix::SetUnit()
{
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			if(j==i){
				m_rElement[j][i] = real(1.0);
			}
			else{
				m_rElement[j][i] = real(0.0);
			}
		}
	}

	return *this;
}

BOOL CMatrix::IsUnit() const
{
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			if(
				(j==i && m_rElement[j][i]!=real(1.0)) ||
				         m_rElement[j][i]!=real(0.0)){

				return FALSE;
			}
		}
	}

	return TRUE;
}

CMatrix& CMatrix::AddXRotation(real rot)
{
#if defined(_ASSERT_ADD_X_ROTATION) && defined(_DEBUG)
	CMatrix	mtrxOrg = *this;
#endif
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	real	rTemp10 = m_rElement[1][0]*nCos - m_rElement[2][0]*nSin,
			rTemp20 = m_rElement[1][0]*nSin + m_rElement[2][0]*nCos,

			rTemp11 = m_rElement[1][1]*nCos - m_rElement[2][1]*nSin,
			rTemp21 = m_rElement[1][1]*nSin + m_rElement[2][1]*nCos,

			rTemp12 = m_rElement[1][2]*nCos - m_rElement[2][2]*nSin,
			rTemp22 = m_rElement[1][2]*nSin + m_rElement[2][2]*nCos,

			rTemp13 = m_rElement[1][3]*nCos - m_rElement[2][3]*nSin,
			rTemp23 = m_rElement[1][3]*nSin + m_rElement[2][3]*nCos;

	m_rElement[1][0] = rTemp10;
	m_rElement[2][0] = rTemp20;

	m_rElement[1][1] = rTemp11;
	m_rElement[2][1] = rTemp21;

	m_rElement[1][2] = rTemp12;
	m_rElement[2][2] = rTemp22;

	m_rElement[1][3] = rTemp13;
	m_rElement[2][3] = rTemp23;

#if defined(_ASSERT_ADD_X_ROTATION) && defined(_DEBUG)
	{
		CMatrix	mtrxTranslation,
				mtrxResult;
		mtrxTranslation.SetXRotation(rot);
		mtrxResult = mtrxOrg * mtrxTranslation;

		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				assert(m_rElement[j][i]==mtrxResult.m_rElement[j][i]);
			}
		}
	}
#endif
	
	return *this;
}

CMatrix& CMatrix::SetXRotation(real rot)
{
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	m_rElement[0][0] = real(1.0);
	m_rElement[1][0] = real(0.0);
	m_rElement[2][0] = real(0.0);
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = real(0.0);
	m_rElement[1][1] = nCos;
	m_rElement[2][1] = nSin;
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = real(0.0);
	m_rElement[1][2] = -nSin;
	m_rElement[2][2] = nCos;
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = real(0.0);
	m_rElement[1][3] = real(0.0);
	m_rElement[2][3] = real(0.0);
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::AddYRotation(real rot)
{
#if defined(_ASSERT_ADD_Y_ROTATION) && defined(_DEBUG)
	CMatrix	mtrxOrg = *this;
#endif
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	real	rTemp00 =  m_rElement[0][0]*nCos + m_rElement[2][0]*nSin,
			rTemp20 = -m_rElement[0][0]*nSin + m_rElement[2][0]*nCos,

			rTemp01 =  m_rElement[0][1]*nCos + m_rElement[2][1]*nSin,
			rTemp21 = -m_rElement[0][1]*nSin + m_rElement[2][1]*nCos,

			rTemp02 =  m_rElement[0][2]*nCos + m_rElement[2][2]*nSin,
			rTemp22 = -m_rElement[0][2]*nSin + m_rElement[2][2]*nCos,

			rTemp03 =  m_rElement[0][3]*nCos + m_rElement[2][3]*nSin,
			rTemp23 = -m_rElement[0][3]*nSin + m_rElement[2][3]*nCos;

	m_rElement[0][0] = rTemp00;
	m_rElement[2][0] = rTemp20;

	m_rElement[0][1] = rTemp01;
	m_rElement[2][1] = rTemp21;

	m_rElement[0][2] = rTemp02;
	m_rElement[2][2] = rTemp22;

	m_rElement[0][3] = rTemp03;
	m_rElement[2][3] = rTemp23;

#if defined(_ASSERT_ADD_Y_ROTATION) && defined(_DEBUG)
	{
		CMatrix	mtrxTranslation,
				mtrxResult;
		mtrxTranslation.SetYRotation(rot);
		mtrxResult = mtrxOrg * mtrxTranslation;

		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				assert(m_rElement[j][i]==mtrxResult.m_rElement[j][i]);
			}
		}
	}
#endif
	
	return *this;
}

CMatrix& CMatrix::SetYRotation(real rot)
{
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	m_rElement[0][0] = nCos;
	m_rElement[1][0] = real(0.0);
	m_rElement[2][0] = -nSin;
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = real(0.0);
	m_rElement[1][1] = real(1.0);
	m_rElement[2][1] = real(0.0);
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = nSin;
	m_rElement[1][2] = real(0.0);
	m_rElement[2][2] = nCos;
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = real(0.0);
	m_rElement[1][3] = real(0.0);
	m_rElement[2][3] = real(0.0);
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::AddZRotation(real rot)
{
#if defined(_ASSERT_ADD_Z_ROTATION) && defined(_DEBUG)
	CMatrix	mtrxOrg = *this;
#endif
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	real	rTemp00 = m_rElement[0][0]*nCos - m_rElement[1][0]*nSin,
			rTemp10 = m_rElement[0][0]*nSin + m_rElement[1][0]*nCos,

			rTemp01 = m_rElement[0][1]*nCos - m_rElement[1][1]*nSin,
			rTemp11 = m_rElement[0][1]*nSin + m_rElement[1][1]*nCos,

			rTemp02 = m_rElement[0][2]*nCos - m_rElement[1][2]*nSin,
			rTemp12 = m_rElement[0][2]*nSin + m_rElement[1][2]*nCos,

			rTemp03 = m_rElement[0][3]*nCos - m_rElement[1][3]*nSin,
			rTemp13 = m_rElement[0][3]*nSin + m_rElement[1][3]*nCos;

	m_rElement[0][0] = rTemp00;
	m_rElement[1][0] = rTemp10;

	m_rElement[0][1] = rTemp01;
	m_rElement[1][1] = rTemp11;

	m_rElement[0][2] = rTemp02;
	m_rElement[1][2] = rTemp12;

	m_rElement[0][3] = rTemp03;
	m_rElement[1][3] = rTemp13;

#if defined(_ASSERT_ADD_Z_ROTATION) && defined(_DEBUG)
	{
		CMatrix	mtrxTranslation,
				mtrxResult;
		mtrxTranslation.SetZRotation(rot);
		mtrxResult = mtrxOrg * mtrxTranslation;

		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				assert(m_rElement[j][i]==mtrxResult.m_rElement[j][i]);
			}
		}
	}
#endif
	
	return *this;
}

CMatrix& CMatrix::SetZRotation(real rot)
{
	real	nSin = (real)sin(rot),
			nCos = (real)cos(rot);

	m_rElement[0][0] = nCos;
	m_rElement[1][0] = nSin;
	m_rElement[2][0] = real(0.0);
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = -nSin;
	m_rElement[1][1] = nCos;
	m_rElement[2][1] = real(0.0);
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = real(0.0);
	m_rElement[1][2] = real(0.0);
	m_rElement[2][2] = real(1.0);
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = real(0.0);
	m_rElement[1][3] = real(0.0);
	m_rElement[2][3] = real(0.0);
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::AddTranslation(real x, real y, real z)
{
#if defined(_ASSERT_ADD_TRANSLATION) && defined(_DEBUG)
	CMatrix	mtrxOrg = *this;
#endif

	real	rTemp00 = m_rElement[0][0] + m_rElement[3][0] * x,
			rTemp10 = m_rElement[1][0] + m_rElement[3][0] * y,
			rTemp20 = m_rElement[2][0] + m_rElement[3][0] * z,
			rTemp30 = m_rElement[3][0],

			rTemp01 = m_rElement[0][1] + m_rElement[3][1] * x,
			rTemp11 = m_rElement[1][1] + m_rElement[3][1] * y,
			rTemp21 = m_rElement[2][1] + m_rElement[3][1] * z,
			rTemp31 = m_rElement[3][1],

			rTemp02 = m_rElement[0][2] + m_rElement[3][2] * x,
			rTemp12 = m_rElement[1][2] + m_rElement[3][2] * y,
			rTemp22 = m_rElement[2][2] + m_rElement[3][2] * z,
			rTemp32 = m_rElement[3][2],

			rTemp03 = m_rElement[0][3] + m_rElement[3][3] * x,
			rTemp13 = m_rElement[1][3] + m_rElement[3][3] * y,
			rTemp23 = m_rElement[2][3] + m_rElement[3][3] * z,
			rTemp33 = m_rElement[3][3];

	m_rElement[0][0] = rTemp00;
	m_rElement[1][0] = rTemp10;
	m_rElement[2][0] = rTemp20;
	m_rElement[3][0] = rTemp30;

	m_rElement[0][1] = rTemp01;
	m_rElement[1][1] = rTemp11;
	m_rElement[2][1] = rTemp21;
	m_rElement[3][1] = rTemp31;

	m_rElement[0][2] = rTemp02;
	m_rElement[1][2] = rTemp12;
	m_rElement[2][2] = rTemp22;
	m_rElement[3][2] = rTemp32;

	m_rElement[0][3] = rTemp03;
	m_rElement[1][3] = rTemp13;
	m_rElement[2][3] = rTemp23;
	m_rElement[3][3] = rTemp33;

#if defined(_ASSERT_ADD_TRANSLATION) && defined(_DEBUG)
	{
		CMatrix	mtrxTranslation,
				mtrxResult;
		mtrxTranslation.SetTranslation(x, y, z);
		mtrxResult = mtrxOrg * mtrxTranslation;

		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				assert(m_rElement[j][i]==mtrxResult.m_rElement[j][i]);
			}
		}
	}
#endif

	return *this;
}

CMatrix& CMatrix::SetTranslation(real x, real y, real z)
{
	m_rElement[0][0] = real(1.0);
	m_rElement[1][0] = real(0.0);
	m_rElement[2][0] = real(0.0);
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = real(0.0);
	m_rElement[1][1] = real(1.0);
	m_rElement[2][1] = real(0.0);
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = real(0.0);
	m_rElement[1][2] = real(0.0);
	m_rElement[2][2] = real(1.0);
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = x;
	m_rElement[1][3] = y;
	m_rElement[2][3] = z;
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::SetRotation(real x, real y, real z, real rot)
{
	real	nSin = real(sin(rot)),
			nCos = real(cos(rot)),
			nSinX = x*nSin,
			nSinY = y*nSin,
			nSinZ = z*nSin,
			nCos2 = 1-nCos,
			x2 = x*x,
			y2 = y*y,
			z2 = z*z,
			xy = x*y,
			yz = y*z,
			xz = z*x;

	m_rElement[0][0] = x2 + (1-x2)*nCos;
	m_rElement[1][0] = xy*nCos2 + nSinZ;
	m_rElement[2][0] = xz*nCos2 - nSinY;
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = xy*nCos2 - nSinZ;
	m_rElement[1][1] = y2 + (1-y2)*nCos;
	m_rElement[2][1] = yz*nCos2 + nSinX;
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = yz*nCos2 + nSinY;
	m_rElement[1][2] = yz*nCos2 - nSinX;
	m_rElement[2][2] = z2 + (1-z2)*nCos;
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = real(0.0);
	m_rElement[1][3] = real(0.0);
	m_rElement[2][3] = real(0.0);
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::AddScale(real x, real y, real z)
{
#if defined(_ASSERT_ADD_SCALE) && defined(_DEBUG)
	CMatrix	mtrxOrg = *this;
#endif

	real	rTemp00 = m_rElement[0][0] * x,
			rTemp10 = m_rElement[1][0] * y,
			rTemp20 = m_rElement[2][0] * z,
//			rTemp30 = m_rElement[3][0],
			
			rTemp01 = m_rElement[0][1] * x,
			rTemp11 = m_rElement[1][1] * y,
			rTemp21 = m_rElement[2][1] * z,
//			rTemp31 = m_rElement[3][1],

			rTemp02 = m_rElement[0][2] * x,
			rTemp12 = m_rElement[1][2] * y,
			rTemp22 = m_rElement[2][2] * z,
//			rTemp32 = m_rElement[3][2],

			rTemp03 = m_rElement[0][3] * x,
			rTemp13 = m_rElement[1][3] * y,
			rTemp23 = m_rElement[2][3] * z;
//			rTemp33 = m_rElement[3][3];

	m_rElement[0][0] = rTemp00;
	m_rElement[1][0] = rTemp10;
	m_rElement[2][0] = rTemp20;
//	m_rElement[3][0] = rTemp30;

	m_rElement[0][1] = rTemp01;
	m_rElement[1][1] = rTemp11;
	m_rElement[2][1] = rTemp21;
//	m_rElement[3][1] = rTemp31;

	m_rElement[0][2] = rTemp02;
	m_rElement[1][2] = rTemp12;
	m_rElement[2][2] = rTemp22;
//	m_rElement[3][2] = rTemp32;

	m_rElement[0][3] = rTemp03;
	m_rElement[1][3] = rTemp13;
	m_rElement[2][3] = rTemp23;
//	m_rElement[3][3] = rTemp33;

#if defined(_ASSERT_ADD_SCALE) && defined(_DEBUG)
	{
		CMatrix	mtrxScale,
				mtrxResult;
		mtrxScale.SetScale(x, y, z);
		mtrxResult = mtrxOrg * mtrxScale;

		for(int i=0; i<4; i++){
			for(int j=0; j<4; j++){
				assert(m_rElement[j][i]==mtrxResult.m_rElement[j][i]);
			}
		}
	}		
#endif

	return *this;
}

CMatrix& CMatrix::SetScale(real x, real y, real z)
{
	m_rElement[0][0] = x;
	m_rElement[1][0] = real(0.0);
	m_rElement[2][0] = real(0.0);
	m_rElement[3][0] = real(0.0);

	m_rElement[0][1] = real(0.0);
	m_rElement[1][1] = y;
	m_rElement[2][1] = real(0.0);
	m_rElement[3][1] = real(0.0);

	m_rElement[0][2] = real(0.0);
	m_rElement[1][2] = real(0.0);
	m_rElement[2][2] = z;
	m_rElement[3][2] = real(0.0);

	m_rElement[0][3] = real(0.0);
	m_rElement[1][3] = real(0.0);
	m_rElement[2][3] = real(0.0);
	m_rElement[3][3] = real(1.0);

	return *this;
}

CMatrix& CMatrix::InverseMatrix()
{
	CMatrix	mtrx;

	for(int col=0; col<3; col++){
		for(int row=0; row<3; row++){
			mtrx.m_rElement[row][col]  = m_rElement[col][row];
			mtrx.m_rElement[col][  3] -= m_rElement[row][col] * m_rElement[row][  3];
		}
	}

#if defined(_ASSERT_INVERSE_MATRIX2) && defined(_DEBUG)
	{
		CMatrix	mtrxTemp;

		for(int col=0; col<3; col++){
			for(int row=0; row<3; row++){
				mtrxTemp.m_rElement[row][col]  = m_rElement[col][row];
			}
		}
		
		for(col=0; col<3; col++){
			real	val = real(0.0);
			for(int row=0; row<3; row++){
				mtrxTemp.m_rElement[row][col]  = m_rElement[col][row];
			}
			mtrx.m_rElement[col][3] = -val;
		}

		assert(mtrx==mtrxTemp);
	}
#endif

#if defined(_ASSERT_INVERSE_MATRIX) && defined(_DEBUG)
	{
		CMatrix	mtrxResult,
				mtrxUnit;
		mtrxResult = (*this) * mtrx;
		
		for(int col=0; col<4; col++){
			for(int row=0; row<4; row++){
				assert(mtrxResult.m_rElement[row][col]==mtrxUnit.m_rElement[row][col]);
			}
		}
	}
#endif

	return (*this = mtrx);
}

CMatrix& CMatrix::operator=(CMatrix const& mtrx)
{
	memcpy((void*)m_rElement, (void*)mtrx.m_rElement, sizeof m_rElement);
	return *this;
}

BOOL CMatrix::operator==(CMatrix const& mtrx) const
{
	for(int col=0; col<4; col++){
		for(int row=0; row<4; row++){
			if(m_rElement[row][col]!=mtrx.m_rElement[row][col]){
				return FALSE;
			}
		}
	}
	return TRUE;
}

BOOL CMatrix::operator!=(CMatrix const& mtrx) const
{
	return !((*this)==mtrx);
}

CMatrix CMatrix::operator*(CMatrix const& mtrx) const
{
	CMatrix	mtrxResult;

	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			real	val = real(0.0);
			
			for(int k=0; k<4; k++){
				val +=
					m_rElement[k][j] *
					mtrx.m_rElement[i][k];
			}

			mtrxResult.m_rElement[i][j] = val;
		}
	}
	
	return mtrxResult;
}

//Polygon
//ポリゴンの描画
void DrawPolygon(CCanvas const& canvas,CPolygon const& poly)
{
	struct CFRect {
		real left,top,right,bottom;
	} clip;
	int i,n,na;
	int  top,bottom;//index
	real leftX,rightX,topY,bottomY;
	real lx,rx,lxd,rxd,y;
	CTLVertex const* ver;
	CPolygon const* pPoly;

	leftX=20000;rightX=-20000;
	topY=20000;bottomY=-20000;
	//バウンディングボックスを求める
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	for(i=0;i<n;i++){
		if(ver[i].m_vctrPos.m_rX<leftX) leftX=ver[i].m_vctrPos.m_rX;
		if(ver[i].m_vctrPos.m_rX>rightX) rightX=ver[i].m_vctrPos.m_rX;
		if(ver[i].m_vctrPos.m_rY<topY){
			topY=ver[i].m_vctrPos.m_rY;
			top=i;
		}
		if(ver[i].m_vctrPos.m_rY>bottomY){
			bottomY=ver[i].m_vctrPos.m_rY;
			bottom=i;
		}
	}
	//画面の外に描画しようとしたら、即リターン
	clip.left=clip.top=0;
	clip.right=(real)(canvas.m_nWidth-1);clip.bottom=(real)(canvas.m_nHeight-1);
	lx=max(leftX,clip.left);rx=min(rightX,clip.right);
	if(lx>rx) return;
	lxd=max(topY,clip.top);rxd=min(bottomY,clip.bottom);
	if(lxd>rxd) return;
	//クリッピングが必要か？
	pPoly=&poly;
	if(lx!=leftX || rx!=rightX || lxd!=topY || rxd!=bottomY){//クリッピング必要
		pPoly=ClipPolygon(canvas,poly);
		if(!pPoly) return;
		n=pPoly->m_nVertices;
		ver=pPoly->m_pVertices;
		topY=20000;bottomY=-20000;//topYとbottomYの再計算
		for(i=0;i<n;i++){
			if(ver[i].m_vctrPos.m_rY<topY){
				topY=ver[i].m_vctrPos.m_rY;
				top=i;
			}
			if(ver[i].m_vctrPos.m_rY>bottomY){
				bottomY=ver[i].m_vctrPos.m_rY;
				bottom=i;
			}
		}
	}
	//時計周りか反時計回りか
	int nCw;
	if((pPoly->m_dwFlags&DP_CULL_MASK)==DP_CULL_CW){//時計回り
		nCw=1;
		if((int)topY==(int)bottomY) nCw=0;
	} else if((pPoly->m_dwFlags&DP_CULL_MASK)==DP_CULL_CCW){//反時計回り
		nCw=-1;
		if((int)topY==(int)bottomY) nCw=0;
	} else {//不明
		nCw=0;
		if((int)topY!=(int)bottomY){
			lx=ver[n-1].m_vctrPos.m_rX;
			y=ver[n-1].m_vctrPos.m_rY;
			for(i=0;i<n;i++){
				na=(i+1)%n;
				nCw=(int)((ver[i].m_vctrPos.m_rX-lx)*(ver[na].m_vctrPos.m_rY-ver[i].m_vctrPos.m_rY)
					-(ver[na].m_vctrPos.m_rX-ver[i].m_vctrPos.m_rX)*(ver[i].m_vctrPos.m_rY-y));
				if(nCw!=0){
					nCw=(nCw>0 ? 1 : -1);
					break;
				}
				lx=ver[i].m_vctrPos.m_rX;y=ver[i].m_vctrPos.m_rY;
			}
		}
	}
	int nTex;
	//テクスチャ
	if(pPoly->m_pTexture){
		if((pPoly->m_dwFlags&DP_TEXTURE_MASK)!=DP_TEXTURE_ANY){
			nTex=GetTextureType(pPoly->m_dwFlags)-1;
		} else {//不明
			nTex=1;//テクスチャ＋アルファ
		}
	} else {
		nTex=0;
	}
	//カラー
	int nCol;
	if((pPoly->m_dwFlags&DP_COLOR_MASK)!=DP_COLOR_ANY){
		nCol=GetColorType(pPoly->m_dwFlags)-1;
	} else {//不明
		nCol=0;//mono model
		for(int i=0;i<pPoly->m_nVertices;i++){
			if(!(pPoly->m_pVertices[i].m_color.r==pPoly->m_pVertices[i].m_color.g
				&& pPoly->m_pVertices[i].m_color.g==pPoly->m_pVertices[i].m_color.b)){
				nCol=1;//rgb model
				break;
			}
		}
	}
	//シェード
	int nShade;
	if((pPoly->m_dwFlags&DP_SHADE_MASK)!=DP_SHADE_ANY){
		nShade=GetShadeType(pPoly->m_dwFlags)-1;
	} else {//不明
		nShade=0;//flat shading
		real r=pPoly->m_pVertices[0].m_color.r;
		real g=pPoly->m_pVertices[0].m_color.g;
		real b=pPoly->m_pVertices[0].m_color.b;
		for(int i=1;i<pPoly->m_nVertices;i++){
			if(!(r==pPoly->m_pVertices[i].m_color.r
				&& g==pPoly->m_pVertices[i].m_color.g
				&& b==pPoly->m_pVertices[i].m_color.b)){
				nShade=1;//gouraud shading
				break;
			}
		}
	}
	//アルファ
	int nAlpha;
	if((pPoly->m_dwFlags&DP_ALPHA_MASK)!=DP_ALPHA_ANY){
		nAlpha=GetAlphaType(pPoly->m_dwFlags)-1;
		if(nAlpha>0){
			if(GetBlendType(pPoly->m_dwFlags)){
				nAlpha+=2;//BLEND_ALPHA_ONE
			}
		}
	} else {//不明
		nAlpha=1;//flat
		real a=pPoly->m_pVertices[0].m_color.a;
		for(int i=1;i<pPoly->m_nVertices;i++){
			if(a!=pPoly->m_pVertices[i].m_color.a){
				nAlpha=2;//variable
				if(GetBlendType(pPoly->m_dwFlags)){
					nAlpha+=2;//BLEND_ALPHA_ONE
				}
				break;
			}
		}
		if(nAlpha==1){
			if(pPoly->m_pVertices[0].m_color.a==real(1.0)){
				nAlpha=0;//no alpha
			} else if(pPoly->m_pVertices[0].m_color.a==real(0.0)){
				return;//no draw
			}
		}
	}
	//bit per pixel
	int nBpp=1;
	if(canvas.m_pixelFormat==DP_PF_RGB565){
		nBpp=0;
	}

	//下位の関数を呼ぶ
	(*g_pDrawPolygon[nTex*4*5*2+(nShade*2+nCol)*5*2+nAlpha*2+nBpp])(
		canvas,*pPoly,nCw,top,bottom,topY,bottomY);
}


//クリッピング
#define XPOS(a)		(a>=clipx1 ? (a>clipx2 ? 2 : 1) : 0)
#define YPOS(a)		(a>=clipy1 ? (a>clipy2 ? 2 : 1) : 0)
#define MULVER() \
				pOutVer[idx].m_color.r=r1-(r2-r1)*t1/t2;\
				pOutVer[idx].m_color.g=g1-(g2-g1)*t1/t2;\
				pOutVer[idx].m_color.b=b1-(b2-b1)*t1/t2;\
				pOutVer[idx].m_color.a=a1-(a2-a1)*t1/t2;\
				pOutVer[idx].m_rU=u1-(u2-u1)*t1/t2;\
				pOutVer[idx].m_rV=v1-(v2-v1)*t1/t2;
#define COPYVER1() \
				pOutVer[idx].m_color.r=r1;\
				pOutVer[idx].m_color.g=g1;\
				pOutVer[idx].m_color.b=b1;\
				pOutVer[idx].m_color.a=a1;\
				pOutVer[idx].m_rU=u1;\
				pOutVer[idx].m_rV=v1;
#define COPYVER2() \
				pOutVer[idx].m_color.r=r2;\
				pOutVer[idx].m_color.g=g2;\
				pOutVer[idx].m_color.b=b2;\
				pOutVer[idx].m_color.a=a2;\
				pOutVer[idx].m_rU=u2;\
				pOutVer[idx].m_rV=v2;

static CPolygon* ClipPolygon(CCanvas const& canvas,CPolygon const& poly)
{
	static CPolygon outPoly;
	CTLVertex	const *pVer;
	CTLVertex	*pOutVer;
	int i,start,end,idx=0,idx0;
	real t1,t2;
	real clipx1,clipy1,clipx2,clipy2;
	real x1,y1,x2,y2;
	real r1,r2,g1,g2,b1,b2,a1,a2,u1,u2,v1,v2;

	//
	assert(g_pVertClipY);//OpenDPLow()していないとき
	assert(g_pVertClipX);
	//テンポラリバッファの確保
	if(poly.m_nVertices>g_nVertClip){
		assert(g_pVertClipX && g_pVertClipY);
		g_nVertClip=poly.m_nVertices+4;
		delete [] g_pVertClipX;
		delete [] g_pVertClipY;
		g_pVertClipX=new CTLVertex[g_nVertClip+4];
		g_pVertClipY=new CTLVertex[g_nVertClip+4];
	}
	clipx1=0;
	clipy1=0;
	clipx2=(real)(canvas.m_nWidth-1);
	clipy2=(real)(canvas.m_nHeight-1);
	//ｘのクリッピング*******************************************************
	pOutVer=g_pVertClipX;
	pVer=&poly.m_pVertices[poly.m_nVertices-1];
	x2=pVer->m_vctrPos.m_rX;
	y2=pVer->m_vctrPos.m_rY;
	r2=pVer->m_color.r;
	g2=pVer->m_color.g;
	b2=pVer->m_color.b;
	a2=pVer->m_color.a;
	u2=pVer->m_rU;
	v2=pVer->m_rV;
	pVer=poly.m_pVertices;
	start=XPOS(x2);
	for(i=0;i<poly.m_nVertices;i++){
		x1=x2; y1=y2; r1=r2; g1=g2; b1=b2; a1=a2; u1=u2; v1=v2;
		x2=pVer->m_vctrPos.m_rX; y2=pVer->m_vctrPos.m_rY;
		r2=pVer->m_color.r;g2=pVer->m_color.g;b2=pVer->m_color.b;a2=pVer->m_color.a;
		u2=pVer->m_rU;v2=pVer->m_rV;
		pVer++;
		end=XPOS(x2);
		switch(start*3+end){
		case 0://start=left,end=left ******************************************
			start=end;
			break;
		case 1://start=left,end=in ********************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx1;
			if(x2 != x1){
				t1=x1-clipx1;t2=x2-x1;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++;
			start=end;
			break;
		case 2://start=left,end=right *****************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx1;
			if(x2 != x1){
				t1=x1-clipx1;t2=x2-x1;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=clipx2;
			if(x2 != x1){
				t1=(clipx2-x1);t2=(x1-x2);
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++; start=end;
			break;
		case 3://start=in,end=left ********************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx1;
			if(x2 != x1){
				t1=x1-clipx1;t2=x2-x1;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++; start=end;
			break;
		case 4://start=in,end=in **********************************************
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++; start=end;
			break;
		case 5://start=in,end=right *******************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx2;
			if(x2 != x1){
				t1=clipx2-x1;t2=x1-x2;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++; start=end;
			break;
		case 6://start=right,end=left *****************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx2;
			if(x2 != x1){
				t1=clipx2-x1;t2=x1-x2;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=clipx1;
			if(x2 != x1){
				t1=x1-clipx1;t2=x2-x1;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++; start=end;
			break;
		case 7://start=right,end=in *******************************************
			pOutVer[idx].m_vctrPos.m_rX=clipx2;
			if(x2 != x1){
				t1=clipx2-x1;t2=x1-x2;
				pOutVer[idx].m_vctrPos.m_rY=y1-(y2-y1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rY=y1;
				COPYVER1();
			}
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++; start=end;
			break;
		case 8://start=right,end=right ****************************************
			start=end;
			break;
		}
	}
	//ｙのクリッピング ********************************************************
	pOutVer=g_pVertClipY;
	idx0=idx;
	idx=0;
	pVer=&g_pVertClipX[idx0-1];
	x2=pVer->m_vctrPos.m_rX;
	y2=pVer->m_vctrPos.m_rY;
	r2=pVer->m_color.r;
	g2=pVer->m_color.g;
	b2=pVer->m_color.b;
	a2=pVer->m_color.a;
	u2=pVer->m_rU;
	v2=pVer->m_rV;
	pVer=g_pVertClipX;
	start=YPOS(y2);
	for(i=0;i<idx0;i++){
		x1=x2;
		y1=y2;
		r1=r2;
		g1=g2;
		b1=b2;
		a1=a2;
		u1=u2;
		v1=v2;
		x2=pVer->m_vctrPos.m_rX;
		y2=pVer->m_vctrPos.m_rY;
		r2=pVer->m_color.r;
		g2=pVer->m_color.g;
		b2=pVer->m_color.b;
		a2=pVer->m_color.a;
		u2=pVer->m_rU;v2=pVer->m_rV;
		pVer++;
		end=YPOS(y2);
		switch(start*3+end){
		case 0://start=top,end=top ********************************************
			start=end;
			break;
		case 1://start=top,end=in *********************************************
			if(y2 != y1){
				t1=y1-clipy1;t2=y2-y1;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy1;
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++;
			start=end;
			break;
		case 2://start=top,end=bottom *****************************************
			if(y2 != y1){
				t1=y1-clipy1;t2=y2-y1;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy1;
			idx++;
			if(y2 != y1){
				t1=clipy2-y1;t2=y1-y2;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy2;
			idx++;
			start=end;
			break;
		case 3://start=in,end=top *********************************************
			if(y2 != y1){
				t1=y1-clipy1;t2=y2-y1;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy1;
			idx++;
			start=end;
			break;
		case 4://start=in,end=in **********************************************
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++; start=end;
			break;
		case 5://start=in,end=bottom ******************************************
			if(y2 != y1){
				t1=clipy2-y1;t2=y1-y2;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy2;
			idx++;
			start=end;
			break;
		case 6://start=bottom,end=top *****************************************
			if(y2 != y1){
				t1=clipy2-y1;t2=y1-y2;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy2;
			idx++;
			if(y2 != y1){
				t1=y1-clipy1;t2=y2-y1;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy1;
			idx++;
			start=end;
			break;
		case 7://start=bottom,end=in ******************************************
			if(y2 != y1){
				t1=clipy2-y1;t2=y1-y2;
				pOutVer[idx].m_vctrPos.m_rX=x1-(x2-x1)*t1/t2;
				MULVER();
			} else {
				pOutVer[idx].m_vctrPos.m_rX=x1;
				COPYVER1();
			}
			pOutVer[idx].m_vctrPos.m_rY=clipy2;
			idx++;
			pOutVer[idx].m_vctrPos.m_rX=x2;
			pOutVer[idx].m_vctrPos.m_rY=y2;
			COPYVER2();
			idx++;
			start=end;
			break;
		case 8://start=bottom,end=bottom **************************************
			start=end;
			break;
		}
	}
	if(idx>=3){
		outPoly.m_dwFlags=poly.m_dwFlags;
		outPoly.m_nVertices=idx;
		outPoly.m_pVertices=pOutVer;
		outPoly.m_pTexture=poly.m_pTexture;
		return &outPoly;
	} else {
		return NULL;
	}
}

//PatBlt
//キャンバス全体の描画
void PatBlt(CCanvas const& canvas,CPattern const& pat)
{
	if(pat.m_pTexture==NULL){
		int a=(int)(pat.m_color.a*255.999f);
		if(a==255){
			DWORD* pBuf=(DWORD*)canvas.m_pBuf;
			DWORD len=(canvas.m_nPitch*canvas.m_nHeight)/4;
			DWORD tmp,col;
			int r=(int)(pat.m_color.r*255.999f);
			int g=(int)(pat.m_color.g*255.999f);
			int b=(int)(pat.m_color.b*255.999f);
			if(canvas.m_pixelFormat==DP_PF_RGB555){
				col=RGB8_555(r,g,b);
			} else if(canvas.m_pixelFormat==DP_PF_RGB565){
				col=RGB8_565(r,g,b);
			} else {
				assert(FALSE);
			}
			tmp=col;col<<=16;col|=tmp;
			while(len>0){
				*(pBuf++)=col;
				len--;
			}
		} else {
			WORD* pBuf=(WORD*)canvas.m_pBuf;
			DWORD len=(canvas.m_nPitch*canvas.m_nHeight)/2;
			WORD back;
			int r=(int)(pat.m_color.r*65535.999f);
			int g=(int)(pat.m_color.g*65535.999f);
			int b=(int)(pat.m_color.b*65535.999f);
			int a=(int)(pat.m_color.a*65535.999f);
			int na=65535-a;
			if(canvas.m_pixelFormat==DP_PF_RGB555){
				while(len>0){
					back=*(pBuf);
					*(pBuf++)=RGBA16_555(back,r,g,b,a,na);
					len--;
				}
			} else if(canvas.m_pixelFormat==DP_PF_RGB565){
				while(len>0){
					back=*(pBuf);
					*(pBuf++)=RGBA16_565(back,r,g,b,a,na);
					len--;
				}
			} else {
				assert(FALSE);
			}
		}
	} else {//テクスチャONのときは、DrawPolygonを呼ぶ
		CTLVertex ver[4];
		real w=(real)(canvas.m_nWidth-1);
		real h=(real)(canvas.m_nHeight-1);
		ver[0].m_vctrPos.x=0.f;
		ver[0].m_vctrPos.y=0.f;
		ver[1].m_vctrPos.x=0.f;
		ver[1].m_vctrPos.y=h;
		ver[2].m_vctrPos.x=w;
		ver[2].m_vctrPos.y=h;
		ver[3].m_vctrPos.x=w;
		ver[3].m_vctrPos.y=0.f;
		real x[4],y[4];
		w=(real)canvas.m_nWidth/(real)pat.m_pTexture->m_nWidth;
		h=(real)canvas.m_nHeight/(real)pat.m_pTexture->m_nHeight;
		x[0]=0.f;
		y[0]=0.f;
		x[1]=0.f;
		y[1]=h;
		x[2]=w;
		y[2]=h;
		x[3]=w;
		y[3]=0.f;
		for(int i=0;i<4;i++){
			ver[i].m_rU=pat.m_matrix.GetElement(0,0)*x[i]+pat.m_matrix.GetElement(0,1)*y[i]+pat.m_matrix.GetElement(0,3)/(real)pat.m_pTexture->m_nWidth;
			ver[i].m_rV=pat.m_matrix.GetElement(1,0)*x[i]+pat.m_matrix.GetElement(1,1)*y[i]+pat.m_matrix.GetElement(1,3)/(real)pat.m_pTexture->m_nHeight;
			ver[i].m_color=pat.m_color;
		}
		CPolygon poly;
		poly.m_dwFlags=DP_SHADE_FLAT;
		poly.m_nVertices=4;
		poly.m_pTexture=pat.m_pTexture;
		poly.m_pVertices=ver;
		DrawPolygon(canvas,poly);
	}
}

//CCanvasクラス
CCanvas::CCanvas()
{
	m_lpSurface=NULL;
	m_lpSurfaceForLock=NULL;
	m_pBuf=NULL;
}


CCanvas::~CCanvas(){
	this->Release();
}

void CCanvas::Release(){
#ifdef _NDEBUG
	RELEASE(m_lpSurface);
#else
	if(m_lpSurface){
//		SetError(0,"release canvas");
		m_lpSurface->Release();
		m_lpSurface=NULL;
//		SetError(0,"released canvas");
	}
#endif
}

//キャンバスのロック
BOOL CCanvas::Lock()
{
	assert(m_lpSurfaceForLock==NULL);
	if(m_lpSurface==NULL){
		return FALSE;
	}
	DDSURFACEDESC ddsd;
	ZeroMemory(&ddsd,sizeof ddsd);
	ddsd.dwSize=sizeof ddsd;
	HRESULT err;
	err=m_lpSurface->Lock(NULL,&ddsd,DDLOCK_SURFACEMEMORYPTR|DDLOCK_WAIT,NULL);
	if(err!=DD_OK){
		SetError(DP_ERR_CAN_NOT_LOCK
			,"Can't lock surface in CCanvas::Lock().");
		return FALSE;
	}
	m_nBpp=ddsd.ddpfPixelFormat.dwRGBBitCount;
	m_nHeight=ddsd.dwHeight;
	m_nPitch=ddsd.lPitch;
	m_nWidth=ddsd.dwWidth;
	m_pBuf=(BYTE*)ddsd.lpSurface;
	if(ddsd.ddpfPixelFormat.dwGBitMask==0x7E0){
		m_pixelFormat=DP_PF_RGB565;
	} else if(ddsd.ddpfPixelFormat.dwGBitMask==0x3E0){
		m_pixelFormat=DP_PF_RGB555;
	} else {
		SetError(DP_ERR_NO_SUPPORT_SURFACE_PIXEL_FORMAT
			,"No support pixel format in CCanvas::Lock().");
		return FALSE;
	}
	m_lpSurfaceForLock=(LPDIRECTDRAWSURFACE)ddsd.lpSurface;
	return TRUE;
}

//キャンバスのアンロック
BOOL CCanvas::Unlock()
{
	assert(m_lpSurfaceForLock);
	if(m_lpSurface==NULL){
		return FALSE;
	}
	HRESULT err=m_lpSurface->Unlock(m_lpSurfaceForLock);
	m_lpSurfaceForLock=NULL;
	if(err!=DD_OK){
		SetError(DP_ERR_CAN_NOT_UNLOCK
			,"Can't unlock surface in CCanvas::Unlock().");
		return FALSE;
	}
	m_pBuf=NULL;
	return TRUE;
}

BOOL CCanvas::GetDC(HDC* pHdc)
{
	if(m_lpSurface==NULL){
		return FALSE;
	}
	HRESULT err=m_lpSurface->GetDC(pHdc);
	if(err!=DD_OK){
		SetError(DP_ERR_CAN_NOT_GET_DC
			,"Can't get dc from surface in CCanvas::GetDC().");
		return FALSE;
	}
	return TRUE;
}

BOOL CCanvas::ReleaseDC(HDC hdc)
{
	if(m_lpSurface==NULL){
		return FALSE;
	}
	HRESULT err=m_lpSurface->ReleaseDC(hdc);
	if(err!=DD_OK){
		SetError(DP_ERR_CAN_NOT_RELEACE_DC
			,"Can't release dc from surface in CCanvas::ReleaseDC().");
		return FALSE;
	}
	return TRUE;
}

void CCanvas::SetMember()
{
	HRESULT err;
	DDSURFACEDESC ddsd;
	ZeroMemory(&ddsd,sizeof ddsd);
	ddsd.dwSize=sizeof ddsd;
	err=m_lpSurface->Lock(NULL,&ddsd,DDLOCK_SURFACEMEMORYPTR|DDLOCK_WAIT,NULL);
	if(err==DD_OK){
		m_nBpp=ddsd.ddpfPixelFormat.dwRGBBitCount;
		m_nHeight=ddsd.dwHeight;
		m_nPitch=ddsd.lPitch;
		m_nWidth=ddsd.dwWidth;
		m_pBuf=NULL;
		if(ddsd.ddpfPixelFormat.dwGBitMask==0x7E0){
			m_pixelFormat=DP_PF_RGB565;
		} else if(ddsd.ddpfPixelFormat.dwGBitMask==0x3E0){
			m_pixelFormat=DP_PF_RGB555;
		} else {
			assert(FALSE);
		}
		m_lpSurface->Unlock(ddsd.lpSurface);
	}
}

//CPrimaryCanvas
//static data members
int CPrimaryCanvas::m_nDisplayMode=0;
CDisplayMode* CPrimaryCanvas::m_pDisplayMode=NULL;

//functions
CPrimaryCanvas::CPrimaryCanvas()
{
	m_nDisplayMode=0;
	m_pBackCanvas=NULL;
	m_pClipper=NULL;
	m_pDisplayMode=NULL;
	m_screenType=DP_ST_WINDOW;
}
CPrimaryCanvas::~CPrimaryCanvas()
{
	this->Release();
}


void CPrimaryCanvas::Release()
{
	if(m_pBackCanvas){
		delete m_pBackCanvas;
		m_pBackCanvas=NULL;
//		m_pBackCanvas->Release();
	}
	if(m_pDisplayMode){
		delete [] m_pDisplayMode;
		m_pDisplayMode=NULL;
		m_nDisplayMode=0;
	}
#ifdef _NDEBUG
	RELEASE(m_lpSurface);
#else
	if(m_lpSurface){
//		SetError(0,"release primary");
		m_lpSurface->Release();
		m_lpSurface=NULL;
//		SetError(0,"released primary");
	}
#endif
#ifdef _NDEBUG
	RELEASE(m_pClipper);
#else
	if(m_pClipper){
//		SetError(0,"release clipper");
		m_pClipper->Release();
		m_pClipper=NULL;
//		SetError(0,"released clipper");
	}
#endif
}

//ディスプレイモードの変更
BOOL CPrimaryCanvas::SetDisplayMode(CDisplayMode const& dm,EScreenType type,DWORD flags/*=0*/)
{
	return SetDisplayMode(dm.m_nWidth,dm.m_nHeight,dm.m_nBpp,type,flags);
}

//ディスプレイモードの変更
BOOL CPrimaryCanvas::SetDisplayMode(int width,int height,int bpp,EScreenType type,DWORD flags/*=0*/)
{
	HRESULT err;
	if(bpp!=16){//16bpp以外は未対応
		SetError(DP_ERR_NO_SUPPORT_DISPLAY_PIXEL_FORMAT
					,"Display mode must be 16bit/pixel at window mode in CPrimaryCanvas::SetDisplayMode().");
		return FALSE;
	}
	this->Release();//一旦、破壊。
	ReleaseDD();
	//再構築
	LPDIRECTDRAW pDD=GetDD();//DirectDrawの作成
	if(pDD==NULL) return FALSE;
	if(type==DP_ST_FULLSCREEN || type==DP_ST_FULLSCREEN_CLIP){//フルスクリーンのとき
		err=pDD->SetCooperativeLevel(GetMainHWND()
			,DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWMODEX 
			| DDSCL_ALLOWREBOOT);
		if(err!=DD_OK){//失敗したら、ALOOWMODEXをはずして再挑戦
			err=pDD->SetCooperativeLevel(GetMainHWND()
			,DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWREBOOT);
			if(err!=DD_OK){
				SetError(DP_ERR_CAN_NOT_SET_COOPERATIVE_LEVEL
					,"Can't set cooperative level to fullscreen in SetDisplayMode().");
				return FALSE;
			}
		}
		err=pDD->SetDisplayMode(width,height,bpp);
		if(err==DD_OK){//フルスクリーン成功
			//プライマリサーフェースとバックサーフェースの作成
			DDSURFACEDESC ddsd;
			ZeroMemory(&ddsd, sizeof(ddsd));
			ddsd.dwSize = sizeof(ddsd);
			ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
			ddsd.dwBackBufferCount = 1;
			ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE |
								DDSCAPS_FLIP |
								DDSCAPS_COMPLEX |
//								DDSCAPS_VIDEOMEMORY;
								DDSCAPS_SYSTEMMEMORY;
			err=pDD->CreateSurface(&ddsd,&m_lpSurface,NULL);
			if(err!=DD_OK){
				SetError(DP_ERR_CAN_NOT_CREATE_SURFACE
					,"Can't create primary and back surface in SetDisplayMode().");
				return FALSE;
			}
			//バックサーフェースの取得
			assert(m_pBackCanvas==NULL);
			m_pBackCanvas=new CBackCanvas(this);
			if (!m_pBackCanvas->Attach(*this)){
				delete m_pBackCanvas;
				m_pBackCanvas=NULL;
				return FALSE;
			}
		} else {//擬似フルスクリーンモード
			int n;
			CDisplayMode* pDM;
			EnumDisplayModes(&n,&pDM);
			if(n>0){
				err=pDD->SetDisplayMode(pDM[0].m_nWidth,pDM[0].m_nHeight,pDM[0].m_nBpp);
			} else {
				err=pDD->SetDisplayMode(640,480,16);
			}
			if(err!=DD_OK){
				SetError(DP_ERR_CAN_NOT_CHANGE_DISPLAY_MODE
					,"Can't change display mode to %dx%dx%d at fullscreen in SetDisplayMode().",width,height,bpp);
				return FALSE;
			}
			//プライマリサーフェスを生成
			DDSURFACEDESC ddsd;
			ZeroMemory( &ddsd, sizeof( ddsd ) );
			ddsd.dwSize = sizeof( ddsd );
			ddsd.dwFlags = DDSD_CAPS;
			ddsd.ddsCaps.dwCaps =	DDSCAPS_PRIMARYSURFACE ;
			err=pDD->CreateSurface( &ddsd, &m_lpSurface, NULL );
			if (err != DD_OK){
				SetError(DP_ERR_CAN_NOT_CREATE_SURFACE
					,"Can't create primary surface at window mode in SetDisplayMode().");
				return FALSE;
			}
			assert(m_pBackCanvas==NULL);
			m_pBackCanvas=new CBackCanvas(this,GetMainHWND());
			if(!m_pBackCanvas->Create(width,height,bpp)){
				delete m_pBackCanvas;
				m_pBackCanvas=NULL;
				return FALSE;
			}
		}
	} else {//ウインドウモードのとき
		//ディスプレイのBPPを調べる
		HDC hdc = ::GetDC(GetMainHWND());
		int bpp = GetDeviceCaps(hdc,BITSPIXEL);
		::ReleaseDC(GetMainHWND(),hdc);
		if(bpp!=16){//16bpp以外は未対応
			SetError(DP_ERR_NO_SUPPORT_DISPLAY_PIXEL_FORMAT
				,"Display mode must be 16bit/pixel at window mode in SetDisplayMode().");
			return FALSE;
		}
		err=pDD->SetCooperativeLevel(GetMainHWND(),DDSCL_NORMAL);
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_SET_COOPERATIVE_LEVEL
				,"Can't set cooperative level to window mode in SetDisplayMode().");
			return FALSE;
		}
		//プライマリサーフェスを生成
		DDSURFACEDESC ddsd;
		ZeroMemory( &ddsd, sizeof( ddsd ) );
		ddsd.dwSize = sizeof( ddsd );
		ddsd.dwFlags = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps =	DDSCAPS_PRIMARYSURFACE ;
		err=pDD->CreateSurface( &ddsd, &m_lpSurface, NULL );
		if (err != DD_OK){
			SetError(DP_ERR_CAN_NOT_CREATE_SURFACE
				,"Can't create primary surface at window mode in SetDisplayMode().");
			return FALSE;
		}
		//バックサーフェースの作成
		assert(m_pBackCanvas==NULL);
		m_pBackCanvas=new CBackCanvas(this,GetMainHWND());
		if(!m_pBackCanvas->Create(width,height,bpp)){
			delete m_pBackCanvas;
			m_pBackCanvas=NULL;
			return FALSE;
		}
	}
	//クリッパーの作成
	if(type==DP_ST_WINDOW || type==DP_ST_FULLSCREEN_CLIP){
		err=pDD->CreateClipper(0,&m_pClipper,NULL);
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_CREATE_CLIPPER
				,"Can't create clipper in SetDisplayMode().");
			return FALSE;
		}
		err=m_pClipper->SetHWnd(0,GetMainHWND());
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_CREATE_CLIPPER
				,"Can't create clipper in SetDisplayMode().");
			return FALSE;
		}
	    err=m_lpSurface->SetClipper(m_pClipper);
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_CREATE_CLIPPER
				,"Can't create clipper in SetDisplayMode().");
			return FALSE;
		}
	}
	m_screenType=type;
	if(type==DP_ST_WINDOW){
		RECT cr,wr;
		GetClientRect(GetMainHWND(),&cr);
		GetWindowRect(GetMainHWND(),&wr);
		int w=width+(wr.right-wr.left)-(cr.right-cr.left);
		int h=height+(wr.bottom-wr.top)-(cr.bottom-cr.top);
		int x=(GetSystemMetrics(SM_CXFULLSCREEN)-w)/2;
		int y=(GetSystemMetrics(SM_CYFULLSCREEN)-h)/2;
		SetWindowPos(GetMainHWND(),0,x,y,w,h,SWP_NOZORDER);
	}
	SetMember();
	if(m_pBackCanvas){
		m_pBackCanvas->SetMember();
	}
	return TRUE;
}

BOOL CPrimaryCanvas::Flip()
{
	if(m_pBackCanvas){
		if(!m_pBackCanvas->Flip()){
			return FALSE;
		}
		return TRUE;
	}
	SetError(DP_ERR_NO_ATTACHED_CANVAS
		,"Primary canvas don't attached back canvas in CPrimaryCanvas::Flip().");
	return FALSE;
}


//ディスプレイモードの列挙
void CPrimaryCanvas::EnumDisplayModes(int* num,CDisplayMode** ppDM)
{
	if(m_pDisplayMode!=NULL && m_nDisplayMode!=0){
		if(num!=NULL) *num=m_nDisplayMode;
		if(ppDM!=NULL) *ppDM=m_pDisplayMode;
		return;
	}
	LPDIRECTDRAW pDD=GetDD();
	m_nDisplayMode=0;
	pDD->EnumDisplayModes(0,NULL,NULL/*カウントモード*/
		,EnumDisplayModesCallback);
	if(m_pDisplayMode){
		delete [] m_pDisplayMode;
	}
	m_pDisplayMode=new CDisplayMode[m_nDisplayMode];
	m_nDisplayMode=0;
	pDD->EnumDisplayModes(0,NULL,(LPVOID)m_pDisplayMode,EnumDisplayModesCallback);
	if(num!=NULL) *ppDM=m_pDisplayMode;
	if(ppDM!=NULL) *num=m_nDisplayMode;
	qsort(m_pDisplayMode,m_nDisplayMode,sizeof(CDisplayMode)
		,EnumDisplayModesSort);
}

//ディスプレイモードの列挙のコールバック
HRESULT CALLBACK CPrimaryCanvas::EnumDisplayModesCallback(LPDDSURFACEDESC pdds, LPVOID lParam)
{
	if(pdds->ddpfPixelFormat.dwRGBBitCount!=16) return S_FALSE;
	if(lParam==NULL){//カウントモード
		m_nDisplayMode++;
		return S_FALSE;
	} else {
		CDisplayMode* pDM=(CDisplayMode*)lParam;
		pDM[m_nDisplayMode].m_nWidth=pdds->dwWidth;
		pDM[m_nDisplayMode].m_nHeight=pdds->dwHeight;
		pDM[m_nDisplayMode].m_nBpp=pdds->ddpfPixelFormat.dwRGBBitCount;
		m_nDisplayMode++;
		return S_FALSE;//S_FALSEは、列挙を続ける
	}
}

//ディスプレイモードの列挙のソート関数
int CPrimaryCanvas::EnumDisplayModesSort(const void* A, const void* B)
{
	CDisplayMode* a=(CDisplayMode*)A;
	CDisplayMode* b=(CDisplayMode*)B;
	return (a->m_nWidth*a->m_nHeight*(a->m_nBpp/8))
		-(b->m_nWidth*b->m_nHeight*(b->m_nBpp/8));
}

//ウインドウフレームの再描画
BOOL CPrimaryCanvas::RedrawWindowFrame()
{
	LPDIRECTDRAW pDD=GetDD();
	if(pDD==NULL || m_lpSurface==NULL){
		return FALSE;
	}
	HRESULT err;
	DDSCAPS caps;
	err=m_lpSurface->GetCaps(&caps);
	if(err==DD_OK && caps.dwCaps & DDSCAPS_MODEX){
		SetDisplayMode(640,480,16,DP_ST_FULLSCREEN);
	}
	pDD->FlipToGDISurface();
	DrawMenuBar(GetMainHWND());
	RedrawWindow(GetMainHWND(),NULL,NULL,RDW_FRAME);
	return TRUE;
}

CBackCanvas* CPrimaryCanvas::GetBackCanvas()
{
	if(m_pBackCanvas){
		return m_pBackCanvas;
	} else {
		SetError(DP_ERR_NO_ATTACHED_CANVAS
			,"Primary canvas don't attached back canvas in CPrimaryCanvas::GetBackCanvas.");
		return NULL;
	}
}

//CBackCanvas
CBackCanvas::CBackCanvas(CPrimaryCanvas* pCanvas,HWND hwnd/*=NULL*/)
{
	m_hwnd=hwnd;
	m_pPrimary=pCanvas;
	m_bFlippable=FALSE;
}

CBackCanvas::~CBackCanvas()
{
}

//Flip
BOOL CBackCanvas::Flip()
{
	if(m_pPrimary==NULL || m_pPrimary->m_lpSurface==NULL || m_lpSurface==NULL){
		return FALSE;
	}
	if (m_pPrimary->m_lpSurface->IsLost()==DDERR_SURFACELOST)
		m_pPrimary->m_lpSurface->Restore();//サーフェースの再構築
	HRESULT err;
	if(m_bFlippable){//Flip
		err=m_pPrimary->m_lpSurface->Flip(NULL, DDFLIP_WAIT);
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_FLIP
				,"Can't flip at fullscreen mode in CBackCanvas::Flip().");
			return FALSE;
		}
	} else {//Blit
		RECT src={0,0,m_nWidth,m_nHeight};
		RECT dest;
		if(m_hwnd==NULL){//Flip先のhwndが指定されていないとき
			//プライマリキャンバスへBlit
			EScreenType st=m_pPrimary->GetScreenType();
			if(st==DP_ST_WINDOW){
				HWND hwnd=GetMainHWND();
				GetClientRect(hwnd,&dest);
				POINT pn={0,0};
				ClientToScreen(hwnd,&pn);
				dest.left=pn.x+1;
				dest.top=pn.y+1;
				pn.x=dest.right;
				pn.y=dest.bottom;
				ClientToScreen(hwnd,&pn);
				dest.right=pn.x+1;
				dest.bottom=pn.y+1;
			} else if(st==DP_ST_FULLSCREEN || st==DP_ST_FULLSCREEN_CLIP){
				dest.left=dest.top=0;
				dest.right=m_pPrimary->m_nWidth;
				dest.bottom=m_pPrimary->m_nHeight;
			} else {
				assert(FALSE);
			}
		} else {//Flip先のhwndが指定されているとき
			//ウインドウのクライアント領域へBlit
			GetClientRect(m_hwnd,&dest);
			POINT pn={0,0};
			ClientToScreen(m_hwnd,&pn);
			dest.left=pn.x+1;
			dest.top=pn.y+1;
			pn.x=dest.right;
			pn.y=dest.bottom;
			ClientToScreen(m_hwnd,&pn);
			dest.right=pn.x+1;
			dest.bottom=pn.y+1;
		}
		err=m_pPrimary->m_lpSurface->Blt(
			&dest,m_lpSurface,&src,DDBLT_WAIT,NULL);
		if(err!=DD_OK){
			SetError(DP_ERR_CAN_NOT_FLIP
				,"Can't flip at window mode in CBackCanvas::Flip().");
			return FALSE;
		}
	}
	return TRUE; 
}

BOOL CBackCanvas::Attach(CPrimaryCanvas& primary)
{
	HRESULT err;
	DDSCAPS caps;
	caps.dwCaps=DDSCAPS_BACKBUFFER;
	err=primary.m_lpSurface->GetAttachedSurface(&caps,&m_lpSurface);
	if (err != DD_OK){
		SetError(DP_ERR_CAN_NOT_ATTACH_SURFACE
			,"Can't attach back suface to primary surface at fullscreen mode in CBackCanvas::Attach().");
		return FALSE;
	}
	m_bFlippable=TRUE;
	SetMember();
	m_pPrimary=&primary;
	return TRUE;
}

BOOL CBackCanvas::Create(int width,int height,int bpp,DWORD flags)
{
	HRESULT err;
	DDSURFACEDESC ddsd;
	ZeroMemory(&ddsd,sizeof ddsd);
	//バックサーフェースの作成
	ddsd.dwSize=sizeof ddsd;
	ddsd.dwFlags = DDSD_WIDTH | DDSD_HEIGHT | DDSD_CAPS;
	ddsd.dwWidth = width;
	ddsd.dwHeight = height;
	ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
	ddsd.ddsCaps.dwCaps |= DDSCAPS_SYSTEMMEMORY;
	err=GetDD()->CreateSurface( &ddsd, &m_lpSurface, NULL );
	if (err != DD_OK){
		SetError(DP_ERR_CAN_NOT_CREATE_SURFACE
			,"Can't create back surface at window mode in SetDisplayMode().");
		return FALSE;
	}
	m_bFlippable=FALSE;
	SetMember();
	return TRUE;
}

void CBackCanvas::SetMember()
{
	CCanvas::SetMember();
	//ピクセルフォーマットの取得
	//なぜか、IDirectDrawSurface::GetPixelFormat()では取得できなずDDERR_INVALIDPARAMSが帰る
	if(m_pixelFormat==DP_PF_RGB565){
		ConvertTextureColorTable(DP_PF_RGB565);
	} else if(m_pixelFormat==DP_PF_RGB555){
		ConvertTextureColorTable(DP_PF_RGB555);
	} else {
//		assert(0);
	}
}

//CTexture
BOOL CTexture::Load(LPCTSTR filename,COLORREF transparent)
{
	if(m_pBuffer!=NULL){
		delete [] m_pBuffer;
		m_pBuffer=NULL;
	}
	FILE* fp=fopen(filename,"rb");
	if(fp==NULL){
		SetError(DP_ERR_FILE_NOT_FOUND
			,"Can't open %s file in CTexture::Load().",filename);
		return FALSE;
	}
	long len=filelength(fileno(fp))-14;
	char fhead[14];
	fread(fhead,14,1,fp);
	if(!(fhead[0]=='B' && fhead[1]=='M')){
		fclose(fp);
		SetError(DP_ERR_FILE_NOT_SUPPORT
			,"Can't load %s file does not support format in CTexture::Load().",filename);
		return FALSE;
	}
	BITMAPINFOHEADER* pBif=(BITMAPINFOHEADER*)new char[len];
	fread(pBif,len,1,fp);
	fclose(fp);
/*	fprintf(stderr,"%s\n",argv[1]);
	fprintf(stderr,"  Width         %7d\n",pBif->biWidth);
	fprintf(stderr,"  Height        %7d\n",pBif->biHeight);
	fprintf(stderr,"  Planes        %7u\n",pBif->biPlanes);
	fprintf(stderr,"  BitCount      %7u\n",pBif->biBitCount);
	fprintf(stderr,"  Compression   %7u\n",pBif->biCompression);
	fprintf(stderr,"  ImageSize     %7u\n",pBif->biSizeImage);
	fprintf(stderr,"  XPerMeter     %7d\n",pBif->biXPelsPerMeter);
	fprintf(stderr,"  YPerMeter     %7d\n",pBif->biYPelsPerMeter);
	fprintf(stderr,"  ColorUsed     %7u\n",pBif->biClrUsed);
	fprintf(stderr,"  ColorImportant%7u\n",pBif->biClrImportant);*/
	if(pBif->biBitCount!=8){//8bppのみ
		delete pBif;
		SetError(DP_ERR_NO_SUPPORT_FORMAT
			,"Can't load %s file isn't 8bit/pixel in CTexture::Load().",filename);
		return FALSE;
	}
	int i,j;
	for(i=0,j=0;i<32;i++){//Widthは2のべき乗のみ
		if(pBif->biWidth&(1<<i)) j++;
	}
	if(j!=1){
		delete pBif;
		SetError(DP_ERR_NO_SUPPORT_FORMAT
			,"Can't load %s file image width isn't 2^n in CTexture::Load().",filename);
		return FALSE;
	}
	for(i=0,j=0;i<32;i++){//Widthは2のべき乗のみ
		if(pBif->biHeight&(1<<i)) j++;
	}
	if(j!=1){
		delete pBif;
		SetError(DP_ERR_NO_SUPPORT_FORMAT
			,"Can't load %s file image height isn't 2^n in CTexture::Load().",filename);
		return FALSE;
	}

	m_pBuffer=new BYTE [pBif->biWidth*pBif->biHeight*4];
	m_dwFlags=0;
	m_nWidth=pBif->biWidth;
	m_nHeight=pBif->biHeight;
	RGBQUAD* pal=(RGBQUAD*)((char*)pBif+pBif->biSize);
	if(pBif->biClrUsed==0) pBif->biClrUsed=256;
	BYTE* buf=(BYTE*)(pBif)+pBif->biSize+sizeof(RGBQUAD)*pBif->biClrUsed;
	BYTE* pOut=m_pBuffer;
	int orient=-1,y=pBif->biHeight-1;
	if(pBif->biHeight<0){
		orient=1;
		y=0;
		pBif->biHeight=-pBif->biHeight;
	}
	if(transparent==DP_TEX_LEFTTOP){//左上を透明色に指定した場合
		i=buf[y*DibWidthBytes(pBif)];
		transparent=RGB(pal[i].rgbRed,pal[i].rgbGreen,pal[i].rgbBlue);
	}
	for(i=0;i<pBif->biHeight;i++){
		for(j=0;j<pBif->biWidth;j++){
			if((int)(j%DibWidthBytes(pBif))<(int)pBif->biWidth){
				BYTE idx=buf[y*DibWidthBytes(pBif)+j];
				*(pOut++)=pal[idx].rgbRed;
				*(pOut++)=pal[idx].rgbGreen;
				*(pOut++)=pal[idx].rgbBlue;
				if(transparent!=DP_TEX_NOCOLOR
					&& pal[idx].rgbRed==GetRValue(transparent)
					&& pal[idx].rgbGreen==GetGValue(transparent)
					&& pal[idx].rgbBlue==GetBValue(transparent)){//透明色
					*(pOut++)=0;
				} else {//不透明色
					*(pOut++)=255;
				}
			}
		}
		y+=orient;
	}
	delete pBif;
	//umask,vmaskの設定
	for(m_dwUMask=m_nWidth,i=0;(m_dwUMask&1)==0;m_dwUMask>>=1,i++);
	for(m_dwUMask=0;i>0;i--,m_dwUMask|=1,m_dwUMask<<=1);
	m_dwUMask>>=1;
	for(m_dwVMask=m_nHeight,i=0;(m_dwVMask&1)==0;m_dwVMask>>=1,i++);
	for(m_dwVMask=0;i>0;i--,m_dwVMask|=1,m_dwVMask<<=1);
	m_dwVMask>>=1;
	MakeIndexBuffer();
	//名前
	if(m_szName){
		delete [] m_szName;
	}
	m_szName=new char[strlen(filename)+1];
	strcpy(m_szName,filename);
	return TRUE;
}

//アフファプレーンをビットマップからロード（R成分をアルファとする)
BOOL CTexture::LoadAlpha(LPCTSTR filename)
{
	if(m_pBuffer==NULL){//先に作成されていなければならない
		SetError(0,"Must create before CTexture::LoadAlpha().");
		return FALSE;
	}
	FILE* fp=fopen(filename,"rb");
	if(fp==NULL){
		SetError(DP_ERR_FILE_NOT_FOUND
			,"Can't open %s file in CTexture::LoadAlpha().",filename);
		return FALSE;
	}
	long len=filelength(fileno(fp))-14;
	char fhead[14];
	fread(fhead,14,1,fp);
	if(!(fhead[0]=='B' && fhead[1]=='M')){
		fclose(fp);
		SetError(DP_ERR_FILE_NOT_SUPPORT
			,"Can't load %s file does not support format in CTexture::Load().",filename);
		return FALSE;
	}
	BITMAPINFOHEADER* pBif=(BITMAPINFOHEADER*)new char[len];
	fread(pBif,len,1,fp);
	fclose(fp);
	if(pBif->biBitCount!=8){//8bppのみ
		delete pBif;
		SetError(DP_ERR_NO_SUPPORT_FORMAT
			,"Can't load %s file isn't 8bit/pixel in CTexture::LoadAlpha().",filename);
		return FALSE;
	}
	if(pBif->biWidth!=m_nWidth || pBif->biHeight!=m_nHeight){//テクスチャとアルファのサイズが違うとき
		SetError(0,"Size is different.COLOR(%d,%d)!=ALPHA(%d,%d) in CTexture::LoadAlpha()"
			,m_nWidth,m_nHeight,pBif->biWidth,pBif->biHeight);
		delete pBif;
		return FALSE;
	}
	int i,j;
	m_pBuffer=new BYTE [pBif->biWidth*pBif->biHeight*4];
	m_dwFlags=0;
	m_nWidth=pBif->biWidth;
	m_nHeight=pBif->biHeight;
	RGBQUAD* pal=(RGBQUAD*)((char*)pBif+pBif->biSize);
	if(pBif->biClrUsed==0) pBif->biClrUsed=256;
	BYTE* buf=(BYTE*)(pBif)+pBif->biSize+sizeof(RGBQUAD)*pBif->biClrUsed;
	BYTE* pOut=m_pBuffer;
	int orient=-1,y=pBif->biHeight-1;
	if(pBif->biHeight<0){
		orient=1;
		y=0;
		pBif->biHeight=-pBif->biHeight;
	}
	for(i=0;i<pBif->biHeight;i++){
		for(j=0;j<pBif->biWidth;j++){
			if((int)(j%DibWidthBytes(pBif))<(int)pBif->biWidth){
				BYTE idx=buf[y*DibWidthBytes(pBif)+j];
				pOut+=3;//skip RGB
				*(pOut++)=pal[idx].rgbRed;
			}
		}
		y+=orient;
	}
	delete pBif;
	return TRUE;
}

//インデックスバッファの作成
void CTexture::MakeIndexBuffer()
{
	if(m_pBuffer==NULL) return;
	if(m_pIndexBuffer==NULL){
		m_pIndexBuffer=new WORD[m_nWidth*m_nHeight];
	}
	BYTE* pBuf=m_pBuffer;
	WORD* pIndex=m_pIndexBuffer;
	for(int j=0;j<m_nHeight;j++){
		for(int i=0;i<m_nWidth;i++){
			CColorRef color(*(pBuf+0),*(pBuf+1),*(pBuf+2),*(pBuf+3));
			*(pIndex++)=GetTextureColorIndex(color);
			pBuf+=4;
		}
	}
}

//DrawPolygonLow
//Tex=TOff Col=CGrayFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD col;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	col=RGB8((int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.r*255.f));
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=col;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD col;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	col=RGB8((int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.r*255.f));
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=col;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD col;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	col=RGB8((int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.g*255.f)
	,(int)(ver[0].m_color.b*255.f));
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=col;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD col;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	col=RGB8((int)(ver[0].m_color.r*255.f)
	,(int)(ver[0].m_color.g*255.f)
	,(int)(ver[0].m_color.b*255.f));
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=col;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	WORD back;
	int cR,cG,cB;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				count=(rx>>16)-(lx>>16);
				if(count){
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		count=rx-lx;
		if(count){
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=GRAY16(cR);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=GRAY16(cR);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=GRAYA16(back,cR,cA,na);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=GRAYA16(back,cR,cA,na);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=GRAYA16(back,cR,cA,na);
					cR+=cRd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=GRAYA16(back,cR,cA,na);
					cR+=cRd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cR*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cR*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cR*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cR*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cR*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cR*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cR*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cR*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=RGB16(cR,cG,cB);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=RGB16(cR,cG,cB);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,cR,cG,cB,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	WORD back;
	int cA;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon0340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOff Col=CColGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon0341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	WORD back;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					int r,g,b;
					back=*pBuf;
					r=((DWORD)(cR*cA)>>16)+GetRValue16(back);
					if(r>65535) r=65535;
					g=((DWORD)(cG*cA)>>16)+GetGValue16(back);
					if(g>65535) g=65535;
					b=((DWORD)(cB*cA)>>16)+GetBValue16(back);
					if(b>65535) b=65535;
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					*pBuf++=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					*pBuf++=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					*pBuf++=RGB16(r,g,b);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					*pBuf++=RGB16(r,g,b);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					*pBuf++=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					*pBuf++=RGB16(r,g,b);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					na=65535-cA;
					*pBuf++=RGBA16(back,r,g,b,cA,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon1340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAOff Col=CColGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon1341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				WORD i;
				for(;count>=0;count--){
					i=pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)];
					if(i!=DPTCI_TRANSPARENT){
						*pBuf++=pTable[i];
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				WORD i;
				for(;count>=0;count--){
					i=pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)];
					if(i!=DPTCI_TRANSPARENT){
						*pBuf++=pTable[i];
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						*pBuf++=RGB16(r,g,b);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						*pBuf++=RGB16(r,g,b);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD i;
				for(;count>=0;count--){
					i=pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)];
					if(i!=DPTCI_TRANSPARENT){
						*pBuf++=g_ppTexShade[cR>>8][i];
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD i;
				for(;count>=0;count--){
					i=pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)];
					if(i!=DPTCI_TRANSPARENT){
						*pBuf++=g_ppTexShade[cR>>8][i];
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						*pBuf++=RGB16(r,g,b);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						*pBuf++=RGB16(r,g,b);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						na=65535-cA;
						*pBuf++=RGBA16(back,r,g,b,cA,na);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon2340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnADigital Col=CColGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon2341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=*pTexBuf++;
					if(a!=0){
						back=*pBuf;
						int tR,tG,tB;
						tR=((DWORD)(r*cA)>>16)+GetRValue16(back);
						if(tR>65535) tR=65535;
						tG=((DWORD)(g*cA)>>16)+GetGValue16(back);
						if(tG>65535) tG=65535;
						tB=((DWORD)(b*cA)>>16)+GetBValue16(back);
						if(tB>65535) tB=65535;
						*pBuf++=RGB16(tR,tG,tB);
					} else {
						pBuf++;
					}
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3000(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3001(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3010(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3011(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3020(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3021(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3030(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3031(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3040(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3041(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.r*65535.f);
	cB=(int)(ver[0].m_color.r*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				WORD* pTable=g_ppTexShade[cR>>8];
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=pTable[pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3100(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3101(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3110(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3111(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3120(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3121(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3130(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3131(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3140(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColFlat Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3141(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cR,cG,cB;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cR=(int)(ver[0].m_color.r*65535.f);
	cG=(int)(ver[0].m_color.g*65535.f);
	cB=(int)(ver[0].m_color.b*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3200(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3201(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3210(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3211(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3220(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3221(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3230(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3231(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3240(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CGrayGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3241(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					WORD i=g_ppTexShade[cR>>8][pTex->m_pIndexBuffer[((cV>>16)&vmask)
						*pTex->m_nWidth+((cU>>16)&umask)]];
					r=GetRValue16(i);
					g=GetGValue16(i);
					b=GetBValue16(i);
					pTexBuf+=3;
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;rR+=rRd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AOff Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3300(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AOff Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3301(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					back=*pBuf;
					a=(int)(*pTexBuf++)<<8;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AFlat Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3310(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AFlat Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3311(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AVariable Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3320(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AVariable Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3321(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					na=65535-a;
					*pBuf++=RGBA16(back,r,g,b,a,na);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AFlatAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3330(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AFlatAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3331(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	int cA;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	cA=(int)(ver[0].m_color.a*65535.f);
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AVariableAdd Bpp=B16_565 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_565
#define RGB16 RGB16_565
#define GRAY16(g) RGB16((g),(g),(g))
#define RGBA16 RGBA16_565
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_565(n)
#define GetGValue16(n) GetGValue16_565(n)
#define GetBValue16(n) GetBValue16_565(n)
#define PIXELFORMATMACRO
static void DrawPolygon3340(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
//Tex=TOnAVariable Col=CColGouraud Alpha=AVariableAdd Bpp=B16_555 
#ifdef PIXELFORMATMACRO
#undef RGB8
#undef RGB16
#undef GRAY16
#undef RGBA16
#undef GRAYA16
#undef GetRValue16
#undef GetGValue16
#undef GetBValue16
#undef PIXELFORMATMACRO
#endif
#define RGB8 RGB8_555
#define RGB16 RGB16_555
#define GRAY16(g) RGB16_555((g),(g),(g))
#define RGBA16 RGBA16_555
#define GRAYA16(back,g,a,na) RGBA16(back,g,g,g,a,na)
#define GetRValue16(n) GetRValue16_555(n)
#define GetGValue16(n) GetGValue16_555(n)
#define GetBValue16(n) GetBValue16_555(n)
#define PIXELFORMATMACRO
static void DrawPolygon3341(CCanvas const& canvas,CPolygon const& poly,int cw,int top,int bottom,real rTopY,real rBottomY)
{
	int i,pitch,n,na;
	int y,ny,count;//y/next y
	int li,ri,ni,nli,nri;//index(left/right/next)
	int lx,lxd,rx,rxd;//edge
	int topY,bottomY;
	WORD *pBuf,*pBufBak;
	CTLVertex const* ver;
	int lR,lRd,rR,rRd,cR,cRd;
	int lG,lGd,rG,rGd,cG,cGd;
	int lB,lBd,rB,rBd,cB,cBd;
	int lA,lAd,rA,rAd,cA,cAd;
	int lU,lV,lUd,lVd;//左テクスチャ用
	int rU,rV,rUd,rVd;//右テクスチャ用
	int cU,cV,cUd,cVd;//水平線のテクスチャ用
	DWORD umask,vmask;
	CTexture const* pTex;
	WORD back;
	BYTE *pTexBuf;
	int r,g,b;
	int a;
	n=poly.m_nVertices;
	ver=poly.m_pVertices;
	pitch=canvas.m_nPitch/2;
	topY=(int)rTopY;
	bottomY=(int)rBottomY;
	pTex=poly.m_pTexture;
	umask=pTex->m_dwUMask;vmask=pTex->m_dwVMask;
	if(cw!=0){//普通のとき(水平線以外のとき)
		y=topY;
		lx=rx=(int)(ver[top].m_vctrPos.m_rX*65535.f);
		rR=lR=(int)(ver[top].m_color.r*65535.f);
		rG=lG=(int)(ver[top].m_color.g*65535.f);
		rB=lB=(int)(ver[top].m_color.b*65535.f);
		rA=lA=(int)(ver[top].m_color.a*65535.f);
		rU=lU=(int)(ver[top].m_rU*(real)pTex->m_nWidth*65535.f);
		rV=lV=(int)(ver[top].m_rV*(real)pTex->m_nHeight*65535.f);
		li=ri=top;
		nli=(li+n-cw)%n;
		nri=(ri+n+cw)%n;
		pBuf=pBufBak=(WORD*)canvas.m_pBuf+pitch*topY;
		while((int)ver[nli].m_vctrPos.m_rY==y){//水平をスキップ
			li=nli;
			nli=(li+n-cw)%n;
			lx=(int)(ver[li].m_vctrPos.m_rX*65535.f);
			lR=(int)(ver[li].m_color.r*65535.f);
			lG=(int)(ver[li].m_color.g*65535.f);
			lB=(int)(ver[li].m_color.b*65535.f);
			lA=(int)(ver[li].m_color.a*65535.f);
			lU=(int)(ver[li].m_rU*(real)pTex->m_nWidth*65535.f);
			lV=(int)(ver[li].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		while((int)ver[nri].m_vctrPos.m_rY==y){//水平をスキップ
			ri=nri;
			nri=(ri+n+cw)%n;
			rx=(int)(ver[ri].m_vctrPos.m_rX*65535.f);
			rR=(int)(ver[ri].m_color.r*65535.f);
			rG=(int)(ver[ri].m_color.g*65535.f);
			rB=(int)(ver[ri].m_color.b*65535.f);
			rA=(int)(ver[ri].m_color.a*65535.f);
			rU=(int)(ver[ri].m_rU*(real)pTex->m_nWidth*65535.f);
			rV=(int)(ver[ri].m_rV*(real)pTex->m_nHeight*65535.f);
		}
		do{
			while((int)ver[nli].m_vctrPos.m_rY==y){li=nli;nli=(li+n-cw)%n;}//左右のインデックスを進める
			while((int)ver[nri].m_vctrPos.m_rY==y){ri=nri;nri=(ri+n+cw)%n;}
			if((int)ver[nli].m_vctrPos.m_rY < (int)ver[nri].m_vctrPos.m_rY){//次のインデックスを求める
				ni=nli;nli=(li+n-cw)%n;
			} else {
				ni=nri;nri=(ri+n+cw)%n;
			}
			ny=(int)ver[ni].m_vctrPos.m_rY;
			if((int)ver[li].m_vctrPos.m_rY==y){//左の傾き
				na=(int)ver[nli].m_vctrPos.m_rY-y;
				lxd=((int)(ver[nli].m_vctrPos.m_rX*65535.f)-lx)/na;
				lRd=((int)(ver[nli].m_color.r*65535.f)-lR)/na;
				lGd=((int)(ver[nli].m_color.g*65535.f)-lG)/na;
				lBd=((int)(ver[nli].m_color.b*65535.f)-lB)/na;
				lAd=((int)(ver[nli].m_color.a*65535.f)-lA)/na;
				lUd=((int)(ver[nli].m_rU*(real)pTex->m_nWidth*65535.f)-lU)/na;
				lVd=((int)(ver[nli].m_rV*(real)pTex->m_nHeight*65535.f)-lV)/na;
			}
			if((int)ver[ri].m_vctrPos.m_rY==y){//右の傾き
				na=((int)ver[nri].m_vctrPos.m_rY-y);
				rxd=((int)(ver[nri].m_vctrPos.m_rX*65535.f)-rx)/na;
				rRd=((int)(ver[nri].m_color.r*65535.f)-rR)/na;
				rGd=((int)(ver[nri].m_color.g*65535.f)-rG)/na;
				rBd=((int)(ver[nri].m_color.b*65535.f)-rB)/na;
				rAd=((int)(ver[nri].m_color.a*65535.f)-rA)/na;
				rUd=((int)(ver[nri].m_rU*(real)pTex->m_nWidth*65535.f)-rU)/na;
				rVd=((int)(ver[nri].m_rV*(real)pTex->m_nHeight*65535.f)-rV)/na;
			}
			do{//縦のループ
				pBuf=pBufBak+(lx>>16);
				cR=lR;cG=lG;cB=lB;
				cA=lA;
				cU=lU;cV=lV;
				count=(rx>>16)-(lx>>16);
				if(count){
					cRd=(rR-lR)/count;
					cGd=(rG-lG)/count;
					cBd=(rB-lB)/count;
					cAd=(rA-lA)/count;
					cUd=(rU-lU)/count;
					cVd=(rV-lV)/count;
				}
				//横のループ===================================================
				for(;count>=0;count--){
					pTexBuf=pTex->m_pBuffer+((cV>>16)&vmask)*pTex->m_nWidth*4
						+((cU>>16)&umask)*4;
					if(cR<(128<<8)){
						r=((int)(*pTexBuf++))*cR;r>>=7;
					} else {
						r=*pTexBuf++;
						r=(255-r)*(cR-(32767))+(r<<15);r>>=7;
					}
					if(cG<(128<<8)){
						g=((int)(*pTexBuf++))*cG;g>>=7;
					} else {
						g=*pTexBuf++;
						g=(255-g)*(cG-(32767))+(g<<15);g>>=7;
					}
					if(cB<(128<<8)){
						b=((int)(*pTexBuf++))*cB;b>>=7;
					} else {
						b=*pTexBuf++;
						b=(255-b)*(cB-(32767))+(b<<15);b>>=7;
					}
					a=(((int)(*pTexBuf++))*cA);a>>=8;
					back=*pBuf;
					int tR,tG,tB;
					tR=((DWORD)(r*a)>>8)+GetRValue16(back);
					if(tR>65535) tR=65535;
					tG=((DWORD)(g*a)>>8)+GetGValue16(back);
					if(tG>65535) tG=65535;
					tB=((DWORD)(b*a)>>8)+GetBValue16(back);
					if(tB>65535) tB=65535;
					*pBuf++=RGB16(tR,tG,tB);
					cR+=cRd;cG+=cGd;cB+=cBd;
					cA+=cAd;
					cU+=cUd;cV+=cVd;
				}//end of for() (with texture on)
				pBufBak+=pitch;
				lx+=lxd;
				rx+=rxd;
				lR+=lRd;lG+=lGd;lB+=lBd;
				rR+=rRd;rG+=rGd;rB+=rBd;
				lA+=lAd;rA+=rAd;
				lU+=lUd;lV+=lVd;
				rU+=rUd;rV+=rVd;
				y++;
			}
			while(y<ny || y==bottomY);
			//alphaのときにポリゴンの稜線が見えるために
			//最後の一ラインを描画しないように変更するには
			//y==bottomYを削除する
		}while(y<bottomY);
	} else {//すべての点のYが同じのとき（水平線のとき）
		lx=20000;rx=-20000;
		for(i=0;i<n;i++){//最小のXと最大のXを求める
			if(ver[i].m_vctrPos.m_rX<lx){
				lx=(int)ver[i].m_vctrPos.m_rX;
				lxd=i;
			}
			if(ver[i].m_vctrPos.m_rX>rx){
				rx=(int)ver[i].m_vctrPos.m_rX;
				rxd=i;
			}
		}
		pBuf=(WORD*)canvas.m_pBuf+pitch*topY+lx;
		lR=(int)(ver[lxd].m_color.r*65535.f);
		rR=(int)(ver[rxd].m_color.r*65535.f);cR=lR;
		lG=(int)(ver[lxd].m_color.g*65535.f);
		rG=(int)(ver[rxd].m_color.g*65535.f);cG=lG;
		lB=(int)(ver[lxd].m_color.b*65535.f);
		rB=(int)(ver[rxd].m_color.b*65535.f);cB=lB;
		lA=(int)(ver[lxd].m_color.a*65535.f);
		rA=(int)(ver[rxd].m_color.a*65535.f);cA=lA;
		lU=(int)(ver[lxd].m_rU*(real)pTex->m_nWidth*65535.f);
		rU=(int)(ver[rxd].m_rU*(real)pTex->m_nWidth*65535.f);cU=lU;
		lV=(int)(ver[lxd].m_rV*(real)pTex->m_nHeight*65535.f);
		rV=(int)(ver[rxd].m_rV*(real)pTex->m_nHeight*65535.f);cV=lV;
		count=rx-lx;
		if(count){
			cRd=(rR-lR)/count;
			cGd=(rG-lG)/count;
			cBd=(rB-lB)/count;
			cAd=(rA-lA)/count;
			cUd=(rU-lU)/count;
			cVd=(rV-lV)/count;
		}
		for(;count>=0;count--){//横のループ
			//後でコピー
		}
	}
}
