//DpHigh.cpp --- copyright Urban Reflection
#include <assert.h>
#include <math.h>
#include <d3drm.h>
#include "DpLow.h"
#include "DpHigh.h"
//#undef assert
//#define assert(f) if(!(f)){DebugBreak();}

#define AssertColor(col)	assert(0.f<=col.r && col.r<=1.f && \
					0.f<=col.g && col.g<=1.f && \
					0.f<=col.b && col.b<=1.f);

//CBox ----------------------------------------------------------------------
void CBox::Update(CVector const& vctr)
{
	if(m_bEmpty){
		m_vctrMin=m_vctrMax=vctr;
		m_bEmpty=FALSE;
	} else {
		if(vctr.x<m_vctrMin.x){
			m_vctrMin.x=vctr.x;
		} else if(vctr.x>m_vctrMax.x){
			m_vctrMax.x=vctr.x;
		}
		if(vctr.y<m_vctrMin.y){
			m_vctrMin.y=vctr.y;
		} else if(vctr.y>m_vctrMax.y){
			m_vctrMax.y=vctr.y;
		}
		if(vctr.z<m_vctrMin.z){
			m_vctrMin.z=vctr.z;
		} else if(vctr.z>m_vctrMax.z){
			m_vctrMax.z=vctr.z;
		}
	}
}
//CFace ---------------------------------------------------------------------
void CFace::Create(int nVertex,int* pnVertices,int* pnNormals
	,int* pnTextureCoords,int nMaterial){
	assert(m_nVertex==0);
	m_nVertex=nVertex;
	m_pnVertices=new int[nVertex];
	if(pnVertices) memcpy(m_pnVertices,pnVertices,sizeof(int)*nVertex);
	m_pnNormals=new int[nVertex];
	if(pnNormals) memcpy(m_pnNormals,pnNormals,sizeof(int)*nVertex);
	m_pnTextureCoords=new int[nVertex];
	if(pnTextureCoords) memcpy(m_pnTextureCoords,pnTextureCoords,sizeof(int)*nVertex);
	m_nMaterial=nMaterial;
}

//CFrame --------------------------------------------------------------------
void CFrame::PrepareRendering(CViewport* pViewport)
{
	if(GetParent()){
		m_mtrxWorld=m_matrix*GetParent()->m_mtrxWorld;
	} else {
		m_mtrxWorld=m_matrix;
	}
	int i;
	for(i=0;i<m_aLight.GetSize();i++){
		CLight* p=m_aLight.GetAt(i);
		if(p->GetLightType()==CLight::ambient){
			pViewport->m_colAmbient+=p->GetColor();
			pViewport->m_nAmbient++;
		} else {
			//光線ベクトルをワールド座標変換
			p->m_vctrTempDir.Set(0,0,1);
			p->m_vctrTempDir*=m_mtrxWorld;
			p->m_vctrTempDir.Normalize();
/*			p->m_vctrTempR=p->m_vctrTempDir*p->GetColor().r;
			p->m_vctrTempG=p->m_vctrTempDir*p->GetColor().g;
			p->m_vctrTempB=p->m_vctrTempDir*p->GetColor().b;*/
			//Viewportに通知
			pViewport->m_aLight.Add(p);
		}
	}
	for(i=0;i<m_aChildren.GetSize();i++){
		m_aChildren.GetAt(i)->PrepareRendering(pViewport);
	}
}
void CFrame::Render(CCanvas const& canvas,CMatrix const& mtrxView,CViewport* pViewport)
{
	if(m_pMesh){
		m_pMesh->Render(canvas,m_mtrxWorld,mtrxView,pViewport);
	}
	for(int i=0;i<m_aChildren.GetSize();i++){
		m_aChildren.GetAt(i)->Render(canvas,mtrxView,pViewport);
	}
}

//CVieport ------------------------------------------------------------------
BOOL CViewport::Render(CCanvas const& canvas,CFrame* pFrame)
{
	assert(pFrame);
	if(m_pCamera==NULL){
		SetError(0,"Camera frame is not setted in CViewport::Render()");
		return FALSE;
	}
	//準備
	m_aLight.RemoveAll();
	m_aPolygons.RemoveAll();
	m_aTLVertices.RemoveAll();
	m_colAmbient.Set(0.f);
	m_nAmbient=0;
	pFrame->PrepareRendering(this);
	CTLVertex* pTLVertexCheck=m_aTLVertices.GetPtr(0);
	//環境光
	if(m_nAmbient){
		m_colAmbient/=(real)m_nAmbient;
	} else {
		m_colAmbient.Set(0.f);
	}
	//Specularベクトルの計算
	CVector vctrView=CVector(0,0,1)*m_pCamera->GetWorldMatrix()
		-CVector(0,0,0)*m_pCamera->GetWorldMatrix();//視線ベクトル
	assert(vctrView.GetLength()==1.0f);
	for(int i=0;i<m_aLight.GetSize();i++){
		CLight* pLight=m_aLight.GetAt(i);
		pLight->m_vctrTempSpecular=pLight->m_vctrTempDir;
		pLight->m_vctrTempSpecular+=vctrView;
		pLight->m_vctrTempSpecular/=(real)2;
		pLight->m_vctrTempSpecular.Normalize();
	}
	//視野行列の設定
	SetMatrix(canvas);
	//変換＆照光
	CMatrix m=m_pCamera->GetWorldMatrix();
	m.InverseMatrix();
	m*=m_matrix;
	pFrame->Render(canvas,m,this);
	//ソート
	CPolygon** ppPoly=new CPolygon*[m_aPolygons.GetSize()];
	if(pTLVertexCheck==m_aTLVertices.GetPtr(0)){//可変長配列の再確保が行われていないとき
		for(i=0;i<m_aPolygons.GetSize();i++){
			ppPoly[i]=m_aPolygons.GetPtr(i);
		}
	} else {
		int offset=0;
		for(i=0;i<m_aPolygons.GetSize();i++){
			ppPoly[i]=m_aPolygons.GetPtr(i);
			ppPoly[i]->m_pVertices=m_aTLVertices.GetPtr(offset);
			offset+=ppPoly[i]->m_nVertices;
		}
	}
	qsort((void*)ppPoly,i,sizeof(CPolygon*),SortBackToFront);
	//ラスタライズ
	for(i=0;i<m_aPolygons.GetSize();i++){
		DrawPolygon(canvas,*ppPoly[i]);
	}
	delete ppPoly;
	return TRUE;
}

void CViewport::SetMatrix(CCanvas const& canvas){
	//DirectXのヘルプ、Direct3Dの概要、イントロダクション、RMViewport、変換を参照
	assert(m_rFront>0);
	real rSx=(real)canvas.m_nWidth;
	real rSy=(real)canvas.m_nHeight;
	real rOx=rSx/2.f;
	real rOy=rSy/2.f;
	real rAspect=1.f;
	real rWidth=2.f*m_rFront*(real)tan(m_rAngle);
	real rHeight=rWidth*rSy/rSx;
	real rLength=m_rBack-m_rFront;
	m_matrix(0,0)=rSy;
	m_matrix(1,1)=-rSy;
	m_matrix(0,2)=rHeight*rOx/m_rFront;
	m_matrix(1,2)=rHeight*rOy/m_rFront;
	m_matrix(2,2)=rHeight*m_rBack/(m_rFront*rLength);
	m_matrix(3,2)=rHeight/m_rFront;
	m_matrix(2,3)=-rHeight*m_rBack/rLength;
	m_matrix(3,3)=real(0);
}

//CMesh ---------------------------------------------------------------------------------
CMesh::CMesh(){
	m_nVertex=0;
	m_pVertices=NULL;
	m_pnVertexReference=NULL;
	m_nNormal=0;
	m_pNormals=NULL;
	m_nFace=0;
	m_pFaces=NULL;
	m_pbFaceVisible=NULL;
	m_nMaterial=0;
	m_pMaterials=NULL;
	m_nTextureCoord=0;
	m_pUs=NULL;
	m_pVs=NULL;
	m_pTempDiffuse=NULL;
	m_bDirtyFaceNormal=TRUE;
}
CMesh::~CMesh(){
	if(m_pVertices){delete [] m_pVertices;}
	if(m_pnVertexReference){delete [] m_pnVertexReference;}
	if(m_pNormals){delete [] m_pNormals;}
	if(m_pFaces){delete [] m_pFaces;}
	if(m_pbFaceVisible){delete [] m_pbFaceVisible;}
	if(m_pMaterials){delete [] m_pMaterials;}
	if(m_pUs){delete [] m_pUs;}
	if(m_pVs){delete [] m_pVs;}
	if(m_pTempDiffuse){delete [] m_pTempDiffuse;}
}
BOOL CMesh::Load(LPCTSTR szFilename)
{
	assert(m_nVertex==0);
	LPDIRECT3DRM			pD3DRM = NULL;
	LPDIRECT3DRMMESHBUILDER	pMeshBuilder = NULL;
	//初期化
	HRESULT	rval = Direct3DRMCreate(&pD3DRM);
	if(rval!=D3DRM_OK){
		SetError(0,"Can't create Direct3DRM in CMesh::Load().");
		return FALSE;
	}
	rval = pD3DRM->CreateMeshBuilder(&pMeshBuilder);
	if(rval!=D3DRM_OK){
		pD3DRM->Release();
		SetError(0,"Can't create MeshBuilder in CMesh::Load().");
		return FALSE;
	}
	//ファイルの存在チェック
	HFILE hFile=(HFILE)CreateFile(szFilename,GENERIC_READ,0,NULL,OPEN_EXISTING,0,0);
	CloseHandle((void*)hFile);
	if(hFile==(HFILE)INVALID_HANDLE_VALUE){
		SetError(0,"File not found :'%s' in CMesh::Load().",szFilename);
		return FALSE;
	}
	//ロード
	rval = pMeshBuilder->Load(
		(void*)szFilename, NULL, D3DRMLOAD_FROMFILE, NULL, NULL);
	if(rval!=D3DRM_OK){
		pMeshBuilder->Release();
		pD3DRM->Release();
		SetError(0,"Can't load '%s' mesh in CMesh::Load().",szFilename);
		return FALSE;
	}
	//情報取得
	DWORD nFaceDataSize;
	pMeshBuilder->GetVertices((DWORD*)&m_nVertex, NULL,
		(DWORD*)&m_nNormal,NULL,&nFaceDataSize, NULL);
	m_nFace = pMeshBuilder->GetFaceCount();
	m_pVertices = new CVector[m_nVertex];
	m_pFaces = new CFace[m_nFace];
	m_pNormals=new CVector[m_nNormal];
	m_pUs=new real[m_nVertex];
	m_pVs=new real[m_nVertex];
	D3DVECTOR*	pVector = new D3DVECTOR[m_nVertex];
	D3DVECTOR*	pNormal = new D3DVECTOR[m_nNormal];
	DWORD*		pFaceData = new DWORD[nFaceDataSize];
	pMeshBuilder->GetVertices(
		(DWORD*)&m_nVertex, pVector,
		(DWORD*)&m_nNormal, pNormal,
		&nFaceDataSize, pFaceData);
	int i;
	//頂点のコピー
	for(i=0;i<m_nVertex;i++){
		m_pVertices[i]=CVector(pVector[i].dvX,pVector[i].dvY,pVector[i].dvZ);
	}
	//法線のコピー
	for(i=0;i<m_nNormal;i++){
		m_pNormals[i]=CVector(pNormal[i].dvX,pNormal[i].dvY,pNormal[i].dvZ);
	}
	//テクスチャ座標のコピー
	for(i=0;i<m_nVertex;i++){
		pMeshBuilder->GetTextureCoordinates(i,&m_pUs[i],&m_pVs[i]);
	}
	//面の情報取得
	LPDIRECT3DRMFACEARRAY	pFaceArray = NULL;
	pMeshBuilder->GetFaces(&pFaceArray);
	TArray<LPDIRECT3DRMMATERIAL> rmMaterialArray;
	TArray<CMaterial*> materialArray;
	int offset=0;
	if(pFaceArray){
		for(int i=0; i<m_nFace; i++){
			LPDIRECT3DRMFACE	pRmFace = NULL;
			pFaceArray->GetElement(i, &pRmFace);
			assert(pRmFace);
			LPDIRECT3DRMMATERIAL pRmMaterial=NULL;
			pRmFace->GetMaterial(&pRmMaterial);
			assert(pRmMaterial);
			CMaterial* pMaterial=NULL;
			int nMaterial=rmMaterialArray.Find(0,pRmMaterial);
			if(nMaterial==rmMaterialArray.End()){//新規マテリアル
				nMaterial=materialArray.GetSize();
				rmMaterialArray.Add(pRmMaterial);
				pMaterial=new CMaterial;
				materialArray.Add(pMaterial);
			} else {//既存マテリアル
				pMaterial=materialArray.GetAt(nMaterial);
			}
			//色のコピー
			D3DCOLOR	c = pRmFace->GetColor();
			pMaterial->SetColor(
				CColor(
				(real)((c & 0x00ff0000)>>16)/255.f,
				(real)((c & 0x0000ff00)>> 8)/255.f,
				(real)((c & 0x000000ff)>> 0)/255.f,
				(real)((c & 0xff000000)>>24)/255.f));
			//パワーのコピー
			real rPower=pRmMaterial->GetPower();
			if(rPower>1.f){//あんまり小さくてもこまるしぃ(真っ白な物体になっちゃうから）
				pMaterial->SetPower(pRmMaterial->GetPower());
			}
			//スペキュラのコピー
			D3DVALUE r,g,b;
			pRmMaterial->GetSpecular(&r,&g,&b);
			pMaterial->SetSpecular(0.3f*r+0.59f*g+0.11f*b);
			//面の法線のコピー
			//D3DVECTOR vctrNormal;
			//pRmFace->GetNormal(&vctrNormal);
			//m_vctrNormal=CVector(vctrNormal.dvX,vctrNormal.dvY,vctrNormal.dvZ);
			//テクスチャのコピー
			LPDIRECT3DRMTEXTURE pRmTexture;
			pRmFace->GetTexture(&pRmTexture);
			if(pRmTexture){
				char szName[256];
				DWORD nSize=256;
				pRmTexture->GetName(&nSize,szName);
				pMaterial->SetTexture(GetTexture(szName));
			}
			//面のコピー
			CFace*	pFace =&m_pFaces[i];
			int	nVertex = pFaceData[offset++];
			int* pnV=new int[nVertex];
			int* pnN=new int[nVertex];
			int* pnT=new int[nVertex];
			for(int j=0;j<nVertex;j++){
				pnV[j]=pFaceData[offset++];
				pnN[j]=pFaceData[offset++];
				pnT[j]=pnV[j];
			}
			pFace->Create(nVertex,pnV,pnN,pnT,nMaterial);
			delete [] pnV;
			delete [] pnN;
			delete [] pnT;
			pRmFace->Release();
		}
		pFaceArray->Release();
	} else {
		assert(FALSE);
	}
	//マテリアルの作成
	m_nMaterial=materialArray.GetSize();
	m_pMaterials=new CMaterial[m_nMaterial];
	for(i=0;i<materialArray.GetSize();i++){
		m_pMaterials[i]=*materialArray.GetAt(i);
		delete materialArray.GetAt(i);
	}
	delete[] pNormal;
	delete[] pVector;
	delete[] pFaceData;
	pMeshBuilder->Release();
	pD3DRM->Release();
	return TRUE;
}

void CMesh::CreateCylinder(real rRadius, real rLength, CMaterial const& material,int nDiv)
{
	assert(m_nVertex==0 && m_nNormal==0 && m_nMaterial==0 && m_nTextureCoord==0);
	//create vertex & normal
	m_nVertex=nDiv*2;
	m_pVertices=new CVector[m_nVertex];
	m_nNormal=nDiv+2;
	m_pNormals=new CVector[m_nNormal];
	int i;
	for(i=0;i<nDiv;i++){
		double t=2.f*DP_PI*(real)i/(real)nDiv;
		m_pVertices[i     ].Set(rRadius*(real)cos(t),rRadius*(real)sin(t), rLength/2.f);
		m_pVertices[i+nDiv].Set(rRadius*(real)cos(t),rRadius*(real)sin(t),-rLength/2.f);
		m_pNormals[i].Set((real)cos(t),(real)sin(t),0.f);
	}
	m_pNormals[nDiv].Set(0.f,0.f,1.f);
	m_pNormals[nDiv+1].Set(0.f,0.f,-1.f);
	//create material
	m_nMaterial=1;
	m_pMaterials=new CMaterial[1];
	m_pMaterials[0]=material;
	//create face
	m_nFace=nDiv+2;
	m_pFaces=new CFace[m_nFace];
	for(i=0;i<nDiv;i++){
		int v[4],n[4];
		v[0]=i;
		v[1]=i+nDiv;
		v[2]=(i+1)%nDiv+nDiv;
		v[3]=(i+1)%nDiv;
		n[0]=i;
		n[1]=i;
		n[2]=(i+1)%nDiv;
		n[3]=(i+1)%nDiv;
		m_pFaces[i].Create(4,v,n,NULL,0);
	}
	int* v=new int[nDiv];
	int* n=new int[nDiv];
	for(i=0;i<nDiv;i++){
		v[i]=i;
		n[i]=nDiv;
	}
	m_pFaces[nDiv].Create(nDiv,v,n,NULL,0);
	for(i=0;i<nDiv;i++){
		v[i]=2*nDiv-i-1;
		n[i]=nDiv+1;
	}
	m_pFaces[nDiv+1].Create(nDiv,v,n,NULL,0);
	delete [] v;
	delete [] n;
}
void CMesh::CreateSphere(real rRadius, CMaterial const& material,int nDivR,int nDivH)
{
	assert(m_nVertex==0 && m_nNormal==0 && m_nMaterial==0 && m_nTextureCoord==0);
	//create vertex & normal
	m_nVertex=nDivR*nDivH+2;
	m_pVertices=new CVector[m_nVertex];
	m_nNormal=nDivR*nDivH+2;
	m_pNormals=new CVector[m_nNormal];
	int i;
	for(i=0;i<nDivH;i++){
		double rH=DP_PI*(real)(i+1)/(real)(nDivH+1);
		for(int j=0;j<nDivR;j++){
			double rR=2.f*DP_PI*(real)j/(real)nDivR;
			m_pVertices[i*nDivR+j].Set(rRadius*(real)(cos(rR)*sin(rH))
				,rRadius*(real)(sin(rR)*sin(rH))
				,rRadius*(real)cos(rH));
			m_pNormals[i*nDivR+j]=m_pVertices[i*nDivR+j];
			m_pNormals[i*nDivR+j].Normalize();
		}
	}
	m_pVertices[nDivR*nDivH].Set(0.f,0.f,rRadius);
	m_pVertices[nDivR*nDivH+1].Set(0.f,0.f,-rRadius);
	m_pNormals[nDivR*nDivH].Set(0.f,0.f,1.f);
	m_pNormals[nDivR*nDivH+1].Set(0.f,0.f,-1.f);
	//create material
	m_nMaterial=1;
	m_pMaterials=new CMaterial[1];
	m_pMaterials[0]=material;
	//create face
	m_nFace=nDivR*(nDivH+1);
	m_pFaces=new CFace[m_nFace];
	for(i=0;i<nDivR;i++){//top
		int v[3];
		v[0]=nDivR*nDivH;
		v[1]=i;
		v[2]=(i+1)%nDivR;
		m_pFaces[i].Create(3,v,v,NULL,0);
	}
	for(i=0;i<nDivH-1;i++){//mid
		for(int j=0;j<nDivR;j++){
			int v[4];
			v[0]= i   *nDivR+ j;
			v[1]=(i+1)*nDivR+ j;
			v[2]=(i+1)*nDivR+(j+1)%nDivR;
			v[3]= i   *nDivR+(j+1)%nDivR;
			m_pFaces[(i+1)*nDivR+j].Create(4,v,v,NULL,0);
		}
	}
	for(i=0;i<nDivR;i++){//top
		int v[3];
		v[0]=nDivR*(nDivH-1)+i;
		v[1]=nDivR*nDivH+1;
		v[2]=nDivR*(nDivH-1)+(i+1)%nDivR;
		m_pFaces[nDivR*nDivH+i].Create(3,v,v,NULL,0);
	}
}

void CMesh::Render(CCanvas const & canvas,CMatrix const& mtrxWorld,CMatrix const& mtrxView
				   ,CViewport* pViewport)
{
	int i;
	//初期化
	if(m_pnVertexReference==NULL){
		m_pnVertexReference=new int[m_nVertex];
	}
	if(m_pbFaceVisible==NULL){
		m_pbFaceVisible=new BOOL[m_nFace];
	}
	if(m_pTempDiffuse==NULL){
		m_pTempDiffuse=new CColor[m_nNormal];
	}
	CalcFaceNormals();
	//視線ベクトルの逆回転（陰面処理用）
	CMatrix mtrxCamera=pViewport->m_pCamera->GetWorldMatrix();
	mtrxCamera.InverseMatrix();
	CMatrix mtrxInverse=mtrxWorld*mtrxCamera;
	CVector vctrToMesh=CVector(0,0,0)*mtrxInverse;
	mtrxInverse.InverseMatrix();
	CVector vctrView=vctrToMesh*mtrxInverse-CVector(0,0,0)*mtrxInverse;
	//陰面処理
	memset(m_pnVertexReference,0,sizeof(int)*m_nVertex);
	for(i=0;i<m_nFace;i++){
		CFace& face=m_pFaces[i];
		real r=face.GetFaceNormal().DotProduct(vctrView);
		if(r<=0.f){//見える面なら
			for(int j=0;j<face.GetVertexCount();j++){
				m_pnVertexReference[face.GetVertex()[j]]++;
			}
			m_pbFaceVisible[i]=TRUE;
		} else {
			m_pbFaceVisible[i]=FALSE;
		}
	}
	//頂点の変換(transform stage)
	CMatrix matrix=mtrxWorld*mtrxView;
	CVector* pTVertex=new CVector[m_nVertex];
	for(i=0;i<m_nVertex;i++){
		if(m_pnVertexReference[i]){//見える頂点のみ変換
			pTVertex[i]=m_pVertices[i]*matrix;
			if(pTVertex[i].z<pViewport->GetFront()) break;
			pTVertex[i].Homogenize();
		}
	}
	if(i==m_nVertex){
		//光線ベクトルの逆回転
		for(i=0;i<pViewport->m_aLight.GetSize();i++){
			CLight* pLight=pViewport->m_aLight.GetAt(i);
			CMatrix mtrxInverse=mtrxWorld;
			mtrxInverse.InverseMatrix();
			CVector v=CVector(0,0,0)*mtrxInverse;
			pLight->m_vctrTempDir2=pLight->m_vctrTempDir*mtrxInverse;
			pLight->m_vctrTempDir2-=v;
			pLight->m_vctrTempDir2.Normalize();
			pLight->m_vctrTempSpecular2=pLight->m_vctrTempSpecular*mtrxInverse;
			pLight->m_vctrTempSpecular2-=v;
			pLight->m_vctrTempSpecular2.Normalize();
		}
		//Diffuseの計算
		for(i=0;i<m_nNormal;i++){
			m_pTempDiffuse[i]=CColor(0,0,0,0);
			for(int k=0;k<pViewport->m_aLight.GetSize();k++){
				CLight* pLight=pViewport->m_aLight.GetAt(k);
				CVector& n=m_pNormals[i];
				if(pLight->m_vctrTempDir2.DotProduct(n)<0.f){//光が当たるとき
					real r=-pLight->m_vctrTempDir2.DotProduct(n);
					m_pTempDiffuse[i]+=pLight->GetColor()*r;
				}
			}
			m_pTempDiffuse[i].LimitUpper();
			m_pTempDiffuse[i].r=pViewport->m_colAmbient.r+(1.f-pViewport->m_colAmbient.r)*m_pTempDiffuse[i].r;
			m_pTempDiffuse[i].g=pViewport->m_colAmbient.g+(1.f-pViewport->m_colAmbient.g)*m_pTempDiffuse[i].g;
			m_pTempDiffuse[i].b=pViewport->m_colAmbient.b+(1.f-pViewport->m_colAmbient.b)*m_pTempDiffuse[i].b;
			assert(0.f<=m_pTempDiffuse[i].r && m_pTempDiffuse[i].r<=1.f &&
				0.f<=m_pTempDiffuse[i].g && m_pTempDiffuse[i].g<=1.f &&
				0.f<=m_pTempDiffuse[i].b && m_pTempDiffuse[i].b<=1.f);
		}
		for(i=0;i<m_nFace;i++){//面の数だけループ
			if(m_pbFaceVisible[i]==FALSE) continue;//見えない面はスキップ
			CTLVertex* pTLV=pViewport->m_aTLVertices.AddEmpty(m_pFaces[i].GetVertexCount());
			CMaterial* pMaterial=&m_pMaterials[m_pFaces[i].GetMaterial()];
			const int* pVertex=m_pFaces[i].GetVertex();
			const int* pNormal=m_pFaces[i].GetNormal();
			real rMinZ=99999999999.f;
			for(int j=0;j<m_pFaces[i].GetVertexCount();j++){//角数だけループ
				int nIdx=pNormal[j];
				CVector& n=m_pNormals[nIdx];
				//Specularの計算
				CColor colSpecular(0,0,0,0);
				if(pMaterial->GetSpecular()>=0.01f){
					for(int k=0;k<pViewport->m_aLight.GetSize();k++){
						CLight* pLight=pViewport->m_aLight.GetAt(k);
						real r=-pLight->m_vctrTempSpecular2.DotProduct(n);
						if(r>0.f){
							r=pMaterial->GetSpecular()*(real)pow(r,pMaterial->GetPower());//pow()は重い
							colSpecular+=pLight->GetColor()*r;
							colSpecular.a+=r;
						}
					}
				}
				//Diffuse・マテリアルの合成
				CColor& colDiffuse=m_pTempDiffuse[nIdx];
				pTLV[j].m_color=colDiffuse*pMaterial->GetColor();
				if(colSpecular.a>0.01f){//Specularあり
					real r=colDiffuse.r*pMaterial->GetColor().r;
					pTLV[j].m_color+=colSpecular;
					if(pMaterial->GetTexture()){//textureあり
						pTLV[j].m_color*=0.5;
					} else {
						pTLV[j].m_color.LimitUpper();
					}
				} else {//Specularなし
					if(pMaterial->GetTexture()){//textureあり
						pTLV[j].m_color*=0.5;
					}
				}
				pTLV[j].m_color.a=pMaterial->GetColor().a;
				//set CTLVertex
				pTLV[j].m_vctrPos=pTVertex[pVertex[j]];
				if(pTLV[j].m_vctrPos.z<rMinZ) rMinZ=pTLV[j].m_vctrPos.z;
				assert(0.f<=pTLV[j].m_color.r && pTLV[j].m_color.r<=1.f &&
					0.f<=pTLV[j].m_color.g && pTLV[j].m_color.g<=1.f &&
					0.f<=pTLV[j].m_color.b && pTLV[j].m_color.b<=1.f);
			}
			CPolygon* pPoly=pViewport->m_aPolygons.AddEmpty();
			pPoly->m_dwFlags=DP_CULL_CW|DP_COLOR_RGB|DP_SHADE_GOURAUD;
			pPoly->m_nVertices=m_pFaces[i].GetVertexCount();
			pPoly->m_pVertices=pTLV;
			pPoly->m_rReserved=rMinZ;
			if(pMaterial->GetTexture()){//テクスチャ
				for(int j=0;j<m_pFaces[i].GetVertexCount();j++){//角数だけループ
					int nTex=m_pFaces[i].GetTextureCoord()[j];
					pTLV[j].m_rU=m_pUs[nTex];
					pTLV[j].m_rV=m_pVs[nTex];
				}
				pPoly->m_pTexture=pMaterial->GetTexture();
			}
		}
	}
	delete [] pTVertex;
}

void CMesh::CalcFaceNormals()
{
	if(m_bDirtyFaceNormal){
		for(int i=0;i<m_nFace;i++){
			CFace& face=m_pFaces[i];
			CVector v1=m_pVertices[face.GetVertex()[1]]
				-m_pVertices[face.GetVertex()[0]];
			CVector v2=m_pVertices[face.GetVertex()[2]]
				-m_pVertices[face.GetVertex()[1]];
			face.SetFaceNormal(v1.CrossProduct(v2));
		}
		m_bDirtyFaceNormal=FALSE;
	}
}

void CMesh::AddTransform(CMatrix& matrix)
{
	int i;
	for(i=0;i<m_nVertex;i++){
		m_pVertices[i]*=matrix;
	}
//	for(i=0;i<m_nNormal;i++){//回転だけならよいが、移動・スケーリングがあるとだめ。
//		m_pNormals[i]*=matrix;
//	}
}

void CMesh::CalcBoundingBox()
{
	for(int i=0;i<m_nVertex;i++){
		m_box.Update(m_pVertices[i]);
	}
}

void CMesh::Wrap(EWrapType type
		,CVector const& vctrOrign
		,CVector const& vctrDir
		,CVector const& vctrUp
		,real ou,real ov
		,real su,real sv
		,CTexture const& texture)
{
	if(type==wrapFlat){
		//テクスチャ座標のアロケーション
		if(m_nTextureCoord){
			delete [] m_pUs;
			m_pUs=new real[m_nVertex];
			delete [] m_pVs;
			m_pVs=new real[m_nVertex];
			m_nTextureCoord=m_nVertex;
		}
		//テクスチャ座標を求める
		CMatrix matrix;
		for(int i=0;i<m_nVertex;i++){
			CVector v=m_pVertices[i];
			m_pUs[i]=v.x;
			m_pVs[i]=v.y;
		}
		//面のテクスチャ座標の設定
		for(i=0;i<m_nFace;i++){
			CFace& face=m_pFaces[i];
			for(int j=0;j<face.GetVertexCount();j++){
				face.GetTextureCoord()[j]=face.GetVertex()[j];
			}
		}
		//マテリアルの設定
		for(i=0;i<m_nMaterial;i++){
			m_pMaterials[i].SetTexture(&texture);
			m_pMaterials[i].SetSpecular(1.f);
		}
	} else {
		assert(FALSE);
	}
}
