
// OHM controler for HIOKI 3540


#if !defined(_BCL_OHM_H3540)
#define _BCL_OHM_H3540

#include <j:/dev2/unit/ohm/skeleton.h>

class H3540 : public OHM
{
   private:

     char buff[128];
     unsigned int len;

   public:

     H3540(ifLine *iface);
     
     double  read();
     void    setRange(double ohm);
     char    *getName(void) { return "H3540"; }
};

#endif

