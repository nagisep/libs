// OHM controler for HIOKI 3227

#if !defined(_BCL_OHM_H3227)
#define _BCL_OHM_H3227

#include <dev2/unit/ohm/skeleton.h>

class H3227 : public OHM
{
   public:

     H3227(ifLine *iface);
     
     double  read();
     void    setSpeed(speedType sp);
     char    *getName(void) { return "H3227"; }
};

#endif
