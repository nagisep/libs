
#if !defined(_BCL_OHMSkeleton)
#define _BCL_OHMSkeleton
#include <j:/dev2/unit/unit.h>

//  精密抵抗計を実現するためのスケルトン

class OHM : public Unit
{

 public:
   enum speedType {SLOW, MEDIUM, FAST};

   OHM(ifLine *iface) : Unit(iface) { }

   double  minStepRead()  { return 0;}
   double  minStepWrite() { return -1; }

   virtual double       read(void)          = 0;
   virtual void         setSpeed(speedType sp)  {  }
   virtual void         setRange(double ohm) { }  // ohm を十分に測定可能な最大レンジをセット。
                                                  // ohm < 0 の時は、（可能なら） auto range

   char          *getName()           { return "OHM Skeleton"; }
};

#endif

