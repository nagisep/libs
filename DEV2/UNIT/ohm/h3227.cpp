#include <windows.h>
#include <cstdlib>
#include <dev2/unit/ohm/H3227.h>

H3227::H3227(ifLine *iface) : OHM(iface)
{
   myIface->setDelim(ifLine::lf + ifLine::eoi);
   myIface->talk(":HEAD OFF");
   Sleep(50);
   myIface->talk(":RESI:AUTO ON");
   Sleep(50);
   myIface->talk(":SAMPLE FAST");
   Sleep(50);
}

double H3227::read()
{
   char buff[128];
   unsigned int len = 128;

   myIface->talk(":MEAS:RESI?");
   Sleep(50);
   myIface->listen(buff, len);
   if (strstr(buff, "OFF")) return std::strtod(buff, NULL);
   if (strstr(buff, "OF")) return -999;
   if (strstr(buff, "NG")) return -999;
   return std::strtod(buff, NULL);
}

void   H3227::setSpeed(OHM::speedType sp)
{
  switch (sp)
  {  case OHM::FAST:   myIface->talk("SAMPLE FAST"); break;
     case OHM::MEDIUM: myIface->talk("SAMPLE MEDI"); break;
     case OHM::SLOW:   myIface->talk("SAMPLE SLOW"); break;
  }
}


