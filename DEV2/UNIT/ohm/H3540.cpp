
#include "H3540.h"
#include <cstdlib>

H3540::H3540(ifLine *iface) : OHM(iface)
{
  // auto range
  myIface->talk("AUTO 1");
  len = 128;
  myIface->listen(buff, len);
  
  // comparator off
  myIface->talk("COMP 0");
  len = 128;
  myIface->listen(buff, len);
  
  // 電源は 60Hz
  myIface->talk("HZ 1");
  len = 128;
  myIface->listen(buff, len);
  
  // サンプリング FAST
  myIface->talk("HZ 1");
  len = 128;
  myIface->listen(buff, len);
  
  // HOLD しない
  myIface->talk("HOLD 0");
  len = 128;
  myIface->listen(buff, len);
}

double H3540::read()
{
  myIface->talk("RMES");
  len = 128;
  myIface->listen(buff, len);

  double result;
  if (buff[0] == 'O')  //  result == "OF" : overflow
    result = -1;
  else if (buff[0] == 'C') // result == "CC ERR" : 電流値異常
    result = -1;
  else
    result = std::strtod(buff, 0);

  return result;
}

void H3540::setRange(double ohm)
{
  if (ohm < 0) // set Auto Range
  {
    myIface->talk("ATUO 1");
    len = 128;
    myIface->listen(buff, len);
  }
  else
  {
    double fullScale = ohm * 1.2; // 与えられた抵抗値の 1.2 倍までが計測できるレンジをねらう
    if (fullScale >     30000.)   myIface->talk("RNG 6"); // 本当は適切なレンジがない（高すぎる）ので、最大レンジにしておく
    else if (fullScale > 3000.)   myIface->talk("RNG 6");
    else if (fullScale >  300.)   myIface->talk("RNG 5");
    else if (fullScale >   30.)   myIface->talk("RNG 4");
    else if (fullScale >    3.)   myIface->talk("RNG 3");
    else if (fullScale >    0.3)  myIface->talk("RNG 2");
    else if (fullScale >    0.03) myIface->talk("RNG 1");
    else                          myIface->talk("RNG 0");

    len = 128;
    myIface->listen(buff, len);
  }
}

