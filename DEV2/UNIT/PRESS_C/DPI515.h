
#if !defined(_BCL_PRESSC_DPI515)
#define _BCL_PRESSC_DPI515

#include <j:\dev2\unit\press_c\skeleton.h>

class DPI515 : public PRESS_C {

  public:

	  DPI515(ifLine *iface);

	  Unit::stat setMode(PRESS_C::mode smode);
	  Unit::stat write(double press);

	  char *getName(void) { return "Druck DPI515"; }
	  Unit::stat zeroCal();
	  Unit::stat outOn(void);
	  Unit::stat outOff(void);
	  int        getStat();
	  double     minStepWrite() {return 0.1;}

      ~DPI515();

};

#endif

 