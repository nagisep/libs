// xpress == セッティングマノメータとマノメータを連結して
// フィードバックを可能にした抽象セッティングマノメータ


#if !defined(_BCL_PressC_XPress)
#define      _BCL_PressC_XPress

// #define isDebug

#include <j:\dev2\unit\press_c\skeleton.h>
#include <j:\dev2\unit\press_d\skeleton.h>
#include <fstream.h>
#include <windows.h>
#include <stdlib.h>

const int offListNumbers = 128;
const double offListScale = 10.0;

class XPress : public PRESS_C
{
   private:

     int toIndex(double press) 
     {
       int wk = press * offListScale + 0.5;
       if (wk >= offListNumbers) wk = offListNumbers - 1;
       return wk;
     }

	 
     struct {
		int src;
		double dest;
	 }  offList[offListNumbers];

     int offListPtr;

	 PRESS_C  *myControl;
	 PRESS_D  *myDisp;

	 double    offset;
	 int       inifilePresents;
	 ifstream  *inifile;
	 int       p1delay, p2delay;
	 double    pCheckWidth;

         int       pressCheckTime;
         double    pressCheckWidth;


   public:
	 XPress(PRESS_C *control, PRESS_D *disp, int checkTime = 1000, double checkWidth = 0.0005);
	 ~XPress();


	 double     read() { return myDisp->read(); }
	 double     minStepWrite() { return myControl->minStepWrite(); }

	 Unit::stat setMode(mode smode);
	 Unit::stat write(double press);
	 Unit::stat writeWithCheck(double press);
	 char *getName()             { return "XPress Controler"; }

	 Unit::stat outOn()          { return myControl->outOn();  }
	 Unit::stat outOff()
	 {  return myControl->outOff();}    // 大気開放し

	 Unit::stat zeroCal()
	 {  char *ptr;
		long delay;

#if defined(isDebug)
	cout << "p1delay = " << p1delay << "\n";
	cout << "p2delay = " << p2delay << "\n";
#endif

		Sleep(p1delay);
		myControl->zeroCal();

		Sleep(p2delay);
		return myDisp->zeroCal();       // 其の状態で、ゼロ・キャリブレーション
	 }


};

#endif

