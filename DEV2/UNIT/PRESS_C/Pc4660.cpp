#include <stdio.h>
#include <dev2\unit\press_c\pc4660.h>

pc4660::pc4660(Power *control, int full)
	  : PRESS_C(0)
      {  myControl   = control;
         myFullScale = full;
		 myControl->write(0);
		 myControl->outOn();
      }

Unit::stat pc4660::setMode(PRESS_C::mode smode)
{ return (smode == PRESS_C::mmH2O) ? normal : error; }

Unit::stat pc4660::write(double press)
{
  myControl->write(press * 5.0 / myFullScale);
  return Unit::normal;
}

pc4660::~pc4660(void)
{ myControl->write(0);
  delete myControl;
}

