// for pc-4660

#if !defined(_BCL_PRESSCpc4660)
#define _BCL_PRESSCpc4660

#include <dev2\unit\power\skeleton.h>
#include <dev2\unit\press_c\skeleton.h>

class pc4660 : public PRESS_C {

  private:
      int  myFullScale;
	  Power *myControl;
  
  public:
      double      minStepWrite() { return 0.1;}
      pc4660(Power *control, int full = 1000);
      
      Unit::stat  setMode(PRESS_C::mode smode);
      Unit::stat  write(double press);
      
      char *getName(void) { return "Press Control Type pc4660"; }
      Unit::stat outOn()  { return Unit::normal; }
	  Unit::stat outOff() { return Unit::normal; }
	  ~pc4660(void);
	  virtual Unit::stat zeroCal()  { return Unit::normal; }


};

#endif

 