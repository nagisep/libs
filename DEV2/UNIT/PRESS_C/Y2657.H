// For 2657 (Yokogawa)

#if !defined(_BCL_PRESSC2657)
#define _BCL_PRESSC2657

#include <j:\dev2\unit\press_c\skeleton.h>

class Y2657 : public PRESS_C {

  public:
      
      Y2657(ifLine *iface);
      
      Unit::stat setMode(PRESS_C::mode smode);
      Unit::stat write(double press);
      
      char *getName(void) { return "Press Control Type 2657"; }
      Unit::stat zeroCal();
	  Unit::stat outOn(void);
      Unit::stat outOff(void);
      int        getStat();
      double     minStepWrite() {return 0.1;}

      ~Y2657(void);

};

#endif

 