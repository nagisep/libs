#include <windows.h>
#include <cstdio>
#include <cmath>
#include <j:\dev2\unit\press_c\dpi515.h>

DPI515::DPI515(ifLine *iface) : PRESS_C(iface)
{     // gpib �̏������{�C���O�o�͂n�e�e��ݒ肷��
	  // �I�[�g�[��������Y�ꂸ�� ^^;

      myIface->talk(":UNIT:PRES KPA");
      myIface->talk(":SOUR:RANGE \"7.00barg\"");
}

Unit::stat DPI515::zeroCal()
{
   return Unit::normal;
}


Unit::stat DPI515::setMode(PRESS_C::mode smode)
{  Unit::stat stat = normal;
   return stat;
}

Unit::stat DPI515::write(double press)
{ char buff[32];

  if (press < 0) return write(0);

  std::sprintf(buff, ":SOUR %f", press);
  myIface->talk(buff);
  
  int timeCount = 0;
  for(int i = 0; i < 60; i++)
  {
     myIface->talk("*CLS");
     myIface->talk(":SENS?");
     unsigned int len = 32;
     myIface->listen(buff, len);
     double v = std::strtod(buff, 0);
     if (std::fabs(v - press) <= 0.01)
       timeCount ++;
     else
       timeCount = 0;

     if (timeCount > 5) break;
     Sleep(1000);
  }

  return Unit::normal;
}

int   DPI515::getStat()  // return 1 if over,  -1 if under, 0 if even
{
	  return 0;
}

Unit::stat DPI515::outOn()
{
     myIface->talk(":OUTP:STAT ON");
	 return normal;
}

Unit::stat DPI515::outOff()
{
     myIface->talk(":OUTP:STAT OFF");
	 return normal;
}

DPI515::~DPI515(void)
{
	 char sendData[16];
	  write(0);

//          Sleep(10000);
      outOff();
      myIface->talk("*LOC");
}


