//  argstage  傾斜ステージをコントロールする

#if !defined(_BCL_UnitEtcArgStage)
#define      _BCL_UnitEtcArgStage

#define		CAT_OFFSET 35	/* offset of origin			*/

#define		CAT_LMP	 10100	/* +Softlimit				*/
#define		CAT_LMM	-10100	/* -Softlimit				*/
#define		CAT_BK	    0	/* Revise backrash			*/
#define		CAT_MO	    1	/* method of return set DC maicro mode	*/
#define		CAT_DO	    0	/* direction is CCW			*/
#define		CAT_LOWV 2500	/* [pps]Sets low velocity		*/
#define		CAT_HIGV 5000	/* [pps]Sets high velocity		*/
#define		CAT_ACC  100	/* [ms]Sets acceleration		*/
#define		CAT_DACC 100	/* [ms] deacceleration			*/
#define		CAT_FSP  0	/* full step				*/
#define		CAT_HSP  1	/* half step				*/

#define		CAT_SETVA  "D:XP%dP%dP%dP%d"
										/* Set velocity and acceleration
										   lowv,highv,acc,deacc		*/
#define		CAT_SETPA "P:X%dP%d"    /* set paramater		*/
#define		CAT_SETSP "S:X%d"       /* set step			*/

#define		CAT_PNLMP  01	/* paramater No of +limit		*/
#define		CAT_PNLMM  02	/* 		   -limit		*/
#define		CAT_PNBK   03	/* 		   backrash		*/
#define		CAT_PNMO   04	/* 		   method to get origin	*/
#define		CAT_PNDO   05	/* 		   direction to origin	*/
#define		CAT_PNLV   06	/* 		   			*/
#define		CAT_PNHV   07	/* 					*/
#define		CAT_PNA    08	/* 					*/
#define		CAT_PNMA   09	/* 					*/

#define		CAT_ON	  "C:X0" 	/* Magnet on   			*/
#define		CAT_OFF	  "C:X1" 	/* Magnet off  			*/
#define		SOP 0.002			/*step of pulse		*/



#include <j:/dev2/unit/unit.h>
// #include <user\timer.h>
#include <windows.h>

class argstage : public Unit {

 protected:
   void    waitReady();
   virtual int toPluse(double  arg);   //  角度をパルス数に換算
   int posNow;
   int withWait;
   int spLow, spHigh, spAcc;

 public:
   enum {isWait, notWait};
   argstage(ifLine *iface);
   virtual ~argstage();
   void setHome();           //  機械原点復帰
   void moveTo(double arg);  //  所定の位置まで傾斜（角度、またはＧ）
   void moveToPulse(int pulse); // 傾斜をパルスで制御

   char *getName(void) { return "Arg Stage"; }
   void setSpeed(int low, int high, int acc); //  低速・高速・加速時間を
											  //  pps で設定する
   void setWait(int stat) { withWait = stat;}


   double minStepWrite()  { return 0; }
   double minStepRead()   { return 0; }

   int    getPosition()   { return posNow; }
};


class Gstage : public argstage { // 傾斜ステージをＧステージとして使う

  protected :
	int toPluse(double g); // Ｇをパルス数に換算

  public:
	Gstage(ifLine *iface) : argstage(iface) { }
	~Gstage() { }
};

#endif




