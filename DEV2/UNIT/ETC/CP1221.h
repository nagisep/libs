///////////////////////////////////////////////////////////
//  CP1221.h
//  Implementation of the Class CP1221
//  Created on:      2016/01/08 11:00:25
//  Original author: 015150900
///////////////////////////////////////////////////////////

#if !defined(_BCL_CP1221)
#define _BCL_CP1221

#include <windows.h>
#include <system.hpp>

#include <map>
#include <string>
#include "SLABCP2112.h"
#include "I2C.h"


class CP1221 : public I2C
{
private:
	/**
	 * 接続されているI/Fのデバイスリスト
	 * HID_SMBUS_DEVICE[Serial] の map 表現とする。
	 * 
	 * 最初のインスタンス精製時に、map を生成する。
	 * （open まで完了させておく）
	 */
     
     // deviceList を 配列で持つように変更する
     
     static DWORD numberOfDevice;
     static HID_SMBUS_DEVICE eachDevice[8];
     static std::string eachSerial[8];

    HID_SMBUS_DEVICE myDevice; // それぞれのインスタンスが使うデバイス

public:
	static void init(int addressLen);
	static void close();

	virtual ~CP1221() { }
	CP1221(int ch, std::string serial);

    void nRead(unsigned int addr, BYTE transBytes, char *result);
	void nWrite(unsigned int addr, BYTE transBytes, BYTE *data);

};
#endif // !defined(_BCL_CP1221)

