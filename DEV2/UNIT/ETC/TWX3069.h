/******************************************************************************
  TWX3069.h
  更新日:2008/1/30
  ファイルバージョン:1.1.3.1
  
  X-I16(P)用ヘッダーファイル
  USBM3069.DLL 3.1.0.1以降が必要です。

  Ver.1.1.3.1 TWX3069.DLL Ver.1.1.3.1 に対応

  テクノウェーブ株式会社 Copyright 2007-2008
*******************************************************************************/

#ifndef __TWX3069_H__
#define __TWX3069_H__

#ifndef __TW_BASE_TYPES__
#define __TW_BASE_TYPES__
typedef PVOID TW_HANDLE;
typedef ULONG TW_STATUS;



//>Ver.1.1.3.1
//初期化オプション
#define TWX_INIT_PORT_DATA 0x00000002
#define TWX_INIT_BUS       0x00000004
#define TWX_INIT_DMA       0x00000008
#define TWX_INIT_TIMER     0x00000010
#define TWX_INIT_AD        0x00000020
#define TWX_INIT_SCI       0x00000040
#define TWX_INIT_PC        0x00000080
#define TWX_INIT_TCPY      0x00000100
#define TWX_INIT_ALL       0xffffffff
//<Ver.1.1.3.1


//関数の戻り値
enum {
  TW_OK,                   //正常終了
  TW_INVALID_HANDLE,       //ハンドルが無効
  TW_DEVICE_NOT_FOUND,     //デバイスが見つからない
  TW_DEVICE_NOT_OPENED,
  TW_IO_ERROR,             //送受信中にエラーが発生した
  TW_INSUFFICIENT_RESOURCES,
  TW_INVALID_PARAMETER,
  TW_INVALID_BAUD_RATE,

  TW_DEVICE_NOT_OPENED_FOR_ERASE,
  TW_DEVICE_NOT_OPENED_FOR_WRITE,
  TW_FAILED_TO_WRITE_DEVICE,
  TW_EEPROM_READ_FAILED,
  TW_EEPROM_WRITE_FAILED,
  TW_EEPROM_ERASE_FAILED,
  TW_EEPROM_NOT_PRESENT,
  TW_EEPROM_NOT_PROGRAMMED,
  TW_INVALID_ARGS,         //引数が無効
  TW_NOT_SUPPORTED,        //サポートされない機能
  TW_OTHER_ERROR           //その他のエラー
};
enum{
  TW_TIMEOUT = 0xffff0001, //送信または受信処理がタイムアウト
  TW_FILE_ERROR,           //ファイルに関するエラー
  TW_MEMORY_ERROR,         //メモリの確保エラー
  TW_DATA_NOT_FOUND,       //有効なデータが無い
  TW_SOCKET_ERROR,         //Socket API のエラー
  TW_ACCESS_DENIED         //認証エラー
};

#endif //__TW_BASE_TYPES__

//製品タイプ
#define TWX_IF_USB     0x80000000
#define TWX_IF_LAN     0x40000000
#define TWX_IF_ANY     0xff000000

#define TWX_USBX_I16   0x80800000
#define TWX_LANX_I16   0x40800000
#define TWX_TYPE_I16   0x00800000
#define TWX_TYPE_ANY   0x00ffff00

#define TWX_ANY_DEVICE 0xffffff00

//パルスカウンタの定数
#define TWX_PC0 0x10
#define TWX_PC1 0x20
#define TWX_PC2 0x02
#define TWX_PC3 0x04
#define TWX_PC0_PC1 0x30
#define TWX_PC2_PC3 0x06

#define TWX_PC_DEFAULT 0
#define TWX_PC_2PHASE 1
#define TWX_PC_3PHASE 2

//ポートの定数
#define TWX_POUT 0xffffffff  //POUT
#define TWX_P1   0x00ffffd0  //P1
#define TWX_P2   0x00ffffd1  //P2
#define TWX_P4   0x00ffffd3  //P4
#define TWX_PA   0x00ffffd9  //PA


#ifdef __cplusplus
extern "C" {
TW_STATUS _stdcall TWX_Open(TW_HANDLE *phDev, long Number = 0, long ProductsType = TWX_ANY_DEVICE);
TW_STATUS _stdcall TWX_Close(TW_HANDLE hDev);
TW_STATUS _stdcall TWX_Initialize(TW_HANDLE hDev);
TW_STATUS _stdcall TWX_PortWrite(TW_HANDLE hDev, DWORD Port, BYTE Data, BYTE Mask = 0xff);
TW_STATUS _stdcall TWX_PortRead(TW_HANDLE hDev, DWORD Port, BYTE *pData);
TW_STATUS _stdcall TWX_ADRead(TW_HANDLE hDev, long CH, long *pData);
TW_STATUS _stdcall TWX_DAWrite(TW_HANDLE hDev, long CH, BYTE Data);
TW_STATUS _stdcall TWX_PCReadReg(TW_HANDLE hDev, BYTE *pReg);
TW_STATUS _stdcall TWX_PCWriteReg(TW_HANDLE hDev, BYTE Reg);
TW_STATUS _stdcall TWX_AnalogReadReg(TW_HANDLE hDev, BYTE *pReg);
TW_STATUS _stdcall TWX_AnalogWriteReg(TW_HANDLE hDev, BYTE Reg);
TW_STATUS _stdcall TWX_PCSetMode(TW_HANDLE hDev, long Mode, long CHBits);
TW_STATUS _stdcall TWX_PCStart(TW_HANDLE hDev, BYTE CHBits);
TW_STATUS _stdcall TWX_PCStop(TW_HANDLE hDev, BYTE CHBits);
TW_STATUS _stdcall TWX_PCReadCnt(TW_HANDLE hDev, long CHBits, long *pCnt);
TW_STATUS _stdcall TWX_PCSetCnt(TW_HANDLE hDev, long CHBits, long Cnt);
TW_STATUS _stdcall TWX_InitializeA(TW_HANDLE hDev, DWORD InitOption = TWX_INIT_ALL);
}
#else
TW_STATUS _stdcall TWX_Open(TW_HANDLE *phDev, long Number, long ProductsType);
TW_STATUS _stdcall TWX_Close(TW_HANDLE hDev);
TW_STATUS _stdcall TWX_Initialize(TW_HANDLE hDev);
TW_STATUS _stdcall TWX_PortWrite(TW_HANDLE hDev, DWORD Port, BYTE Data, BYTE Mask);
TW_STATUS _stdcall TWX_PortRead(TW_HANDLE hDev, DWORD Port, BYTE *pData);
TW_STATUS _stdcall TWX_ADRead(TW_HANDLE hDev, long CH, long *pData);
TW_STATUS _stdcall TWX_DAWrite(TW_HANDLE hDev, long CH, BYTE Data);
TW_STATUS _stdcall TWX_PCReadReg(TW_HANDLE hDev, BYTE *pReg);
TW_STATUS _stdcall TWX_PCWriteReg(TW_HANDLE hDev, BYTE Reg);
TW_STATUS _stdcall TWX_AnalogReadReg(TW_HANDLE hDev, BYTE *pReg);
TW_STATUS _stdcall TWX_AnalogWriteReg(TW_HANDLE hDev, BYTE Reg);
TW_STATUS _stdcall TWX_PCSetMode(TW_HANDLE hDev, long Mode, long CHBits);
TW_STATUS _stdcall TWX_PCStart(TW_HANDLE hDev, BYTE CHBits);
TW_STATUS _stdcall TWX_PCStop(TW_HANDLE hDev, BYTE CHBits);
TW_STATUS _stdcall TWX_PCReadCnt(TW_HANDLE hDev, long CHBits, long *pCnt);
TW_STATUS _stdcall TWX_PCSetCnt(TW_HANDLE hDev, long CHBits, long Cnt);
TW_STATUS _stdcall TWX_InitializeA(TW_HANDLE hDev, DWORD InitOption);
#endif

//****** 以下の行は VisualStudio の IntelliSense のために追加されています *****
//****** 必要がない場合にはコメントアウトしてください                     *****
#ifdef __USBM_INTELLISENSE__
TW_STATUS _stdcall TWX_Open(TW_HANDLE *phDev, long Number = 0, long ProductsType = TWX_TYPE_ANY){ return 0; } //デバイスをオープンします
/******************************************************************************
  TW_STATUS TWX_Open(TW_HANDLE *phDev, long Number, long ProductsType)

  phDev        : 取得したハンドルの格納先
  Number       : 接続する製品の番号
  ProductsType : 接続する製品のタイプを以下の値で指定します
                 TWX_IF_USB     インタフェースがUSBの製品
                 TWX_USBX_I16   USBX-I16またはUSBX-I16P
                 TWX_ANY_DEVICE オープン可能な製品全てと一致

  デバイスに接続します。成功すると phDev にデバイスへのハンドルが格納されます
*****************************************************************************/

TW_STATUS _stdcall TWX_Close(TW_HANDLE hDev){ return 0; } //デバイスをクローズします
/******************************************************************************
  TW_STATUS TWX_Close(TW_HANDLE hDev)

  hDev : デバイスのハンドル

  ハンドルをクローズし、デバイスへのアクセスを終了します。
*****************************************************************************/

TW_STATUS _stdcall TWX_Initialize(TW_HANDLE hDev){ return 0; } //デバイスを初期化します
/******************************************************************************
  TW_STATUS TWX_Initialize(TW_HANDLE hDev)

  hDev : デバイスのハンドル

  デバイスの初期化を行います。未対応の製品の場合TW_NOT_SUPPORTEDが返ります。
*****************************************************************************/

TW_STATUS _stdcall TWX_PortWrite(TW_HANDLE hDev, DWORD Port, BYTE Data, BYTE Mask = 0xff){ return 0; } //出力ポートに書き込みます
/******************************************************************************
  TW_STATUS TWX_PortWrite(TW_HANDLE hDev, DWORD Port, BYTE Data, BYTE Mask)

  hDev  : デバイスのハンドル
  Port  : 出力するポート
          TWX_P4   : P40-P47を操作
          TWX_PA   : PA0-PA7を操作
          TWX_POUT : POUT0-POUT7を操作
  Data  : 出力データ
  Mask  : 操作するビットを指定するマスク

  出力ポートの出力値を変更します。特定のビットだけを操作する場合にはMaskに値を
  設定してください。例えばP47だけを操作する場合、Mask = 0x80 とします。
*****************************************************************************/

TW_STATUS _stdcall TWX_PortRead(TW_HANDLE hDev, DWORD Port, BYTE *pData){ return 0; } //入力ポートから読み出します
/******************************************************************************
  TW_STATUS TWX_PortRead(TW_HANDLE hDev, DWORD Port, BYTE *pData)

  hDev  : デバイスのハンドル
  Port  : 出力するポート
          TWX_P1 : P10-P17から入力
          TWX_P2 : P20-P27から入力
  pData : 入力データの格納先

  入力ポートの入力値を読みます。
*****************************************************************************/

TW_STATUS _stdcall TWX_ADRead(TW_HANDLE hDev, long CH, long *pData){ return 0; } //AD変換結果を読み出します
/******************************************************************************
  TW_STATUS TWX_ADRead(TW_HANDLE hDev, long CH, long *pData)

  hDev  : デバイスのハンドル
  CH    : 入力するチャンネル(0,1,2,3)
  pData : 入力データの格納先

  指定チャンネルをAD変換した結果を読み出します。変換結果は0〜1023までの値とな
  ります。
*****************************************************************************/

TW_STATUS _stdcall TWX_DAWrite(TW_HANDLE hDev, long CH, BYTE Data){ return 0; } //DAC出力値を設定します
/******************************************************************************
  TW_STATUS TWX_DAWrite(TW_HANDLE hDev, long CH, BYTE Data)

  hDev  : デバイスのハンドル
  CH    : 出力するチャンネル(0,1)
  Data  : 出力するデータ

  指定チャンネルをDAコンバータの出力値を変更します。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCReadReg(TW_HANDLE hDev, BYTE *pReg){ return 0; } //パルスカウンタの設定レジスタを読み出します
/******************************************************************************
  TW_STATUS TWX_PCReadReg(TW_HANDLE hDev, BYTE *pReg)

  hDev : デバイスのハンドル
  pReg : レジスタ値の格納先

  パルスカウンタの設定用レジスタの値を読み出します。レジスタ値の意味は
  TWX_PCWriteReg()の説明を参照してください。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCWriteReg(TW_HANDLE hDev, BYTE Reg){ return 0; } //パルスカウンタの設定レジスタに書き込みます
/******************************************************************************
  TW_STATUS TWX_PCWriteReg(TW_HANDLE hDev, BYTE Reg)

  hDev : デバイスのハンドル
  Reg  : レジスタ値。ビットの意味は以下。
         ビット0 : PC0は立ち上がり("OFF"→"ON")をカウントします。
         ビット1 : PC0は立ち下がり("ON"→"OFF")をカウントします。
         ビット2 : PC1は立ち上がり("OFF"→"ON")をカウントします。
         ビット3 : PC1は立ち下がり("ON"→"OFF")をカウントします。
         ビット4 : PC2は立ち上がり("OFF"→"ON")をカウントします。
         ビット5 : PC2は立ち下がり("ON"→"OFF")をカウントします。
         ビット6 : PC3は立ち上がり("OFF"→"ON")をカウントします。
         ビット7 : PC3は立ち下がり("ON"→"OFF")をカウントします。

  パルスカウンタの設定用レジスタに値を書き込みます。このレジスタはパルスカウン
  タのカウントエッジを指定します。初期設定では全て0になっています。
*****************************************************************************/

TW_STATUS _stdcall TWX_AnalogReadReg(TW_HANDLE hDev, BYTE *pReg){ return 0; } //アナログ入出力の設定レジスタを読み出します
/******************************************************************************
  TW_STATUS TWX_AnalogReadReg(TW_HANDLE hDev, BYTE *pReg)

  hDev : デバイスのハンドル
  pReg : レジスタ値の格納先

  アナログ入出力の設定用レジスタの値を読み出します。レジスタ値の意味は
  TWX_AnalogWriteReg()の説明を参照してください。
*****************************************************************************/

TW_STATUS _stdcall TWX_AnalogWriteReg(TW_HANDLE hDev, BYTE Reg){ return 0; } //アナログ入出力の設定レジスタに書き込みます
/******************************************************************************
  TW_STATUS TWX_AnalogWriteReg(TW_HANDLE hDev, BYTE Reg)

  hDev : デバイスのハンドル
  pReg : レジスタ値。ビットの意味は以下。
         ビット0 : 0のときDA0がユニポーラ出力。1のときバイポーラ出力
         ビット1 : 0のときDA1がユニポーラ出力。1のときバイポーラ出力
         ビット2 : 0のときAD0がユニポーラ入力。1のときバイポーラ入力
         ビット3 : 0のときAD1がユニポーラ入力。1のときバイポーラ入力
         ビット4 : 0のときADTRIGの立ち下がり､1のとき立ち上がりがトリガ入力
         ビット5-7 : 予約。無視されます。

  アナログ入出力の設定用レジスタに値を書き込みます。このレジスタでアナログチャ
  ンネルのユニポーラ／バイポーラ設定、ADTRIG信号の極性設定を行います。
  ユニポーラ設定では0〜5V、バイポーラ設定では-2.5〜2.5Vの範囲の入出力となりま
  す。AD2,AD3は設定に無関係にユニポーラ入力です。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCSetMode(TW_HANDLE hDev, long Mode, long CHBits){ return 0; } //パルスカウンタのカウント方法を設定します
/******************************************************************************
  TW_STATUS TWX_PCSetMode(TW_HANDLE hDev, long Mode, long CH)

  hDev   : デバイスのハンドル
  Mode   : カウントモード
           TWX_PC_DEFAULT : 初期状態に戻します。各チャンネルは独立です
           TWX_PC_2PHASE  : 2相のエンコーダ信号をカウントします
           TWX_PC_3PHASE  : 2相のエンコーダ信号をカウントし、Z信号でクリア
  CHBits : 使用チャンネル(Mode = TWX_PC_2PHASEのときのみ有効)
           TWX_PC0_PC1 : PC0とPC1を使用してカウントします。
           TWX_PC2_PC3 : PC2とPC3を使用してカウントします。

  パルスカウンタを2相のエンコーダー信号(A相、B相)をカウントできるように設定し
  ます。CHで指定したそれぞれの端子にA相、B相信号を入力してください。
  
  ModeにTWX_PC_3PHASEを指定すると、PC0がカウンタクリア、PC2とPC3で2相カウント
  する設定になります。PC2、PC3にA相、B相信号を入力し、PC0にZ相信号を入力する
  と1周毎にPC2とPC3のカウンタがクリアされ、PC0のカウンタがカウントアップします
*****************************************************************************/

TW_STATUS _stdcall TWX_PCStart(TW_HANDLE hDev, BYTE CHBits){ return 0; } //パルスカウンタをスタートします
/******************************************************************************
  TW_STATUS TWX_PCStart(TW_HANDLE hDev, BYTE CHBits)

  hDev   : デバイスのハンドル
  CHBits : パルスカウンタのチャンネル
           TWX_PC0_PC1 : PC0とPC1をスタート
           TWX_PC2_PC3 : PC2とPC3をスタート
           TWX_PC0     : PC0をスタート
           TWX_PC1     : PC1をスタート
           TWX_PC2     : PC2をスタート
           TWX_PC3     : PC3をスタート

  指定チャンネルのパルスカウンタの計数をスタートさせます。CHBitsに指定する定数
  はORで結合することができます。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCStop(TW_HANDLE hDev, BYTE CHBits){ return 0; } //パルスカウンタをストップします
/******************************************************************************
  TW_STATUS TWX_PCStop(TW_HANDLE hDev, BYTE CHBits)

  hDev   : デバイスのハンドル
  CHBits : パルスカウンタのチャンネル
           TWX_PC0_PC1 : PC0とPC1をストップ
           TWX_PC2_PC3 : PC2とPC3をストップ
           TWX_PC0     : PC0をストップ
           TWX_PC1     : PC1をストップ
           TWX_PC2     : PC2をストップ
           TWX_PC3     : PC3をストップ

  指定チャンネルのパルスカウンタの計数をストップします。CHBitsに指定する定数
  はORで結合することができます。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCReadCnt(TW_HANDLE hDev, long CHBits, long *pCnt){ return 0; } //パルスカウンタの値を読み出します
/******************************************************************************
  TW_STATUS TWX_PCReadCnt(TW_HANDLE hDev, long CHBits, long *pCnt)

  hDev   : デバイスのハンドル
  CHBits : パルスカウンタのチャンネル
           TWX_PC0_PC1 : PC0とPC1で計数した値
           TWX_PC2_PC3 : PC2とPC3で計数した値
           TWX_PC0     : PC0のカウンタ値
           TWX_PC1     : PC1のカウンタ値
           TWX_PC2     : PC2のカウンタ値
           TWX_PC3     : PC3のカウンタ値
  pCnt   : カウント値の格納先

  指定チャンネルのパルスカウンタの値を読み出します。チャンネルは番号ではなく
  定数で指定しますので、ご注意してください。
*****************************************************************************/

TW_STATUS _stdcall TWX_PCSetCnt(TW_HANDLE hDev, long CHBits, long Cnt){ return 0; } //パルスカウンタの値を設定します
/******************************************************************************
  TW_STATUS _stdcall TWX_PCSetCnt(TW_HANDLE hDev, long CHBits, long Cnt)

  hDev   : デバイスのハンドル
  CHBits : パルスカウンタのチャンネル
           TWX_PC0_PC1 : PC0とPC1で計数した値
           TWX_PC2_PC3 : PC2とPC3で計数した値
           TWX_PC0     : PC0のカウンタ値
           TWX_PC1     : PC1のカウンタ値
           TWX_PC2     : PC2のカウンタ値
           TWX_PC3     : PC3のカウンタ値
  Cnt    : セットする値

  指定チャンネルのパルスカウンタに値をセットします。クリアする場合はCntを0とし
  て呼び出します。チャンネルは番号ではなく定数で指定しますのでご注意ください。
*****************************************************************************/

TW_STATUS _stdcall TWX_InitializeA(TW_HANDLE hDev, DWORD InitOption){ return 0; } //機能を指定して初期化します
/******************************************************************************
  TW_STATUS _stdcall TWX_InitializeA(TW_HANDLE hDev, DWORD InitOption)

  hDev : デバイスのハンドル
  InitOption : 初期化する機能を指定。以下をORで結合します。
               TWX_INIT_PORT_DATA(0x0002) : ポートのデータを初期化
               TWX_INIT_BUS(0x0004)       : 外部バス設定を初期化
               TWX_INIT_DMA(0x0008)       : DMAを初期化
               TWX_INIT_TIMER(0x0010)     : 16ビットタイマを初期化
               TWX_INIT_AD(0x0020)        : ADコンバータを初期化
               TWX_INIT_SCI(0x0040)       : シリアルポートを初期化
               TWX_INIT_PC(0x0080)        : パルスカウンタを初期化
               TWX_INIT_TCPY(0x0100)      : タイマコピーを初期化
               TWX_INIT_ALL(0xffffffff)   : 全ての機能を初期化

  デバイスの初期化を行います。InitOptionで初期化する機能を指定できます。
  ただし、アナログ入出力の設定用レジスタとパルスカウンタの設定用レジスタは影響
  を受けません。
  未対応の製品の場合TW_NOT_SUPPORTEDが返ります。

  ※この関数はVer.3.2.1以降のファームウェアが必要です。
*****************************************************************************/

#endif //__USBM_INTELLISENSE__

#endif