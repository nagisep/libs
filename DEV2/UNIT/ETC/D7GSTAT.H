// 評価基盤の情報を元に　ｄ７ｇの発信周波数を読みとる
// 評価ボードは、10ms 間のパルス数を返すので、
// 関数内部で周波数に変換する

#if !define(_BCL_UnitEtcD7g)
#define     _BCL_UnitEtcD7g

class d7gstat {

   public:
	 d7gStat():
	 double xRead();
	 double yRead() { return 0.; }
}

#endif


