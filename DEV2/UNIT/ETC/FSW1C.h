
// 小杉技研・フットスイッチ
// FSW1C

#if !defined(_BCL_UNIT_ETC_FSW1C)
#define      _BCL_UNIT_ETC_FSW1C

#include <U:\DEV2\IFLINE\RSLINE\winrs.h>

class FSW1C
{

   private:
     WinRS *myPort;

	 volatile bool nowON;     // 現状のスイッチ状態を監視する
					 // スイッチは、make 信号のみ返すモードを基準とする。
					 // breka 信号は（あっても）無視する。

	 void get();      // stat と、スイッチ信号から現在の ON/OFF を判定する。
                     // 結果は、nowON に反映させる


   public:

	 FSW1C(WinRS *port);
	 ~FSW1C();

	 operator void *();

     void clear();   // スイッチ情報を OFF にする
	 bool isON();    // 前回の呼び出し or clear() 呼び出し以降にスイッチが ON したかどうかを問い合わせる
     void wait();    // スイッチが OFF -> ON に変化するまで待つ

};

#endif


