
#include "CP1221.h"

const WORD vID = 0x10c4;
const WORD pID = 0xea90;

DWORD             CP1221::numberOfDevice;
HID_SMBUS_DEVICE CP1221::eachDevice[8];
std::string      CP1221::eachSerial[8];

void CP1221::init(int addrLen)
{
  HID_SMBUS_STATUS func_result;
  HID_SMBUS_DEVICE aDevice;
  BYTE len;
  char strSerial[128];
  HidSmbus_GetNumDevices(&numberOfDevice, vID, pID);
  for(DWORD i = 0; i < numberOfDevice; i++)
  {
 	func_result = HidSmbus_Open(&eachDevice[i],  i,  vID, pID);
 	func_result = HidSmbus_GetSerialString(eachDevice[i], strSerial, &len);
    eachSerial[i] = strSerial;
  }
  addressLen = addrLen;
}

void CP1221::close()
{
  HID_SMBUS_STATUS func_result;

  for(DWORD i = 0; i < numberOfDevice; i++)
  {
	func_result = HidSmbus_Close(eachDevice[i]);
  }
}

CP1221::CP1221(int ch, std::string serial):I2C(ch)
{
  for(DWORD i = 0; i < numberOfDevice; i++)
  {
    if(serial == eachSerial[i])
    {
      myDevice = eachDevice[i];
      return;
    }
  }

  myDevice = 0;
}


void CP1221::nRead(unsigned int addr, BYTE transBytes, char *result)
{
  HID_SMBUS_STATUS func_result;

  HID_SMBUS_S0 s0_status = 0;
  BYTE readNum;

  BYTE tergetAddress[16];
  makeAddr(addr, tergetAddress);
  func_result = HidSmbus_AddressReadRequest(myDevice, myCha * 2, transBytes, addressLen, tergetAddress);
  func_result = HidSmbus_ForceReadResponse(myDevice, transBytes);
  func_result = HidSmbus_GetReadResponse(myDevice, &s0_status, result, 61, &readNum);
}


void CP1221::nWrite(unsigned int addr, BYTE transBytes, BYTE *data)
{
  HID_SMBUS_STATUS func_result;

  BYTE otBuffer[128];
  makeAddr(addr, otBuffer); // この結果、[0] から [addressLen - 1] までにアドレスが入って帰ってくる
  for(int i = 0; i < transBytes; i++)
    otBuffer[addressLen + i] = data[i]; // 最初のデータは otBuffer[addressLen に入れば良い

  transBytes += addressLen;             // アドレス幅の分を送信データに含める
  func_result = HidSmbus_WriteRequest (myDevice, myCha * 2, otBuffer, transBytes);
}


