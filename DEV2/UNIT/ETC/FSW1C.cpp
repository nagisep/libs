
#include <string>
#include "FSW1C.h"

FSW1C::FSW1C(WinRS *port) : myPort(port)
{
  nowON = false;
}

FSW1C::~FSW1C()
{
  delete myPort;
}

void FSW1C::get()
{
   char buff[128];
   unsigned int len;

   while(myPort->loc()) // 信号がある……前回呼び出しからの複数回踏み込みに対応して、最後まで動きを調べる
   {
	 myPort->listen(buff, len);
	 if (static_cast<std::string>(buff) == "FS1ON ") // ON 信号検出
	   nowON = true;
   }
}

void FSW1C::clear()
{
   nowON = false;
}

void FSW1C::wait()
{
   while(! nowON)
      get();

   nowON = false;
}

bool FSW1C::isON()
{
   get();
   bool wk = nowON;
   nowON = false;   // 一度問い合わせがあったら、 OFF にしておく。
   return wk;
}

FSW1C::operator void *()
{
   return (myPort) ? this : 0;
}


