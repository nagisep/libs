#if !defined(_BCL_UNIT_ETC_PIO1616B)
#define      _BCL_UNIT_ETC_PIO1616B
#include <conio.h>

class pio1616b
{  private:
	 unsigned int address;

   public:
	 pio1616b(unsigned int addr = 0x01d0)
	 { address = addr; }

	 int read(int ch = 0)
	 {  int result = 0;

		result = inp(address + ch);
		result &= 0x00ff;
		return result;
	 }

	 unsigned int readWord()
	 {  unsigned int result;
		return (read(1) * 0x100 + read(0));
	 }

	 void write(int value, int ch = 0)
	 {  outp(address + ch, value); }

};

#endif
