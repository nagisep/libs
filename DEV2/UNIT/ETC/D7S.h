///////////////////////////////////////////////////////////
//  D7S.h
//  Implementation of the Class D7S
//  Created on:      2016/01/08 11:00:55
//  Original author: 015150900
///////////////////////////////////////////////////////////

#if !defined(_BCL_D7S)
#define _BCL_D7S

#include <string>

#include "CP1221.h"

/** 形D7Sを表すクラス  */
class D7S
{
private:
	I2C * port;

public:
	virtual ~D7S();
//	D7S(int ch, std::string serial);
	D7S(int ch, char *strSerial);

	D7S();


	void intoTestMode();
	void getGal(double *galSet);
	void leaveTestMode();
    
    unsigned int getValue(unsigned int portAddr);
    
    unsigned int getDelayTime()
    {
       return getValue(0x5035);
    }
    
    unsigned int getSerial()
    {
       unsigned int serial;
       serial = getValue(0x6004) * 0x100;
       serial += getValue(0x6005);
       return serial;
    }

    unsigned int getTypeID()
    {
       return getValue(0x6008);
    }


};
#endif // !defined(_BCL_D7S)
