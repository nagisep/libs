#include <j:/dev2/unit/unit.h>
#include <stdlib.h>
#include <stdio.h>

#include <fstream>
#include <J:/DEV2/IFLINE/RSLINE/winrs.h>

const int waitTime = 600;

class sec_v111 : public Unit
{
   private:
	 char sendBuff[128];
     char recvBuff[128];

     ifLine *relayPort;
     int  myAddr;

     void readLine(char *buff)
     { // パケットの読みこみ（改行がないので、ここで１行に整形する）
       char c;
       char bcc;
       int  rPtr = 0;

       WinRS *port = dynamic_cast<WinRS *>(myIface);
       

       while (1)
       {
         while(! port->loc()); // buff 入力待ち
         c = port->getc1();

         if (c == '\x02')
         {
           rPtr = 0;
           continue;
         }

         if (c == '\x03') // ETX 
         {
           c = '\0';      // ETX が入るべき場所をターミネータに置き換えるため
           break;
         }
         buff[rPtr++] = c;
       }
       while(! port->loc()); // buff 入力待ち
       bcc = port->getc1();
     }

     void writeLine(char *buff)
     {
        // この時点で、buff には、コマンド＋パラメータのみ設定されているものとする

        WinRS *port = dynamic_cast<WinRS *>(myIface);

        char bcc = 0;
        
        for(int i = 0; buff[i] != 0; bcc += buff[i++]);
        bcc += 3;
        bcc %= 128;

        std::sprintf(sendBuff, "@%02d\x02" "%s\x03%c\x03\x03\x03\x03\x03\x03\x03\x03\x03", myAddr, buff, bcc);
        // コマンドの最終に ETX が９個付加されているのは、BCC が @ or * になったときの対策

        for(int i = 0; sendBuff[i] != '\0'; port->putc1(sendBuff[i++]));
     }


   public:

     sec_v111(ifLine *iface, int addr = 1, ifLine *relay = 0) : Unit(iface)
     { 
       // realy ポートは、流量０の時に、流路を閉じるリレーの制御用ポート（オプション）
       myAddr = addr;
       relayPort = relay;
     }

	 virtual double minStepWrite()         { return 0; }
	 virtual double minStepRead()          { return 0; }
     ~sec_v111()                           
    {
      delete relayPort;  // iface は、~UNIT() で削除される。
                     // 自力で確保したものだけを削除する
    }

	 double read()
	 {
        char buff[128];
        writeLine("RFV");
        Sleep(waitTime);
        readLine(buff);
        Sleep(waitTime);
        return std::strtod(buff, 0);
	 }

     void write(double flow)
     {
        char buff[128];
        std::sprintf(buff, "AFC%f,D", flow);
        writeLine(buff);
        Sleep(waitTime);
        readLine(buff);
        Sleep(waitTime);

        if (relayPort != 0)
        {
          char buff[64];
          unsigned int len = 64;

          if (flow > 0.1)
          {
            relayPort->talk("PCR00"); // 電磁弁解放
 //           relayPort->listen(buff, len);
          }
          else
          {
            relayPort->talk("PCR01"); // 電磁弁閉鎖
 //           relayPort->listen(buff, len);
          }
        }
     }

};






