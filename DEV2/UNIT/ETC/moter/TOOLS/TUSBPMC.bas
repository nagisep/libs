Attribute VB_Name = "TUSBPMC"
Declare Function Tusbpmc_Device_Open Lib "USBPMC.DLL" (ByVal id As Integer) As Integer
Declare Sub Tusbpmc_Device_Close Lib "USBPMC.DLL" (ByVal id As Integer)
Declare Function Tusbpmc_Set_Exec Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal strtSpeed As Integer, ByVal cnstSpeed As Integer, ByVal accRate As Integer, ByVal freqRange As Integer) As Integer
Declare Function Tusbpmc_Get_PmcStatus Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByRef strtSpeed As Integer, ByRef cnstSpeed As Integer, ByRef accRate As Integer, ByRef freqRange As Integer) As Integer
Declare Function Tusbpmc_Sel_Pout Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal sel As Integer) As Integer
Declare Function Tusbpmc_Sel_Encoder Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal sel As Integer, ByVal mode As Integer) As Integer
Declare Function Tusbpmc_Set_Slowdown Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal sel As Integer, ByVal dat As Long) As Integer
Declare Function Tusbpmc_Set_Limitmode Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal dat As Integer) As Integer
Declare Function Tusbpmc_Get_LimitStatus Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByRef status As Integer) As Integer
Declare Function Tusbpmc_Get_Status Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByRef status As Integer) As Integer
Declare Function Tusbpmc_Start_Control Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal dir As Integer, ByVal mode As Integer, ByVal IndexData As Long) As Integer
Declare Function Tusbpmc_Stop_Control Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal mode As Integer) As Integer
Declare Function Tusbpmc_Set_Sync Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal onoff As Integer) As Integer
Declare Function Tusbpmc_Hold_Off Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal onoff As Integer) As Integer
Declare Function Tusbpmc_Set_AddrCounter Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByVal dat As Long) As Integer
Declare Function Tusbpmc_Get_AddrCounter Lib "USBPMC.DLL" (ByVal id As Integer, ByVal ch As Integer, ByRef count As Long) As Integer
Declare Function Tusbpmc_Set_Unit Lib "USBPMC.DLL" (ByVal id As Integer, ByVal onoff As Integer) As Integer
