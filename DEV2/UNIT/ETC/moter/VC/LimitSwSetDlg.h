#if !defined(AFX_LIMITSWSETDLG_H__E61493C4_658D_11D5_935E_006008B03B0A__INCLUDED_)
#define AFX_LIMITSWSETDLG_H__E61493C4_658D_11D5_935E_006008B03B0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LimitSwSetDlg.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CLimitSwSetDlg ダイアログ

class CLimitSwSetDlg : public CDialog
{
// コンストラクション
public:
	short m_BitPattern;
	CLimitSwSetDlg(CWnd* pParent = NULL);   // 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CLimitSwSetDlg)
	enum { IDD = IDD_DIALOG1 };
	int		m_Bit0;
	int		m_Bit1;
	int		m_Bit2;
	int		m_Bit3;
	int		m_Bit4;
	int		m_Bit5;
	int		m_Bit6;
	int		m_Bit7;
	int		m_Bit8;
	int		m_Bit9;
	//}}AFX_DATA


// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CLimitSwSetDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV サポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CLimitSwSetDlg)
	virtual void OnOK();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_LIMITSWSETDLG_H__E61493C4_658D_11D5_935E_006008B03B0A__INCLUDED_)
