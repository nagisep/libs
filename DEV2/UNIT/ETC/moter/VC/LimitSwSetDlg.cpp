// LimitSwSetDlg.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "vcsample.h"
#include "LimitSwSetDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLimitSwSetDlg ダイアログ


CLimitSwSetDlg::CLimitSwSetDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLimitSwSetDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLimitSwSetDlg)
	m_Bit0 = -1;
	m_Bit1 = -1;
	m_Bit2 = -1;
	m_Bit3 = -1;
	m_Bit4 = -1;
	m_Bit5 = -1;
	m_Bit6 = -1;
	m_Bit7 = -1;
	m_Bit8 = -1;
	m_Bit9 = -1;
	//}}AFX_DATA_INIT
}


void CLimitSwSetDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLimitSwSetDlg)
	DDX_Radio(pDX, IDC_B0_0, m_Bit0);
	DDX_Radio(pDX, IDC_B1_0, m_Bit1);
	DDX_Radio(pDX, IDC_B2_0, m_Bit2);
	DDX_Radio(pDX, IDC_B3_0, m_Bit3);
	DDX_Radio(pDX, IDC_B4_0, m_Bit4);
	DDX_Radio(pDX, IDC_B5_0, m_Bit5);
	DDX_Radio(pDX, IDC_B6_0, m_Bit6);
	DDX_Radio(pDX, IDC_B7_0, m_Bit7);
	DDX_Radio(pDX, IDC_B8_0, m_Bit8);
	DDX_Radio(pDX, IDC_B9_0, m_Bit9);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLimitSwSetDlg, CDialog)
	//{{AFX_MSG_MAP(CLimitSwSetDlg)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLimitSwSetDlg メッセージ ハンドラ

void CLimitSwSetDlg::OnOK() 
{
	UpdateData(TRUE);
	m_BitPattern = m_Bit0;
	m_BitPattern += m_Bit1 << 1;
	m_BitPattern += m_Bit2 << 2;
	m_BitPattern += m_Bit3 << 3;
	m_BitPattern += m_Bit4 << 4;
	m_BitPattern += m_Bit5 << 5;
	m_BitPattern += m_Bit6 << 6;
	m_BitPattern += m_Bit7 << 7;
	m_BitPattern += m_Bit8 << 8;
	m_BitPattern += m_Bit9 << 9;
	CDialog::OnOK();
}

int CLimitSwSetDlg::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_Bit0 = m_BitPattern & 0x01;
	m_Bit1 = ( m_BitPattern >> 1 ) & 0x01;
	m_Bit2 = ( m_BitPattern >> 2 ) & 0x01;
	m_Bit3 = ( m_BitPattern >> 3 ) & 0x01;
	m_Bit4 = ( m_BitPattern >> 4 ) & 0x01;
	m_Bit5 = ( m_BitPattern >> 5 ) & 0x01;
	m_Bit6 = ( m_BitPattern >> 6 ) & 0x01;
	m_Bit7 = ( m_BitPattern >> 7 ) & 0x01;
	m_Bit8 = ( m_BitPattern >> 8 ) & 0x01;
	m_Bit9 = ( m_BitPattern >> 9 ) & 0x01;
	return 0;
}
