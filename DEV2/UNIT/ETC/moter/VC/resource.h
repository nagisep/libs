//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by vcsample.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_VCSAMPLE_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDC_DEV_OPEN                    1000
#define IDC_DEV_CLOSE                   1001
#define IDC_DEV_ID                      1002
#define IDC_START_SPEED                 1003
#define IDC_CONST_SPEED                 1004
#define IDC_ACC_RATE                    1005
#define IDC_FREQ_RATE                   1006
#define IDC_PARAM_SET                   1007
#define IDC_PARAM_READ                  1008
#define IDC_PULSE_CHECK                 1009
#define IDC_ENCODER_CHECK               1010
#define IDC_INPUT_SEL1                  1011
#define IDC_SLOW_DOWN_POINT             1012
#define IDC_INPUT_SEL2                  1013
#define IDC_INPUT_SEL3                  1014
#define IDC_INPUT_SEL4                  1015
#define IDC_SLOWDOWN_CHECK              1016
#define IDC_LIMIT_SET                   1017
#define IDC_LIMIT_READ                  1018
#define IDC_CONT_START                  1019
#define IDC_CW_CCW_SEL1                 1020
#define IDC_CW_CCW_SEL2                 1021
#define IDC_CONT_TYPE1                  1022
#define IDC_CONT_TYPE2                  1023
#define IDC_INDEX_DATA                  1024
#define IDC_STOP                        1025
#define IDC_SLOW_STOP                   1026
#define IDC_SYNC_SET                    1027
#define IDC_HOLD_OFF                    1028
#define IDC_ADD_COUNTER                 1029
#define IDC_ADD_WRITE                   1030
#define IDC_ADD_READ                    1031
#define IDC_SYNC                        1032
#define IDC_CH_TAB                      1034
#define IDC_START_SPEED_MON             1035
#define IDC_CONST_SPEED_MON             1036
#define IDC_ACC_RATE_MON                1037
#define IDC_FREQ_RATE_MON               1038
#define IDC_ADD_COUNTER_MON             1039
#define IDC_B9_0                        1044
#define IDC_B9_1                        1046
#define IDC_B8_0                        1047
#define IDC_B8_1                        1048
#define IDC_READ_STATUS                 1048
#define IDC_B7_0                        1049
#define IDC_B7_1                        1050
#define IDC_B6_0                        1051
#define IDC_B6_1                        1052
#define IDC_B5_0                        1053
#define IDC_B5_1                        1054
#define IDC_B1_0                        1055
#define IDC_B1_1                        1056
#define IDC_B0_0                        1057
#define IDC_B0_1                        1058
#define IDC_B4_0                        1059
#define IDC_B4_1                        1060
#define IDC_B3_0                        1061
#define IDC_B3_1                        1062
#define IDC_B2_0                        1063
#define IDC_B2_1                        1064

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1050
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
