; CLW ファイルは MFC ClassWizard の情報を含んでいます。

[General Info]
Version=1
LastClass=CVcsampleDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "vcsample.h"

ClassCount=4
Class1=CVcsampleApp
Class2=CVcsampleDlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_VCSAMPLE_DIALOG
Class4=CLimitSwSetDlg
Resource4=IDD_DIALOG1

[CLS:CVcsampleApp]
Type=0
HeaderFile=vcsample.h
ImplementationFile=vcsample.cpp
Filter=N

[CLS:CVcsampleDlg]
Type=0
HeaderFile=vcsampleDlg.h
ImplementationFile=vcsampleDlg.cpp
Filter=D
LastObject=IDC_READ_STATUS
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=vcsampleDlg.h
ImplementationFile=vcsampleDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_VCSAMPLE_DIALOG]
Type=1
Class=CVcsampleDlg
ControlCount=48
Control1=IDC_DEV_OPEN,button,1342242816
Control2=IDC_DEV_CLOSE,button,1342242816
Control3=IDC_DEV_ID,combobox,1344340227
Control4=IDC_START_SPEED,edit,1350631552
Control5=IDC_CONST_SPEED,edit,1350631552
Control6=IDC_ACC_RATE,edit,1350631552
Control7=IDC_FREQ_RATE,edit,1350631552
Control8=IDC_START_SPEED_MON,static,1342312960
Control9=IDC_CONST_SPEED_MON,static,1342312960
Control10=IDC_ACC_RATE_MON,static,1342312960
Control11=IDC_FREQ_RATE_MON,static,1342312960
Control12=IDC_STATIC,static,1342308864
Control13=IDC_STATIC,static,1342308864
Control14=IDC_STATIC,static,1342308864
Control15=IDC_STATIC,static,1342308864
Control16=IDC_PARAM_SET,button,1342242816
Control17=IDC_PARAM_READ,button,1342242816
Control18=IDC_PULSE_CHECK,button,1342242819
Control19=IDC_ENCODER_CHECK,button,1342242819
Control20=IDC_INPUT_SEL1,button,1342308361
Control21=IDC_INPUT_SEL2,button,1342177289
Control22=IDC_INPUT_SEL3,button,1342177289
Control23=IDC_INPUT_SEL4,button,1342242825
Control24=IDC_SLOWDOWN_CHECK,button,1342242819
Control25=IDC_SLOW_DOWN_POINT,edit,1350631552
Control26=IDC_LIMIT_SET,button,1342242816
Control27=IDC_LIMIT_READ,button,1342242816
Control28=IDC_CONT_START,button,1342242816
Control29=IDC_CW_CCW_SEL1,button,1342308361
Control30=IDC_CW_CCW_SEL2,button,1342242825
Control31=IDC_CONT_TYPE1,button,1342308361
Control32=IDC_CONT_TYPE2,button,1342177289
Control33=IDC_INDEX_DATA,edit,1350631552
Control34=IDC_STATIC,static,1342308864
Control35=IDC_STOP,button,1342242816
Control36=IDC_SLOW_STOP,button,1342242816
Control37=IDC_SYNC_SET,button,1342242819
Control38=IDC_HOLD_OFF,button,1342242819
Control39=IDC_STATIC,button,1342177287
Control40=IDC_STATIC,button,1342177287
Control41=IDC_ADD_COUNTER,edit,1350631552
Control42=IDC_ADD_WRITE,button,1342242816
Control43=IDC_ADD_READ,button,1342242816
Control44=IDC_ADD_COUNTER_MON,static,1342312960
Control45=IDC_STATIC,static,1342308352
Control46=IDC_SYNC,button,1342242819
Control47=IDC_CH_TAB,SysTabControl32,1342177280
Control48=IDC_READ_STATUS,button,1342242816

[DLG:IDD_DIALOG1]
Type=1
Class=CLimitSwSetDlg
ControlCount=32
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_B9_0,button,1342308361
Control4=IDC_B9_1,button,1342177289
Control5=IDC_STATIC,static,1342308864
Control6=IDC_B8_0,button,1342308361
Control7=IDC_B8_1,button,1342177289
Control8=IDC_STATIC,static,1342308864
Control9=IDC_B7_0,button,1342308361
Control10=IDC_B7_1,button,1342177289
Control11=IDC_STATIC,static,1342308864
Control12=IDC_B6_0,button,1342308361
Control13=IDC_B6_1,button,1342177289
Control14=IDC_STATIC,static,1342308864
Control15=IDC_B5_0,button,1342308361
Control16=IDC_B5_1,button,1342177289
Control17=IDC_STATIC,static,1342308864
Control18=IDC_B1_0,button,1342308361
Control19=IDC_B1_1,button,1342177289
Control20=IDC_STATIC,static,1342308864
Control21=IDC_B0_0,button,1342308361
Control22=IDC_B0_1,button,1342177289
Control23=IDC_STATIC,static,1342308864
Control24=IDC_B4_0,button,1342308361
Control25=IDC_B4_1,button,1342177289
Control26=IDC_STATIC,static,1342308864
Control27=IDC_B3_0,button,1342308361
Control28=IDC_B3_1,button,1342177289
Control29=IDC_STATIC,static,1342308864
Control30=IDC_B2_0,button,1342308361
Control31=IDC_B2_1,button,1342177289
Control32=IDC_STATIC,static,1342308864

[CLS:CLimitSwSetDlg]
Type=0
HeaderFile=LimitSwSetDlg.h
ImplementationFile=LimitSwSetDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=CLimitSwSetDlg
VirtualFilter=dWC

