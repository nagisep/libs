#include <windows.h>

extern "C"
{
	__declspec(dllexport) short __stdcall Tusbpmc_Device_Open(short id);
	__declspec(dllexport) void __stdcall Tusbpmc_Device_Close(short id);
	__declspec(dllexport) short __stdcall Tusbpmc_Set_Exec( short id, char ch, short strtSpeed,short cnstSpeed,short accRate,short freqRange);
	__declspec(dllexport) short __stdcall Tusbpmc_Get_PmcStatus( short id, char ch , short* strtSpeed,short* cnstSpeed,short* accRate,short* freqRange);
	__declspec(dllexport) short __stdcall Tusbpmc_Sel_Pout(  short id, char ch , char sel );
	__declspec(dllexport) short __stdcall Tusbpmc_Sel_Encoder( short id, char ch , char sel ,char mode );
	__declspec(dllexport) short __stdcall Tusbpmc_Set_Slowdown( short id, char ch , char sel ,long dat );
	__declspec(dllexport) short __stdcall Tusbpmc_Set_Limitmode( short id, char ch , short dat );
	__declspec(dllexport) short __stdcall Tusbpmc_Get_LimitStatus( short id, char ch ,short *status);
	__declspec(dllexport) short __stdcall Tusbpmc_Get_Status( short id, char ch ,short *status);
	__declspec(dllexport) short __stdcall Tusbpmc_Start_Control( short id, char ch ,char dir , char mode ,long dat );
	__declspec(dllexport) short __stdcall Tusbpmc_Stop_Control( short id, char ch ,char mode );
	__declspec(dllexport) short __stdcall Tusbpmc_Set_Sync( short id, char ch ,char onoff );
	__declspec(dllexport) short __stdcall Tusbpmc_Hold_Off( short id, char ch ,char onoff );
	__declspec(dllexport) short __stdcall Tusbpmc_Set_AddrCounter( short id, char ch ,long dat );
	__declspec(dllexport) short __stdcall Tusbpmc_Get_AddrCounter( short id, char ch ,long *count);
	__declspec(dllexport) short __stdcall Tusbpmc_Set_Unit( short id, char onoff);
}
