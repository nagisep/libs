// vcsampleDlg.h : ヘッダー ファイル
//

#if !defined(AFX_VCSAMPLEDLG_H__B41072E7_64B6_11D5_935E_006008B03B0A__INCLUDED_)
#define AFX_VCSAMPLEDLG_H__B41072E7_64B6_11D5_935E_006008B03B0A__INCLUDED_

#include "LimitSwSetDlg.h"	// ClassView によって追加されました。
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CVcsampleDlg ダイアログ

typedef struct {
	short StartSpeed;
	short ConstSpeed;
	short AccRate;
	short FreqRate;
	CString StartSpeedMon;
	CString ConstSpeedMon;
	CString AccRateMon;
	CString FreqRateMon;
	BOOL PulseCheck;
	BOOL EncoderCheck;
	int InputSel;
	BOOL SlowDownCheck;
	long SlowDownPoint;
	short LimitSwPattern;
	BOOL CwCcwSel;
	BOOL ContType;
	long IndexData;
	BOOL SyncSetCheck;
	BOOL HoldOffCheck;
	long AddCounter;
	CString AddCounterMon;
} AllSettingType;

class CVcsampleDlg : public CDialog
{
// 構築
public:
	CVcsampleDlg(CWnd* pParent = NULL);	// 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CVcsampleDlg)
	enum { IDD = IDD_VCSAMPLE_DIALOG };
	CButton	m_SyncCheck;
	CButton	m_AddReadBtn;
	CButton	m_AddWriteBtn;
	CButton	m_SyncSetCheck;
	CButton	m_HoldOffCheck;
	CButton	m_SlowStopBtn;
	CButton	m_StopBtn;
	CButton	m_CwCcwSel2;
	CButton	m_CwCcwSel1;
	CButton	m_ContType2;
	CButton	m_ContType1;
	CButton	m_ContStartBtn;
	CButton	m_LimitReadBtn;
	CButton	m_LimitSetBtn;
	CButton	m_SlowDownCheck;
	CButton	m_InputSel1;
	CButton	m_InputSel2;
	CButton	m_InputSel3;
	CButton	m_InputSel4;
	CButton	m_EncoderCheck;
	CButton	m_PulseCheck;
	CButton	m_ParamSetBtn;
	CButton	m_ParamReadBtn;
	CButton	m_DevCloseBtn;
	CButton	m_DevOpenBtn;
	CComboBox	m_DeviceId;
	CTabCtrl	m_ChTab;
	int		m_DeviceIdVal;
	long	m_SlowDownPoint;
	UINT	m_StartSpeedVal;
	UINT	m_ConstSpeedVal;
	UINT	m_AccRateVal;
	UINT	m_FreqRateVal;
	CString	m_StartSpeedMon;
	CString	m_ConstSpeedMon;
	CString	m_AccRateMon;
	CString	m_FreqRateMon;
	int		m_InputSelect;
	int		m_CwCcwVal;
	int		m_ContTypeVal;
	long	m_IndexDataVal;
	long	m_AddCounterVal;
	CString	m_AddCounterMon;
	//}}AFX_DATA

	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CVcsampleDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	HICON m_hIcon;

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CVcsampleDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSelchangeChTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDevOpen();
	afx_msg void OnDevClose();
	afx_msg void OnParamSet();
	afx_msg void OnParamRead();
	afx_msg void OnPulseCheck();
	afx_msg void OnEncoderCheck();
	afx_msg void OnSlowdownCheck();
	afx_msg void OnLimitSet();
	afx_msg void OnLimitRead();
	afx_msg void OnContStart();
	afx_msg void OnStop();
	afx_msg void OnSlowStop();
	afx_msg void OnSyncSet();
	afx_msg void OnHoldOff();
	afx_msg void OnAddWrite();
	afx_msg void OnAddRead();
	afx_msg void OnSync();
	afx_msg void OnSelchangingChTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClose();
	afx_msg void OnReadStatus();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void ParamInitialize(void);
	AllSettingType m_Setting[2];
	CLimitSwSetDlg m_LimitSwDlg;
	void EnableControl(int sel);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_VCSAMPLEDLG_H__B41072E7_64B6_11D5_935E_006008B03B0A__INCLUDED_)
