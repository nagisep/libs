// vcsampleDlg.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "vcsample.h"
#include "vcsampleDlg.h"
#include "TUSBPMC.H"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// アプリケーションのバージョン情報で使われている CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard は仮想関数のオーバーライドを生成します
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// メッセージ ハンドラがありません。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVcsampleDlg ダイアログ

CVcsampleDlg::CVcsampleDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVcsampleDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVcsampleDlg)
	m_DeviceIdVal = -1;
	m_SlowDownPoint = 0;
	m_StartSpeedVal = 0;
	m_ConstSpeedVal = 0;
	m_AccRateVal = 0;
	m_FreqRateVal = 0;
	m_StartSpeedMon = _T("");
	m_ConstSpeedMon = _T("");
	m_AccRateMon = _T("");
	m_FreqRateMon = _T("");
	m_InputSelect = -1;
	m_CwCcwVal = -1;
	m_ContTypeVal = -1;
	m_IndexDataVal = 0;
	m_AddCounterVal = 0;
	m_AddCounterMon = _T("");
	//}}AFX_DATA_INIT
	// メモ: LoadIcon は Win32 の DestroyIcon のサブシーケンスを要求しません。
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVcsampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVcsampleDlg)
	DDX_Control(pDX, IDC_SYNC, m_SyncCheck);
	DDX_Control(pDX, IDC_ADD_READ, m_AddReadBtn);
	DDX_Control(pDX, IDC_ADD_WRITE, m_AddWriteBtn);
	DDX_Control(pDX, IDC_SYNC_SET, m_SyncSetCheck);
	DDX_Control(pDX, IDC_HOLD_OFF, m_HoldOffCheck);
	DDX_Control(pDX, IDC_SLOW_STOP, m_SlowStopBtn);
	DDX_Control(pDX, IDC_STOP, m_StopBtn);
	DDX_Control(pDX, IDC_CW_CCW_SEL2, m_CwCcwSel2);
	DDX_Control(pDX, IDC_CW_CCW_SEL1, m_CwCcwSel1);
	DDX_Control(pDX, IDC_CONT_TYPE2, m_ContType2);
	DDX_Control(pDX, IDC_CONT_TYPE1, m_ContType1);
	DDX_Control(pDX, IDC_CONT_START, m_ContStartBtn);
	DDX_Control(pDX, IDC_LIMIT_READ, m_LimitReadBtn);
	DDX_Control(pDX, IDC_LIMIT_SET, m_LimitSetBtn);
	DDX_Control(pDX, IDC_SLOWDOWN_CHECK, m_SlowDownCheck);
	DDX_Control(pDX, IDC_INPUT_SEL1, m_InputSel1);
	DDX_Control(pDX, IDC_INPUT_SEL2, m_InputSel2);
	DDX_Control(pDX, IDC_INPUT_SEL3, m_InputSel3);
	DDX_Control(pDX, IDC_INPUT_SEL4, m_InputSel4);
	DDX_Control(pDX, IDC_ENCODER_CHECK, m_EncoderCheck);
	DDX_Control(pDX, IDC_PULSE_CHECK, m_PulseCheck);
	DDX_Control(pDX, IDC_PARAM_SET, m_ParamSetBtn);
	DDX_Control(pDX, IDC_PARAM_READ, m_ParamReadBtn);
	DDX_Control(pDX, IDC_DEV_CLOSE, m_DevCloseBtn);
	DDX_Control(pDX, IDC_DEV_OPEN, m_DevOpenBtn);
	DDX_Control(pDX, IDC_DEV_ID, m_DeviceId);
	DDX_Control(pDX, IDC_CH_TAB, m_ChTab);
	DDX_CBIndex(pDX, IDC_DEV_ID, m_DeviceIdVal);
	DDX_Text(pDX, IDC_SLOW_DOWN_POINT, m_SlowDownPoint);
	DDV_MinMaxLong(pDX, m_SlowDownPoint, 0, 16777215);
	DDX_Text(pDX, IDC_START_SPEED, m_StartSpeedVal);
	DDV_MinMaxUInt(pDX, m_StartSpeedVal, 1, 8191);
	DDX_Text(pDX, IDC_CONST_SPEED, m_ConstSpeedVal);
	DDV_MinMaxUInt(pDX, m_ConstSpeedVal, 1, 8191);
	DDX_Text(pDX, IDC_ACC_RATE, m_AccRateVal);
	DDV_MinMaxUInt(pDX, m_AccRateVal, 1, 8191);
	DDX_Text(pDX, IDC_FREQ_RATE, m_FreqRateVal);
	DDV_MinMaxUInt(pDX, m_FreqRateVal, 1, 8191);
	DDX_Text(pDX, IDC_START_SPEED_MON, m_StartSpeedMon);
	DDX_Text(pDX, IDC_CONST_SPEED_MON, m_ConstSpeedMon);
	DDX_Text(pDX, IDC_ACC_RATE_MON, m_AccRateMon);
	DDX_Text(pDX, IDC_FREQ_RATE_MON, m_FreqRateMon);
	DDX_Radio(pDX, IDC_INPUT_SEL1, m_InputSelect);
	DDX_Radio(pDX, IDC_CW_CCW_SEL1, m_CwCcwVal);
	DDX_Radio(pDX, IDC_CONT_TYPE1, m_ContTypeVal);
	DDX_Text(pDX, IDC_INDEX_DATA, m_IndexDataVal);
	DDV_MinMaxLong(pDX, m_IndexDataVal, 1, 16777215);
	DDX_Text(pDX, IDC_ADD_COUNTER, m_AddCounterVal);
	DDV_MinMaxLong(pDX, m_AddCounterVal, -134217728, 134217727);
	DDX_Text(pDX, IDC_ADD_COUNTER_MON, m_AddCounterMon);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CVcsampleDlg, CDialog)
	//{{AFX_MSG_MAP(CVcsampleDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_CH_TAB, OnSelchangeChTab)
	ON_BN_CLICKED(IDC_DEV_OPEN, OnDevOpen)
	ON_BN_CLICKED(IDC_DEV_CLOSE, OnDevClose)
	ON_BN_CLICKED(IDC_PARAM_SET, OnParamSet)
	ON_BN_CLICKED(IDC_PARAM_READ, OnParamRead)
	ON_BN_CLICKED(IDC_PULSE_CHECK, OnPulseCheck)
	ON_BN_CLICKED(IDC_ENCODER_CHECK, OnEncoderCheck)
	ON_BN_CLICKED(IDC_SLOWDOWN_CHECK, OnSlowdownCheck)
	ON_BN_CLICKED(IDC_LIMIT_SET, OnLimitSet)
	ON_BN_CLICKED(IDC_LIMIT_READ, OnLimitRead)
	ON_BN_CLICKED(IDC_CONT_START, OnContStart)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_BN_CLICKED(IDC_SLOW_STOP, OnSlowStop)
	ON_BN_CLICKED(IDC_SYNC_SET, OnSyncSet)
	ON_BN_CLICKED(IDC_HOLD_OFF, OnHoldOff)
	ON_BN_CLICKED(IDC_ADD_WRITE, OnAddWrite)
	ON_BN_CLICKED(IDC_ADD_READ, OnAddRead)
	ON_BN_CLICKED(IDC_SYNC, OnSync)
	ON_NOTIFY(TCN_SELCHANGING, IDC_CH_TAB, OnSelchangingChTab)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_READ_STATUS, OnReadStatus)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVcsampleDlg メッセージ ハンドラ

BOOL CVcsampleDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// "バージョン情報..." メニュー項目をシステム メニューへ追加します。

	// IDM_ABOUTBOX はコマンド メニューの範囲でなければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログ用のアイコンを設定します。フレームワークはアプリケーションのメイン
	// ウィンドウがダイアログでない時は自動的に設定しません。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンを設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンを設定
	
	// TODO: 特別な初期化を行う時はこの場所に追加してください。
	m_ChTab.InsertItem(0,"CH 1");
	m_ChTab.InsertItem(1,"CH 2");
	m_StartSpeedVal = 1;
	m_ConstSpeedVal = 1;
	m_AccRateVal = 1;
	m_FreqRateVal = 1;
	m_IndexDataVal = 1;
	UpdateData(FALSE);
	m_DeviceId.SetCurSel(0);
	EnableControl(0);
	return TRUE;  // TRUE を返すとコントロールに設定したフォーカスは失われません。
}

void CVcsampleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// もしダイアログボックスに最小化ボタンを追加するならば、アイコンを描画する
// コードを以下に記述する必要があります。MFC アプリケーションは document/view
// モデルを使っているので、この処理はフレームワークにより自動的に処理されます。

void CVcsampleDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画用のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// クライアントの矩形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンを描画します。
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// システムは、ユーザーが最小化ウィンドウをドラッグしている間、
// カーソルを表示するためにここを呼び出します。
HCURSOR CVcsampleDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CVcsampleDlg::OnSelchangeChTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int ch;

	ch = m_ChTab.GetCurFocus();
	m_StartSpeedVal = m_Setting[ch].StartSpeed;
	m_ConstSpeedVal = m_Setting[ch].ConstSpeed;
	m_AccRateVal = m_Setting[ch].AccRate;
	m_FreqRateVal = m_Setting[ch].FreqRate;
	m_StartSpeedMon = m_Setting[ch].StartSpeedMon;
	m_ConstSpeedMon = m_Setting[ch].ConstSpeedMon;
	m_AccRateMon = m_Setting[ch].AccRateMon;
	m_FreqRateMon = m_Setting[ch].FreqRateMon;
	m_PulseCheck.SetCheck(m_Setting[ch].PulseCheck);
	m_EncoderCheck.SetCheck(m_Setting[ch].EncoderCheck);
	m_InputSelect = m_Setting[ch].InputSel;
	m_SlowDownCheck.SetCheck(m_Setting[ch].SlowDownCheck);
	m_SlowDownPoint = m_Setting[ch].SlowDownPoint;
	m_CwCcwVal = m_Setting[ch].CwCcwSel;
	m_ContTypeVal = m_Setting[ch].ContType;
	m_IndexDataVal = m_Setting[ch].IndexData;
	m_SyncSetCheck.SetCheck(m_Setting[ch].SyncSetCheck);
	m_HoldOffCheck.SetCheck(m_Setting[ch].HoldOffCheck);
	m_AddCounterVal = m_Setting[ch].AddCounter;
	m_AddCounterMon = m_Setting[ch].AddCounterMon;

	UpdateData(FALSE);
	*pResult = 0;
}

void CVcsampleDlg::OnDevOpen() 
{
	CString message;
	UpdateData(TRUE);
	switch( Tusbpmc_Device_Open(m_DeviceId.GetCurSel()) )
	{
	case 0:/* オープン成功 */
		message = "表示画面は初期化されますが、デバイス内レジスタは変更しません。\n";
		message += "表示される値は電源投入後の初期化値です。電源投入後初めての起動\n";
		message += "でない場合には、表示状態と実際の設定状態が異なる事があります。";
		AfxMessageBox(message);
		ParamInitialize();
		EnableControl(1);/* 各操作用コントロールを有効にする */
		break;
	default:/* 失敗 */
		AfxMessageBox("オープンできませんでした");
		break;
	}
}

void CVcsampleDlg::EnableControl(int sel)
{
	/* sel == 0 の時はデバイスクローズ状態 */
	/* sel == 1 の時はデバイスオープン状態 */
	switch( sel)
	{
	case 0:
		m_DevOpenBtn.EnableWindow(TRUE);
		m_DevCloseBtn.EnableWindow(FALSE);
		m_DeviceId.EnableWindow(TRUE);
		m_ParamSetBtn.EnableWindow(FALSE);
		m_ParamReadBtn.EnableWindow(FALSE);
		m_PulseCheck.EnableWindow(FALSE);
		m_EncoderCheck.EnableWindow(FALSE);
		m_InputSel1.EnableWindow(FALSE);
		m_InputSel2.EnableWindow(FALSE);
		m_InputSel3.EnableWindow(FALSE);
		m_InputSel4.EnableWindow(FALSE);
		m_SlowDownCheck.EnableWindow(FALSE);
		m_LimitSetBtn.EnableWindow(FALSE);
		m_LimitReadBtn.EnableWindow(FALSE);
		m_ContStartBtn.EnableWindow(FALSE);
		m_CwCcwSel1.EnableWindow(FALSE);
		m_CwCcwSel2.EnableWindow(FALSE);
		m_ContType1.EnableWindow(FALSE);
		m_ContType2.EnableWindow(FALSE);
		m_StopBtn.EnableWindow(FALSE);
		m_SlowStopBtn.EnableWindow(FALSE);
		m_SyncSetCheck.EnableWindow(FALSE);
		m_HoldOffCheck.EnableWindow(FALSE);
		m_AddWriteBtn.EnableWindow(FALSE);
		m_AddReadBtn.EnableWindow(FALSE);
		m_SyncCheck.EnableWindow(FALSE);
		m_ChTab.EnableWindow(FALSE);
		break;
	case 1:
		m_DevOpenBtn.EnableWindow(FALSE);
		m_DevCloseBtn.EnableWindow(TRUE);
		m_DeviceId.EnableWindow(FALSE);
		m_ParamSetBtn.EnableWindow(TRUE);
		m_ParamReadBtn.EnableWindow(TRUE);
		m_PulseCheck.EnableWindow(TRUE);
		m_EncoderCheck.EnableWindow(TRUE);
		m_InputSel1.EnableWindow(TRUE);
		m_InputSel2.EnableWindow(TRUE);
		m_InputSel3.EnableWindow(TRUE);
		m_InputSel4.EnableWindow(TRUE);
		m_SlowDownCheck.EnableWindow(TRUE);
		m_LimitSetBtn.EnableWindow(TRUE);
		m_LimitReadBtn.EnableWindow(TRUE);
		m_ContStartBtn.EnableWindow(TRUE);
		m_CwCcwSel1.EnableWindow(TRUE);
		m_CwCcwSel2.EnableWindow(TRUE);
		m_ContType1.EnableWindow(TRUE);
		m_ContType2.EnableWindow(TRUE);
		m_StopBtn.EnableWindow(TRUE);
		m_SlowStopBtn.EnableWindow(TRUE);
		m_SyncSetCheck.EnableWindow(TRUE);
		m_HoldOffCheck.EnableWindow(TRUE);
		m_AddWriteBtn.EnableWindow(TRUE);
		m_AddReadBtn.EnableWindow(TRUE);
		m_SyncCheck.EnableWindow(TRUE);
		m_ChTab.EnableWindow(TRUE);
		break;
	}
}

void CVcsampleDlg::OnDevClose() 
{
	Tusbpmc_Device_Close(m_DeviceId.GetCurSel());
	EnableControl(0);
}

void CVcsampleDlg::OnParamSet() 
{
	UpdateData(TRUE);
	if( Tusbpmc_Set_Exec(m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_StartSpeedVal,//初速度
		m_ConstSpeedVal,//定速度
		m_AccRateVal,//加減速レート
		m_FreqRateVal) )//周波数レンジ
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnParamRead() 
{
	short StartSpeed,ConstSpeed,AccRate,FreqRate;

	if( Tusbpmc_Get_PmcStatus( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		&StartSpeed,//初速度
		&ConstSpeed,//定速度
		&AccRate,//加減速レート
		&FreqRate) )//周波数レンジ
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
	UpdateData(TRUE);
	m_StartSpeedMon.Format("%d",StartSpeed);
	m_ConstSpeedMon.Format("%d",ConstSpeed);
	m_AccRateMon.Format("%d",AccRate);
	m_FreqRateMon.Format("%d",FreqRate);
	UpdateData(FALSE);
}

void CVcsampleDlg::OnPulseCheck() 
{
	if( Tusbpmc_Sel_Pout(m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_PulseCheck.GetCheck() ) )//チェック状態
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnEncoderCheck() 
{
	UpdateData(TRUE);
	if( Tusbpmc_Sel_Encoder(m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_EncoderCheck.GetCheck(),//チェック状態
		m_InputSelect) )//入力選択
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnSlowdownCheck() 
{
	if( Tusbpmc_Set_Slowdown(m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_SlowDownCheck.GetCheck(),//チェック状態
		m_SlowDownPoint) )//減速ポイント
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnLimitSet() 
{
	m_LimitSwDlg.m_BitPattern = m_Setting[m_ChTab.GetCurFocus()].LimitSwPattern;
	if( m_LimitSwDlg.DoModal() == IDOK )
	{
		if( Tusbpmc_Set_Limitmode( m_DeviceId.GetCurSel(),//ID
			m_ChTab.GetCurFocus(),//CH
			m_LimitSwDlg.m_BitPattern) )
		{
			AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
			OnDevClose();
			return;
		}
		m_Setting[m_ChTab.GetCurFocus()].LimitSwPattern = m_LimitSwDlg.m_BitPattern;
	}
}

void CVcsampleDlg::OnLimitRead() 
{
	short tmp;
	CString message;

	if( Tusbpmc_Get_LimitStatus( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		&tmp) )
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
	m_Setting[m_ChTab.GetCurFocus()].LimitSwPattern = tmp;
	if( tmp & 0x200 )
		message = "H.P LS 停止モード      急停止\n";
	else
		message = "H.P LS 停止モード      緩停止\n";
	if( tmp & 0x100 )
		message += "H.P LS 入力           有効\n";
	else
		message += "H.P LS 入力           無効\n";
	if( tmp & 0x80 )
		message += "H.P LS 接点タイプ     N.O\n";
	else
		message += "H.P LS 接点タイプ     N.C\n";
	if( tmp & 0x40 )
		message += "CCW LS 停止モード     急停止\n";
	else
		message += "CCW LS 停止モード     緩停止\n";
	if( tmp & 0x20 )
		message += "CW LS 停止モード      急停止\n";
	else
		message += "CW LS 停止モード      緩停止\n";
	if( tmp & 0x10 )
		message += "CCW LS 入力           有効\n";
	else
		message += "CCW LS 入力           無効\n";
	if( tmp & 0x8 )
		message += "CW LS 入力            有効\n";
	else
		message += "CW LS 入力            無効\n";
	if( tmp & 0x4 )
		message += "CW<->CCW 入力切替     有効\n";
	else
		message += "CW<->CCW 入力切替     無効\n";
	if( tmp & 0x2 )
		message += "CCW LS 接点タイプ     N.O\n";
	else
		message += "CCW LS 接点タイプ     N.C\n";
	if( tmp & 0x1 )
		message += "CW LS 接点タイプ      N.O";
	else
		message += "CW LS 接点タイプ      N.C";
	AfxMessageBox(message);
}

void CVcsampleDlg::OnContStart() 
{
	if( UpdateData(TRUE) )
	{
		if( Tusbpmc_Start_Control( m_DeviceId.GetCurSel(),//ID
			m_ChTab.GetCurFocus(),//CH
			m_CwCcwVal,//CW CCW 方向選択
			m_ContTypeVal,//連続　指定数選択
			m_IndexDataVal) )
		{
			AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
			OnDevClose();
			return;
		}
	}
}

void CVcsampleDlg::OnStop() 
{
	if( Tusbpmc_Stop_Control( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		0) )//急停止
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnSlowStop() 
{
	if( Tusbpmc_Stop_Control( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		1) )//緩停止
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnSyncSet() 
{
	if( Tusbpmc_Set_Sync( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_SyncSetCheck.GetCheck() ) ) //On or Off
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnHoldOff() 
{
	if( Tusbpmc_Hold_Off( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		m_HoldOffCheck.GetCheck() ) )// On or Off
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnAddWrite() 
{
	if( UpdateData(TRUE) )
	{
		if( Tusbpmc_Set_AddrCounter( m_DeviceId.GetCurSel(),//ID
			m_ChTab.GetCurFocus(),//CH
			m_AddCounterVal) )
		{
			AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
			OnDevClose();
			return;
		}
	}
}

void CVcsampleDlg::OnAddRead() 
{
	long tmp;
	if( Tusbpmc_Get_AddrCounter( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		&tmp) )
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
	UpdateData(TRUE);
	m_AddCounterMon.Format("%ld",tmp);
	UpdateData(FALSE);
}

void CVcsampleDlg::OnSync() 
{
	if( Tusbpmc_Set_Unit( m_DeviceId.GetCurSel(),//ID
		m_SyncCheck.GetCheck() ) )
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
}

void CVcsampleDlg::OnSelchangingChTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int ch;
	ch = m_ChTab.GetCurFocus();
	UpdateData(TRUE);
	m_Setting[ch].StartSpeed = m_StartSpeedVal;
	m_Setting[ch].ConstSpeed = m_ConstSpeedVal;
	m_Setting[ch].AccRate = m_AccRateVal;
	m_Setting[ch].FreqRate = m_FreqRateVal;
	m_Setting[ch].StartSpeedMon = m_StartSpeedMon;
	m_Setting[ch].ConstSpeedMon = m_ConstSpeedMon;
	m_Setting[ch].AccRateMon = m_AccRateMon;
	m_Setting[ch].FreqRateMon = m_FreqRateMon;
	m_Setting[ch].PulseCheck = m_PulseCheck.GetCheck();
	m_Setting[ch].EncoderCheck = m_EncoderCheck.GetCheck();
	m_Setting[ch].InputSel = m_InputSelect;
	m_Setting[ch].SlowDownCheck = m_SlowDownCheck.GetCheck();
	m_Setting[ch].SlowDownPoint = m_SlowDownPoint;
	m_Setting[ch].CwCcwSel = m_CwCcwVal;
	m_Setting[ch].ContType = m_ContTypeVal;
	m_Setting[ch].IndexData = m_IndexDataVal;
	m_Setting[ch].SyncSetCheck = m_SyncSetCheck.GetCheck();
	m_Setting[ch].HoldOffCheck = m_HoldOffCheck.GetCheck();
	m_Setting[ch].AddCounter = m_AddCounterVal;
	m_Setting[ch].AddCounterMon = m_AddCounterMon;
	*pResult = 0;
}

void CVcsampleDlg::ParamInitialize()
{
	int ch;
	for( ch = 0 ; ch < 2 ; ch++ )
	{
		m_Setting[ch].StartSpeed = 100;
		m_Setting[ch].ConstSpeed = 1000;
		m_Setting[ch].AccRate = 1000;
		m_Setting[ch].FreqRate = 50;
		m_Setting[ch].StartSpeedMon = "100";
		m_Setting[ch].ConstSpeedMon = "1000";
		m_Setting[ch].AccRateMon = "1000";
		m_Setting[ch].FreqRateMon = "50";
		m_Setting[ch].PulseCheck = 0;
		m_Setting[ch].EncoderCheck = 0;
		m_Setting[ch].InputSel = 0;
		m_Setting[ch].SlowDownCheck = 0;
		m_Setting[ch].SlowDownPoint = 0;
		m_Setting[ch].CwCcwSel = 0;
		m_Setting[ch].ContType = 0;
		m_Setting[ch].IndexData = 1;
		m_Setting[ch].SyncSetCheck = 0;
		m_Setting[ch].HoldOffCheck = 0;
		m_Setting[ch].AddCounter = 0;
		m_Setting[ch].AddCounterMon = "0";
	}

	ch = 0;
	m_StartSpeedVal = m_Setting[ch].StartSpeed;
	m_ConstSpeedVal = m_Setting[ch].ConstSpeed;
	m_AccRateVal = m_Setting[ch].AccRate;
	m_FreqRateVal = m_Setting[ch].FreqRate;
	m_StartSpeedMon = m_Setting[ch].StartSpeedMon;
	m_ConstSpeedMon = m_Setting[ch].ConstSpeedMon;
	m_AccRateMon = m_Setting[ch].AccRateMon;
	m_FreqRateMon = m_Setting[ch].FreqRateMon;
	m_PulseCheck.SetCheck(m_Setting[ch].PulseCheck);
	m_EncoderCheck.SetCheck(m_Setting[ch].EncoderCheck);
	m_InputSelect = m_Setting[ch].InputSel;
	m_SlowDownCheck.SetCheck(m_Setting[ch].SlowDownCheck);
	m_SlowDownPoint = m_Setting[ch].SlowDownPoint;
	m_CwCcwVal = m_Setting[ch].CwCcwSel;
	m_ContTypeVal = m_Setting[ch].ContType;
	m_IndexDataVal = m_Setting[ch].IndexData;
	m_SyncSetCheck.SetCheck(m_Setting[ch].SyncSetCheck);
	m_HoldOffCheck.SetCheck(m_Setting[ch].HoldOffCheck);
	m_AddCounterVal = m_Setting[ch].AddCounter;
	m_AddCounterMon = m_Setting[ch].AddCounterMon;
	UpdateData(FALSE);
}

void CVcsampleDlg::OnClose() 
{
	/* アプリケーション終了 */
	if( m_DevCloseBtn.IsWindowEnabled() )/* デバイスがオープンしていたら */
	{
		OnDevClose();/* デバイスをクローズする */
	}
	CDialog::OnClose();
}

void CVcsampleDlg::OnReadStatus() 
{
	short tmp;
	CString message;

	if( Tusbpmc_Get_Status( m_DeviceId.GetCurSel(),//ID
		m_ChTab.GetCurFocus(),//CH
		&tmp) )
	{
		AfxMessageBox("USBドライバ関数の実行に失敗しました\nデバイスをクローズします");
		OnDevClose();
		return;
	}
	if( tmp & 0x01 )
		message = "CW   Limit SW    開いている(OFF)\n";
	else
		message = "CW   Limit SW    閉じている(O N)\n";
	if( tmp & 0x02 )
		message += "CCW  Limit SW    開いている(OFF)\n";
	else
		message += "CCW  Limit SW    閉じている(O N)\n";
	if( tmp & 0x04 )
		message += "Home Position SW 開いている(OFF)\n";
	else
		message += "Home Position SW 閉じている(O N)\n";
	AfxMessageBox(message);
}
