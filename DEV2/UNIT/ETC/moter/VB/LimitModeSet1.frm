VERSION 5.00
Begin VB.Form LimitModeSet1 
   Caption         =   "CH1リミットスイッチ状態設定"
   ClientHeight    =   5700
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4935
   ScaleHeight     =   5700
   ScaleWidth      =   4935
   StartUpPosition =   3  'Windows の既定値
   Begin VB.Frame Frame12 
      BorderStyle     =   0  'なし
      Caption         =   "Frame12"
      Height          =   495
      Left            =   240
      TabIndex        =   25
      Top             =   3360
      Width           =   4335
      Begin VB.OptionButton ExChgE1 
         Caption         =   "有効"
         Height          =   255
         Left            =   3360
         TabIndex        =   39
         Top             =   240
         Width           =   850
      End
      Begin VB.OptionButton ExChgD1 
         Caption         =   "無効"
         Height          =   255
         Left            =   2280
         TabIndex        =   38
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.Label Label15 
         Caption         =   "CW<->CCW 入力切替"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame11 
      BorderStyle     =   0  'なし
      Caption         =   "Frame11"
      Height          =   495
      Left            =   240
      TabIndex        =   24
      Top             =   4320
      Width           =   4335
      Begin VB.OptionButton CwNO1 
         Caption         =   "N.O"
         Height          =   255
         Left            =   3360
         TabIndex        =   43
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CwNC1 
         Caption         =   "N.C"
         Height          =   255
         Left            =   2280
         TabIndex        =   42
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label14 
         Caption         =   "CW LS 接点タイプ"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame10 
      BorderStyle     =   0  'なし
      Caption         =   "Frame10"
      Height          =   495
      Left            =   240
      TabIndex        =   23
      Top             =   3840
      Width           =   4335
      Begin VB.OptionButton CcwNO1 
         Caption         =   "N.O"
         Height          =   255
         Left            =   3360
         TabIndex        =   41
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CcwNC1 
         Caption         =   "N.C"
         Height          =   255
         Left            =   2280
         TabIndex        =   40
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label13 
         Caption         =   "CCW LS 接点タイプ"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame9 
      BorderStyle     =   0  'なし
      Caption         =   "Frame9"
      Height          =   495
      Left            =   240
      TabIndex        =   22
      Top             =   2880
      Width           =   4335
      Begin VB.OptionButton CwEna1 
         Caption         =   "有効"
         Height          =   255
         Left            =   3360
         TabIndex        =   37
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CwDis1 
         Caption         =   "無効"
         Height          =   255
         Left            =   2280
         TabIndex        =   36
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label12 
         Caption         =   "CW LS 入力"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame8 
      BorderStyle     =   0  'なし
      Caption         =   "Frame8"
      Height          =   495
      Left            =   240
      TabIndex        =   21
      Top             =   2400
      Width           =   4335
      Begin VB.OptionButton CcwEna1 
         Caption         =   "有効"
         Height          =   255
         Left            =   3360
         TabIndex        =   35
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CcwDis1 
         Caption         =   "無効"
         Height          =   255
         Left            =   2280
         TabIndex        =   34
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label5 
         Caption         =   "CCW　LS 入力"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame7 
      BorderStyle     =   0  'なし
      Caption         =   "Frame7"
      Height          =   495
      Left            =   240
      TabIndex        =   20
      Top             =   1920
      Width           =   4335
      Begin VB.OptionButton CwEstop1 
         Caption         =   "急停止"
         Height          =   255
         Left            =   3360
         TabIndex        =   28
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CwSstop1 
         Caption         =   "緩停止"
         Height          =   255
         Left            =   2280
         TabIndex        =   27
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label4 
         Caption         =   "CW LS 停止モード"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame6 
      BorderStyle     =   0  'なし
      Caption         =   "Frame6"
      Height          =   495
      Left            =   240
      TabIndex        =   10
      Top             =   1440
      Width           =   4335
      Begin VB.OptionButton CcwEstop1 
         Caption         =   "急停止"
         Height          =   255
         Left            =   3360
         TabIndex        =   19
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.OptionButton CcwSstop1 
         Caption         =   "緩停止"
         Height          =   255
         Left            =   2280
         TabIndex        =   18
         Top             =   240
         Width           =   850
      End
      Begin VB.Label Label3 
         Caption         =   "CCW LS 停止モード"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame5 
      BorderStyle     =   0  'なし
      Caption         =   "Frame5"
      Height          =   495
      Left            =   240
      TabIndex        =   9
      Top             =   960
      Width           =   4335
      Begin VB.OptionButton HpNO1 
         Caption         =   "N.O"
         Height          =   255
         Left            =   3360
         TabIndex        =   16
         Top             =   240
         Width           =   850
      End
      Begin VB.OptionButton HpNC1 
         Caption         =   "N.C"
         Height          =   255
         Left            =   2280
         TabIndex        =   15
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.Label Label2 
         Caption         =   "H.P LS 接点タイプ"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame4 
      BorderStyle     =   0  'なし
      Caption         =   "Frame4"
      Height          =   495
      Left            =   240
      TabIndex        =   8
      Top             =   480
      Width           =   4335
      Begin VB.OptionButton HpEna1 
         Caption         =   "有効"
         Height          =   255
         Left            =   3360
         TabIndex        =   13
         Top             =   240
         Width           =   850
      End
      Begin VB.OptionButton HpDis1 
         Caption         =   "無効"
         Height          =   255
         Left            =   2280
         TabIndex        =   12
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.Label Label1 
         Caption         =   "H.P LS 入力"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.Frame Frame1 
      BorderStyle     =   0  'なし
      Caption         =   "Frame1"
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   0
      Width           =   4335
      Begin VB.Frame Frame3 
         Caption         =   "Frame3"
         Height          =   15
         Left            =   0
         TabIndex        =   7
         Top             =   480
         Width           =   2775
      End
      Begin VB.Frame Frame2 
         Caption         =   "Frame2"
         Height          =   15
         Left            =   0
         TabIndex        =   6
         Top             =   480
         Width           =   4335
      End
      Begin VB.OptionButton HpEstop1 
         Caption         =   "急停止"
         Height          =   255
         Left            =   3360
         TabIndex        =   5
         Top             =   240
         Width           =   850
      End
      Begin VB.OptionButton HpSstop1 
         Caption         =   "緩停止"
         Height          =   255
         Left            =   2280
         TabIndex        =   4
         Top             =   240
         Value           =   -1  'True
         Width           =   850
      End
      Begin VB.Label Label11 
         Caption         =   "H.P LS 停止モード"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   1800
      End
   End
   Begin VB.CommandButton LimitModeSetNon1 
      Caption         =   "キャンセル"
      Height          =   375
      Left            =   2880
      TabIndex        =   1
      Top             =   5160
      Width           =   1335
   End
   Begin VB.CommandButton LimitModeSetOK1 
      Caption         =   "OK"
      Height          =   375
      Left            =   720
      TabIndex        =   0
      Top             =   5160
      Width           =   1335
   End
End
Attribute VB_Name = "LimitModeSet1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub LimitModeSetNon1_Click()
    LimitModeSet1.Visible = Flase
End Sub

Private Sub LimitModeSetOK1_Click()
    Dim dat As Integer
    dat = 0
    If (CwNO1.Value = False) Then
        dat = dat + 1
    End If
    If (CcwNO1.Value = False) Then
        dat = dat + 2
    End If
    If (ExChgD1.Value = False) Then
        dat = dat + 4
    End If
    If (CwDis1.Value = False) Then
        dat = dat + 8
    End If
    If (CcwDis1.Value = False) Then
        dat = dat + 16
    End If
    If (CwSstop1.Value = False) Then
        dat = dat + 32
    End If
    If (CcwSstop1.Value = False) Then
        dat = dat + 64
    End If
    If (HpNO1.Value = False) Then
        dat = dat + 128
    End If
    If (HpDis1.Value = False) Then
        dat = dat + 256
    End If
    If (HpSstop1.Value = False) Then
        dat = dat + 512
    End If
    Tusbpmc_Set_Limitmode VBSampleMenu!Id1.Text, Val(VBSampleMenu!SSTab1.Tab), dat
    LimitModeSet1.Visible = False
End Sub
