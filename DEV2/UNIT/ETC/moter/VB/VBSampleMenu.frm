VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form VBSampleMenu 
   BorderStyle     =   5  '可変ﾂｰﾙ ｳｨﾝﾄﾞｳ
   Caption         =   "TUSB-02PMC Visual Basic Sample"
   ClientHeight    =   6585
   ClientLeft      =   60
   ClientTop       =   285
   ClientWidth     =   11235
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6585
   ScaleWidth      =   11235
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows の既定値
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CheckBox UnitSyncCheck 
      Caption         =   "同期"
      Enabled         =   0   'False
      Height          =   375
      Left            =   8640
      TabIndex        =   4
      Top             =   120
      Width           =   1215
   End
   Begin TabDlg.SSTab SSTab1 
      CausesValidation=   0   'False
      Height          =   5895
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   10398
      _Version        =   393216
      Style           =   1
      MousePointer    =   2
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      Enabled         =   0   'False
      TabCaption(0)   =   "CH 1"
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "SetFrame1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "ControleFrame1(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "CH 2"
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "ControleFrame2(1)"
      Tab(1).Control(1)=   "SetFrame2(1)"
      Tab(1).ControlCount=   2
      Begin VB.Frame ControleFrame2 
         Caption         =   "制御"
         ClipControls    =   0   'False
         Enabled         =   0   'False
         Height          =   1935
         Index           =   1
         Left            =   -74760
         TabIndex        =   74
         Top             =   3840
         Width           =   10575
         Begin VB.OptionButton CcwCheck2 
            Caption         =   "CCW"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   2640
            TabIndex        =   84
            Top             =   240
            Width           =   800
         End
         Begin VB.Frame Frame2 
            BorderStyle     =   0  'なし
            Caption         =   "Frame1"
            Height          =   495
            Index           =   1
            Left            =   3480
            TabIndex        =   87
            Top             =   120
            Width           =   2535
            Begin VB.OptionButton ContCheck2 
               Caption         =   "連続"
               Enabled         =   0   'False
               Height          =   255
               Index           =   1
               Left            =   480
               TabIndex        =   89
               Top             =   120
               Value           =   -1  'True
               Width           =   975
            End
            Begin VB.OptionButton IndexCheck2 
               Caption         =   "指定数"
               Enabled         =   0   'False
               Height          =   225
               Index           =   1
               Left            =   1440
               TabIndex        =   88
               Top             =   120
               Width           =   975
            End
         End
         Begin VB.CommandButton StartButton2 
            Caption         =   "スタート"
            Enabled         =   0   'False
            Height          =   1335
            Index           =   1
            Left            =   240
            TabIndex        =   86
            Top             =   360
            Width           =   975
         End
         Begin VB.OptionButton CwCheck2 
            Caption         =   "CW"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   1920
            TabIndex        =   85
            Top             =   240
            Value           =   -1  'True
            Width           =   800
         End
         Begin VB.TextBox IndexDataText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   83
            Text            =   "0"
            Top             =   600
            Width           =   2775
         End
         Begin VB.CommandButton EmergencyStopButton2 
            Caption         =   "急停止"
            Enabled         =   0   'False
            Height          =   495
            Index           =   1
            Left            =   1920
            TabIndex        =   82
            Top             =   1200
            Width           =   2535
         End
         Begin VB.CommandButton SlowStopButton2 
            Caption         =   "緩停止"
            Enabled         =   0   'False
            Height          =   495
            Index           =   1
            Left            =   4680
            TabIndex        =   81
            Top             =   1200
            Width           =   1335
         End
         Begin VB.CheckBox SyncCheck2 
            Caption         =   "同期設定"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   6840
            TabIndex        =   80
            Top             =   240
            Width           =   1095
         End
         Begin VB.CheckBox HoldoffCheck2 
            Caption         =   "HOLD OFF"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   8160
            TabIndex        =   79
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton CounterWriteButton2 
            Caption         =   "書込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   6840
            TabIndex        =   78
            Top             =   840
            Width           =   975
         End
         Begin VB.CommandButton CounterReadButton2 
            BackColor       =   &H80000009&
            Caption         =   "読込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   6840
            TabIndex        =   77
            Top             =   1320
            Width           =   975
         End
         Begin VB.TextBox CountText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   7920
            TabIndex        =   76
            Text            =   "0"
            Top             =   840
            Width           =   2415
         End
         Begin VB.TextBox CountReadData2 
            BackColor       =   &H8000000A&
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   7920
            TabIndex        =   75
            Text            =   "0"
            Top             =   1320
            Width           =   2415
         End
         Begin VB.Label IndexDataLabel2 
            Caption         =   "IndexData"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   2040
            TabIndex        =   91
            Top             =   720
            Width           =   1005
         End
         Begin VB.Label AddressText2 
            Caption         =   "アドレスカウンタ"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   8520
            TabIndex        =   90
            Top             =   600
            Width           =   1335
         End
      End
      Begin VB.Frame SetFrame2 
         Caption         =   "設定"
         ClipControls    =   0   'False
         Enabled         =   0   'False
         Height          =   3255
         Index           =   1
         Left            =   -74760
         TabIndex        =   49
         Top             =   480
         Width           =   10575
         Begin VB.CommandButton GetStatus2 
            Caption         =   "リミットスイッチ状態読込"
            Height          =   495
            Index           =   1
            Left            =   8160
            TabIndex        =   92
            Top             =   240
            Width           =   2200
         End
         Begin VB.CommandButton DataSetButton2 
            Caption         =   "設定"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   69
            Top             =   240
            Width           =   1335
         End
         Begin VB.CommandButton SetDataRead2 
            Caption         =   "設定値読込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   68
            Top             =   240
            Width           =   1335
         End
         Begin VB.CheckBox PulseSelect2 
            Caption         =   "1パルス出力"
            Enabled         =   0   'False
            Height          =   300
            Index           =   1
            Left            =   5760
            TabIndex        =   67
            Top             =   360
            Width           =   1500
         End
         Begin VB.CheckBox EncoderCheck2 
            Caption         =   "エンコーダ入力"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   5760
            TabIndex        =   66
            Top             =   840
            Width           =   1500
         End
         Begin VB.CheckBox DownPointSel2 
            Caption         =   "減速ポイント"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   5760
            TabIndex        =   65
            Top             =   1920
            Width           =   1500
         End
         Begin VB.TextBox DownPointText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   7560
            TabIndex        =   64
            Text            =   "0"
            Top             =   1800
            Width           =   2500
         End
         Begin VB.OptionButton UpDownSel2 
            Caption         =   "Up/Down"
            Enabled         =   0   'False
            Height          =   225
            Index           =   1
            Left            =   7560
            TabIndex        =   63
            Top             =   840
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton AB1Sel2 
            Caption         =   "A/B相1逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   9000
            TabIndex        =   62
            Top             =   840
            Width           =   1335
         End
         Begin VB.OptionButton AB2Sel2 
            Caption         =   "A/B相2逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   7560
            TabIndex        =   61
            Top             =   1200
            Width           =   1335
         End
         Begin VB.OptionButton AB4Sel2 
            Caption         =   "A/B相4逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   9000
            TabIndex        =   60
            Top             =   1200
            Width           =   1335
         End
         Begin VB.CommandButton LimitDataSet2 
            Caption         =   "リミットスイッチ設定"
            Enabled         =   0   'False
            Height          =   495
            Index           =   1
            Left            =   5760
            TabIndex        =   59
            Top             =   2520
            Width           =   2200
         End
         Begin VB.CommandButton GetLimitStatus2 
            Caption         =   "LS設定状態読込"
            Enabled         =   0   'False
            Height          =   495
            Index           =   1
            Left            =   8160
            TabIndex        =   58
            Top             =   2520
            Width           =   2200
         End
         Begin VB.TextBox StartText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   57
            Text            =   "0"
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox ConstText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   56
            Text            =   "0"
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox RateText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   55
            Text            =   "0"
            Top             =   2040
            Width           =   1335
         End
         Begin VB.TextBox RangeText2 
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   54
            Text            =   "0"
            Top             =   2640
            Width           =   1335
         End
         Begin VB.TextBox StartReadData2 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   53
            Text            =   "0"
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox ConstReadData2 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   52
            Text            =   "0"
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox RateReadData2 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   51
            Text            =   "0"
            Top             =   2040
            Width           =   1335
         End
         Begin VB.TextBox RangeReadData2 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   3240
            TabIndex        =   50
            Text            =   "0"
            Top             =   2640
            Width           =   1335
         End
         Begin VB.Label StartLabel2 
            Caption         =   "初速値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   73
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label ConstLabel2 
            Caption         =   "定速値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   72
            Top             =   1560
            Width           =   1335
         End
         Begin VB.Label RateLabel2 
            Caption         =   "加減速レート値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   71
            Top             =   2160
            Width           =   1335
         End
         Begin VB.Label RangeLabel2 
            Caption         =   "周波数レンジ値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   70
            Top             =   2760
            Width           =   1335
         End
      End
      Begin VB.Frame ControleFrame1 
         Caption         =   "制御"
         ClipControls    =   0   'False
         Enabled         =   0   'False
         Height          =   1935
         Index           =   0
         Left            =   240
         TabIndex        =   11
         Top             =   3840
         Width           =   10575
         Begin VB.TextBox CountReadData1 
            BackColor       =   &H8000000A&
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   7920
            TabIndex        =   24
            Text            =   "0"
            Top             =   1320
            Width           =   2415
         End
         Begin VB.TextBox CountText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   7920
            TabIndex        =   23
            Text            =   "0"
            Top             =   840
            Width           =   2415
         End
         Begin VB.CommandButton CounterReadButton1 
            BackColor       =   &H80000009&
            Caption         =   "読込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   6840
            TabIndex        =   22
            Top             =   1320
            Width           =   975
         End
         Begin VB.CommandButton CounterWriteButton1 
            Caption         =   "書込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   6840
            TabIndex        =   21
            Top             =   840
            Width           =   975
         End
         Begin VB.CheckBox HoldoffCheck1 
            Caption         =   "HOLD OFF"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   8160
            TabIndex        =   20
            Top             =   240
            Width           =   1215
         End
         Begin VB.CheckBox SyncCheck1 
            Caption         =   "同期設定"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   6840
            TabIndex        =   19
            Top             =   240
            Width           =   1095
         End
         Begin VB.CommandButton SlowStopButton1 
            Caption         =   "緩停止"
            Enabled         =   0   'False
            Height          =   495
            Index           =   0
            Left            =   4680
            TabIndex        =   18
            Top             =   1200
            Width           =   1335
         End
         Begin VB.CommandButton EmergencyStopButton1 
            Caption         =   "急停止"
            Enabled         =   0   'False
            Height          =   495
            Index           =   0
            Left            =   1920
            TabIndex        =   17
            Top             =   1200
            Width           =   2535
         End
         Begin VB.TextBox IndexDataText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   15
            Text            =   "0"
            Top             =   600
            Width           =   2775
         End
         Begin VB.OptionButton CcwCheck1 
            Caption         =   "CCW"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   2640
            TabIndex        =   14
            Top             =   240
            Width           =   800
         End
         Begin VB.OptionButton CwCheck1 
            Caption         =   "CW"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   1920
            TabIndex        =   13
            Top             =   240
            Value           =   -1  'True
            Width           =   800
         End
         Begin VB.CommandButton StartButton1 
            Caption         =   "スタート"
            Enabled         =   0   'False
            Height          =   1335
            Index           =   0
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   975
         End
         Begin VB.Frame Frame1 
            BorderStyle     =   0  'なし
            Caption         =   "Frame1"
            Height          =   495
            Index           =   0
            Left            =   3480
            TabIndex        =   26
            Top             =   120
            Width           =   2535
            Begin VB.OptionButton IndexCheck1 
               Caption         =   "指定数"
               Enabled         =   0   'False
               Height          =   225
               Index           =   0
               Left            =   1440
               TabIndex        =   30
               Top             =   120
               Width           =   975
            End
            Begin VB.OptionButton ContCheck1 
               Caption         =   "連続"
               Enabled         =   0   'False
               Height          =   255
               Index           =   0
               Left            =   480
               TabIndex        =   29
               Top             =   120
               Value           =   -1  'True
               Width           =   975
            End
         End
         Begin VB.Label AddressText1 
            Caption         =   "アドレスカウンタ"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   8520
            TabIndex        =   25
            Top             =   600
            Width           =   1335
         End
         Begin VB.Label IndexDataLabel1 
            Caption         =   "IndexData"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   2040
            TabIndex        =   16
            Top             =   720
            Width           =   1005
         End
      End
      Begin VB.Frame SetFrame1 
         Caption         =   "設定"
         ClipControls    =   0   'False
         Enabled         =   0   'False
         Height          =   3255
         Index           =   0
         Left            =   240
         TabIndex        =   5
         Top             =   480
         Width           =   10575
         Begin VB.CommandButton GetStatus1 
            Caption         =   "リミットスイッチ状態読込"
            Enabled         =   0   'False
            Height          =   495
            Index           =   0
            Left            =   8160
            TabIndex        =   93
            Top             =   240
            Width           =   2200
         End
         Begin VB.TextBox RangeReadData1 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   48
            Text            =   "0"
            Top             =   2640
            Width           =   1335
         End
         Begin VB.TextBox RateReadData1 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   47
            Text            =   "0"
            Top             =   2040
            Width           =   1335
         End
         Begin VB.TextBox ConstReadData1 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   46
            Text            =   "0"
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox StartReadData1 
            BackColor       =   &H8000000B&
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   45
            Text            =   "0"
            Top             =   840
            Width           =   1335
         End
         Begin VB.TextBox RangeText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   1680
            TabIndex        =   44
            Text            =   "0"
            Top             =   2640
            Width           =   1335
         End
         Begin VB.TextBox RateText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   1680
            TabIndex        =   43
            Text            =   "0"
            Top             =   2040
            Width           =   1335
         End
         Begin VB.TextBox ConstText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   1680
            TabIndex        =   42
            Text            =   "0"
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox StartText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   1680
            TabIndex        =   41
            Text            =   "0"
            Top             =   840
            Width           =   1335
         End
         Begin VB.CommandButton GetLimitStatus1 
            Caption         =   "LS設定状態読込"
            Enabled         =   0   'False
            Height          =   495
            Index           =   0
            Left            =   8160
            TabIndex        =   40
            Top             =   2520
            Width           =   2200
         End
         Begin VB.CommandButton LimitDataSet1 
            Caption         =   "リミットスイッチ設定"
            Enabled         =   0   'False
            Height          =   495
            Index           =   0
            Left            =   5760
            TabIndex        =   39
            Top             =   2520
            Width           =   2200
         End
         Begin VB.OptionButton AB4Sel1 
            Caption         =   "A/B相4逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   9000
            TabIndex        =   38
            Top             =   1200
            Width           =   1335
         End
         Begin VB.OptionButton AB2Sel1 
            Caption         =   "A/B相2逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   7560
            TabIndex        =   37
            Top             =   1200
            Width           =   1335
         End
         Begin VB.OptionButton AB1Sel1 
            Caption         =   "A/B相1逓倍"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   9000
            TabIndex        =   36
            Top             =   840
            Width           =   1335
         End
         Begin VB.OptionButton UpDownSel1 
            Caption         =   "Up/Down"
            Enabled         =   0   'False
            Height          =   225
            Index           =   0
            Left            =   7560
            TabIndex        =   35
            Top             =   840
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.TextBox DownPointText1 
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   7560
            TabIndex        =   34
            Text            =   "0"
            Top             =   1800
            Width           =   2500
         End
         Begin VB.CheckBox DownPointSel1 
            Caption         =   "減速ポイント"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   5760
            TabIndex        =   33
            Top             =   1920
            Width           =   1500
         End
         Begin VB.CheckBox EncoderCheck1 
            Caption         =   "エンコーダ入力"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   5760
            TabIndex        =   32
            Top             =   840
            Width           =   1500
         End
         Begin VB.CheckBox PulseSelect1 
            Caption         =   "1パルス出力"
            Enabled         =   0   'False
            Height          =   300
            Index           =   0
            Left            =   5760
            TabIndex        =   31
            Top             =   360
            Width           =   1500
         End
         Begin VB.CommandButton SetDataRead1 
            Caption         =   "設定値読込"
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3240
            TabIndex        =   28
            Top             =   240
            Width           =   1335
         End
         Begin VB.CommandButton DataSetButton1 
            Caption         =   "設定"
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   1680
            TabIndex        =   27
            Top             =   240
            Width           =   1335
         End
         Begin VB.Label RangeLabel1 
            Caption         =   "周波数レンジ値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   10
            Top             =   2760
            Width           =   1335
         End
         Begin VB.Label RateLabel1 
            Caption         =   "加減速レート値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   8
            Top             =   2160
            Width           =   1335
         End
         Begin VB.Label ConstLabel1 
            Caption         =   "定速値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   7
            Top             =   1560
            Width           =   1335
         End
         Begin VB.Label StartLabel1 
            Caption         =   "初速値"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   6
            Top             =   960
            Width           =   1215
         End
      End
   End
   Begin VB.ComboBox Id1 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1041
         SubFormatType   =   1
      EndProperty
      Height          =   300
      ItemData        =   "VBSampleMenu.frx":0000
      Left            =   2880
      List            =   "VBSampleMenu.frx":0034
      TabIndex        =   2
      Text            =   "0"
      Top             =   120
      Width           =   1095
   End
   Begin VB.CommandButton DevCloseButton 
      Caption         =   "DevClose"
      Enabled         =   0   'False
      Height          =   375
      Left            =   4680
      TabIndex        =   1
      Top             =   120
      Width           =   1800
   End
   Begin VB.CommandButton DevOpenButton 
      Caption         =   "DevOpen"
      Height          =   375
      Left            =   480
      TabIndex        =   0
      Top             =   120
      Width           =   1800
   End
   Begin VB.Label Label4 
      BorderStyle     =   1  '実線
      Caption         =   "Label4"
      Height          =   495
      Left            =   5040
      TabIndex        =   9
      Top             =   3000
      Width           =   1215
   End
End
Attribute VB_Name = "VBSampleMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CounterReadButton1_Click(Index As Integer)
    CountReadData1(0).Enabled = True
    CountReadData1(0).BackColor = &H80000009
    If (Tusbpmc_Get_AddrCounter(Id1.Text, 0, dat) = 0) Then
    CountReadData1(0).Text = dat
    Else
        MsgBox ("データ取得できませんでした。")
    End If
End Sub


Private Sub CounterReadButton2_Click(Index As Integer)
    CountReadData2(1).Enabled = True
    CountReadData2(1).BackColor = &H80000009
    If (Tusbpmc_Get_AddrCounter(Id1.Text, 1, dat) = 0) Then
    CountReadData2(1).Text = dat
    Else
        MsgBox ("データ取得できませんでした。")
    End If
End Sub

Private Sub CounterWriteButton1_Click(Index As Integer)
    Tusbpmc_Set_AddrCounter Id1.Text, 0, CountText1(0).Text
End Sub

Private Sub CounterWriteButton2_Click(Index As Integer)
    Tusbpmc_Set_AddrCounter Id1.Text, 1, CountText2(1).Text
End Sub

Private Sub DataSetButton1_Click(Index As Integer)
    Dim a As Integer
    a = 0
    If (StartText1(0).Text < 1 Or StartText1(0).Text > 8191) Then
        MsgBox ("初速値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (ConstText1(0).Text < 1 Or ConstText1(0).Text > 8191) Then
        MsgBox ("定速値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (RateText1(0).Text < 1 Or RateText1(0).Text > 8191) Then
        MsgBox ("加減速レート値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (RangeText1(0).Text < 1 Or RangeText1(0).Text > 8191) Then
        MsgBox ("周波数レンジ値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (a = 0) Then
        Tusbpmc_Set_Exec Id1.Text, 0, Val(StartText1(0).Text), Val(ConstText1(0).Text), Val(RateText1(0).Text), Val(RangeText1(0).Text)
    Else
        MsgBox ("データに誤りがあるので設定は実行されませんでした。")
    End If
End Sub

Private Sub DataSetButton2_Click(Index As Integer)
    Dim a As Integer
    a = 0
    If (StartText2(1).Text < 1 Or StartText2(1).Text > 8191) Then
        MsgBox ("初速値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (ConstText2(1).Text < 1 Or ConstText2(1).Text > 8191) Then
        MsgBox ("定速値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (RateText2(1).Text < 1 Or RateText2(1).Text > 8191) Then
        MsgBox ("加減速レート値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (RangeText2(1).Text < 1 Or RangeText2(1).Text > 8191) Then
        MsgBox ("周波数レンジ値の設定範囲は1〜8191です。再設定をしてください。")
        a = 1
    Else
        a = 0
    End If
    If (a = 0) Then
        Tusbpmc_Set_Exec Id1.Text, 1, Val(StartText2(1).Text), Val(ConstText2(1).Text), Val(RateText2(1).Text), Val(RangeText2(1).Text)
    Else
        MsgBox ("データに誤りがあるので設定は実行されませんでした。")
    End If
End Sub

Private Sub DevCloseButton_Click()
    Tusbpmc_Device_Close (Val(Id1.Text))
    DevCloseButton.Enabled = False
    DevOpenButton.Enabled = True
    Id1.Enabled = True
End Sub

Private Sub DevOpenButton_Click()
    If (Tusbpmc_Device_Open(Val(Id1.Text)) = 0) Then
       DevOpenButton.Enabled = False
       Id1.Enabled = False
       MsgBox ("表示画面は初期化されますが、デバイス内レジスタは変更されません。") + Chr$(13) & Chr$(10) + ("表示される値は電源投入後の初期化値です。電源投入後初めての起動") + Chr$(13) + Chr$(10) + ("でない場合には、表示状態と実際の設定状態が異なる事があります。") + Chr$(13) + Chr$(10), vbQuestion
       DevCloseButton.Enabled = True
       SSTab1.Enabled = True
       UnitSyncCheck.Enabled = True
       SetFrame1(0).Enabled = True
       DataSetButton1(1).Enabled = True
       SetDataRead1(0).Enabled = True
       StartLabel1(0).Enabled = True
       ConstLabel1(0).Enabled = True
       RateLabel1(0).Enabled = True
       RangeLabel1(0).Enabled = True
       StartText1(0).Enabled = True
       ConstText1(0).Enabled = True
       RateText1(0).Enabled = True
       RangeText1(0).Enabled = True
       PulseSelect1(0).Enabled = True
       EncoderCheck1(0).Enabled = True
       UpDownSel1(0).Enabled = True
       AB1Sel1(0).Enabled = True
       AB2Sel1(0).Enabled = True
       AB4Sel1(0).Enabled = True
       DownPointSel1(0).Enabled = True
       DownPointText1(0).Enabled = True
       LimitDataSet1(0).Enabled = True
       GetLimitStatus1(0).Enabled = True
       GetStatus1(0).Enabled = True
       ControleFrame1(0).Enabled = True
       StartButton1(0).Enabled = True
       CwCheck1(0).Enabled = True
       CcwCheck1(0).Enabled = True
       ContCheck1(0).Enabled = True
       IndexCheck1(0).Enabled = True
       IndexDataLabel1(0).Enabled = True
       IndexDataText1(0).Enabled = True
       EmergencyStopButton1(0).Enabled = True
       SlowStopButton1(0).Enabled = True
       SyncCheck1(0).Enabled = True
       HoldoffCheck1(0).Enabled = True
       CounterWriteButton1(0).Enabled = True
       CountText1(0).Enabled = True
       CounterReadButton1(0).Enabled = True
       CountReadData1(0).Enabled = True
       AddressText1(0).Enabled = True
       GetStatus1(0).Enabled = True
    Else
        MsgBox ("オープンできません")
    End If
End Sub


Private Sub DownPointSel1_Click(Index As Integer)
    If DownPointText1(0).Text >= 16777216 Then
    MsgBox ("数値の範囲は0〜16,777,215です。再設定してください。" + Chr$(13) + Chr$(10) + ("本コマンドは実行されませんでした") + Chr$(13) + Chr$(10))
    DownPointSel1(0).Value = False
    GoTo jump1:
    End If
    If DownPointSel1(0).Value = 1 Then
    Tusbpmc_Set_Slowdown Id1.Text, 0, 1, Val(DownPointText1(0).Text)
    Else
    Tusbpmc_Set_Slowdown Id1.Text, 0, 0, 0
    End If
jump1:
End Sub

Private Sub DownPointSel2_Click(Index As Integer)
    If DownPointText2(1).Text >= 16777216 Then
    MsgBox ("数値の範囲は0〜16,777,215です。再設定してください。" + Chr$(13) + Chr$(10) + ("本コマンドは実行されませんでした") + Chr$(13) + Chr$(10))
    DownPointSel2(1).Value = False
    GoTo jump2:
    End If
    If DownPointSel2(1).Value = 1 Then
    Tusbpmc_Set_Slowdown Id1.Text, 1, 1, Val(DownPointText2(1).Text)
    Else
    Tusbpmc_Set_Slowdown Id1.Text, 1, 0, 0
    End If
jump2:
End Sub

Private Sub EmergencyStopButton1_Click(Index As Integer)
    Tusbpmc_Stop_Control Id1.Text, 0, 0
End Sub

Private Sub EmergencyStopButton2_Click(Index As Integer)
    Tusbpmc_Stop_Control Id1.Text, 1, 0
End Sub


Private Sub EncoderCheck1_Click(Index As Integer)
        Dim mode As Integer
            If UpDownSel1(0).Value = 1 Then
                mode = 0
            ElseIf AB1Sel1(0).Value = 1 Then
                mode = 1
            ElseIf AB2Sel1(0).Value = 1 Then
                mode = 2
            ElseIf AB4Sel1(0).Value = 1 Then
                mode = 3
            Else
                mode = 0
            End If
        If EncoderCheck1(0).Value = 1 Then
            Tusbpmc_Sel_Encoder Id1.Text, 0, 1, mode
        Else
            Tusbpmc_Sel_Encoder Id1.Text, 0, 0, mode
        End If
End Sub

Private Sub EncoderCheck2_Click(Index As Integer)
        Dim mode As Integer
            If UpDownSel2(1).Value = 1 Then
                mode = 0
            ElseIf AB1Sel2(1).Value = 1 Then
                mode = 1
            ElseIf AB2Sel2(1).Value = 1 Then
                mode = 2
            ElseIf AB4Sel2(1).Value = 1 Then
                mode = 3
            Else
                mode = 0
            End If
        If EncoderCheck2(1).Value = 1 Then
            Tusbpmc_Sel_Encoder Id1.Text, 1, 1, mode
        Else
            Tusbpmc_Sel_Encoder Id1.Text, 1, 0, mode
        End If
End Sub

Private Sub GetLimitStatus1_Click(Index As Integer)
        Dim Label1a As String
        Dim Label2a As String
        Dim Label3a As String
        Dim Label4a As String
        Dim Label5a As String
        Dim Label6a As String
        Dim Label7a As String
        Dim Label8a As String
        Dim Label9a As String
        Dim Label10a As String
        Dim chkdataa As Integer
        Dim tmpdataa As Integer
        Tusbpmc_Get_LimitStatus Id1.Text, 0, status
        chkdataa = status
        tmpdataa = chkdataa And 1
        If tmpdataa = 0 Then
            Label1a = "N.C"
        Else
            Label1a = "N.O"
        End If
        tmpdataa = chkdataa And 2
        If tmpdataa = 0 Then
            Label2a = "N.C"
        Else
            Label2a = "N.O"
        End If
        tmpdataa = chkdataa And 4
        If tmpdataa = 0 Then
            Label3a = "無効"
        Else
            Label3a = "有効"
        End If
        tmpdataa = chkdataa And 8
        If tmpdataa = 0 Then
            Label4a = "無効"
        Else
            Label4a = "有効"
        End If
        tmpdataa = chkdataa And 16
        If tmpdataa = 0 Then
            Label5a = "無効"
        Else
            Label5a = "有効"
        End If
        tmpdataa = chkdataa And 32
        If tmpdataa = 0 Then
            Label6a = "緩停止"
        Else
            Label6a = "急停止"
        End If
        tmpdataa = chkdataa And 64
        If tmpdataa = 0 Then
            Label7a = "緩停止"
        Else
            Label7a = "急停止"
        End If
        tmpdataa = chkdataa And 128
        If tmpdataa = 0 Then
            Label8a = "N.C"
        Else
            Label8a = "N.O"
        End If
        tmpdataa = chkdataa And 256
        If tmpdataa = 0 Then
            Label9a = "無効"
        Else
            Label9a = "有効"
        End If
        tmpdataa = chkdataa And 512
        If tmpdataa = 0 Then
            Label10a = "緩停止"
        Else
            Label10a = "急停止"
        End If
        MsgBox ("H.P LS 停止モード　　　　　" + Label10a + Chr$(&HD) + Chr$(&HA) + "H.P LS 入力 　　　　　　　　" + Label9a + Chr$(&HD) + Chr$(&HA) + "H.P LS 接点タイプ　　　　　" + Label8a + Chr$(&HD) + Chr$(&HA) + "CCW LS 停止モード　　　　" + Label7a + Chr$(&HD) + Chr$(&HA) + "CW LS 停止モード　 　　　 " + Label6a + Chr$(&HD) + Chr$(&HA) + "CCW LS 入力　　　　　　　 " + Label5a + Chr$(&HD) + Chr$(&HA) + "CW LS 入力　　　　　　　　 " + Label4a + Chr$(&HD) + Chr$(&HA) + "CW<->CCW入力入替 　　" + Label3a + Chr$(&HD) + Chr$(&HA) + "CCW LS 接点タイプ　　　　" + Label2a + Chr$(&HD) + Chr$(&HA) + "CW LS 接点タイプ　　　　　" + Label1a + Chr$(&HD) + Chr$(&HA))
End Sub

Private Sub GetLimitStatus2_Click(Index As Integer)
        Dim Label1b As String
        Dim Label2b As String
        Dim Label3b As String
        Dim Label4b As String
        Dim Label5b As String
        Dim Label6b As String
        Dim Label7b As String
        Dim Label8b As String
        Dim Label9b As String
        Dim Label10b As String
        Dim chkdatab As Integer
        Dim tmpdatab As Integer
        Tusbpmc_Get_LimitStatus Id1.Text, 1, status
        chkdatab = status
        tmpdatab = chkdatab And 1
        If tmpdatab = 0 Then
            Label1b = "N.C"
        Else
            Label1b = "N.O"
        End If
        tmpdatab = chkdatab And 2
        If tmpdatab = 0 Then
            Label2b = "N.C"
        Else
            Label2b = "N.O"
        End If
        tmpdatab = chkdatab And 4
        If tmpdatab = 0 Then
            Label3b = "無効"
        Else
            Label3b = "有効"
        End If
        tmpdatab = chkdatab And 8
        If tmpdatab = 0 Then
            Label4b = "無効"
        Else
            Label4b = "有効"
        End If
        tmpdatab = chkdatab And 16
        If tmpdatab = 0 Then
            Label5b = "無効"
        Else
            Label5b = "有効"
        End If
        tmpdatab = chkdatab And 32
        If tmpdatab = 0 Then
            Label6b = "緩停止"
        Else
            Label6b = "急停止"
        End If
        tmpdatab = chkdatab And 64
        If tmpdatab = 0 Then
            Label7b = "緩停止"
        Else
            Label7b = "急停止"
        End If
        tmpdatab = chkdatab And 128
        If tmpdatab = 0 Then
            Label8b = "N.C"
        Else
            Label8b = "N.O"
        End If
        tmpdatab = chkdatab And 256
        If tmpdatab = 0 Then
            Label9b = "無効"
        Else
            Label9b = "有効"
        End If
        tmpdatab = chkdatab And 512
        If tmpdatab = 0 Then
            Label10b = "緩停止"
        Else
            Label10b = "急停止"
        End If
        MsgBox ("H.P LS 停止モード　　　　　" + Label10b + Chr$(&HD) + Chr$(&HA) + "H.P LS 入力 　　　　　　　　" + Label9b + Chr$(&HD) + Chr$(&HA) + "H.P LS 接点タイプ　　　　　" + Label8b + Chr$(&HD) + Chr$(&HA) + "CCW LS 停止モード　　　　" + Label7b + Chr$(&HD) + Chr$(&HA) + "CW LS 停止モード　 　　　 " + Label6b + Chr$(&HD) + Chr$(&HA) + "CCW LS 入力　　　　　　　 " + Label5b + Chr$(&HD) + Chr$(&HA) + "CW LS 入力　　　　　　　　 " + Label4b + Chr$(&HD) + Chr$(&HA) + "CW<->CCW入力入替 　　" + Label3b + Chr$(&HD) + Chr$(&HA) + "CCW LS 接点タイプ　　　　" + Label2b + Chr$(&HD) + Chr$(&HA) + "CW LS 接点タイプ　　　　　" + Label1b + Chr$(&HD) + Chr$(&HA))
End Sub

Private Sub GetStatus1_Click(Index As Integer)
        Dim LS1a As String
        Dim LS2a As String
        Dim HP3a As String
        Dim chka As Integer
        Dim tmpa As Integer
        Tusbpmc_Get_Status Id1.Text, 0, status
        chka = status
        tmpa = chka And 1
        If tmpa = 0 Then
            LS1a = "OFF---接点が開いています。"
        Else
            LS1a = "O N---接点が閉じています。"
        End If
        tmpa = chka And 2
        If tmpa = 0 Then
            LS2a = "OFF---接点が開いています。"
        Else
            LS2a = "O N---接点が閉じています。"
        End If
        tmpa = chka And 4
        If tmpa = 0 Then
            LS3a = "OFF---接点が開いています。"
        Else
            LS3a = "O N---接点が閉じています。"
        End If
        MsgBox (" CW LS 入力状態は現在、" + LS1a + Chr$(&HD) + Chr$(&HA) + "CCW LS 入力状態は現在、" + LS2a + Chr$(&HD) + Chr$(&HA) + "H.P LS 入力状態は現在、" + LS3a + Chr$(&HD) + Chr$(&HA))
End Sub

Private Sub GetStatus2_Click(Index As Integer)
        Dim LS1b As String
        Dim LS2b As String
        Dim HP3b As String
        Dim chkb As Integer
        Dim tmpb As Integer
        Tusbpmc_Get_Status Id1.Text, 1, status
        chkb = status
        tmpb = chkb And 1
        If tmpb = 0 Then
            LS1b = "OFF---接点が開いています。"
        Else
            LS1b = "O N---接点が閉じています。"
        End If
        tmpb = chkb And 2
        If tmpb = 0 Then
            LS2b = "OFF---接点が開いています。"
        Else
            LS2b = "O N---接点が閉じています。"
        End If
        tmpb = chkb And 4
        If tmpb = 0 Then
            LS3b = "OFF---接点が開いています。"
        Else
            LS3b = "O N---接点が閉じています。"
        End If
        MsgBox (" CW LS 入力状態は現在、" + LS1b + Chr$(&HD) + Chr$(&HA) + "CCW LS 入力状態は現在、" + LS2b + Chr$(&HD) + Chr$(&HA) + "H.P LS 入力状態は現在、" + LS3b + Chr$(&HD) + Chr$(&HA))
End Sub

Private Sub HoldoffCheck1_Click(Index As Integer)
        If HoldoffCheck1(0).Value = 1 Then
            Tusbpmc_Hold_Off Id1.Text, 0, 1
        Else
            Tusbpmc_Hold_Off Id1.Text, 0, 0
        End If
End Sub

Private Sub HoldoffCheck2_Click(Index As Integer)
        If HoldoffCheck2(1).Value = 1 Then
            Tusbpmc_Hold_Off Id1.Text, 1, 1
        Else
            Tusbpmc_Hold_Off Id1.Text, 1, 0
        End If
End Sub

Private Sub LimitDataSet1_Click(Index As Integer)
        LimitModeSet1.Visible = True
End Sub

Private Sub LimitDataSet2_Click(Index As Integer)
        LimitModeSet1.Visible = True
End Sub

Private Sub PulseSelect1_Click(Index As Integer)
    Dim sel As Integer
    If PulseSelect1(0).Value = 0 Then
    sel = 0
    Else
    sel = 1
    End If
    Tusbpmc_Sel_Pout Id1.Text, 0, sel
End Sub

Private Sub PulseSelect2_Click(Index As Integer)
    Dim sel As Integer
    If PulseSelect2(1).Value = 0 Then
    sel = 0
    Else
    sel = 1
    End If
    Tusbpmc_Sel_Pout Id1.Text, 1, sel
End Sub

Private Sub SetDataRead1_Click(Index As Integer)
       StartReadData1(0).Enabled = True
       StartReadData1(0).BackColor = &H80000009
       ConstReadData1(0).Enabled = True
       ConstReadData1(0).BackColor = &H80000009
       RateReadData1(0).Enabled = True
       RateReadData1(0).BackColor = &H80000009
       RangeReadData1(0).Enabled = True
       RangeReadData1(0).BackColor = &H80000009
        If (Tusbpmc_Get_PmcStatus(Id1.Text, 0, strtSpeed, constSpeed, accRate, freqRange) = 0) Then
            StartReadData1(0).Text = strtSpeed
            ConstReadData1(0).Text = constSpeed
            RateReadData1(0).Text = accRate
            RangeReadData1(0).Text = freqRange
        Else
            MsgBox ("取得に失敗しました")
        End If
End Sub

Private Sub SetDataRead2_Click(Index As Integer)
       StartReadData2(1).Enabled = True
       StartReadData2(1).BackColor = &H80000009
       ConstReadData2(1).Enabled = True
       ConstReadData2(1).BackColor = &H80000009
       RateReadData2(1).Enabled = True
       RateReadData2(1).BackColor = &H80000009
       RangeReadData2(1).Enabled = True
       RangeReadData2(1).BackColor = &H80000009
        If (Tusbpmc_Get_PmcStatus(Id1.Text, 1, strtSpeed, constSpeed, accRate, freqRange) = 0) Then
            StartReadData2(1).Text = strtSpeed
            ConstReadData2(1).Text = constSpeed
            RateReadData2(1).Text = accRate
            RangeReadData2(1).Text = freqRange
        Else
            MsgBox ("取得に失敗しました")
        End If
End Sub

Private Sub SlowStopButton1_Click(Index As Integer)
    Tusbpmc_Stop_Control Id1.Text, 0, 1
End Sub

Private Sub SlowStopButton2_Click(Index As Integer)
    Tusbpmc_Stop_Control Id1.Text, 1, 1
End Sub

Private Sub SSTab1_Click(Index As Integer)
    If SSTab1.Tab = 1 Then
       SetFrame2(1).Enabled = True
       DataSetButton2(1).Enabled = True
       SetDataRead2(1).Enabled = True
       StartLabel2(1).Enabled = True
       ConstLabel2(1).Enabled = True
       RateLabel2(1).Enabled = True
       RangeLabel2(1).Enabled = True
       StartText2(1).Enabled = True
       ConstText2(1).Enabled = True
       RateText2(1).Enabled = True
       RangeText2(1).Enabled = True
       PulseSelect2(1).Enabled = True
       EncoderCheck2(1).Enabled = True
       UpDownSel2(1).Enabled = True
       AB1Sel2(1).Enabled = True
       AB2Sel2(1).Enabled = True
       AB4Sel2(1).Enabled = True
       DownPointSel2(1).Enabled = True
       DownPointText2(1).Enabled = True
       LimitDataSet2(1).Enabled = True
       GetLimitStatus2(1).Enabled = True
       ControleFrame2(1).Enabled = True
       StartButton2(1).Enabled = True
       CwCheck2(1).Enabled = True
       CcwCheck2(1).Enabled = True
       ContCheck2(1).Enabled = True
       IndexCheck2(1).Enabled = True
       IndexDataLabel2(1).Enabled = True
       IndexDataText2(1).Enabled = True
       EmergencyStopButton2(1).Enabled = True
       SlowStopButton2(1).Enabled = True
       SyncCheck2(1).Enabled = True
       HoldoffCheck2(1).Enabled = True
       CounterWriteButton2(1).Enabled = True
       CountText2(1).Enabled = True
       CounterReadButton2(1).Enabled = True
       CountReadData2(1).Enabled = True
       AddressText2(1).Enabled = True
    Else
       SetFrame1(0).Enabled = True
       DataSetButton1(1).Enabled = True
       SetDataRead1(0).Enabled = True
       StartLabel1(0).Enabled = True
       ConstLabel1(0).Enabled = True
       RateLabel1(0).Enabled = True
       RangeLabel1(0).Enabled = True
       StartText1(0).Enabled = True
       ConstText1(0).Enabled = True
       RateText1(0).Enabled = True
       RangeText1(0).Enabled = True
       PulseSelect1(0).Enabled = True
       EncoderCheck1(0).Enabled = True
       UpDownSel1(0).Enabled = True
       AB1Sel1(0).Enabled = True
       AB2Sel1(0).Enabled = True
       AB4Sel1(0).Enabled = True
       DownPointSel1(0).Enabled = True
       DownPointText1(0).Enabled = True
       LimitDataSet1(0).Enabled = True
       GetLimitStatus1(0).Enabled = True
       ControleFrame1(0).Enabled = True
       StartButton1(0).Enabled = True
       CwCheck1(0).Enabled = True
       CcwCheck1(0).Enabled = True
       ContCheck1(0).Enabled = True
       IndexCheck1(0).Enabled = True
       IndexDataLabel1(0).Enabled = True
       IndexDataText1(0).Enabled = True
       EmergencyStopButton1(0).Enabled = True
       SlowStopButton1(0).Enabled = True
       SyncCheck1(0).Enabled = True
       HoldoffCheck1(0).Enabled = True
       CounterWriteButton1(0).Enabled = True
       CountText1(0).Enabled = True
       CounterReadButton1(0).Enabled = True
       CountReadData1(0).Enabled = True
       AddressText1(0).Enabled = True
    End If
End Sub

Private Sub StartButton1_Click(Index As Integer)
    Dim a As Integer
    Dim dir As Integer
    Dim mode As Integer
    Dim dat As Long
    If (IndexDataText1(0).Text < 1 Or IndexDataText1(0).Text > 16777215) Then
        MsgBox ("IndexDataの値は1〜16,777,215です。値が範囲外なので再設定してください。")
        a = 1
    Else
        a = 0
    End If
    If (CwCheck1(0).Value = True) Then
        dir = 0
    Else
        dir = 1
    End If
    If (CcwCheck1(0).Value = True) Then
        dir = 1
    Else
        dir = 0
    End If
    If (ContCheck1(0).Value = True) Then
        mode = 0
    Else
        mode = 1
    End If
    If (IndexCheck1(0).Value = True) Then
        mode = 1
    Else
        mode = 0
    End If
    If (a = 0) Then
        dat = Val(IndexDataText1(0).Text)
        Tusbpmc_Start_Control Id1.Text, 0, dir, mode, dat
    Else
        MsgBox ("設定値に誤りがあります。この関数は実行されませんでした。")
    End If
End Sub

Private Sub StartButton2_Click(Index As Integer)
    Dim a As Integer
    Dim dir As Integer
    Dim mode As Integer
    Dim dat As Long
    If (IndexDataText2(1).Text < 1 Or IndexDataText2(1).Text > 16777215) Then
        MsgBox ("IndexDataの値は1〜16,777,215です。値が範囲外なので再設定してください。")
        a = 1
    Else
        a = 0
    End If
    If (CwCheck2(1).Value = True) Then
        dir = 0
    Else
        dir = 1
    End If
    If (CcwCheck2(1).Value = True) Then
        dir = 1
    Else
        dir = 0
    End If
    If (ContCheck2(1).Value = True) Then
        mode = 0
    Else
        mode = 1
    End If
    If (IndexCheck2(1).Value = True) Then
        mode = 1
    Else
        mode = 0
    End If
    If (a = 0) Then
        dat = Val(IndexDataText2(1).Text)
        Tusbpmc_Start_Control Id1.Text, 1, dir, mode, dat
    Else
        MsgBox ("設定値に誤りがあります。この関数は実行されませんでした。")
    End If
End Sub

Private Sub SyncCheck1_Click(Index As Integer)
        If SyncCheck1(0).Value = 1 Then
            Tusbpmc_Set_Sync Id1.Text, 0, 1
        Else
            Tusbpmc_Set_Sync Id1.Text, 0, 0
        End If
End Sub

Private Sub SyncCheck2_Click(Index As Integer)
        If SyncCheck2(1).Value = 1 Then
            Tusbpmc_Set_Sync Id1.Text, 1, 1
        Else
            Tusbpmc_Set_Sync Id1.Text, 1, 0
        End If
End Sub

Private Sub UnitSyncCheck_Click()
        If UnitSyncCheck.Value = 1 Then
            Tusbpmc_Set_Unit Id1.Text, 1
        Else
            Tusbpmc_Set_Unit Id1.Text, 0
        End If
End Sub
