/******************************************************************************
  USBM3069.h
  更新日:2008/5/23
  ファイルバージョン:3.4.2.1
  
  USBM3069.DLL用ヘッダーファイル
  
  2008/5/23  Ver.3.4.2.1
  ・USBM_OpenByPI()のOptにデフォルト値を設定
  ・DLL3.4.2.xに対応
    フラッシュメモリ操作関数を追加
    USBM_OpenA()をフラッシュ書換えモードに対応
    USBM_GetIFType()の仕様変更
    一部の関数がマルチスレッドに対応
    フラッシュ書換えモードのLANM3069への接続を改善

  2008/1/23  Ver.3.3.2.1 DLL3.3.2.1に対応
  2007/10/18 Ver.3.3.1.3 DLL3.3.1.3に対応 
  2007/7/20  Ver.3.3.0.3 DLL3.3.0.xに対応
  2007/6/27  Ver.3.1.1.2 コメントを一部修正
  2007/5/15  Ver.3.1.1.1 Windows CE に対応した記述に変更

  テクノウェーブ株式会社 Copyright 2005-2008
*******************************************************************************/
#ifndef _USBM3069_H_
#define _USBM3069_H_

#include "oleauto.h"


#ifndef __TW_BASE_TYPES__
#define __TW_BASE_TYPES__

typedef PVOID TW_HANDLE;
typedef ULONG TW_STATUS;

enum {
  TW_OK,                     //正常終了
  TW_INVALID_HANDLE,         //ハンドルが無効
  TW_DEVICE_NOT_FOUND,       //デバイスが見つからない
  TW_DEVICE_NOT_OPENED,
  TW_IO_ERROR,               //送受信中にエラーが発生した
  TW_INSUFFICIENT_RESOURCES, //リソースエラー(デバイスの最大数を超えた場合など)
  TW_INVALID_PARAMETER,
  TW_INVALID_BAUD_RATE,

  TW_DEVICE_NOT_OPENED_FOR_ERASE,
  TW_DEVICE_NOT_OPENED_FOR_WRITE,
  TW_FAILED_TO_WRITE_DEVICE,
  TW_EEPROM_READ_FAILED,
  TW_EEPROM_WRITE_FAILED,
  TW_EEPROM_ERASE_FAILED,
  TW_EEPROM_NOT_PRESENT,
  TW_EEPROM_NOT_PROGRAMMED,
  TW_INVALID_ARGS,         //引数が無効
  TW_NOT_SUPPORTED,        //サポートされない機能
  TW_OTHER_ERROR           //その他のエラー
};
//>Ver.3.1.0.x
enum{
  TW_TIMEOUT = 0xffff0001, //送信または受信処理がタイムアウト
  TW_FILE_ERROR,           //ファイルに関するエラー
  TW_MEMORY_ERROR,         //メモリの確保エラー
  TW_DATA_NOT_FOUND,       //有効なデータが無い
  TW_SOCKET_ERROR,         //Socket API のエラー
  TW_ACCESS_DENIED,        //認証エラー
  //>Ver.3.4.0.1
  TW_NOT_SUPPORTED_MODE,   //呼び出された関数は現在のモードではサポートされない
  TW_FLASH_MODE_DEVICE,    //接続したデバイスが通常モードで無いため制御できない
  //<Ver.3.4.0.1
};
#endif 
//<Ver.3.1.0.x


//FTD2XX.hのインクルードが必要な場合は、このファイルより先に読み込んでください。
#ifndef FTD2XX_H //FTD2XX.hがインクルードされていない場合

typedef PVOID FT_HANDLE;
typedef ULONG FT_STATUS;

enum {
  FT_OK,
  FT_INVALID_HANDLE,
  FT_DEVICE_NOT_FOUND,
  FT_DEVICE_NOT_OPENED,
  FT_IO_ERROR,
  FT_INSUFFICIENT_RESOURCES,
  FT_INVALID_PARAMETER,
  FT_INVALID_BAUD_RATE,

  FT_DEVICE_NOT_OPENED_FOR_ERASE,
  FT_DEVICE_NOT_OPENED_FOR_WRITE,
  FT_FAILED_TO_WRITE_DEVICE,
  FT_EEPROM_READ_FAILED,
  FT_EEPROM_WRITE_FAILED,
  FT_EEPROM_ERASE_FAILED,
  FT_EEPROM_NOT_PRESENT,
  FT_EEPROM_NOT_PROGRAMMED,
  FT_INVALID_ARGS,
  FT_NOT_SUPPORTED,
  FT_OTHER_ERROR
};

#define FT_PURGE_RX 1
#define FT_PURGE_TX 2

#endif


//>Ver.3.1.0.x
//ATF関連エラー
enum{
  TW_ATF_ERR_FILE_VERSION = 0xffff0101, //対応するバージョンより新しいファイルフォーマット
  TW_ATF_ERR_ILLEGAL_FILE,              //ファイルの内容が不正
  TW_ATF_ERR_SERVICE_VERSION,           //使用するサービスをデバイスがサポートしていない
  TW_ATF_ERR_PASSWORD_REQUIRED,         //ATFを復号するためのパスワードが必要
};

//>Ver.3.4.x.x
//フラッシュ操作関連エラー
enum{
  TW_FLASH_ERR_CLASS = 0xffff0200,
  TW_FLASH_ERR_DOWNLOAD = 0xffff0201, //ダウンロードエラー(フラッシュ用プログラムの転送に失敗)
  TW_FLASH_ERR_INIT,                  //初期化エラー
  TW_FLASH_ERR_ERASE,                 //消去エラー
  TW_FLASH_ERR_WRITE,                 //書込みエラー
  TW_FLASH_ERR_COMMAND,               //コマンドエラー
  TW_FLASH_ERR_CHKSUM,                //チェックサムエラー
  TW_FLASH_ERR_SIZE = 0xffff0210,     //サイズの指定が不正
  TW_FLASH_ERR_ADDRESS,               //アドレスの指定が不正
  TW_FLASH_ERR_NOT_ERASED,            //書き込む前に消去が必要
};
//<Ver3.4.x.x


#pragma pack(1)
//ATFの情報を取得するための構造体
typedef struct {
  DWORD FormatVer;      //ファイルフォーマットのバージョン
  DWORD FirmwareVer;    //必要なファームウェアのバージョン
  char Description[32]; //ATFの名称、説明など
  char Manufacture[32]; //製造元
  DWORD ProgramVer;     //納められているプログラムのバージョン
  DWORD AddressTop;     //使用するRAM領域の先頭アドレス
  DWORD AddressBottom;  //使用するRAM領域の最終アドレス+1
  DWORD CommandAddress; //コマンドハンドラのアドレス
  WORD  EncryptMode;    //暗号(0:なし、1:予約、2:ユーザーパスワードによる暗号)
  WORD  wRsv;
  DWORD MainAddress;    //メイン関数のアドレス
} USBM_ATF_INFO;


//製品情報を取得するための構造体
typedef struct {
  WORD Empty;           //必ず0とする
  WORD wRsv;            //予約
  UUID ID;              //UUID構造体(UUIDの保存)
  DWORD Number;         //任意の番号
  char Description[32]; //製品情報
  char Manufacture[32]; //製造元情報
  BYTE Reserve[40];     //予約
} USBM_PRODUCT_INFO;
#pragma pack()


#define USBM_IF_USB 0x80000000   //USBインタフェース
#define USBM_IF_LAN 0x40000000   //LANインタフェース
#define USBM_IF_ANY 0xff000000   //全てのインタフェースとマッチ

#define USBM_LIST_UPDATE 0x00000001 //LANデバイスの内部テーブル情報をを更新するかのフラグ
//>Ver.3.4.0.1
#define USBM_MODE_FLASH  0x00800000 //フラッシュ書換えモードとなっているデバイスに接続する
//<Ver.3.4.0.1

#define USBM_MAX_DEVICE 256      //オープンできる最大のデバイス数
#define USBM_DEFAULT_PORT 0xc000 //LANデバイスのアクセスに使用するデフォルトのポート番号

//<Ver.3.1.0.x

//>Ver.3.3.0.x
#define TW_ATF_STATUS  0xfffffe00 //ATFで共通に使用するステータスの先頭番号
#define TW_USER_STATUS 0xffffff00 //ユーザー定義のステータスの先頭番号
//<Ver.3.3.0.x

//>Ver.3.3.2.1
//初期化オプション
#define USBM_INIT_PORT_DIR  0x00000001
#define USBM_INIT_PORT_DATA 0x00000002
#define USBM_INIT_BUS       0x00000004
#define USBM_INIT_DMA       0x00000008
#define USBM_INIT_TIMER     0x00000010
#define USBM_INIT_AD        0x00000020
#define USBM_INIT_SCI       0x00000040
#define USBM_INIT_PC        0x00000080
#define USBM_INIT_TCPY      0x00000100
#define USBM_INIT_ALL       0xffffffff
//<Ver.3.3.2.1

//>Ver.3.4.0.1
//フラッシュのアドレス
#define USBM_EB1_TOP 0x1000 //EB1の先頭アドレス
#define USBM_EB1_BTM 0x1fff //EB1の最終アドレス
#define USBM_EB2_TOP 0x2000 //EB2の先頭アドレス
#define USBM_EB2_BTM 0x2fff //EB2の最終アドレス
#define USBM_EB3_TOP 0x3000 //EB3の先頭アドレス
#define USBM_EB3_BTM 0x3fff //EB3の最終アドレス
//<Ver.3.4.0.1

#define USBM_PURGE_RX 1
#define USBM_PURGE_TX 2


/*ステータス*/
#define USBM_STS_TIMEOUT        0x0001
#define USBM_STS_ILLEGAL_ACCESS 0x0002

/*ポートアドレス*/
#define USBM_POUT 0xffffffff  //POUT(出力専用)を示す特別のアドレス
#define USBM_P1   0x00ffffd0  //P1(入力専用)
#define USBM_P2   0x00ffffd1  //P2(入力専用)
#define USBM_P4   0x00ffffd3  //P4(入出力)
#define USBM_P5   0x00ffffd4  //P5(入力専用)
#define USBM_PA   0x00ffffd9  //PA(入出力)

//DAコンバータのアドレス
#define USBM_DA0 0x00ffff9c
#define USBM_DA1 0x00ffff9d

//外部バスのアドレス
#define USBM_AREA0_TOP 0x100000
#define USBM_AREA2_TOP 0x400000
#define USBM_AREA3_TOP 0x600000
#define USBM_AREA5_TOP 0xa00000
#define USBM_USER_AREA 0xffbf20

//PCピン(IRQ)を読むためのIOポートアドレス
#define USBM_PC0_ADDR 0xffffd8
#define USBM_PC1_ADDR 0xffffd8
#define USBM_PC2_ADDR 0xffffd7
#define USBM_PC3_ADDR 0xffffd7

//PCピン(IRQ)を読むためのビットマスク
#define USBM_PC0_BIT 0x10
#define USBM_PC1_BIT 0x20
#define USBM_PC2_BIT 0x02
#define USBM_PC3_BIT 0x04

//16ビットタイマクロック
#define USBM_TCLK25000  0x00  //25MHz
#define USBM_TCLK12500  0x01  //12.5MHz
#define USBM_TCLK6250   0x02  //6250kHz
#define USBM_TCLK3125   0x03  //3125kHz
#define USBM_TCLKA      0x04  //TCLKA
#define USBM_TCLKB      0x05  //TCLKB

//16ビットタイマインプットキャプチャ用エッジ設定
#define USBM_CAPT_RISE  0x04
#define USBM_CAPT_FALL  0x05
#define USBM_CAPT_BOTH  0x07

//TCPY(8ビットタイマ)専用
#define USBM_TCLK390    0x06  //390.625kHz
#define USBM_TCLK3      0x07  //3052Hz
#define USBM_TCLKUP     0x08  //外部クロック立ち上がり
#define USBM_TCLKDOWN   0x09  //外部クロック立ち下がり
#define USBM_TCLKBOTH   0x0a  //外部クロック両エッジ

//SCI
#define USBM_SCI_DATA8    0x00  //8ビットデータ
#define USBM_SCI_DATA7    0x40  //7ビットデータ
#define USBM_SCI_NOPARITY 0x00  //パリティなし
#define USBM_SCI_EVEN     0x20  //偶数パリティ
#define USBM_SCI_ODD      0x30  //奇数パリティ
#define USBM_SCI_STOP1    0x00  //1ストップビット
#define USBM_SCI_STOP2    0x08  //2ストップビット
#define USBM_SCI_BAUD300    0x02A2  //9ビット=CKS1,8ビット=CKS0,7-0ビット=BRR,
#define USBM_SCI_BAUD600    0x0250
#define USBM_SCI_BAUD1200   0x01A2
#define USBM_SCI_BAUD2400   0x0150
#define USBM_SCI_BAUD4800   0x00A2
#define USBM_SCI_BAUD9600   0x0050
#define USBM_SCI_BAUD14400  0x0035
#define USBM_SCI_BAUD19200  0x0028
#define USBM_SCI_BAUD38400  0x0013

//メモリエリアを表す定数
#define USBM_AREA0 0x01
#define USBM_AREA2 0x04
#define USBM_AREA3 0x08
#define USBM_AREA5 0x10

//パルスカウンタを表す定数
#define USBM_PC0 0x10
#define USBM_PC1 0x20
#define USBM_PC2 0x02
#define USBM_PC3 0x04
#define USBM_PC_ALL -1


#ifdef WINCE
typedef LPCWSTR TW_CSTR;
typedef LPWSTR TW_STR;
typedef WCHAR TW_CHAR;
#else
typedef LPCSTR TW_CSTR;
typedef LPSTR TW_STR;
typedef char TW_CHAR;
#endif


#ifdef __cplusplus
extern "C" {
long _stdcall USBM_ListDevices(TW_CHAR (*pSrerial)[9] = NULL, long nSerial = 0);
long _stdcall USBM_ListDevicesByID(WORD VID, WORD PID, TW_CHAR (*pSerial)[9], long nSerial);
TW_HANDLE _stdcall USBM_OpenByUA(TW_CSTR pUA, long nUA);
TW_HANDLE _stdcall USBM_Open(TW_CSTR pSerial = NULL);
TW_HANDLE _stdcall USBM_OpenByID(WORD VID, WORD PID, TW_CSTR pSerial);
TW_STATUS _stdcall USBM_Close(TW_HANDLE hDev);

TW_STATUS _stdcall USBM_Initialize(TW_HANDLE hDev);
TW_STATUS _stdcall USBM_AddressEnable(TW_HANDLE hDev, long nBits);
TW_STATUS _stdcall USBM_CSEnable(TW_HANDLE hDev, long Area);
TW_STATUS _stdcall USBM_BusSetWait(TW_HANDLE hDev, long Area, BYTE Wait);

TW_STATUS _stdcall USBM_PortRead8(TW_HANDLE hDev, DWORD Port, BYTE *pData);
TW_STATUS _stdcall USBM_PortWrite8(TW_HANDLE hDev,DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_PortRead16(TW_HANDLE hDev, DWORD Port, WORD *pData);
TW_STATUS _stdcall USBM_PortWrite16(TW_HANDLE hDev, DWORD Port, WORD Data);
TW_STATUS _stdcall USBM_PortBWrite(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc = 1, BYTE Dma = FALSE);
TW_STATUS _stdcall USBM_PortBRead(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc = 1, BYTE Dma = FALSE);
TW_STATUS _stdcall USBM_PortSetDir(TW_HANDLE hDev, DWORD Port, BYTE Bits);
TW_STATUS _stdcall USBM_PortBCopy(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort, long nData, long SrcInc, long DstInc);
TW_STATUS _stdcall USBM_PortCopy8(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort);
TW_STATUS _stdcall USBM_PortCopy16(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort);

TW_STATUS _stdcall USBM_TimerEnable(TW_HANDLE hDev, BYTE Bits, long MDF);
TW_STATUS _stdcall USBM_TimerSetLevel(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetPulse(TW_HANDLE hDev, long CH, WORD LtoH, WORD HtoL);
TW_STATUS _stdcall USBM_TimerSetClk(TW_HANDLE hDev, long CH, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetCnt(TW_HANDLE hDev, long CH, short Cnt);
TW_STATUS _stdcall USBM_TimerStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerReadCnt(TW_HANDLE hDev, long CH, short *pCnt);
TW_STATUS _stdcall USBM_TimerSetCmp(TW_HANDLE hDev, short CmpA, short CmpB);
TW_STATUS _stdcall USBM_TimerSetCmpOut(TW_HANDLE hDev, long CmpReg, DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_TimerSetCmpClr(TW_HANDLE hDev, BYTE Bits);

TW_STATUS _stdcall USBM_ADRead(TW_HANDLE hDev, WORD *pData, long CH, long Scan = FALSE);
TW_STATUS _stdcall USBM_ADSetCycle(TW_HANDLE hDev, BYTE Cmp, long CLK = USBM_TCLK3125);
TW_STATUS _stdcall USBM_ADBRead(TW_HANDLE hDev, WORD *pData, DWORD nData, long CH, DWORD *pnRead = NULL);

TW_STATUS _stdcall USBM_SCISetMode(TW_HANDLE hDev, long CH, long Mode = 0x00, long Baud = USBM_SCI_BAUD9600);
TW_STATUS _stdcall USBM_SCIReadStatus(TW_HANDLE hDev, long CH, BYTE *pStatus, long *pnReceive);
TW_STATUS _stdcall USBM_SCIRead(TW_HANDLE hDev, long CH, void *pData, long nData, long *pnRead = NULL);
TW_STATUS _stdcall USBM_SCIWrite(TW_HANDLE hDev, long CH, void *pData, long nData);

TW_STATUS _stdcall USBM_PCSetCmp(TW_HANDLE hDev, long CH, long Cmp);
TW_STATUS _stdcall USBM_PCSetCnt(TW_HANDLE hDev, long CH, long Cnt);
TW_STATUS _stdcall USBM_PCReadCnt(TW_HANDLE hDev, long CH, long *pCnt);
TW_STATUS _stdcall USBM_PCSetControl(TW_HANDLE hDev, long CH, BYTE Bits);
TW_STATUS _stdcall USBM_PCSetCondBit(TW_HANDLE hDev, long CH, DWORD Port, BYTE Mask);
TW_STATUS _stdcall USBM_PCSetCmpOut(TW_HANDLE hDev, long CH, DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_PCStart(TW_HANDLE hDev, BYTE Bits);

TW_STATUS _stdcall USBM_TCPYStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TCPYSetParm(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort,
                    long nCopy, long SrcInc, long DstInc, long lLoop);
TW_STATUS _stdcall USBM_TCPYSetCmp(TW_HANDLE hDev, long CH, BYTE Cmp);
TW_STATUS _stdcall USBM_TCPYSetClk(TW_HANDLE hDev, long CH, long CLK);
TW_STATUS _stdcall USBM_TCPYSetCycle(TW_HANDLE hDev, long CH, BYTE Cmp, long CLK = USBM_TCLK3125);

TW_STATUS _stdcall USBM_Write(TW_HANDLE hDev, void *pData, DWORD nData, DWORD *pWritten);
TW_STATUS _stdcall USBM_Read(TW_HANDLE hDev, void *pData, DWORD nData, DWORD *pRead);

TW_STATUS _stdcall USBM_ReadStatus(TW_HANDLE hDev, DWORD *pStat);
TW_STATUS _stdcall USBM_ReadVersion(TW_HANDLE hDev, DWORD *pVer);

TW_STATUS _stdcall USBM_Abort(TW_HANDLE hDev);

TW_STATUS _stdcall USBM_TimerStartA(TW_HANDLE hDev, BYTE Bits, long TrigPC = -1, long Repeat = FALSE, long Cmp = 1);
TW_STATUS _stdcall USBM_TimerStop(TW_HANDLE hDev, BYTE Bits, long TrigPC = -1, long Repeat = FALSE, long Cmp = 1);
TW_STATUS _stdcall USBM_DASetCycle(TW_HANDLE hDev,long CH,long Cmp,long CLK);
TW_STATUS _stdcall USBM_DASetParm(TW_HANDLE hDev,long CH,DWORD SrcPort,long nData,long lLoop);
TW_STATUS _stdcall USBM_DAReadStatus(TW_HANDLE hDev,long CH,long *pnData);
TW_STATUS _stdcall USBM_DAStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_DAStop(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TCPYSetPatternCtrl(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort,
                                       long nCopy, long nSrc, long SrcInc, long Start = 0, BYTE Mask = 0xff);
TW_STATUS _stdcall USBM_TCPYSetTrig(TW_HANDLE hDev, long CH, long StartPC, long StopPC,long Repeat);
TW_STATUS _stdcall USBM_TCPYReadStatus(TW_HANDLE hDev, long CH, long *pnCopy, long *pSrcPos);
TW_STATUS _stdcall USBM_TimerSetCapture(TW_HANDLE hDev, long CH, BYTE EdgeA = USBM_CAPT_RISE, BYTE EdgeB = USBM_CAPT_RISE);
TW_STATUS _stdcall USBM_TimerReadCaptureCnt(TW_HANDLE hDev, long CH, long *pCntA, long *pCntB);
TW_STATUS _stdcall USBM_TimerSetCaptureCnt(TW_HANDLE hDev, long CH, long CntA = 0, long CntB = 0);

TW_STATUS _stdcall USBM_ADStart(TW_HANDLE hDev, long nCnv, long CH, long Scan, long lByte = FALSE, long Trig = FALSE);
TW_STATUS _stdcall USBM_ADCopy(TW_HANDLE hDev, DWORD DstPort, long nCnv, long CH,
                           long CKS, long DMA_CH, long DstInc = TRUE);
TW_STATUS _stdcall USBM_ADReadCopyStatus(TW_HANDLE hDev,long *pnCnv,long DMA_CH);
TW_STATUS _stdcall USBM_ADReadCopyBuffer(TW_HANDLE hDev, DWORD Port, WORD *pData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_ADStopCopy(TW_HANDLE hDev, long DMA_CH);
TW_STATUS _stdcall USBM_SCISetDelimiter(TW_HANDLE hDev, long CH, char *pDelimiter, long nDelimiter);
TW_STATUS _stdcall USBM_GetQueueStatus(TW_HANDLE hDev, DWORD *pnQueue);
TW_STATUS _stdcall USBM_Purge(TW_HANDLE hDev, DWORD dwMask);
TW_STATUS _stdcall USBM_SetTimeouts(TW_HANDLE hDev, DWORD dwReadTimeout, DWORD dwWriteTimeout);
TW_STATUS _stdcall USBM_PCStartA(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_PCStop(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetSinglePulse(TW_HANDLE hDev, long CH, long Setup, long Width, long Pos = FALSE);

FT_HANDLE _stdcall USBM_GetFTHandle(TW_HANDLE hDev);

//>Ver.3.1.0.x
TW_STATUS _stdcall USBM_ReadPI(TW_HANDLE hDev, USBM_PRODUCT_INFO *pInfo);
TW_STATUS _stdcall USBM_OpenByPI(TW_HANDLE *phDev, TW_CSTR pUuid, DWORD Number, long Opt = USBM_IF_ANY | USBM_LIST_UPDATE);
TW_STATUS _stdcall USBM_PortWrite8A(TW_HANDLE hDev, DWORD Port, BYTE Data, BYTE Mask = 0xff);

TW_STATUS _stdcall USBM_SetPassword(TW_CSTR pPass = NULL);
TW_STATUS _stdcall USBM_SetNetworkPort(DWORD PortNumber = 0);
DWORD _stdcall USBM_GetIFType(TW_HANDLE hDev);
TW_STATUS _stdcall USBM_OpenA(TW_HANDLE *phDev, TW_CSTR pIID = NULL, long Opt = USBM_IF_ANY | USBM_LIST_UPDATE);
TW_STATUS _stdcall USBM_ListDevicesA(long *pnDev, TW_STR pIID, long *pnIID, long Opt);
//<Ver.3.1.0.x

//>Ver.3.3.0.x
TW_STATUS _stdcall USBM_ReadVersionA(TW_HANDLE hDev, DWORD *pVersion);
TW_STATUS _stdcall USBM_ATFSetCommand(TW_HANDLE hDev, DWORD Address);
TW_STATUS _stdcall USBM_ATFSetMain(TW_HANDLE hDev, DWORD Address);
TW_STATUS _stdcall USBM_ATFUserCommand(TW_HANDLE hDev, WORD Command = 0, DWORD Param1 = 0, DWORD Param2 = 0, void *pData = NULL, long nData = 0);
TW_STATUS _stdcall USBM_ATFDownload(TW_HANDLE hDev, TW_CSTR FileName, TW_CSTR Reserve = NULL);
TW_STATUS _stdcall USBM_ATFGetInfo(TW_CSTR FileName, USBM_ATF_INFO *pInfo);
//<Ver.3.3.0.x

//>Ver.3.3.2.x
TW_STATUS _stdcall USBM_InitializeA(TW_HANDLE hDev, DWORD InitOption = USBM_INIT_ALL);
//<Ver.3.3.2.x

//>Ver.3.4.0.1
TW_STATUS _stdcall USBM_FlashEraseBlk(TW_HANDLE hDev, long Blk);
TW_STATUS _stdcall USBM_FlashRead(TW_HANDLE hDev, DWORD Address, void *pBuff, DWORD nData);
TW_STATUS _stdcall USBM_FlashWrite(TW_HANDLE hDev, DWORD Address, void *pData, DWORD nData);
//<Ver.3.4.0.1

//  OLEアクセス用
long _stdcall USBM_ListDevices2(LPSAFEARRAY *ppSerial);
long _stdcall USBM_ListDevicesByID2(WORD VID, WORD PID, LPSAFEARRAY *ppSerial);
TW_STATUS _stdcall USBM_PortBWrite2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc = 1, BYTE Dma = TRUE);
TW_STATUS _stdcall USBM_PortBRead2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc = 1, BYTE Dma = TRUE);
TW_STATUS _stdcall USBM_ADRead2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long CH, long Scan = FALSE);
TW_STATUS _stdcall USBM_ADBRead2(TW_HANDLE hDev, LPSAFEARRAY *ppData, DWORD nData, long CH, DWORD *pnRead = NULL);
TW_STATUS _stdcall USBM_SCIRead2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nData, long *pnRead = NULL);
TW_STATUS _stdcall USBM_SCIWrite2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nData);
TW_STATUS _stdcall USBM_PCReadCnt2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppCnt);
TW_STATUS _stdcall USBM_Write2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long nData, long *pWritten);
TW_STATUS _stdcall USBM_Read2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long nData, long *pRead);
TW_STATUS _stdcall USBM_ADReadCopyBuffer2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_SCISetDelimiter2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nDelimiter);
TW_STATUS _stdcall USBM_ATFUserCommand2(TW_HANDLE hDev, WORD Command, DWORD Param1, DWORD Param2, LPVARIANT pData, long nData);
}
#else

long _stdcall USBM_ListDevices(TW_CHAR (*pSrerial)[9],long nSerial);
long _stdcall USBM_ListDevicesByID(WORD VID, WORD PID, TW_CHAR (*pSerial)[9], long nSerial);
TW_HANDLE _stdcall USBM_OpenByUA(TW_CSTR pUA, long nUA);
TW_HANDLE _stdcall USBM_Open(TW_CSTR pSerial);
TW_HANDLE _stdcall USBM_OpenByID(WORD VID, WORD PID, TW_CSTR pSerial);
TW_STATUS _stdcall USBM_Close(TW_HANDLE hDev);

TW_STATUS _stdcall USBM_Initialize(TW_HANDLE hDev);
TW_STATUS _stdcall USBM_AddressEnable(TW_HANDLE hDev, long nBits);
TW_STATUS _stdcall USBM_CSEnable(TW_HANDLE hDev, long Area);
TW_STATUS _stdcall USBM_BusSetWait(TW_HANDLE hDev, long Area, BYTE Wait);

TW_STATUS _stdcall USBM_PortRead8(TW_HANDLE hDev, DWORD Port, BYTE *pData);
TW_STATUS _stdcall USBM_PortWrite8(TW_HANDLE hDev,DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_PortRead16(TW_HANDLE hDev, DWORD Port, WORD *pData);
TW_STATUS _stdcall USBM_PortWrite16(TW_HANDLE hDev, DWORD Port, WORD Data);
TW_STATUS _stdcall USBM_PortBWrite(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_PortBRead(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_PortSetDir(TW_HANDLE hDev, DWORD Port, BYTE Bits);
TW_STATUS _stdcall USBM_PortBCopy(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort, long nData, long SrcInc, long DstInc);
TW_STATUS _stdcall USBM_PortCopy8(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort);
TW_STATUS _stdcall USBM_PortCopy16(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort);

TW_STATUS _stdcall USBM_TimerEnable(TW_HANDLE hDev, BYTE Bits, long MDF);
TW_STATUS _stdcall USBM_TimerSetLevel(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetPulse(TW_HANDLE hDev, long CH, WORD LtoH, WORD HtoL);
TW_STATUS _stdcall USBM_TimerSetClk(TW_HANDLE hDev, long CH, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetCnt(TW_HANDLE hDev, long CH, short Cnt);
TW_STATUS _stdcall USBM_TimerStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerReadCnt(TW_HANDLE hDev, long CH, short *pCnt);
TW_STATUS _stdcall USBM_TimerSetCmp(TW_HANDLE hDev, short CmpA, short CmpB);
TW_STATUS _stdcall USBM_TimerSetCmpOut(TW_HANDLE hDev, long CmpReg, DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_TimerSetCmpClr(TW_HANDLE hDev, BYTE Bits);

TW_STATUS _stdcall USBM_ADRead(TW_HANDLE hDev, WORD *pData, long CH, long Scan);
TW_STATUS _stdcall USBM_ADSetCycle(TW_HANDLE hDev, BYTE Cmp, long CLK);
TW_STATUS _stdcall USBM_ADBRead(TW_HANDLE hDev, WORD *pData, DWORD nData, long CH, DWORD *pnRead);

TW_STATUS _stdcall USBM_SCISetMode(TW_HANDLE hDev, long CH, long Mode, long Baud);
TW_STATUS _stdcall USBM_SCIReadStatus(TW_HANDLE hDev, long CH, BYTE *pStatus, long *pnReceive);
TW_STATUS _stdcall USBM_SCIRead(TW_HANDLE hDev, long CH, void *pData, long nData, long *pnRead);
TW_STATUS _stdcall USBM_SCIWrite(TW_HANDLE hDev, long CH, void *pData, long nData);

TW_STATUS _stdcall USBM_PCSetCmp(TW_HANDLE hDev, long CH, long Cmp);
TW_STATUS _stdcall USBM_PCSetCnt(TW_HANDLE hDev, long CH, long Cnt);
TW_STATUS _stdcall USBM_PCReadCnt(TW_HANDLE hDev, long CH, long *pCnt);
TW_STATUS _stdcall USBM_PCSetControl(TW_HANDLE hDev, long CH, BYTE Bits);
TW_STATUS _stdcall USBM_PCSetCondBit(TW_HANDLE hDev, long CH, DWORD Port, BYTE Mask);
TW_STATUS _stdcall USBM_PCSetCmpOut(TW_HANDLE hDev, long CH, DWORD Port, BYTE Data);
TW_STATUS _stdcall USBM_PCStart(TW_HANDLE hDev, BYTE Bits);

TW_STATUS _stdcall USBM_TCPYStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TCPYSetParm(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort,
                    long nCopy, long SrcInc, long DstInc, long lLoop);
TW_STATUS _stdcall USBM_TCPYSetCmp(TW_HANDLE hDev, long CH, BYTE Cmp);
TW_STATUS _stdcall USBM_TCPYSetClk(TW_HANDLE hDev, long CH, long CLK);
TW_STATUS _stdcall USBM_TCPYSetCycle(TW_HANDLE hDev, long CH, BYTE Cmp, long CLK);

TW_STATUS _stdcall USBM_Write(TW_HANDLE hDev, void *pData, DWORD nData, DWORD *pWritten);
TW_STATUS _stdcall USBM_Read(TW_HANDLE hDev, void *pData, DWORD nData, DWORD *pRead);

TW_STATUS _stdcall USBM_ReadStatus(TW_HANDLE hDev, DWORD *pStat);
TW_STATUS _stdcall USBM_ReadVersion(TW_HANDLE hDev, DWORD *pVer);

TW_STATUS _stdcall USBM_Abort(TW_HANDLE hDev);

TW_STATUS _stdcall USBM_TimerStartA(TW_HANDLE hDev, BYTE Bits, long TrigPC, long Repeat, long Cmp);
TW_STATUS _stdcall USBM_TimerStop(TW_HANDLE hDev, BYTE Bits, long TrigPC, long Repeat, long Cmp);
TW_STATUS _stdcall USBM_DASetCycle(TW_HANDLE hDev,long CH,long Cmp,long CLK);
TW_STATUS _stdcall USBM_DASetParm(TW_HANDLE hDev,long CH,DWORD SrcPort,long nData,long lLoop);
TW_STATUS _stdcall USBM_DAReadStatus(TW_HANDLE hDev,long CH,long *pnData);
TW_STATUS _stdcall USBM_DAStart(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_DAStop(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TCPYSetPatternCtrl(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort,
                                       long nCopy, long nSrc, long SrcInc, long Start, BYTE Mask);
TW_STATUS _stdcall USBM_TCPYSetTrig(TW_HANDLE hDev, long CH, long StartPC, long StopPC,long Repeat);
TW_STATUS _stdcall USBM_TCPYReadStatus(TW_HANDLE hDev, long CH, long *pnCopy, long *pSrcPos);
TW_STATUS _stdcall USBM_TimerSetCapture(TW_HANDLE hDev, long CH, BYTE EdgeA, BYTE EdgeB);
TW_STATUS _stdcall USBM_TimerReadCaptureCnt(TW_HANDLE hDev, long CH, long *pCntA, long *pCntB);
TW_STATUS _stdcall USBM_TimerSetCaptureCnt(TW_HANDLE hDev, long CH, long CntA, long CntB);

TW_STATUS _stdcall USBM_ADStart(TW_HANDLE hDev, long nCnv, long CH, long Scan, long lByte, long Trig);
TW_STATUS _stdcall USBM_ADCopy(TW_HANDLE hDev, DWORD DstPort, long nCnv, long CH,
                           long CKS, long DMA_CH, long DstInc);
TW_STATUS _stdcall USBM_ADReadCopyStatus(TW_HANDLE hDev,long *pnCnv,long DMA_CH);
TW_STATUS _stdcall USBM_ADReadCopyBuffer(TW_HANDLE hDev, DWORD Port, WORD *pData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_ADStopCopy(TW_HANDLE hDev, long DMA_CH);
TW_STATUS _stdcall USBM_SCISetDelimiter(TW_HANDLE hDev, long CH, char *pDelimiter, long nDelimiter);
TW_STATUS _stdcall USBM_GetQueueStatus(TW_HANDLE hDev, DWORD *pnQueue);
TW_STATUS _stdcall USBM_Purge(TW_HANDLE hDev, DWORD dwMask);
TW_STATUS _stdcall USBM_SetTimeouts(TW_HANDLE hDev, DWORD dwReadTimeout, DWORD dwWriteTimeout);
TW_STATUS _stdcall USBM_PCStartA(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_PCStop(TW_HANDLE hDev, BYTE Bits);
TW_STATUS _stdcall USBM_TimerSetSinglePulse(TW_HANDLE hDev, long CH, long Setup, long Width, long Pos);

PVOID _stdcall USBM_GetFTHandle(TW_HANDLE hDev);

//>Ver.3.1.0.x
TW_STATUS _stdcall USBM_ReadPI(TW_HANDLE hDev, USBM_PRODUCT_INFO *pInfo);
TW_STATUS _stdcall USBM_OpenByPI(TW_HANDLE *phDev, TW_CSTR pUuid, DWORD Number, long Opt);
TW_STATUS _stdcall USBM_PortWrite8A(TW_HANDLE hDev,DWORD Port, BYTE Data, BYTE Mask);
TW_STATUS _stdcall USBM_SetPassword(TW_CSTR pPass);
TW_STATUS _stdcall USBM_SetNetworkPort(DWORD PortNumber);
DWORD _stdcall USBM_GetIFType(TW_HANDLE hDev);
TW_STATUS _stdcall USBM_OpenA(TW_HANDLE *phDev, TW_CSTR pIID, long Opt);
TW_STATUS _stdcall USBM_ListDevicesA(long *pnDev, TW_STR pIID, long *pnIID, long Opt);
//<Ver.3.1.0.x

//>Ver.3.3.0.x
TW_STATUS _stdcall USBM_ReadVersionA(TW_HANDLE hDev, DWORD *pVersion);
TW_STATUS _stdcall USBM_ATFSetCommand(TW_HANDLE hDev, DWORD Address);
TW_STATUS _stdcall USBM_ATFSetMain(TW_HANDLE hDev, DWORD Address);
TW_STATUS _stdcall USBM_ATFUserCommand(TW_HANDLE hDev, WORD Command, DWORD Param1, DWORD Param2, void *pData, long nData);
TW_STATUS _stdcall USBM_ATFDownload(TW_HANDLE hDev, TW_CSTR FileName, TW_CSTR Reserve);
TW_STATUS _stdcall USBM_ATFGetInfo(TW_CSTR FileName, USBM_ATF_INFO *pInfo);
//<Ver.3.3.0.x

//>Ver.3.3.2.x
TW_STATUS _stdcall USBM_InitializeA(TW_HANDLE hDev, DWORD InitOption);
//<Ver.3.3.2.x

//>Ver.3.4.0.1
TW_STATUS _stdcall USBM_FlashEraseBlk(TW_HANDLE hDev, long Blk);
TW_STATUS _stdcall USBM_FlashRead(TW_HANDLE hDev, DWORD Address, void *pBuff, DWORD nData);
TW_STATUS _stdcall USBM_FlashWrite(TW_HANDLE hDev, DWORD Address, void *pData, DWORD nData);
//<Ver.3.4.0.1

//OLEアクセス用
long _stdcall USBM_ListDevices2(LPSAFEARRAY *ppSerial);
long _stdcall USBM_ListDevicesByID2(WORD VID, WORD PID, LPSAFEARRAY *ppSerial);
TW_STATUS _stdcall USBM_PortBWrite2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_PortBRead2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_ADRead2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long CH, long Scan);
TW_STATUS _stdcall USBM_ADBRead2(TW_HANDLE hDev, LPSAFEARRAY *ppData, DWORD nData, long CH, DWORD *pnRead);
TW_STATUS _stdcall USBM_SCIRead2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nData, long *pnRead);
TW_STATUS _stdcall USBM_SCIWrite2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nData);
TW_STATUS _stdcall USBM_PCReadCnt2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppCnt);
TW_STATUS _stdcall USBM_Write2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long nData, long *pWritten);
TW_STATUS _stdcall USBM_Read2(TW_HANDLE hDev, LPSAFEARRAY *ppData, long nData, long *pRead);
TW_STATUS _stdcall USBM_ADReadCopyBuffer2(TW_HANDLE hDev, DWORD Port, LPSAFEARRAY *ppData, long nData, long Inc, BYTE Dma);
TW_STATUS _stdcall USBM_SCISetDelimiter2(TW_HANDLE hDev, long CH, LPSAFEARRAY *ppData, long nDelimiter);
TW_STATUS _stdcall USBM_ATFUserCommand2(TW_HANDLE hDev, WORD Command, DWORD Param1, DWORD Param2, LPVARIANT pData, long nData);
#endif

//****** 以下の行は VisualStudio の IntelliSense のために追加されています *****
//****** 実際の関数の処理とは関係ありませんのでご注意ください             *****
//****** 必要がない場合にはコメントアウトしてください                     *****
#ifdef __USBM_INTELLISENSE__

long _stdcall USBM_ListDevices(TW_CHAR (*pSrerial)[9] = NULL, long nSerial = 0){return 0;}; //デバイスの数を返します
/******************************************************************************
  USBM_ListDevices(TW_CHAR (*pSrerial)[9], long nSerial);

  (*pSerial)[9] : 9文字の文字列へのポインタ、シリアル番号を格納します
  nSerial       : 上の配列の数

  接続されているデバイスの数を返します。pSerialがNULLでなければnSerial個まで
  シリアル番号をpSerialに格納します。シリアル番号は製品に予め付けられている
  英数字8文字(ANSI)の文字列で一意な値です。
*****************************************************************************/


long _stdcall USBM_ListDevicesByID(WORD VID, WORD PID, TW_CHAR (*pSerial)[9], long nSerial){return 0;}; //指定IDのデバイス数を返します
/*****************************************************************************
  USBM_ListDevicesByID

  VID           : ベンダID
  PID           : プロダクトID
  (*pSerial)[9] : 9バイトの文字列へのポインタ、シリアル番号を格納します
  nSerial       : 上の配列の数

  指定されたベンダーIDとプロダクトIDに該当するデバイスを検索し、接続されている
  デバイスの数を返します。pSerialがNULLでなければnSerial個までシリアル番号を
  pSerialに格納します。シリアル番号は製品に予め付けられている英数字8文字(ANSI)
  の文字列で一意な値です。
*****************************************************************************/


TW_HANDLE _stdcall USBM_OpenByUA(TW_CSTR pUA, long nUA){return 0;}; //ユーザーエリアの情報を指定してデバイスをオープンします
/*****************************************************************************
  USBM_OpenByUA

  *pUA : ユーザーエリアと比較する文字列
  nUA  : 比較する文字数

  ボード上のシリアルROMのユーザーエリアの値を読み出し、指定文字列と一致した
  デバイスのハンドルを返します。指定デバイスがオープンできなかった場合は0を
  返します。現在使用できるユーザーエリアは8バイトです。
*****************************************************************************/


TW_HANDLE _stdcall USBM_Open(TW_CSTR pSerial = NULL){};  //シリアル番号を指定してデバイスをオープンします
/*****************************************************************************
  USBM_Open

  *pSerial : シリアル番号、NULLで終わる文字列

  pSerial がNULLの場合、または、*pSerialが""の場合、オープンできた最初の
  デバイスのハンドルを返します。それ以外ではシリアル番号が一致したデバイスを
  オープンします。指定デバイスがオープンできなかった場合は0を返します。
*****************************************************************************/


TW_HANDLE _stdcall USBM_OpenByID(WORD VID, WORD PID, TW_CSTR pSerial){return 0;};  //指定IDのデバイスをオープンします
/*****************************************************************************
  USBM_OpenByID

  VID      : ベンダID
  PID      : プロダクトID
  *pSerial : シリアル番号、NULLで終わる文字列

  指定されたベンダーIDとプロダクトIDに該当するデバイスを検索しオープンします。
  pSerial がNULLの場合、または、*pSerialが""の場合、オープンできた最初の
  デバイスのハンドルを返します。それ以外ではシリアル番号が一致したデバイスを
  オープンします。指定デバイスがオープンできなかった場合は0を返します。
*****************************************************************************/


TW_STATUS _stdcall USBM_Close(TW_HANDLE hDev){return 0;};  //デバイスのハンドルをクローズします
/*****************************************************************************
  USBM_Close

  hDev : デバイスのハンドル

  デバイスをクローズします。
*****************************************************************************/


TW_STATUS _stdcall USBM_Initialize(TW_HANDLE hDev){return 0;};  //デバイスを初期化します
/*****************************************************************************
  USBM_Initialize

  hDev : デバイスのハンドル

  マイコンの設定を起動時のものに戻します。デフォルトで使用されないレジスタを
  ユーザ操作で変更した場合にはそのレジスタは初期化されない可能性があります。
*****************************************************************************/


TW_STATUS _stdcall USBM_AddressEnable(TW_HANDLE hDev, long nBits){return 0;};  //アドレスバスを指定ビット数だけ有効にします
/*****************************************************************************
  USBM_AddressEnable

  hDev  : デバイスのハンドル
  nBits : 有効にするビット数(0,8,16,20)

  アドレス出力を有効/無効にします。LSBから指定ビット数が有効になります。
  アドレスバスはポート1,ポート2,ポート5と共用です。アドレスを出力すると
  入力ポートとしては使用できませんので注意してください。アドレスは一部だけを
  出力できますので出力していないポートは入力ポートのまま使用できます。
*****************************************************************************/


TW_STATUS _stdcall USBM_CSEnable(TW_HANDLE hDev, long Area){return 0;};  //指定のチップセレクト信号を有効にします
/*****************************************************************************
  USBM_CSEnable

  hDev : デバイスのハンドル
  Area : CS信号を有効にするエリア
         USBM_AREA2 CS2#を出力
         USBM_AREA3 CS3#を出力

  CS信号を有効にするエリアを指定します。使用できるCS信号のうちCS0#,CS5#は常に
  出力されますのでUSBM_AREA2とUSBM_AREA3が指定できます。両方出力する場合はORで
  結合して指定してください(USBM_AREA2 | USBM_AREA3)。
  CS2#を有効にした場合PC3#が、CS3#を有効にした場合PC2#は使用できませんので該当
  のパルスカウンタは必ず無効にしてください。
*****************************************************************************/


TW_STATUS _stdcall USBM_BusSetWait(TW_HANDLE hDev, long Area, BYTE Wait){return 0;};  //バスのウェイト時間を変更します
/*****************************************************************************
  USBM_BusSetWait

  hDev : デバイスのハンドル
  Area : メモリエリア
         USBM_AREA0 エリア0
         USBM_AREA2 エリア2
         USBM_AREA3 エリア3
         USBM_AREA5 エリア5
  Wait : ウェイト数
         0 ウェイトなし
         1 ウェイト1
         2 ウェイト2
         3 ウェイト3

  メモリエリアのソフトウェアウェイトを設定します。初期状態ではウェイトなしに
  に設定されています。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortRead8(TW_HANDLE hDev, DWORD Port, BYTE *pData){return 0;};  //指定アドレスから8ビット単位で読み出します
/*****************************************************************************
  USBM_PortRead8

  hDev   : デバイスのハンドル
  Port   : 読み取るアドレス
  *pData : 読み出したデータの格納先

  マイコンのPortで指定されるアドレスから1バイトのデータを読み出します。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortWrite8(TW_HANDLE hDev,DWORD Port, BYTE Data){return 0;};  //指定アドレスに8ビット単位で書き込みます
/*****************************************************************************
  USBM_PortWrite8

  hDev : デバイスのハンドル
  Port : 書き込むアドレス
  Data : 書き込むデータ

  マイコンのPortで指定されるアドレスに1バイトのデータを書き込みます。マイコン
  の制御レジスタにも書き込みが行えますので、定数以外のアドレスを指定する場合に
  は注意が必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortRead16(TW_HANDLE hDev, DWORD Port, WORD *pData){return 0;};  //指定アドレスから16ビット単位で読み出します
/*****************************************************************************
  USBM_PortRead16

  hDev   : デバイスのハンドル
  Port   : 書き込むアドレス
  *pData : 書き込むデータ

  マイコンのPortで指定されるアドレスから2バイトのデータを読み出します。
  Portアドレスは偶数番地でなければなりません。バイトアクセス専用の空間では
  1バイトずつ2回リードされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortWrite16(TW_HANDLE hDev, DWORD Port, WORD Data){return 0;};  //指定アドレスに16ビット単位で書き込みます
/*****************************************************************************
  USBM_PortWrite16

  hDev : デバイスのハンドル
  Port : 書き込むアドレス
  Data : 書き込むデータ

  マイコンのPortで指定されるアドレスに2バイトのデータを書き込みます。16ビット
  のデータを同時にアクセスしたい場合に使用します。マイコンの制御レジスタにも
  書き込みが行えますので、定数以外のアドレスを指定する場合には注意が必要です。
  Portアドレスは偶数番地でなければなりません。バイトアクセス専用の空間では
  1バイトずつ2回ライトされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortBWrite(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc = 1, BYTE Dma = TRUE){return 0;}; //指定アドレスにブロック単位で書き込みます
/*****************************************************************************
  USBM_PortBWrite

  hDev   : デバイスのハンドル
  Port   : 書き込むアドレス
  *pData : 書き込むデータへのポインタ
  nData  : データのバイト数(0〜65536)
  Inc    : マイコンのアドレスをインクリメントするかを示すフラグ(TRUE,FALSE)
  Dma    : DMAを使用するかどうかのフラグ(TRUE,FALSE)

  マイコンのPortで指定されるアドレスにnDataで指定される大きさのデータを転送し
  ます。IncをFALSEとするとマイコン側のアドレスがインクリメントされませんので
  FIFOのようなデバイスに書き込みを行えます。DmaをTRUEとするとマイコン内臓のDMA
  を使用してデータを転送します。DMAを使用すると高速ですが、転送終了時に
  PA0/TCLKA端子にTEND#信号を出力するので注意が必要です。PA0/TCLKAに出力が出て
  はいけない場合にはDMA=FALSEとして呼び出してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortBRead(TW_HANDLE hDev, DWORD Port, void *pData, long nData, long Inc = 1, BYTE Dma = TRUE){return 0;};  //指定アドレスからブロック単位で読み出します
/*****************************************************************************
  USBM_PortBRead

  hDev   : デバイスのハンドル
  Port   : 読み出すアドレス
  *pData : 読み出したデータの格納先へのポインタ
  nData  : データのバイト数(0〜65536)
  Inc    : マイコンのアドレスをインクリメントするかを示すフラグ(TRUE,FALSE)
  Dma    : DMAを使用するかどうかのフラグ(TRUE,FALSE)

  マイコンのPortで指定されるアドレスからnDataで指定される大きさのデータを読み
  出します。IncをFALSEとするとマイコン側のアドレスがインクリメントされませんの
  でFIFOのようなデバイスから読み出しを行えます。DmaをTRUEとするとマイコン内臓
  のDMAを使用してデータを転送します。DMAを使用すると高速ですが、転送終了時に
  PA1/TCLKB端子にTEND#信号を出力するので注意が必要です。PA1/TCLKBに出力が出て
  はいけない場合にはDMA=FALSEとして呼び出してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortSetDir(TW_HANDLE hDev, DWORD Port, BYTE Bits){return 0;};  //指定アドレスを入力または出力に設定します
/*****************************************************************************
  USBM_PortSetDir

  hDev : デバイスのハンドル
  Port : 方向を指定するポート
         USBM_P1 P10〜P17端子の方向を設定
         USBM_P2 P20〜P27端子の方向を設定
         USBM_P4 P40〜P47端子の方向を設定
         USBM_P5 P50〜P53端子の方向を設定
         USBM_PA PA0〜PA7端子の方向を設定
  Bits : 方向(1のビット:出力,0のビット:入力)

  ポートの入力/出力を切り替えます。Bitsの0のビットは入力,1のビットは出力と
  なります。
  注:P1,P2,P5は入力専用で出力にするとバスのアドレス出力になります。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortBCopy(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort, long nData, long SrcInc, long DstInc){return 0;};  //任意のアドレス間でブロック単位でデータをコピーします
/*****************************************************************************
  USBM_PortBCopy

  hDev    : デバイスのハンドル
  SrcPort : コピー元
  DstPort : コピー先
  nData   : バイト数(0〜65536)
  SrcInc  : コピー元アドレスのインクリメント(1,0,-1)
  DstInc  : コピー先アドレスのインクリメント(1,0,-1)

  SrcPortからDstPortへ指定バイト数だけコピーを行います。DMAのバーストモードで
  転送しますので、転送が終わるまでマイコンは割込みも含めて他の処理を行えません
*****************************************************************************/


TW_STATUS _stdcall USBM_PortCopy8(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort){return 0;};  //任意のアドレス間で8ビット単位でデータをコピーします
/*****************************************************************************
  USBM_PortCopy8

  hDev    : デバイスのハンドル
  SrcPort : コピー元
  DstPort : コピー先

  SrcPortで示されるのアドレスから1バイトを読み取りDstPortで示されるアドレスに
  書込みます。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortCopy16(TW_HANDLE hDev, DWORD SrcPort, DWORD DstPort){return 0;};  //任意のアドレス間で16ビット単位でデータをコピーします
/*****************************************************************************
  USBM_PortCopy16

  hDev    : デバイスのハンドル
  SrcPort : コピー元
  DstPort : コピー先

  SrcPortで示されるアドレスから16ビットのデータを読み取りDstPortで示される
  アドレスに書込みます。アドレスはどちらも偶数番地でなければなりません。
  バイトアクセス専用の空間では1バイトずつ2回アクセスされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerEnable(TW_HANDLE hDev, BYTE Bits, long MDF){return 0;};  //16ビットタイマの出力ピンを有効にします
/*****************************************************************************
  USBM_TimerEnable

  hDev : デバイスのハンドル
  Bits : PWM出力を行うチャンネルを示すビット
         0ビット(LSB) タイマ0
         1ビット      タイマ1
         2ビット      タイマ2
  MDF  : 0以外を指定するとチャンネル2を位相計数モードに設定します。

  指定されたタイマチャンネルを使用可能にします。指定チャンネルをPWMモードに
  設定しますので対応するチャンネルのTIOCAピンはPWM出力になります。
  MDFを真にするとチャンネル2を位相計数モードに設定します。位相計数モードになる
  とBitsの値に関係なくTCLKA,TCLKBピンがクロック入力となります。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetLevel(TW_HANDLE hDev, BYTE Bits){return 0;};  //16ビットタイマの出力ピンのレベルを変更します
/*****************************************************************************
  USBM_TimerSetLevel

  hDev : デバイスのハンドル
  Bits : タイマ出力端子を表すビット
         0ビット(LSB) TIOCA0
         1ビット      TIOCB0
         2ビット      TIOCA1
         3ビット      TIOCB1
         4ビット      TIOCA2
         5ビット      TIOCB2

  タイマ出力に設定されているピンのレベルを変更します。1にセットすると対応する
  出力ピンが1出力になります。タイマ出力に設定されていないピンは影響を受けませ
  ん。デフォルトでは有効にしたチャンネルのTIOCAピンだけが出力になります。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetPulse(TW_HANDLE hDev, long CH, WORD LtoH, WORD HtoL){return 0;};  //PWMの周期とパルス幅を指定します
/*****************************************************************************
  USBM_TimerSetPulse

  hDev : デバイスのハンドル
  CH   : タイマチャンネル(0,1,2)
  LtoH : タイマ出力(TIOCAピン)が1になるカウント値を指定(1〜65535)
  HtoL : タイマ出力(TIOCAピン)が0になるカウント値を指定(1〜65535)

  タイマ出力ピンが1になるタイミングと0になるタイミングを指定します。デフォルト
  の設定ではタイマはPWMモードに設定されTIOCAピンだけが出力となります。すなわち
  16ビットタイマのカウンタがLtoHの値と等しくなるとTIOCAピンに1が出力され、
  HtoLと等しくなると0が出力されます。またデフォルトでは0になるタイミングで
  カウンタがリセットされるように設定されます。つまり、HtoLの値でカウンタ出力の
  周期をLtoHの値でデューティ比を決定できます。チャンネル間の位相差はタイマの
  スタート前にUSBM_TimerSetCnt()を呼び出すことで調整できます。
  パルスの周期とデューティは次式のようになります。

  Tp = (HtoL + 1) / fclk [s] (fclk:USBM_TimerSetClk()で選択された周波数)
  Don = (1 - (LtoH + 1) / (Low + 1)) * 100 [%] (ONデューティ)
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetClk(TW_HANDLE hDev, long CH, BYTE Bits){return 0;};  //16ビットタイマのクロックを選択します
/*****************************************************************************
  USBM_TimerSetClk

  hDev : デバイスのハンドル
  CH   : タイマチャンネル(0,1,2)
  Bits : USBM_TCLK25000 25MHz
         USBM_TCLK12500 12.5MHz
         USBM_TCLK6250  6250kHz
         USBM_TCLK3125  3125kHz
         USBM_TCLKA     TCLKAからの外部入力
         USBM_TCLKB     TCLKBからの外部入力

  16ビットタイマに使用するクロックを選択します。TCLKAまたはTCLKBを選択すると
  PA0またはPA1ピンをクロック入力として使用します。
  TCLKA,TCLKBはUSBM_PortBWrite()またはUSBM_PortBRead()関数でDMAを使用すると
  一時的にTEND#信号として出力ピンに設定されてしまいますので同時に使用すること
  はできません。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCnt(TW_HANDLE hDev, long CH, short Cnt){return 0;};  //16ビットタイマのカウンタ値を設定します
/*****************************************************************************
  USBM_TimerSetCnt

  hDev : デバイスのハンドル
  CH   : タイマチャンネル(0,1,2)
  Cnt  : 設定する値(0〜65535)

  タイマのカウンタ値を設定します。チャンネル間でPWMの位相を調整する場合などに
  使用できます。書き込みが実行されるまで1ms以上要しますのでタイマをストップ
  した状態で書き込みを行うことをお薦めします。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerStart(TW_HANDLE hDev, BYTE Bits){return 0;};  //16ビットタイマのカウントを開始します
/*****************************************************************************
  USBM_TimerStart

  hDev : デバイスのハンドル
  Bits : 16ビットタイマチャンネルを表すビット
         0ビット(LSB) タイマ0
         1ビット      タイマ1
         2ビット      タイマ2

  タイマのスタート/ストップを制御します。1にセットしたビットに対応するタイマが
  スタートし、0にセットしたビットに対応するタイマはストップします。
*****************************************************************************/

TW_STATUS _stdcall USBM_TimerReadCnt(TW_HANDLE hDev, long CH, short *pCnt){return 0;};  //16ビットタイマのカウンタ値を読み出します
/*****************************************************************************
  USBM_TimerReadCnt

  hDev  : デバイスのハンドル
  CH    : タイマチャンネル(0,1,2)
  *pCnt : カウント値の格納先

  タイマのカウンタ値を読み出します。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCmp(TW_HANDLE hDev, short CmpA, short CmpB){return 0;};  //16ビットタイマのコンペアレジスタの値を設定します
/*****************************************************************************
  USBM_TimerSetCmp

  hDev  : デバイスのハンドル
  CmpA  : コンペアレジスタA(-32768〜32767)
  CmpB  : コンペアレジスタB(-32768〜32767)

  チャンネル2を位相計数モードで使用する場合のコンペア値を設定します。
  コンペア値とチャンネル2のカウンタ値が一致したときの動作は
  USBM_SetCmpOut()関数で設定します。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCmpOut(TW_HANDLE hDev, long CmpReg, DWORD Port, BYTE Data){return 0;};  //16ビットタイマのコンペアアウトを設定します
/*****************************************************************************
  USBM_TimerSetCmpOut

  hDev   : デバイスのハンドル
  CmpReg : 設定するコンペアレジスタ
           0 コンペアレジスタA
           1 コンペアレジスタB
  Port   : 値を書き込むアドレス
  Data   : 書き込むデータ

  タイマチャンネル2のコンペアアウトを設定します。コンペアアウトを設定した
  コンペアレジスタの値とタイマチャンネル2のカウント値が一致するとPortで指定
  されたアドレスにDataがライトされます。出力が行われないようにするにはPortを
  0にして呼び出してください。
  タイマチャンネル2がPWMモードでも位相計数モード機能します。PWMモードでは1に
  なるタイミングがコンペアレジスタAとのマッチングで、0になるタイミング(同時に
  カウンタが0にクリアされるタイミング)がコンペアレジスタBとのマッチングです。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCmpClr(TW_HANDLE hDev, BYTE Bits){return 0;};  //16ビットタイマのコンペアマッチによるカウンタのクリアを設定します
/*****************************************************************************
  USBM_TimerSetCmpClr

  hDev : デバイスのハンドル
  Bits : コンペアマッチでカウンタをクリアするコンペアレジスタを指定
         0ビット(LSB) 1のときコンペアレジスタAとのマッチングでカウンタが
                      クリアされます
         1ビット      1のときコンペアレジスタBとのマッチングでカウンタが
                      クリアされます

  指定されたコンペアレジスタとコンペアマッチが発生したときタイマチャンネル2の
  カウンタがクリアされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_ADRead(TW_HANDLE hDev, WORD *pData, long CH, long Scan = FALSE){return 0;};  //指定チャンネルのAD変換結果を読み出します
/*****************************************************************************
  USBM_ADRead

  hDev  : デバイスのハンドル
  pData : 読み出したデータの格納先へのポインタ
  CH    : ADコンバータのチャンネル(0,1,2,3)
  Scan  : 複数チャンネルをスキャンするかどうかのフラグ(TRUE,FALSE)

  ScanがFALSEのとき指定されたチャンネルのAD変換結果をpData位置に格納します。
  ScanがTRUEのときは0から指定されたチャンネルまでをAD変換しpDataの位置から格納
  します。例えばScan=TRUEでCH=2の場合、0〜2チャンネルの3チャンネル分の変換結果
  をpData位置から3ワード分格納します。pData領域の大きさはチェックされません
  のでチャンネル数に合わせて呼び出し側で確保してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_ADSetCycle(TW_HANDLE hDev, BYTE Cmp, long CLK = USBM_TCLK3125){return 0;};  //連続でAD変換する場合の周期を指定します
/*****************************************************************************
  USBM_ADSetCycle

  hDev : デバイスのハンドル
  Cmp  : コンペアレジスタの値(1〜255)
  CLK  : クロックの選択
         USBM_TCLK3125 3125kHz
         USBM_TCLK390  390.625kHz
         USBM_TCLK3    約3052Hz

  USBM_ADBRead()、USBM_ADStart()で連続してAD変換を行う場合の周期を設定します。
  CLKでクロックの周波数を選択し、Cmpの値でそのクロックを何回カウントしたときに
  変換を開始するかを決定します。CLKを0にして呼び出すとタイマではなく外部トリガ
  入力で変換を開始する設定となります。変換周期は下のようになります。

  Tc = (Cmp + 1) / fclk [s] (fclk : CLKで選択した周波数)
*****************************************************************************/


TW_STATUS _stdcall USBM_ADBRead(TW_HANDLE hDev, WORD *pData, DWORD nData, long CH, DWORD *pnRead = NULL){return 0;};  //連続でAD変換した結果を読み出します
/*****************************************************************************
  USBM_ADBRead

  hDev   : デバイスのハンドル
  pData  : 読み出したデータの格納先へのポインタ
  nData  : 読み出すデータ数(バイト単位ではなくワードデータの数、1〜4294967295)
  CH     : ADコンバータのチャンネル(0,1,2,3)
  pnRead : 実際に読み出したデータ数の格納先(バイト単位ではなくワードの数)

  指定チャンネルのADコンバータで定期的にAD変換し、そのデータを読み出します。
  変換タイミングはマイコン内臓の8ビットタイマで作り出す方法と、外部トリガを
  入力する方法があります。デフォルトでは外部トリガの入力で変換が開始されます
  ので、タイマを利用する場合には、予めUSBM_ADSetCycle()関数でクロックと
  コンペア値を設定しておいてください。
  関数自体はUSBM_SetTimeouts()で指定された時間でタイムアウトしますが、デバイス
  は指定されたデータ数を読み出すまで処理を続けます。中止させるにはUSBM_Abort()
  を使用してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_SCISetMode(TW_HANDLE hDev, long CH, long Mode = 0x00, long Baud = 0x0050){return 0;};  //シリアル通信の各種設定を行います
/*****************************************************************************
  USBM_SCISetMode

  hDev  : デバイスのハンドル
  CH    : SCIチャンネル(0のみ)
  Mode  : キャラクタのビット数,パリティ,ストップビットの設定のORをとって
          渡します。
          USBM_SCI_DATA8    : 8ビットデータ
          USBM_SCI_DATA7    : 7ビットデータ
          USBM_SCI_NOPARITY : パリティなし
          USBM_SCI_EVEN     : 偶数パリティ
          USBM_SCI_ODD      : 奇数パリティ
          USBM_SCI_STOP1    : 1ストップビット
          USBM_SCI_STOP2    : 2ストップビット
  Baud  : ボーレート
          USBM_SCI_BAUD300   : 300bps 
          USBM_SCI_BAUD600   : 600bps
          USBM_SCI_BAUD1200  : 1200bps
          USBM_SCI_BAUD2400  : 2400bps
          USBM_SCI_BAUD4800  : 4800bps
          USBM_SCI_BAUD9600  : 9600bps
          USBM_SCI_BAUD14400 : 14400bps
          USBM_SCI_BAUD19200 : 19200bps
          USBM_SCI_BAUD38400 : 38400bps

  SCIチャンネルの設定を行います。使用できるのは調歩同期のみです。
*****************************************************************************/


TW_STATUS _stdcall USBM_SCIReadStatus(TW_HANDLE hDev, long CH, BYTE *pStatus, long *pnReceive){return 0;};  //シリアル通信チャンネルのステータスを読み出します
/*****************************************************************************
  USBM_SCIReadStatus

  hDev      : デバイスのハンドル
  CH        : チャンネル(0のみ)
  pStatus   : ステータスの格納先へのポインタ
              0(LSB)ビット〜2ビット 0です
              3ビット パリティエラーが起こった場合に1になります
              4ビット フレーミングエラーが起こった場合に1になります
              5ビット オーバーランエラーが起こった場合に1になります
              6ビット〜7ビット(MSB) 0です
  pnReceive : バッファ中のデータ数の格納先へのポインタ

  SCIのステータス情報と受信バッファに格納されているデータ数を読み出します。
*****************************************************************************/


TW_STATUS _stdcall USBM_SCIRead(TW_HANDLE hDev, long CH, void *pData, long nData, long *pnRead = NULL){return 0;};  //シリアルポートからデータを読み出します
/*****************************************************************************
  USBM_SCIRead

  hDev   : デバイスのハンドル
  CH     : チャンネル(0のみ)
  pData  : 格納先へのポインタ
  nData  : データのバイト数(1〜255)
  pnRead : 実際に読み出したバイト数の格納先

  マイコンボード上のシリアルポートからデータを読み出します。関数自体は
  USBM_SetTimeouts()で指定された時間でタイムアウトしますが、デバイスは指定され
  たデータ数を読み出すまで処理を続けます。中止させるためにはUSBM_Abort()関数を
  使用してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_SCIWrite(TW_HANDLE hDev, long CH, void *pData, long nData){return 0;};  //シリアルポートからデータを出力します
/*****************************************************************************
  USBM_SCIWrite

  hDev  : デバイスのハンドル
  CH    : チャンネル(0のみ)
  pData : データへのポインタ
  nData : データのバイト数(1〜255)

  マイコンボード上のシリアルポートからデータを出力します。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCSetCmp(TW_HANDLE hDev, long CH, long Cmp){return 0;};  //パルスカウンタのコンペアレジスタを設定します
/*****************************************************************************
  USBM_PCSetCmp

  hDev : デバイスのハンドル
  CH   : パルスカウンタのチャンネル(0〜3)
  Cmp  : コンペア値(-2147483648〜2147483647)

  パルスカウンタのコンペア値を設定します。設定を行うことでパルスカウンタの
  カウント値がこの値と一致したときにカウンタ値がリセットされるようにしたり、
  指定ポートにデータを出力するようにできます。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCSetCnt(TW_HANDLE hDev, long CH, long Cnt){return 0;};  //パルスカウンタのカウント値をセットします
/*****************************************************************************
  USBM_PCSetCnt

  hDev : デバイスのハンドル
  CH   : パルスカウンタのチャンネル(0〜3)
  Cnt  : カウンタの値(-2147483648〜2147483647)

  パルスカウンタに値を設定します。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCReadCnt(TW_HANDLE hDev, long CH, long *pCnt){return 0;};  //パルスカウンタのカウント値を読み出します
/*****************************************************************************
  USBM_PCReadCnt

  hDev : デバイスのハンドル
  CH   : パルスカウンタのチャンネル(0〜3)
         USBM_PC_ALLを指定すると0〜3チャンネル全てを読み出します
  pCnt : カウンタの値の格納先

  パルスカウンタの値を読み出します。全部のチャンネルを呼び出す場合は
  4チャンネル分の領域をpCntに指定してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCSetControl(TW_HANDLE hDev, long CH, BYTE Bits){return 0;};  //パルスカウンタのカウント方法を指定します
/*****************************************************************************
  USBM_PCSetControl 

  hDev : デバイスのハンドル
  CH   : チャンネル(0,1,2,3)
  Bits : 0ビット(LSB) '1'にするとPC0の入力でカウンタが0にクリアされます
         1ビット      '1'にするとコンペアマッチでカウンタが0にクリアされます
         4,5ビット    '00'にするとパルス入力で無条件にカウントアップします
                      '01'にすると条件ビットが真のときダウン、偽のときアップ
                      '10'にすると条件ビットが真のときアップ、偽のときダウン
                      '11'にすると条件ビットが真のときアップ、偽のときカウント
                      しません
         上記以外のビットは予約です。0にしてください。

  パルスカウンタの制御方法を指定します。Bits下位2ビットでリセット条件を
  4,5ビットでカウントの方法を指定します。下位2ビットがどちらも0の場合、
  カウンタはクリアされません。
  条件ビットとはUSBM_PCSetCondBit()関数で指定されるビットです。 
*****************************************************************************/


TW_STATUS _stdcall USBM_PCSetCondBit(TW_HANDLE hDev, long CH, DWORD Port, BYTE Mask){return 0;};  //パルスカウンタのカウント条件となるビットを指定します
/*****************************************************************************
  USBM_PCSetCondBit

  hDev : デバイスのハンドル
  CH   : チャンネル(0,1,2,3)
  Port : コンディションを読み取るアドレス
  Mask : Portアドレスの値とアンドを取るマスク

  パルスが入力されたときにカウンタの増減を決める条件ビットを指定します。
  条件ビットはPortアドレスから読み取られた値とMask値のアンド(論理積)をとって
  決定されます。例えば2相のロータリーエンコーダー入力の位相関係でカウンタの
  増減を切り替えるような場合に利用できます。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCSetCmpOut(TW_HANDLE hDev, long CH, DWORD Port, BYTE Data){return 0;};  //パルスカウンタのコンペアアウトを設定します
/*****************************************************************************
  USBM_PCSetCmpOut

  hDev : デバイスのハンドル
  CH   : チャンネル(0,1,2,3)
  Port : コンペアマッチ時に書き込むアドレス
  Data : コンペアマッチ時に書き込むデータ

  コンペアマッチ時に行うポート出力を設定します。パルスカウンタの値がコンペア値
  と一致するとPortアドレスにDataを出力します。ポート操作が必要ない場合はPort=0
  としてください。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCStart(TW_HANDLE hDev, BYTE Bits){return 0;};  //パルスカウンタのカウントを開始します
/*****************************************************************************
  USBM_PCStart

  hDev : デバイスのハンドル
  Bits : USBM_PC0  チャンネル0を許可 
         USBM_PC1  チャンネル1を許可
         USBM_PC2  チャンネル2を許可
         USBM_PC3  チャンネル3を許可

  パルスカウントを許可します。許可するチャンネルが複数ある場合には上記の定数を
  ORで結合してBitsに指定してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYStart(TW_HANDLE hDev, BYTE Bits){return 0;};  //タイマコピーを開始します
/*****************************************************************************
  USBM_TCPYStart

  hDev : デバイスのハンドル
  Bits : ビット0(LSB) 1にするとチャンネル0をスタートします。
         ビット1      1にするとチャンネル1をスタートします。

  Bitsで指定されたチャンネルのタイマコピーをスタートします。ストップする場合は
  対応するビットを0にして呼び出します。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYSetParm(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort, long nCopy, long SrcInc, long DstInc, long lLoop){return 0;};  //タイマコピーの条件を設定します
/*****************************************************************************
  USBM_TCPYSetParm

  hDev    : デバイスのハンドル
  CH      : 設定するチャンネル(0,1)
  SrcPort : 転送元アドレス
  DstPort : 転送先アドレス
  nCopy   : コピーするバイト数(1〜65535)
  SrcInc  : 転送毎に転送元アドレスを増加するバイト数(-128〜127)
  DstInc  : 転送毎に転送先アドレスを増加するバイト数(-128〜127)
  lLoop    : 真の場合、転送終了後、初期状態を復帰して再開します。(TRUE,FALSE)

  タイマコピーのパラメータを設定します。タイマコピーでは8ビットタイマが
  コンペア値と一致する度に転送元に指定したアドレスから転送先に指定したアドレス
  に1バイトずつデータをコピーします。
  タイマコピーが動作している間は呼び出さないで下さい。
*****************************************************************************/



TW_STATUS _stdcall USBM_TCPYSetCmp(TW_HANDLE hDev, long CH, BYTE Cmp){return 0;};  //タイマコピーの周期を設定します
/*****************************************************************************
  USBM_TCPYSetCmp

  hDev : デバイスのハンドル
  CH   : 設定するチャンネル(0,1)
  Cmp  : コンペア値(1〜255)

  8ビットタイマがコンペア値と一致するとUSBM_TCPYSetParm()で指定した転送元から
  転送先アドレスに1バイトのデータがコピーされます。またタイマのカウント値は
  リセットされ最初からカウントされますので転送の周期をこの関数で決定します。
  転送の周期は次式のようになります。ただしCLKはUSBM_TCPYSetClk()で指定した
  クロック周波数です。

  Tc = (Cmp + 1) / CLK [s]

  タイマコピーが動作している間は呼び出さないで下さい。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYSetClk(TW_HANDLE hDev, long CH, long CLK){return 0;};  //タイマコピーのクロックを選択します
/*****************************************************************************
  USBM_TCPYSetClk

  hDev : デバイスのハンドル
  CH   : クロックを設定するチャンネル(0,1)
  CLK  : USBM_TCLK31250 3125kHz
         USBM_TCLK390   390.625kHz
         USBM_TCLK3     約3052Hz
         USBM_TCLKUP    外部クロックの立ち上がり
         USBM_TCLKDOWN  外部クロックの立ち下がり
         USBM_TCLKBOTH  外部クロックの両エッジ

  タイマコピーに使用するクロックを選択します。外部クロックはチャネル0では
  TCLKAがチャンネル1ではTCLKBが使用されます。タイマコピーが動作している間は
  呼び出さないで下さい。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYSetCycle(TW_HANDLE hDev, long CH, BYTE Cmp, long CLK = USBM_TCLK3125){return 0;};  //タイマコピーの周期を設定します
/*****************************************************************************
  USBM_TCPYSetCycle

  hDev : デバイスのハンドル
  CH   : 設定するチャンネル(0,1)
  Cmp  : コンペア値(1〜255)
  CLK  : USBM_TCLK31250 3125kHz
         USBM_TCLK390   390.625kHz
         USBM_TCLK3     約3052Hz
         USBM_TCLKUP    外部クロックの立ち上がり
         USBM_TCLKDOWN  外部クロックの立ち下がり
         USBM_TCLKBOTH  外部クロックの両エッジ

  タイマコピーのコピーサイクルを設定します。8ビットタイマのカウンタはCLK選択
  されたクロックでカウントされ、カウンタの値がコンペア値と一致すると、
  USBM_TCPYSetParm()で指定した転送元から転送先に1バイトのデータがコピーされま
  す。タイマカウンタはコンペアと同時にクリアされ動作を繰り返します。
  転送の周期は下のようになります。

  Tc = (Cmp + 1) / CLK [s] (CLKは上の周波数)

  外部クロックを指定する場合は、0チャンネルではTCLKAから、1チャンネルではTCLKB
  から入力してください。タイマコピー動作中はこの関数を呼び出さないでください。
*****************************************************************************/


TW_STATUS _stdcall USBM_Read(TW_HANDLE hDev, void *pData, DWORD nData, DWORD *pRead){return 0;};  //受信バッファからデータを取り出します。
/*****************************************************************************
  USBM_Read

  hDev   : デバイスのハンドル
  pData  : 読み出したデータの格納先へのポインタ
  nData  : 読み出すバイト数(1〜65536)
  pRead  : 読み出したデータのバイト数の格納先へのポインタ

  デバイスから送られたデータを読み出します。指定バイト数のデータを読み出すまで
  関数から戻りません。
  単にFTDIの関数、FT_Read()を呼び出しています。
*****************************************************************************/


TW_STATUS _stdcall USBM_ReadStatus(TW_HANDLE hDev, DWORD *pStat){return 0;};  //デバイスのステータスを読み出します
/*****************************************************************************
  USBM_ReadStatus

  hDev   : デバイスのハンドル
  pStat  : 読み出したステータスの格納先へのポインタ
           USBM_STS_TIMEOUT        処理がタイムアウトした
           USBM_STS_ILLEGAL_ACCESS 不正なアドレスへのアクセス
           USBM_STS_TCPY0_FIN      タイマコピーの0チャンネルが終了
                                   (※ファームVer.2.0で追加)
           USBM_STS_TCPY1_FIN      タイマコピーの1チャンネルが終了
                                   (※ファームVer.2.0で追加)

  デバイスのステータスを読み取ります。上で定義されるステータスビットの組合せ
  が返されます。一度読み出すとデバイスのステータスはクリアされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_ReadVersion(TW_HANDLE hDev, DWORD *pVer){return 0;};  //デバイスのバージョンを読み出します
/*****************************************************************************
  USBM_ReadVersion

  hDev   : デバイスのハンドル
  pVer   : 読み出したバージョンの格納先へのポインタ

  バージョンは、x.xの形式を10倍にして整数として返します。
*****************************************************************************/


TW_STATUS _stdcall USBM_Abort(TW_HANDLE hDev){return 0;};  //デバイスに実行中の処理を中止させます
/*****************************************************************************
  USBM_Abort

  hDev   : デバイスのハンドル

  USBM_ADBRead(),USBM_SCIRead()関数がタイムアウトした場合に、マイコン側の処理
  を中止させる場合に使用します。
*****************************************************************************/



TW_STATUS _stdcall USBM_TimerStartA(TW_HANDLE hDev, BYTE Bits, long TrigPC, long Repeat, long Cmp){return 0;};  //16ビットタイマをスタートします
/*****************************************************************************
  USBM_TimerStartA

  hDev   : デバイスのハンドル
  Bits   : スタートするタイマチャンネル
           ビット0(LSB) 1の場合、チャンネル0をスタート
           ビット1      1の場合、チャンネル1をスタート
           ビット2      1の場合、チャンネル2をスタート
  TrigPC : 起動のトリガとなるパルスカウンタチャンネル
           -1   直ちにタイマを起動します
           0〜3 指定されたPCx#信号でタイマを起動します
  Repeat : 起動トリガを繰り返し機能させるかどうかのフラグ(TRUE,FALSE)
  Cmp    : パルスカウンタのコンペア値(1〜2147483647)

  16ビットタイマの指定チャンネルをスタートします。USBM_TimerStart()との違いは
  スタートするチャンネル以外に影響を与えないことです。
  また、TrigPCにパルスカウンタのチャンネルを指定すると、指定したチャンネルは
  すぐには起動せず、パルスカウンタへの入力待ちになります。パルスカウンタに
  パルスが入力されると8us〜20us以内にタイマが起動されます。この場合、Repeatが
  真ならパルスカウンタ入力は繰り返し受付られ、その度にタイマが起動されます。
  Cmpは通常1ですが、パルスカウンタへ複数のパルス入力によりタイマを起動する場合
  は回数を設定してください。
  パルスカウンタを起動要因とすると、指定のパルスカウンタがカウントを開始します
  ので不要になった場合はUSBM_PCStop()関数を呼び出して停止してください。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerStop(TW_HANDLE hDev,BYTE Bits, long TrigPC, long Repeat, long Cmp){return 0;};  //16ビットタイマをストップします
/*****************************************************************************
  USBM_TimerStop

  hDev   : デバイスのハンドル
  Bits   : ストップするタイマチャンネル
           ビット0(LSB) 1の場合、チャンネル0を停止
           ビット1      1の場合、チャンネル1を停止
           ビット2      1の場合、チャンネル2を停止
  TrigPC : 終了のトリガとなるパルスカウンタチャンネル
           -1    直ちにタイマを停止します
            0〜3 指定されたPCx#信号でタイマを停止します
  Repeat : 終了トリガを繰り返し機能させるかどうかのフラグ(TRUE,FALSE)
  Cmp    : パルスカウンタのコンペア値(1〜2147483647)

  16ビットタイマの指定チャンネルを停止します。USBM_TimerStart()との違いは、
  停止するチャンネル以外に影響を与えないことです。
  また、TrigPCにパルスカウンタのチャンネルを指定すると、指定したチャンネルは
  すぐには停止せず、パルスカウンタへの入力待ちになります。パルスカウンタに
  パルスが入力されると8us〜20us以内にタイマが停止されます。この場合、Repeatが
  真ならパルスカウンタ入力は繰り返し受付られ、その度にタイマが停止されます。
  Cmpは通常1ですが、パルスカウンタへ複数のパルス入力によりタイマを停止する場合
  は回数を設定してください。
  パルスカウンタを停止要因とすると、指定のパルスカウンタがカウントを開始します
  ので不要になった場合はUSBM_PCStop()関数を呼び出して停止してください。
  
  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_DASetCycle(TW_HANDLE hDev,long CH,long Cmp,long CLK){return 0;};  //DAコンバータへのデータ転送周期を設定します
/*****************************************************************************
  USBM_DASetCycle

  hDev : デバイスのハンドル
  CH   : 周期を設定するDAチャンネル
  Cmp  : Clkをカウントする回数
  CLK  : クロックを選択
         USBM_TCLK25000 25MHz
         USBM_TCLK12500 12.5MHz
         USBM_TCLK6250  6250kHz
         USBM_TCLK3125  3125kHz

  DAコンバータの変換周期を設定します。変換周期は下のようになります。

  Tc = (Cmp + 1) / fclk [s] (fclk:CLKで選択した周波数)
  
  DA0のタイミング生成には0チャンネルの、DA1のタイミングには1チャンネルの
  16ビットタイマを利用しますので該当チャンネルの16ビットタイマは他の用途には
  使用できなくなります。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_DASetParm(TW_HANDLE hDev,long CH,DWORD SrcPort,long nData,long lLoop){return 0;};  //DAコンバータへのデータ転送パラメータを設定します
/*****************************************************************************
  USBM_DASetParm

  hDev    : デバイスのハンドル
  CH      : DAチャンネル(0,1)
  SrcPort : 転送元アドレス
  nData   : データ数
            繰り返さない場合、1〜65536
            繰り返し転送する場合、1〜255
  lLoop   : 繰り返し転送するかのフラグ(TRUE,FALSE)

  DAコンバータへのDMA転送設定を行います。転送(変換)の周期はUSBM_DASetCycle()で
  指定してください。転送元アドレスは一般にユーザーメモリ内のアドレスを指定し、
  予めデジタルデータを設定しておく必要があります。lLoopが真の場合、nDataの変換
  終了後に、再びデータの先頭から変換を繰り返します。
  DA0の転送にはDMA0チャンネル、DA1の転送にはDMA1チャンネルを利用しますので、
  該当チャンネルのDMA転送は行えなくなります。具体的にはDA0のDMA転送時には
  USBM_PortBWrite()のDMA転送とUSBM_PortBCopy()が使用できなくなり、DA1のDMA転送
  時にはUSBM_PortBRead()のDMA転送が使えません。また、DAチャンネルと同じチャンネ
  ルの16ビットタイマも使用できなくなります。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_DAReadStatus(TW_HANDLE hDev,long *pnData){return 0;};  //DAコンバータへの転送が実行中かどうか調べます
/*****************************************************************************
  USBM_DAReadStatus

  hDev    : デバイスのハンドル
  CH      : DAのチャンネル(0,1)
  pnData  : 残りの変換データ数の格納先

  DAコンバータの残り変換データ数を調べます。繰り返し変換中は常に0以外の値が返
  ります。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_DAStart(TW_HANDLE hDev, BYTE Bits){return 0;};  //DAコンバータへの転送(変換)を開始します
/*****************************************************************************
  USBM_DAStart

  hDev : デバイスのハンドル
  Bits : スタートするチャンネル
         ビット0(LSB) 1の場合DA0への転送(変換)を開始します。
         ビット1      1の場合DA1への転送(変換)を開始します。

  DAコンバータへのデータ転送を開始します。転送を開始する前に毎回
  USBM_DASetParm()を呼び出してください。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_DAStop(TW_HANDLE hDev, BYTE Bits){return 0;};  //DAコンバータへの転送(変換)を終了します
/*****************************************************************************
  USBM_DAStop

  hDev : デバイスのハンドル
  Bits : 終了するチャンネル
         ビット0(LSB) 1の場合DA0への転送(変換)を終了します。
         ビット1      1の場合DA1への転送(変換)を終了します。

  DAコンバータへのデータ転送を終了します。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYSetPatternCtrl(TW_HANDLE hDev, long CH, DWORD SrcPort, DWORD DstPort, long nCopy, long nSrc, long SrcInc, long Start, BYTE Mask){return 0;};
/*****************************************************************************
  USBM_TCPYSetPatternCtrl

  hDev    : デバイスのハンドル
  CH      : 設定するチャンネル(0,1)
  SrcPort : パターンデータの先頭アドレス
  DstPort : 転送先アドレス
  nCopy   : 転送回数
            1〜65535  指定回数だけコピーを行います
            -1        停止されるまでコピーを繰り返します
  nSrc    : パターンデータのバイト数(1〜65535)
  SrcInc  : 転送毎に転送元アドレスを増減させるバイト数(-128〜127)
  Start   : 開始時の転送元を示す0から始まるインデックス値(0〜65534)
  Mask    : 転送先に反映するビットを指定

  タイマコピーの機能を利用して、任意のポートへのパルスパターン出力を設定します。
  タイマコピーでは8ビットタイマがコンペア値と一致する度に転送元に指定したアド
  レスから転送先に指定したアドレスに1バイトずつデータをコピーされます。
  USBM_TCPYSetParm()との違いは転送元に転送回数とは別にパターン数を指定できる
  点です。転送元アドレスはパターンデータの最後尾を超えると、自動的に先頭に移動
  します。逆に先頭アドレスより小さくなると、自動的に最後尾に移動します。
  また、転送先アドレスは常に固定です。
  Startは転送開始時の転送元の位置を指定します。転送元としてSrcPort+Startから
  開始されます。
  Maskは転送先に反映するビットを指定します。0のビットは書換えられません。Mask
  は出力専用ポート(USBM_POUT)に対しては無効です。
  この関数は該当チャンネルのタイマコピーが動作している間は呼び出さないで下さい

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYSetTrig(TW_HANDLE hDev, long CH, long StartPC, long StopPC,long Repeat){return 0;};  //タイマコピーのトリガを設定します
/*****************************************************************************
  TW_STATUS USBM_TCPYSetTrig

  hDev    : デバイスのハンドル
  CH      : 設定するタイマコピーのチャンネル(0,1)
  StartPC : 起動要因となるパルスカウンタチャンネルを指定します。
            -1   パルスカウンタでは起動しません。
            0〜3 指定されたPCx#信号の立ち下がりで起動します。
  StopPC  : 終了要因となるパルスカウンタチャンネルを指定します。
            -1   パルスカウンタ入力では終了しません。
            0〜3 指定されたPCx#信号の立ち下がりで終了します。
  Repeat  : パルスカウンタ入力を繰り返し受け付けるかのフラグ(TRUE,FALSE)

  タイマコピーの起動要因、終了要因にPC0#〜PC3#のパルスカウンタ入力を指定します
  起動要因と終了要因は、別々のチャンネルを指定してください。
  この関数を呼び出すと、指定のパルスカウンタがカウントを開始しますので、不要
  になった場合にはUSBM_PCStop()関数を呼び出してパルスカウンタを停止してくだ
  さい。
*****************************************************************************/


TW_STATUS _stdcall USBM_TCPYReadStatus(TW_HANDLE hDev, long CH, long *pnCopy, long *pSrcPos){return 0;};  //タイマコピーの転送状況を読み出します
/*****************************************************************************
  TW_STATUS USBM_TCPYReadStatus

  hDev      : デバイスのハンドル
  CH        : 設定するタイマコピーのチャンネル(0,1)
  pnCopy    : 現在の転送回数の格納先
  pSrcPos   : 現在の転送元位置を示す0から始まるインデックス

  指定チャンネルのタイマコピーの終了した転送回数と転送元の位置を返します。
  pSrcPosには設定時に指定した転送元アドレスから何バイトの位置を転送中であるか
  が返ります。
  転送中に呼び出すと、pnCopyの値とpSrcPosの値が同期していない場合があります。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCapture(TW_HANDLE hDev, long CH, BYTE EdgeA, BYTE EdgeB){return 0;};  //16ビットタイマをインプットキャプチャモードに設定します
/*****************************************************************************
  USBM_TimerSetCapture

  hDev  : デバイスのハンドル
  CH    : インプットキャプチャ設定を行うタイマチャンネル(0,1,2)
  EdgeA : TIOCA端子でキャプチャする信号エッジ
          USBM_CAPT_RISE 立ち上がりをキャプチャ
          USBM_CAPT_FALL 立ち下がりをキャプチャ
          USBM_CAPT_BOTH 立ち上がり、立ち下がり両方でキャプチャ
  EdgeB : TIOCB端子でキャプチャする信号エッジ(指定できる値はEdgeAと同じ)

  16ビットタイマの指定チャンネルをインプットキャプチャモードに設定します。
  インプットキャプチャに設定すると該当チャンネルのTIOCA,TIOCB端子は入力端子と
  なり、引数で指定された信号のエッジを検出すると、そのときのカウンタの値が
  マイコンのGRA,GRBレジスタに転送されます。この機能を利用することで、TIOCA端子
  で信号を検出してから、TIOCBで信号が検出するまでの時間差をタイマクロックの
  カウント値で測定することができます。クロックの設定にはUSBM_TimerSetClk()関数
  を使用してください。カウンタはTIOCB端子が信号を検出したときにクリアされます。
  動作中は信号が入力される度に、何度でもキャプチャされます。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerReadCaptureCnt(TW_HANDLE hDev, long CH, long *pCntA, long *pCntB){return 0;};  //インプットキャプチャの結果を読み出します
/*****************************************************************************
  USBM_TimerReadCaptureCnt

  hDev   : デバイスのハンドル
  CH     : タイマチャンネル(0,1,2)
  pCntA : GRAレジスタ値の格納先
  pCntB : GRBレジスタ値の格納先

  インプットキャプチャした結果を読み出します。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetCaptureCnt(TW_HANDLE hDev, long CH, long CntA, long CntB){return 0;};  //インプットキャプチャ用レジスタを初期化します
/*****************************************************************************
  USBM_TimerSetCaptureCnt

  hDev : デバイスのハンドル
  CH   : タイマチャンネル(0,1,2)
  CntA : GRAレジスタにセットする値(0〜65535)
  CntB : GRBレジスタにセットする値(0〜65535)

  GRA、GRBレジスタに値をセットします。主にインブットキャプチャを行う前に
  レジスタをクリアするのに使用します。
*****************************************************************************/



TW_STATUS _stdcall USBM_ADStart(TW_HANDLE hDev, long nCnv, long CH, long Scan, long Byte,long Trig){return 0;};  //ADコンバータを起動します
/*****************************************************************************
  USBM_ADStart

  hDev   : デバイスのハンドル
  nCnv   : 変換回数。複数チャンネルをスキャンする場合は、それぞれnCnv回
           変換されます。-1の場合中止されるまで繰り返します。
           (-1,1〜2147483647)
  CH     : ADコンバータのチャンネル(0,1,2,3)
  Scan   : 複数チャンネルをスキャンするかどうかのフラグ(TRUE,FALSE)
  Byte   : バイト読み出しするかどうかのフラグ(TRUE,FALSE)
  Trig   : 内蔵タイマの起動をADTRG#で行うかどうかのフラグ(TRUE,FALSE)

  ADコンバータを起動し、断続的に変換を行います。変換結果はUSBM_Read()関数で
  取り出してください。変換タイミングはマイコン内臓の8ビットタイマで作り出す
  方法と、ADTRG#端子に信号を入力する方法があります。ADTRG#を選択した場合、
  ADCはトリガ入力待ちとなり、1回のトリガにつき1回の変換を行います。
  タイマを選択した場合、タイマを起動し予め設定したサイクル毎に変換を行います。
  デフォルトでは外部トリガの入力で 変換が開始されますので、タイマを利用する
  場合には、予めUSBM_ADSetCycle()関数でクロックとコンペア値を設定します。

  ScanがTRUEのときは1回のトリガにつき、0から指定されたチャンネルまでを1回ずつ
  AD変換します。例えばScan=TRUEでCH=2の場合、0〜2チャンネルの3チャンネル分の
  変換をトリガ毎に行いホストに転送します。

  ByteがTRUEの場合はAD変換結果の上位8ビットのみをホストに転送します。16ビット
  転送を行う場合よりも、変換レートを上げることが可能となります。
  詳しくはユーザーズマニュアルを参照してください。

  変換タイミングをマイコンの内臓タイマで作る場合でも、TrigをTRUEに設定すると
  外部トリガに変換を同期させることができます。この場合、USBM_ADSetCycle()で
  外部トリガを指定した場合と違い、ADTRG#入力で内蔵タイマを起動し、ADのトリガ
  信号自体はタイマから供給されますので、ADTRG#への入力は1度でよくなります。

  ADBRead()関数との違いは、ADBRead()関数は指定したデータ数の変換が全て終了する
  までデータが取り出せないのに対して、終了したデータだけを逐次取り出せることで
  す。また、複数チャンネルの連続変換も可能です。

  デバイスはこの関数を呼び出した後は、指定の変換回数が終了するまで、
  USBM_Abort()以外の関数を受付ませんのでご注意ください。また、変換終了後は
  バッファ内のデータを確実に取り出してください。データが残っている場合、後の
  関数が誤動作します。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ADCopy(TW_HANDLE hDev, DWORD DstPort, long nCnv, long CH, long CKS, long DMA_CH, long DstInc){return 0;};  //ADコンバータを起動し、変換結果をマイコン内のアドレスに保存します
/*****************************************************************************
  USBM_ADCopy

  hDev    : デバイスのハンドル
  DstPort : 変換結果を格納するマイコン内のアドレス
  nCnv    : 変換回数。複数チャンネルをスキャンする場合は、それぞれnCnv回
            変換されます。(1〜65536)
  CH      : 終了チャンネル(0,1,2,3)
  CKS     : 変換ステート(TRUE,FALSE)
  DMA_CH  : 使用するDMAチャンネル(0,1)
  DstInc  : 転送先アドレスをインクリメントするかどうかのフラグ(TRUE,FALSE)

  AD変換を連続で行い、変換結果をマイコン内のメモリに転送します。
  呼び出すと、USBM_ADSetCycle()で指定した変換周期はクリアされ、起動要因は外部
  トリガに固定されます。設定終了後、変換が終了したかどうかにかかわらず、関数は
  リターンします。
  その後、デバイスがADTRG#の立ち下がりを検出すると、指定回数の変換を自動で行い
  ます。このとき変換は常に最大レートで行われます。また、変換は必ず0チャンネル
  から開始され、CHで指定したチャンネルまで変換されます。例えば、CH=2のとき
  0〜2までの3チャンネルの変換をnCnv回行います。
  指定回数の変換が終了したかどうかを調べるにはUSBM_ADReadCopyStatus()を使用し
  てください。中断して終了する場合、及び変換終了後はUSBM_ADStopCopy()を呼び出
  してください。
  
  CKSがFALSEのときは最初の1サイクルの変換時間が最大5.36usで、以降、連続変換中は
  5.12us/チャンネルです。
  CKSをTRUEとすることで精度を犠牲にして、変換レートを上げることができます。
  この場合、最初の1サイクルの変換時間が最大で2.8usで、以降、連続変換中は
  2.64us/チャンネルとなります。
  変換精度についてはマニュアルを参照してください。

  DstPortとnCnvの設定は慎重に行ってください。変換結果を格納する領域が、
  ユーザーメモリの範囲を超えてもチェックは行われません。使用できる範囲を超えて
  DMA転送が行われると、マイコンの内部メモリの情報を破壊して正常に動作しなく
  なります。

  DMAチャンネルを1つ利用しますので、該当チャンネルのDMA転送は行えなくなります
  DMA0を使用する場合にはUSBM_PortBWrite()のDMA転送とUSBM_PortBCopy()が使用でき
  なくなり、DMA1を使用した場合にはUSBM_PortBRead()のDMA転送が使えません。
  また、DAコンバータへの自動転送も同じ番号のチャンネルが使用できなくなります。
        
  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/



TW_STATUS _stdcall USBM_ADStopCopy(TW_HANDLE hDev, long DMA_CH){return 0;};  //USBM_ADCopy()を終了します
/*****************************************************************************
  USBM_ADStopCopy

  hDev   : デバイスのハンドル
  DMA_CH : USBM_ADCopy()で指定したDMAチャンネル(0,1)

  USBM_ADCopy()の終了処理をします。中断して終了する場合にもこの関数を呼び出し
  てください。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ADReadCopyStatus(TW_HANDLE hDev,long *pnCnv,long DMA_CH){return 0;};  //USBM_ADCopy()の転送状況を読み出します
/*****************************************************************************
  USBM_ADReadCopyStatus

  hDev   : デバイスのハンドル
  pnCnv  : 残りの変換数の格納先
  DMA_CH : USBM_ADCopy()で指定したDMAチャンネル(0,1)

  USBM_ADCopy()の残りの変換回数を調べます。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ADReadCopyBuffer(TW_HANDLE hDev, DWORD Port, WORD *pData, long nData, long Inc, BYTE Dma){return 0;};  //USBM_ADCopy()の変換結果を読み出します
/*****************************************************************************
  USBM_ADReadCopyBuffer

  hDev   : デバイスのハンドル
  Port   : 読み出すアドレス
  pData  : 読み出したデータの格納先
  nData  : データの数(1〜32768、ワード単位)
  Inc    : マイコンのアドレスをインクリメントするかを示すフラグ(TRUE,FALSE)
  Dma    : DMAを使用するかどうかのフラグ(TRUE,FALSE)

  USBM_ADCopy()関数で変換されたデータをデバイス上のバッファから読み出すのに
  使用します。USBM_PortBRead()関数でも読み出せますが、マイコン上のデータは
  Windowsパソコンとバイトオーダーが違うため変換が必要になります。この関数では
  読み出したデータを、パソコンと同じバイトオーダー(リトルエンディアン)に
  並び替えて返します。
  IncをFALSEとするとマイコン側のアドレスがインクリメントされませんのでFIFOの
  ようなデバイスから読み出しを行えます。DmaをTRUEとするとマイコン内臓のDMAを
  使用してデータを転送します。DMAを使用すると高速ですが、転送終了時に
  PA1/TCLKBにTEND#信号を出力するので注意が必要です。PA1/TCLKBに出力が出ては
  いけない場合にはDMA=FALSEとして呼び出してください。
*****************************************************************************/


TW_STATUS _stdcall USBM_SCISetDelimiter(TW_HANDLE hDev, long CH, char *pDelimiter, long nDelimiter){return 0;};  //シリアル通信のデリミタ文字を設定します
/*****************************************************************************
  USBM_SCISetDelimiter

  hDev       : デバイスのハンドル
  CH         : SCIチャンネル(0のみ)
  pDelimiter : デリミタ文字へのポインタ
  nDelimiter : デリミタ文字の数(0〜2)

  SCIチャンネルのデリミタ文字を指定します。デリミタ文字(1文字または2文字)が
  現れると、USBM_SCIRead()関数はシリアルポートからの読み取りを中止し、読み取り
  指定バイトまで0を埋めしてデータを返します。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_GetQueueStatus(TW_HANDLE hDev, DWORD *pnQueue){return 0;};  //受信バッファ中のバイト数を返します
/*****************************************************************************
  USBM_GetQueueStatus

  hDev    : デバイスのハンドル
  pnQueue : 受信バイト数の格納先

  デバイスから受信したデータのバイト数を取得します。USBM_ADStart()を使用時に
  受信した変換結果のバイト数を取得するのに使用します。
  単にFTDIの関数、FT_GetQueueStatus()を呼び出しています。
*****************************************************************************/


TW_STATUS _stdcall USBM_Purge(TW_HANDLE hDev, DWORD dwMask){return 0;};  //送信バッファ、受信バッファをクリアします
/*****************************************************************************
  USBM_Purge

  hDev   : デバイスのハンドル
  dwMask : クリアするバッファを指定
           USBM_PURGE_RX 受信バッファをクリア
           USBM_PURGE_TX 送信バッファをクリア

  デバイスとの通信に使用するバッファをクリアします。特にデバイスからデータを
  読み出す関数がタイムアウトした場合、関数から復帰した後にデバイスから送られた
  データが受信バッファに溜まっている場合があるので、それらをクリアするために
  必要となります。
  単にFTDIの関数、FT_Purge()を呼び出しています。
*****************************************************************************/


TW_STATUS _stdcall USBM_SetTimeouts(TW_HANDLE hDev, DWORD dwReadTimeout, DWORD dwWriteTimeout){return 0;};  //送信及び受信のタイムアウト時間を設定します
/*****************************************************************************
  USBM_SetTimeouts

  hDev           : デバイスのハンドル
  dwReadTimeout  : 読み出しのタイムアウト(ms単位)
  dwWriteTimeout : 書き込みのタイムアウト(ms単位)

  デバイスとの送信及び受信の際に適用されるタイムアウト時間を設定します。
  デフォルトの設定では送信、受信とも5秒です。
  単にFTDIの関数、FT_SetTimeouts()を呼び出しています。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCStartA(TW_HANDLE hDev, BYTE Bits){return 0;};  //パルスカウンタのカウントを許可します
/*****************************************************************************
  USBM_PCStartA

  hDev : デバイスのハンドル
  Bits : USBM_PC0  チャンネル0を許可 
         USBM_PC1  チャンネル1を許可
         USBM_PC2  チャンネル2を許可
         USBM_PC3  チャンネル3を許可

  パルスカウントを許可します。許可するチャンネルが複数ある場合には上記の定数を
  ORで結合してBitsに指定してください。USBM_PCStart()関数との違いは、スタート
  するように指定したチャンネル以外に影響を与えないことです。
*****************************************************************************/


TW_STATUS _stdcall USBM_PCStop(TW_HANDLE hDev, BYTE Bits){return 0;};  //パルスカウンタを停止します
/*****************************************************************************
  USBM_PCStop

  hDev : デバイスのハンドル
  Bits : USBM_PC0  チャンネル0のカウントを終了 
         USBM_PC1  チャンネル1のカウントを終了
         USBM_PC2  チャンネル2のカウントを終了
         USBM_PC3  チャンネル3のカウントを終了

  指定チャンネルのパルスカウントを終了します。終了するチャンネルが複数ある場
  合には上記の定数をORで結合してBitsに指定してください。USBM_PCStart()関数と
  の違いは、終了するように指定したチャンネル以外に影響を与えないことです。
*****************************************************************************/


TW_STATUS _stdcall USBM_TimerSetSinglePulse(TW_HANDLE hDev, long CH, long Setup, long Width, long Pos){return 0;};  //指定された長さのパルスを1回発生します
/*****************************************************************************
  USBM_TimerSetSinglePulse

  hDev  : デバイスのハンドル
  CH    : 使用する16ビットタイマチャンネル(0,1)
  Setup : タイマ起動からパルス出力までの時間をクロック数で指定(1〜65534)
  Width : パルス幅をクロック数で指定(1〜65534)
  Pos   : TRUEの場合、正のパルス、FALSEの場合、負のパルスが出力されます

  16ビットタイマを利用して単発のパルスを発生させます。SetupとWidthを足した値が
  65535を超えてはいけません。

  ※この関数はVer.2.0以降のファームウェアが必要です。
*****************************************************************************/


FT_HANDLE _stdcall USBM_GetFTHandle(TW_HANDLE hDev){return 0};  //D2XX API関数に渡せるハンドルを取得します。
/*****************************************************************************
  USBM_GetFTHandle

  hDev  : デバイスのハンドル

  D2XX API関数に渡せるハンドルを取得します。
*****************************************************************************/


TW_STATUS _stdcall USBM_ReadPI(TW_HANDLE hDev, USBM_PRODUCT_INFO *pInfo){return 0}; //製品情報(PI)を取得します。
/*****************************************************************************
  TW_STATUS USBM_ReadPI(TW_HANDLE hDev, USBM_PRODUCT_INFO *pInfo)

  hDev  : デバイスのハンドル
  pInfo : 製品情報の格納先

  PIエリアから製品情報を読み出します。
  ※この関数はVer.2.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_OpenByPI(TW_HANDLE *phDev, TW_CSTR pUuid, DWORD Number, long Opt){return 0}; //製品情報(PI)を指定してデバイスに接続します。
/*****************************************************************************
  TW_STATUS USBM_OpenByPI(TW_HANDLE *phDev, TW_CSTR pUuid, 
                                   DWORD Number, long Opt)

  phDev  : ハンドルの格納先
  pUuid  : UUIDを指定します。指定が無い場合にはNULLまたは""とします。
  Number : Numberを指定します。指定が無い場合には0とします。
  Opt    : 以下の値の組み合わせ
           USBM_IF_USB : USBデバイスに接続する
           USBM_IF_LAN : LANデバイスに接続する

           USBM_IF_LAN オプションには下のオプションをORで追加可能
           USBM_LIST_UPDATE : LANデバイスの情報を保存した内部テーブルを更新

  PIエリア内のUUIDとNumberが指定と一致するデバイスと接続します。指定が無い項目
  は比較されません。
  Opt は USBM_IF_USB または USBM_IF_LAN でインタフェースを指定します。ORで結合
  して両方を指定することが可能です。USBM_IF_LAN を指定した場合には
  USBM_LIST_UPDATE を結合することができます。このオプションによりLANデバイスの
  情報を格納した内部テーブルを更新することができます。USBM_LIST_UPDATE が指定
  されない場合には、既に取得した内部テーブルの情報からデバイスを探します。
  内部テーブルの更新にはUDPのブロードキャストが用いられ、約1.5秒の時間を要し
  ます。
  USBとLANの両方のインタフェースが指定された場合、まずUSBデバイスに対して接続
  を試み、接続ができなかった場合にLANデバイスへの接続を行います。
  ※この関数はVer.2.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_PortWrite8A(TW_HANDLE hDev,DWORD Port, BYTE Data, BYTE Mask = 0xff){return 0}; //ポートへの書込みを行います
/*****************************************************************************
  USBM_PortWrite8A

  hDev : デバイスのハンドル
  Port : 書き込むアドレス
  Data : 書き込むデータ
  Mask : マスクデータ。1のビットだけ反映されます。

  マイコンのPortで指定されるアドレスに1バイトのデータを書き込みます。マイコン
  の制御レジスタにも書き込みが行えますので、定数以外のアドレスを指定する場合に
  は注意が必要です。Maskが0となっているビットには影響を与えません。
  ※この関数はVer.2.2以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_SetPassword(TW_CSTR pPass = NULL){ return 0}; //LANデバイスのパスワードを指定します
/*****************************************************************************
  TW_STATUS USBM_SetPassword(TW_CSTR pPass)

  pPass : パスワード

  LANM3069デバイスと接続する際のパスワードを入力します。パスワードは設定ツール
  でデバイスに書き込んだものと同じものを指定してください。
  pPass が NULL または "" の場合、デフォルトのパスワードが使用されます。
*****************************************************************************/


TW_STATUS _stdcall USBM_SetNetworkPort(DWORD PortNumber = 0){ return 0}; //LANデバイスのネットワークポートを指定します
/*****************************************************************************
  TW_STATUS USBM_SetNetworkPort(DWORD PortNumber)

  PortNumber : ポート番号

  LANM3069デバイスと通信する場合のネットワークポート番号を指定します。ここで指
  定するポート番号はデバイス側のものですので設定ツールでデバイスに書き込んだも
  のと同じものを指定してください。一般的にポート番号として使用できるのは
  49152〜65535 の値です。
*****************************************************************************/


ULONG _stdcall USBM_GetIFType(TW_HANDLE hDev){ return 0 }; //デバイスのインタフェース種別を調べます
/*****************************************************************************
  ULONG USBM_GetIFType(TW_HANDLE hDev)

  hDev   : デバイスのハンドル

  戻り値 : デバイスのインタフェース種別とフラッシュ書換えモードを示すフラグ
           USBM_IF_USB(0x80000000) : USBインタフェース
           USBM_IF_LAN(0x40000000) : LANインタフェース
           
           フラッシュ書換えモードでは上記に加えて以下のビットが1になります
           USBM_MODE_FLASH(0x00800000) : フラッシュ書換えモードを示す

  ハンドルに結び付けられたデバイスのインタフェースタイプとフラッシュ書換えモー
  ドかどうかのフラグを返します。
*****************************************************************************/


TW_STATUS _stdcall USBM_OpenA(TW_HANDLE *phDev, TW_CSTR pIID = NULL, long Opt = USBM_IF_ANY | USBM_LIST_UPDATE){return 0}; //デバイスに接続します
/*****************************************************************************
  TW_STATUS USBM_OpenA(TW_HANDLE *phDev, TW_CSTR pIID, long Opt)

  phDev  : ハンドルの格納先
  pIID   : USBのシリアル番号またはIPアドレス(NULLで終わる文字列)
  Opt    : インタフェースと接続オプションを指定
           インタフェースは以下のいずれかを指定
           USBM_IF_USB : USBデバイスに接続する
           USBM_IF_LAN : LANデバイスに接続する
           USBM_IF_ANY : インタフェースを問わない

           上記インタフェースに加えて以下のオプションをORで追加可能
           USBM_MODE_FLASH : フラッシュ書換えモードのデバイスに接続
           USBM_LIST_UPDATE : LANデバイスの情報を保存した内部テーブルを更新
                              (LANデバイスのみ有効)

  pIID がNULLか""の場合、接続できた最初のデバイスのハンドルを取得します。それ以
  外では指定されたIDと一致するデバイスと接続します。
  Opt は USBM_IF_USB または USBM_IF_LAN でインタフェースを指定します。ORで結合
  して両方を指定することが可能です。USBM_IF_LAN を指定した場合には
  USBM_LIST_UPDATE を結合することができます。このオプションによりLANデバイスの
  情報を格納した内部テーブルを更新することができます。USBM_LIST_UPDATE が指定
  されない場合には、既に取得した内部テーブルの情報からデバイスを探します。
  内部テーブルの更新にはUDPのブロードキャストが用いられ、約1.5秒の時間を要し
  ます。
  USBとLANの両方のインタフェースが指定された場合、まずUSBデバイスに対して接続
  を試み、接続ができなかった場合にLANデバイスへの接続を行います。

  USBM_MODE_FLASHを指定するとフラッシュ書換えモードのデバイスに接続します。
  LANデイバスの場合はpIIDに指定したIPアドレスを一時的にデバイスに割り当てて
  通信を行います。pIIDの指定がない場合にはDHCPを利用してIPの取得を試みます。
*****************************************************************************/


TW_STATUS _stdcall USBM_ListDevicesA(long *pnDev, TW_STR pIID, long *pnIID, long Opt){return 0}; //デバイスをリストアップします
/*****************************************************************************
  TW_STATUS USBM_ListDevicesA(long *pnDev, TW_STR pIID, long *pnIID, long Opt)

  pnDev  : デバイス数の格納先
  pIID   : USBのシリアル番号またはIPアドレスの格納先
  pnIID  : [入力]pIIDに用意された文字数
           [出力]pIIDに必要な文字数、または実際にコピーした文字数
  Opt    : 次のオプションのどれか
           USBM_IF_USB : USBデバイスをリストアップする
           USBM_IF_LAN : LANデバイスをリストアップする

           USBM_IF_LAN オプションには下のオプションをORで追加可能
           USBM_LIST_UPDATE : LANデバイスの情報を保存した内部テーブルを更新

  接続されているデバイス数を調べます。
  pIID には Opt として USBM_IF_USB を選択した場合USBデバイスのシリアル番号が
  USBM_IF_LAN を指定した場合にはIPアドレスが格納されます。シリアル番号とIPアド
  レスは文字列で格納されます。複数のデバイスが見つかった場合には、それぞれのデ
  バイスのID文字列の間に' '(スペース)が挿入され、最後の要素の後は'\0'が挿入さ
  れます。
  pnIID は入力として pIID に用意された文字数を渡します。出力としては全てのID文
  字列が格納された場合にはその文字数が、格納しきれなかった場合には格納するのに
  十分な文字数を示します。
  Opt は USBM_IF_USB または USBM_IF_LAN でインタフェースを特定します。両方を同
  時に指定することはできません。USBM_IF_LAN を指定した場合には
  USBM_LIST_UPDATE を結合することができます。このオプションによりLANデバイスの
  情報を格納した内部テーブルを更新することができます。USBM_LIST_UPDATE が指定
  されない場合には、既に取得した内部テーブルの情報からデバイスを探します。
  内部テーブルの更新にはUDPのブロードキャストが用いられ、約1.5秒の時間を要し
  ます。
  Opt 以外は不要な場合NULLとすることができます。
*****************************************************************************/


TW_STATUS _stdcall USBM_ReadVersionA(TW_HANDLE hDev, DWORD *pVersion){return 0};
/*****************************************************************************
  TW_STATUS USBM_ReadVersionA(TW_HANDLE hDev, DWORD *pVersion)

  hDev     : デバイスのハンドル
  pVersion : バージョン情報の格納先

  USBM_ReadVersion()よりも詳しいファームウェアバージョンを取得します。
  返されるバージョンの最上位8ビットは予約です。
  次の8ビットはメジャーバージョンです。大幅な機能変更の際に更新されます。
  次の8ビットはマイナーバージョンです。比較的小さな機能変更の際に更新されます。
  最下位8ビットはリビジョンです。バグフィックスやUSBM3069またはLANM3069固有の
  機能変更の際に更新されます。
  例えばファームウェアバージョンがVer.3.0.1の場合、pVersionにはH'00030001が格
  納されます。
*****************************************************************************/


TW_STATUS _stdcall USBM_ATFSetCommand(TW_HANDLE hDev, DWORD Address){return 0};
/*****************************************************************************
  TW_STATUS USBM_ATFSetCommand(TW_HANDLE hDev, DWORD Address)

  hDev    : デバイスのハンドル
  Address : コマンドハンドラのアドレス

  指定のアドレスをユーザー定義コマンドのハンドラとして登録します。削除するには
  アドレスを0として呼び出します。
  登録されたコマンドハンドラはUSBM_ATFUserCommand()関数呼び出し時に呼び出され
  ます。

  ※この関数はVer.3.0.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ATFSetMain(TW_HANDLE hDev, DWORD Address){return 0};
/*****************************************************************************
  TW_STATUS USBM_ATFSetMain(TW_HANDLE hDev, DWORD Address)

  hDev    : デバイスのハンドル
  Address : メイン関数のアドレス

  指定のアドレスをメイン関数として登録します。削除するにはアドレスを0として呼
  び出します。メイン関数はコマンドループの先頭で呼び出される関数です。コマンド
  以外に常時行う処理がある場合に使用します。

  ※この関数はVer.3.0.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ATFUserCommand(TW_HANDLE hDev, WORD Command, DWORD Param1, DWORD Param2, void *pData, long nData){return 0};
/*****************************************************************************
  TW_STATUS USBM_ATFUserCommand(TW_HANDLE hDev, WORD Command, DWORD Param1, 
                                DWORD Param2, void *pData, long nData)

  hDev    : デバイスのハンドル
  Command : ユーザー定義のコマンドID
  Param1  : ユーザー定義のパラメータ1
  Param1  : ユーザー定義のパラメータ2
  pData   : ユーザー定義の追加パラメータ
  nData   : ユーザー定義の追加パラメータのバイト数

  ユーザー定義のコマンドを呼び出します。Command、Param1、Param2はコマンドハン
  ドラに引数として渡されます。追加のパラメータが必要な場合にはpDataに指定しま
  す。

  ※この関数はVer.3.0.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ATFDownload(TW_HANDLE hDev, TW_CSTR FileName, TW_CSTR pReserve){return 0};
/*****************************************************************************
  TW_STATUS USBM_ATFDownload(TW_HANDLE hDev, 
                             TW_CSTR FileName, TW_CSTR pReserve)

  hDev     : デバイスのハンドル
  FileName : ATFファイルのパス
  pReserve : 予約。NULLにしてください

  アタッチメントファーム(ATFファイル)をRAMにダウンロードします。必要であれば、
  コマンドハンドラの登録、メイン関数の登録を行います。

  ※この関数はVer.3.0.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_ATFGetInfo(TW_CSTR FileName, USBM_ATF_INFO *pInfo){return 0};
/*****************************************************************************
  TW_STATUS USBM_ATFGetInfo(TW_CSTR FileName, USBM_ATF_INFO *pInfo)

  FileName : ATFファイルのパス
  pInfo    : 情報を受け取るATF_INFO構造体へのポインタ

  ATFファイルに関する情報を取得します。
*****************************************************************************/


TW_STATUS _stdcall USBM_InitializeA(TW_HANDLE hDev, DWORD InitOption){return 0};
/*****************************************************************************
  TW_STATUS _stdcall USBM_InitializeA(TW_HANDLE hDev, DWORD InitOption)

  hDev       : デバイスのハンドル
  InitOption : 初期化する機能を指定
               USBM_INIT_PORT_DIR(0x0001)  ポートの方向、プルアップを初期化
               USBM_INIT_PORT_DATA(0x0002) ポートのデータを初期化
               USBM_INIT_BUS(0x0004)       外部バス設定を初期化
               USBM_INIT_DMA(0x0008)       DMAを初期化
               USBM_INIT_TIMER(0x0010)     16ビットタイマを初期化
               USBM_INIT_AD(0x0020)        ADコンバータを初期化
               USBM_INIT_SCI(0x0040)       シリアルポートを初期化
               USBM_INIT_PC(0x0080)        パルスカウンタを初期化
               USBM_INIT_TCPY(0x0100)      タイマコピーを初期化
               USBM_INIT_ALL(0xffffffff)   全ての機能を初期化

  デバイスを初期化します。InitOptionで初期化する機能を選択できます。

  ※この関数はVer.3.2.1以降のファームウェアが必要です。
*****************************************************************************/


TW_STATUS _stdcall USBM_FlashEraseBlk(TW_HANDLE hDev, long Blk){return 0};
/*****************************************************************************
  TW_STATUS USBM_FlashEraseBlk(TW_HANDLE hDev, long Blk)

  hDev : デバイスのハンドル
  Blk  : 消去するフラッシュメモリのブロック(1-3)
           
  フラッシュメモリの指定ブロックを消去します。
  ※フラッシュ書換えモード専用です。
*****************************************************************************/


TW_STATUS _stdcall USBM_FlashRead(TW_HANDLE hDev, DWORD Address, void *pBuff, DWORD nData){return 0};
/*****************************************************************************
  TW_STATUS USBM_FlashRead(TW_HANDLE hDev,
                           DWORD Address, void *pBuff, DWORD nData)

  hDev    : デバイスのハンドル
  Address : 読み出すフラッシュメモリアドレス(0x1000-0x3f80)
  pBuff   : [out]読み出したデータの格納先
  nData   : 読み出すバイト数(128の倍数)

  フラッシュメモリの指定アドレスから読み出しを行います。読み出すバイト数は128
  の倍数で指定します。
  ※フラッシュ書換えモード専用です。
*****************************************************************************/


TW_STATUS _stdcall USBM_FlashWrite(TW_HANDLE hDev, DWORD Address, void *pData, DWORD nData){return 0};
/*****************************************************************************
  TW_STATUS USBM_FlashWrite(TW_HANDLE hDev,
                            DWORD Address, void *pData, DWORD nData)

  hDev    : デバイスのハンドル
  Address : 書込みを行うフラッシュメモリアドレス(0x1000-0x3f80)
  pData   : [in]書込みデータ
  nData   : 書き込むバイト数(128の倍数)

  フラッシュメモリの指定アドレスに書込みを行います。アドレスの指定は128バイト
  境界にそろえる必要があり、書き込むバイト数も128の倍数で指定します。
  ※フラッシュ書換えモード専用です。
*****************************************************************************/

#endif
//****** 以上の行は VisualStudio の IntelliSense のために追加されています *****
//****** 必要がない場合にはコメントアウトしてください                     *****


#endif /*_USBM3069_H_*/

