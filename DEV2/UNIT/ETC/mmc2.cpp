// cat-e 傾斜ステージコントロールプログラム

#include <stdio.h>
#include <math.h>
#include <dev2\unit\etc\mmc2.h>

#define BuffSize 40
static char     buf[BuffSize];

mmc2::mmc2(ifLine *iface, int toHome) : Unit(iface)
{
    myIface->setDelim(ifLine::crlf);

	// set limit +
	sprintf(buf,CAT_SETPA,CAT_PNLMP,CAT_LMP);
	myIface->talk(buf);
	// set limti -
	sprintf(buf,CAT_SETPA,CAT_PNLMM,CAT_LMM);
	myIface->talk(buf);

	sprintf(buf,CAT_SETPA,CAT_PNBK,CAT_BK);
	myIface->talk(buf);		/*Set revised backrash		*/

	sprintf(buf,CAT_SETPA,CAT_PNMO,CAT_MO);
	myIface->talk(buf);		/*Return mode sets DC maicro	*/

	sprintf(buf,CAT_SETPA,CAT_PNDO,CAT_DO);
	myIface->talk(buf);		/*Direction sets CCW		*/

	sprintf(buf,CAT_SETVA,CAT_LOWV,CAT_HIGV,CAT_ACC,CAT_DACC);
	myIface->talk(buf);		/*Sets velocity and acceration	*/
	spLow  = CAT_LOWV;
	spHigh = CAT_HIGV;
	spAcc  = CAT_ACC;


	sprintf(buf,CAT_SETSP,CAT_HSP);
	myIface->talk(buf);		/*set half step			*/

	myIface->talk(CAT_ON);
	withWait = isWait;
	if (toHome) setHome();
}

mmc2::~mmc2()
{  myIface->talk(CAT_OFF); }

void mmc2::setSpeed(int low, int high, int acc)
{
  spLow = low;
  spHigh = high;
  spAcc  = acc;

  sprintf(buf, "D:XP%dP%dP%d", low, high, acc);
  myIface->talk(buf);		/*Sets velocity and acceration	*/
  waitReady();
}

void mmc2::waitReady()
{
  do {
       myIface->talk("Q:");
       unsigned int i = BuffSize;
       myIface->listen(buf, i);
     } while (*(buf+24) != 'R');
}

int mmc2::toPluse(double ang)
{ if (abs(ang) > 22.0) ang = 0;             // 傾斜ステージの限界チェック
  return ang/SOP;
}

void mmc2::setHome()
{
  sprintf(buf,CAT_SETVA,CAT_LOWV,CAT_HIGV,CAT_ACC,CAT_DACC);
  myIface->talk(buf);		/*Sets velocity and acceration	*/
  myIface->talk("H:X");
  waitReady();
  setSpeed(spLow, spHigh, spAcc);
  posNow = 0;
}

/*
void mmc2::moveTo(double ang)
{   sprintf(buf, "A:XP%d", toPluse(ang));
	myIface->talk(buf);
	myIface->talk("G:");
	waitReady();
}
*/

void mmc2::moveTo(double ang)
{ int offset = toPluse(ang)-posNow;
  sprintf(buf, "M:XP%d", offset);
  myIface->talk(buf);
  myIface->talk("G:");
  waitReady();
  posNow = toPluse(ang);
}

void mmc2::moveToPulse(int pulse)
{ int offset = pulse-posNow;
  sprintf(buf, "M:XP%d", offset);
  myIface->talk(buf);
  myIface->talk("G:");
  waitReady();
  posNow = pulse;
}

