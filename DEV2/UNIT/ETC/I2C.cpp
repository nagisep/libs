
#include "I2C.h"

int I2C::addressLen = 0;

void I2C::makeAddr(unsigned int addr, BYTE *result)
{
  // set address data
  for(int i = addressLen - 1; i >= 0; i--)
  {
    result[i] = addr % 0x100;
    addr /= 0x100;
  }
}

I2C::I2C(int ch):myCha(ch)
{ }

BYTE I2C::readByte(unsigned int addr)
{
  char result[8];
  nRead(addr, 1, result);
  return result[0];
}

WORD I2C::readWord(unsigned int addr)
{
  char result[8];
  nRead(addr, 2, result);
  return result[0] * 0x100 + result[1];
}

void I2C::writeByte(unsigned int addr, BYTE data)
{
  BYTE dataArray[2];
  dataArray[0] = data;
  nWrite(addr, 1, dataArray);
}

void I2C::writeWord(unsigned int addr, WORD data)
{
  BYTE dataArray[2];
  dataArray[0] = data / 0x100;
  dataArray[1] = data % 0x100;
  nWrite(addr, 2, dataArray);
}
