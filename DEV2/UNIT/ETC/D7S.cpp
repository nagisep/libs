///////////////////////////////////////////////////////////
//  D7S.cpp
//  Implementation of the Class D7S
//  Created on:      2016/01/08 11:00:55
//  Original author: 015150900
///////////////////////////////////////////////////////////

#include "D7S.h"

// D7S::D7S(int addr, std::string serial)
D7S::D7S(int addr, char *strSerial)
{
  std::string serial = strSerial;
  port = new CP1221(addr, serial);
}

D7S::D7S()
{
  std::string serial = "0001";
  port = new CP1221(0x56, serial);
}

D7S::~D7S()
{
  delete port;
}

unsigned int D7S::getValue(unsigned int portAddr)
{
  return port->readByte(portAddr);
}

void D7S::intoTestMode()
{
  port->writeWord(0x90e0, 0x6c14);
  port->writeByte(0x1003, 0x05);
}

void D7S::leaveTestMode()
{
  port->writeByte(0x1003, 0x01);
}


void D7S::getGal(double *galSet)
{
  BYTE galArray[128];
  port->nRead(0x4406, 6, galArray);
  
  for(int i = 0; i < 6; i += 2)
  {
    unsigned int galValue = static_cast<unsigned int>(galArray[i]) * 0x100;
    galValue += static_cast<unsigned int>(galArray[i + 1]);

    int signedGal = galValue;
    if (signedGal > 0x7000)
    {
       signedGal -= 0x10000;
    }

   galSet[i / 2] = signedGal / 10.0;
  }
}
