///////////////////////////////////////////////////////////
//  I2C.h
//  Implementation of the Class I2C
//  Created on:      2016/01/08 11:00:25
//  Original author: 015150900
///////////////////////////////////////////////////////////

#if !defined(_BCL_I2C)
#define _BCL_I2C

#include <windows.h>

class I2C
{
protected:
	static int addressLen;
	/** それぞれのインスタンスの接続アドレス */
	int myCha;
	void makeAddr(unsigned int addr, BYTE *result);

public:

	virtual ~I2C() { }
	I2C(int ch);

    virtual void nRead(unsigned int addr, BYTE transBytes, char* result) = 0;
    virtual BYTE readByte(unsigned int addr);
    virtual WORD readWord(unsigned int addr);

    virtual void nWrite(unsigned int addr, BYTE transBytes, BYTE* data) = 0;
    virtual void writeByte(unsigned int addr, BYTE data);
    virtual void writeWord(unsigned int addr, WORD data);

};
#endif // !defined(_BCL_I2C)
