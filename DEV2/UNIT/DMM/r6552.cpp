// dmm controler for R6552

#include <stdlib.h>
#include <string.h>
#include <j:/dev2/unit/dmm/R6552.h>

R6552::R6552(ifLine *iface) : DMM(iface)
{  
   myIface->setDelim(ifLine::crlf + ifLine::eoi);
   setMode(DMM::DcVolt);
   myIface->talk("H1");
}

Unit::stat R6552::setMode(DMM::mode smode)
{   
    Unit::stat ret = Unit::normal;
    
    switch(smode)
    {  case DcVolt: myIface -> talk("F1R5"); break;
       case AcVolt: myIface -> talk("F2"); break;
       case DcAmp : myIface -> talk("F5R6"); break;
       case AcAmp : myIface -> talk("F6"); break;
       case Ohm2w : myIface -> talk("F3"); break;
       case Ohm4w : myIface -> talk("F4"); break;
                    
       default:     ret = Unit::error;
    }
    return ret;
 }
 
 double R6552::read()
 {  static char buff[128];
    unsigned int  i = 128;

    myIface -> listen(buff, i);
    return strtod(buff + 3, NULL);
 }

 R6552::~R6552()
 {
 }

