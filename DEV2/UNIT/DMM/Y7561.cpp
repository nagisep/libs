// dmm Y7561

#include <stdlib.h>
#include <j:/dev2/unit/dmm/y7561.h>

Y7561::Y7561(ifLine *iface) : DMM(iface)
{    char initParam[] = {'\x1b', 'R', '\0'};
	 myIface->setDelim(ifLine::crlf + ifLine::eoi);
	 if (myIface->ifType() == ifLine::rsline) myIface->talk(initParam);

	 setMode(DMM::DcVolt);
	 myIface->talk("M1");
	 myIface->talk("IT3");
	 myIface->talk("SI20");
	 myIface->talk("TD100");
	 myIface->talk("H1");
	 myIface->talk("DL0");
}


Unit::stat Y7561::setMode(DMM::mode smode)
{
	Unit::stat ret = Unit::normal;

	switch(smode)
	{  case DcVolt:myIface -> talk("F1/R0"); break;
	   case DcAmp :myIface -> talk("F5/R0"); break;
	   case Ohm2w :myIface -> talk("F3/R0"); break;
	   case Ohm4w :myIface -> talk("F4/R0"); break;

	   default:    ret = Unit::error;
	}
	return ret;
 }

 double Y7561::read()
 { static char  buff[128];
   char         getParam[] = { '\x1b', 'D', '\0'};
   unsigned int i = 128;

   if (myIface->ifType() == ifLine::rsline)  myIface->talk(getParam);

   myIface->talk("E");
   myIface->listen(buff, i);
   return strtod(buff + 4, NULL);
 }

Y7561::~Y7561(void)
{ char exitParam[] = {'\x1b', 'L', '\0'};
  if (myIface->ifType() == ifLine::rsline)
	  myIface->talk(exitParam);
}

