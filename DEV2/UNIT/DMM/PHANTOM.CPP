#include <dev\unit\unit.h>
#include <dev\unit\dmm\skeleton.h>
#include <dev\unit\dmm\phantom.h>

int DMMPhantom::SetMode(int mode)
{   int stat = 0;

    switch (mode)
    {  case DcVolt :
       case AcVolt :
       case DcAmp  :
       case AcAmp  :
       case Ohm2w  :
       case Ohm4w  :  Unit::SetMode(mode);
                      stat = 0;
                      break;
                      
       default     :  Unit::SetMode(-1);
                      stat = -1;
    }
    return stat;
}

