// dmm controler for HP34401a

#if !defined(_BCL_DMMHp34401a)
#define _BCL_DMMHp34401a

#include <j:/dev2/unit/dmm/skeleton.h>

class hp34401a : public DMM
{   
   public:

     hp34401a(ifLine *iface);
     ~hp34401a();


     Unit::stat      setMode(DMM::mode smode);
     double          read();

     char           *getName(void) { return "HP34401a"; }
};

#endif

