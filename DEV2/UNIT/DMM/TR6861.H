// dmm controler for Tr6861

#if !defined(_BCL_DMMTr6861)
#define      _BCL_DMMTr6861

#include <dev2\unit\dmm\skeleton.h>

class tr6861 : public DMM
{   
   public:

     tr6861(ifLine *iface);
	 Unit::stat     setMode(DMM::mode smode);
     double         read();
     char          *getName() { return "TR6861"; }
};


#endif

