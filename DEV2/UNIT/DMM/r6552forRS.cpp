// dmm controler for R6552

#include <windows.h>
#include <iostream>

#include <stdlib.h>
#include <string.h>
#include <j:/dev2/unit/dmm/R6552forRS.h>

R6552::R6552(ifLine *iface) : DMM(iface)
{  
   port = dynamic_cast<WinRS *>(myIface);

   while(port->loc()) static_cast<void>(port->getc1());
   port->setDelim(ifLine::crlf);
   setMode(DMM::DcVolt);
   port->talk("H1");
   Sleep(600);
   buffCheck();
}

Unit::stat R6552::setMode(DMM::mode smode)
{   
   Unit::stat ret = Unit::normal;
   while(port->loc()) static_cast<void>(port->getc1());

   switch(smode)
    {  case DcVolt: port -> talk("F1,R5"); break;
       case AcVolt: port -> talk("F2"); break;
       case DcAmp : port -> talk("F5,R6"); break;
       case AcAmp : port -> talk("F6"); break;
       case Ohm2w : port -> talk("F3"); break;
       case Ohm4w : port -> talk("F4"); break;
                    
       default:     ret = Unit::error;
    }
    buffCheck();
    Sleep(600);
    return ret;
 }
 
 double R6552::read()
 {  static char buff[128];
    unsigned int  i = 0;

   while(port->loc()) static_cast<void>(port->getc1());

    Sleep(600);
    port->talk("MD?");

    // MD? を投げる前に、バッファはからになっているので……
    // lf を待つ
    while(port->getc1() != 0x0a);
    // lf まで待つ
    while(1)
    {
      char c = port->getc1();

      if (c == 0x0d) break;
      buff[i++] = c;
    }
    // あとは、できる範囲で、クリア

   buffCheck();

   while(port->loc()) static_cast<void>(port->getc1());
    return strtod(buff + 3, NULL);
 }

 R6552::~R6552()
 {
 }

void R6552::buffCheck()
{
  char c;
  int  check = 0;

  while(1)
  {
    c = port->getc1();

    if (c == '=')  check += 1;
    if (c == '?')  check += 1;
    if (c == '>')  check += 2;
    if (check == 3) break;
  }
}

