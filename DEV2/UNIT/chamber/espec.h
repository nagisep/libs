#if !defined(_BCL_ESPEC)
#define      _BCL_ESPEC

#include <j:/dev2/unit/chamber/skeleton.h>

class ESPEC : public CHAMBER
{
   private:
     int eachAddress;

   public:
     ESPEC(ifLine *iface, int address = -1);
     ~ESPEC();

     Unit::stat writeTemp(double v);
     Unit::stat writeHumi(double v);

     double readTemp();
     double readHumi();

     char *getName() { return "CHAMBER Type ESPEC"; }
     double minStepWrite()   { return 0.1; }
     double minStepRead()    { return 0.1; }

};

#endif

