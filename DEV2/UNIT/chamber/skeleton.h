// 恒温槽を構成するためのスケルトン

#if !defined(_BCL_CHAMBER_Skeleton)
#define _BCL_CHAMBER_Skeleton

#include <j:\dev2\unit\unit.h>

class CHAMBER : public Unit {
   
   private:
     double tempTerget;    // 温度設定値
     double tempNow;       // 温度モニタ値
     double humiTerget;    // 湿度設定値
     double humiNow;       // 湿度モニタ値

     double tempOKError;   // これより差が狭ければ温度は設定値に到達したと判断する
     double humiOKError;   // これより差が狭ければ湿度は設定値に到達したと判断する

     int    preTime;       // 設定値に到達するまでの時間（isAntei() 呼び出し回数）
     int    postTime;      // 設定値に到達してからの時間（isAntei() 呼び出し回数）

     int    anteiOKTime;   // 安定と判断する時間（isAntei() 呼び出し回数）
     bool   nowAntei;      // 今、安定状態に入っている

   public:

    CHAMBER(ifLine *iface) : Unit(iface) 
    {
       tempTerget = 0;
       tempNow    = 0;
       humiTerget = 0;
       humiNow    = 0;

       tempOKError = 0.1;
       humiOKError = 2;
       preTime     = 0;
       postTime    = 0;
       anteiOKTime = 60;

       nowAntei    = false;
    }

    void setTempOKError(double v) { tempOKError = v; }
    void setHumiOKError(double v) { humiOKError = v; }
    void setAnteiOKTime(int    n) { anteiOKTime = n; }

    int  getPreTime()  { return preTime; }
    int  getPostTime() { return postTime; }

    double getTergetTemp() { return tempTerget; }
    double getTergetHumi() { return humiTerget; }


    virtual Unit::stat writeTemp(double v) = 0;
    virtual Unit::stat writeHumi(double v) = 0;

    virtual double readTemp() = 0;
    virtual double readHumi() = 0;

    void setTempHumi(double temp, double humi)
    {
       writeTemp(temp);
       writeHumi(humi);
       postTime = preTime = 0;
       tempTerget = temp;
       humiTerget = humi;
       nowAntei = false;
    } 

    bool isAntei()
    {

       if (! nowAntei) // まだ安定領域に入っていない
       {
         double temp = readTemp();
         double humi = readHumi();
         bool   check = true;
 
         // 温度の安定チェック
         if (temp < (tempTerget - tempOKError - 0.05)) check = false;
         if (temp > (tempTerget + tempOKError + 0.05)) check = false;
 
         // 湿度のチェック
         if (temp > 5) // 温度が５℃以下の場合には、湿度はチェックしない
         {
           if (humi < (humiTerget - humiOKError - 0.5)) check = false;
           if (humi > (humiTerget + humiOKError + 0.5)) check = false;
         }

         if(check) nowAntei = true;
       }

       if (nowAntei)
       {
         postTime ++;
         if (postTime >= anteiOKTime) return true;
       }
       else
       {
         preTime ++;
       }

       return false;
    }

    bool isAntei(double tempError, double humiError, int okTime)
    {
       tempOKError = tempError;
       humiOKError = humiError;
       anteiOKTime = okTime;
       return isAntei();
    }

    virtual char *getName(void) { return "Chamberl type = ??? "; }
};


#endif
