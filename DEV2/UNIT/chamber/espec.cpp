#include <j:/dev2/unit/chamber/ESPEC.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <windows.h>

ESPEC::ESPEC(ifLine *iface, int address) : CHAMBER(iface)
{
   char buff[64];
   unsigned int len = 64;
   eachAddress = address;

   if (eachAddress >= 0)
     std::sprintf(buff, "%02d,POWER,ON", eachAddress);
   else
     std::sprintf(buff, "POWER,ON");

   myIface->talk(buff);
   Sleep(1000);
   myIface->listen(buff, len); // レスポンスが帰ってくるため
}

ESPEC::~ESPEC()
 {

 }

Unit::stat ESPEC::writeTemp(double v)
{
  char buff[64];
  unsigned int len = 64;

  if (eachAddress >= 0)
    std::sprintf(buff, "%02d, TEMP,S%.1f", eachAddress, v);
  else
    std::sprintf(buff, "TEMP,S%.1f", v);

  myIface->talk(buff);
  Sleep(1000);
  myIface->listen(buff, len); // レスポンスが帰ってくるため
  Sleep(1000);
  return Unit::normal;
}

Unit::stat ESPEC::writeHumi(double v)
{
  char buff[64];
  unsigned int len = 64;


  if (eachAddress >= 0)
    std::sprintf(buff, "%02d, HUMI,S%.1f", eachAddress, v);
  else
    std::sprintf(buff, "HUMI,S%.1f", v);

  myIface->talk(buff);
  Sleep(1000);
  myIface->listen(buff, len); // レスポンスが帰ってくるため
  Sleep(1000);
  return Unit::normal;
}

double ESPEC::readTemp()
{
  char buff[128];
  unsigned int len = 128;

  if (eachAddress >= 0)
    std::sprintf(buff, "%02d, TEMP?", eachAddress);
  else
    std::sprintf(buff, "TEMP?");

  myIface->talk(buff);
  Sleep(1000);
  myIface->listen(buff, len);
  Sleep(1000);

  char *p = std::strchr(buff, ',');
  if (p) *p = '\0';

  return std::strtod(buff, 0);
}

double ESPEC::readHumi()
{
  char buff[128];
  unsigned int len = 128;

  if (eachAddress >= 0)
    std::sprintf(buff, "%02d, HUMI?", eachAddress);
  else
    std::sprintf(buff, "HUMI?");

  myIface->talk(buff);
  Sleep(1000);
  myIface->listen(buff, len);
  Sleep(1000);

  char *p = std::strchr(buff, ',');
  if (p) *p = '\0';

  return std::strtod(buff, 0);
}

