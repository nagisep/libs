#include <stdio.h>
#include <j:/dev2/unit/power/R6161.h>

R6161::R6161(ifLine *iface) : Power(iface)
{  
   write(0);
   outOff();
}

Unit::stat R6161::setMode(Power::mode smode) { return Unit::normal;}


Unit::stat   R6161::write(double volt)
{   char buff[80];
	sprintf(buff, "D%05.1fV", volt);
	myIface->talk(buff);
	return Unit::normal;
}

Unit::stat    R6161::outOn(void)  { myIface->talk("OP, E"); return Unit::normal; }
Unit::stat    R6161::outOff(void) { myIface->talk("SB, H"); return Unit::normal; }

R6161::~R6161(void)
{     // �o�͂O�C������
     write(0);
     outOff();
}

