#include <stdio.h>
#include <windows.h>
#include <j:\dev2\unit\power\psp2010.h>

PSP2010::PSP2010(ifLine *iface) : Power(iface)
{  
   write(0);
   outOff();
}

Unit::stat PSP2010::setMode(Power::mode smode) { return Unit::normal;}


Unit::stat   PSP2010::write(double volt)
{   char buff[80];
	sprintf(buff, "SV %05.2f", volt);
	myIface->talk(buff);
	return Unit::normal;
}

Unit::stat    PSP2010::outOn(void)  { myIface->talk("KOE"); return Unit::normal; }
Unit::stat    PSP2010::outOff(void) { myIface->talk("KOD");    Sleep(3000); return Unit::normal; }
// outOff のあと、すぐには動作できない感じなので、Sleep を入れてみた。

PSP2010::~PSP2010(void)
{     // 出力０，ｏｆｆ
     write(0);
     outOff();
}
   