// for R6161 （6161mode で使用のこと）

#if !defined(_BCL_PowerE3640A)
#define _BCL_PowerE3640A

#include <j:/dev2/unit/power/skeleton.h>

class R6161 : public Power {

   public:
   
      R6161(ifLine *iface);
      ~R6161();

	  double        minStepWrite() { return 0.005; }
	  Unit::stat    setMode(Power::mode smode);
	  Unit::stat    write(double volt);
      
      char  *getName(void) {return "Pwore Controler Type R6161"; }
      Unit::stat   outOn(void);
      Unit::stat   outOff(void);
      
};

#endif

