// for hp6632a

#if !defined(_BCL_PowerPSP2010)
#define _BCL_PowerPSP2010

#include <j:\dev2\unit\power\skeleton.h>

class PSP2010 : public Power {

   public:
   
      PSP2010(ifLine *iface);
      ~PSP2010();

	  double        minStepWrite() { return 0.01; }
	  Unit::stat    setMode(Power::mode smode);
	  Unit::stat    write(double volt);
      
      char  *getName(void) {return "Pwore Controler Type PSP2010"; }
      Unit::stat   outOn(void);
      Unit::stat   outOff(void);
      
};

#endif

