// for E3640A

#if !defined(_BCL_PowerE3640A)
#define _BCL_PowerE3640A

#include <j:/dev2/unit/power/skeleton.h>

class E3640A : public Power {

   public:
   
      E3640A(ifLine *iface);
      ~E3640A();

	  double        minStepWrite() { return 0.005; }
	  Unit::stat    setMode(Power::mode smode);
	  Unit::stat    write(double volt);
      
      char  *getName(void) {return "Pwore Controler Type E3640A"; }
      Unit::stat   outOn(void);
      Unit::stat   outOff(void);
      
};

#endif

