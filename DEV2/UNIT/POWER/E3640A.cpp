#include <stdio.h>
#include <j:/dev2/unit/power/E3640A.h>

E3640A::E3640A(ifLine *iface) : Power(iface)
{  
   write(0);
   outOff();
}

Unit::stat E3640A::setMode(Power::mode smode) { return Unit::normal;}


Unit::stat   E3640A::write(double volt)
{   char buff[80];
	sprintf(buff, "APPLY %f", volt);
	myIface->talk(buff);
	return Unit::normal;
}

Unit::stat    E3640A::outOn(void)  { myIface->talk("OUTPUT ON"); return Unit::normal; }
Unit::stat    E3640A::outOff(void) { myIface->talk("OUTPUT OFF"); return Unit::normal; }

E3640A::~E3640A(void)
{     // �o�͂O�C������
     write(0);
     outOff();
}

