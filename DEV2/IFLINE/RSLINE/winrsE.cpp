
#include "winrsE.h"
#include "string.h"
#include <stdio.h>
#include <windows.h>

int  WinRS::HowMany = 0;

WinRS::WinRS(int addr, int bps, ifLine::delim delim) : ifLine(addr, rsline,  delim)
{
   myBps = bps;
   if (HowMany == 0) init();
   HowMany++;
}


int WinRS::init()
{

    char paraBuff[256];

    sprintf(paraBuff, "COM%1d", myAddr);
    handle = CreateFile(paraBuff,
    GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,
    NULL);
    SetupComm(handle,BUFFERSIZE,BUFFERSIZE);
    GetCommState(handle, &dcb1);
    GetCommTimeouts(handle, &timeout1);

    sprintf (paraBuff, "baud=%d parity=E data=8 stop=1", myBps);
    BuildCommDCB(paraBuff, &dcb1);

    dcb1.fRtsControl = RTS_CONTROL_HANDSHAKE;
	dcb1.fOutxDsrFlow = FALSE;
	dcb1.fDsrSensitivity = FALSE;
    dcb1.fAbortOnError = FALSE;

    timeout1.WriteTotalTimeoutConstant = 0;
    timeout1.WriteTotalTimeoutMultiplier = TIMEOUT_MUL;
    timeout1.ReadIntervalTimeout = MAXDWORD;
    timeout1.ReadTotalTimeoutConstant = 0;
    timeout1.ReadTotalTimeoutMultiplier = TIMEOUT_MUL;

    SetCommState(handle,&dcb1);
    SetCommTimeouts(handle,&timeout1);
    return 0;
}

unsigned int WinRS::loc()
{
  ClearCommError(handle, &ErrorMask, &comstat1);
  return comstat1.cbInQue;
}

char WinRS::getc1()
{
  char rBuff;
  unsigned long len;

  ClearCommError(handle, &ErrorMask, &comstat1);

  len = 1;
  ReadFile(handle, &rBuff, len, &len, NULL);
  return rBuff;
}

ifLine::stat WinRS::listen(char *buff, unsigned int &len)
{
  char rBuff;
  int  is_ready = 0;
  int  rLen = 0;

  switch (myDelim & 7)
  {  case  crlf  : is_ready = 0; break;
     case  cr    : is_ready = 2; break; // already get lf
     case  lf    : is_ready = 1; break; // already get cr
  }

  ClearCommError(handle, &ErrorMask, &comstat1);

  while (is_ready != 3)
  {
    rBuff = getc1();
    if      ( rBuff == '\x0d') is_ready |= 1;
    else if ( rBuff == '\x0a') is_ready |= 2;
    else if ( rLen >= (len - 1) ) continue;
    else
    {  rLen ++;
       *(buff++) = rBuff;
    }
  }
  *buff = '\0';
  len = rLen + 1;
  return ifLine::normalEnd;

}



ifLine::stat WinRS::talk(char *mess)
{
  unsigned long dLen;
  dLen = strlen(mess);
  WriteFile(handle, mess, strlen(mess),&dLen, NULL);

  switch (myDelim & 7)
  {  case crlf : putc1('\x0d');
                 putc1('\x0a');
                 break;

     case cr   : putc1('\x0d');
		 break;

     case lf   : putc1('\x0a');
		 break;
   }

  Sleep(100);

   return ifLine::normalEnd;
}

ifLine::stat WinRS::putc1(char c)
{
  unsigned long  dLen;
  char wBuff[16];
  wBuff[0] = c;
  wBuff[1] = '\0';
  WriteFile(handle, wBuff, strlen(wBuff),&dLen, NULL);
  return ifLine::normalEnd;
}

WinRS::~WinRS()
{

 if ( --HowMany == 0)
 {
    PurgeComm(handle, PURGE_RXCLEAR);
    PurgeComm(handle, PURGE_TXCLEAR);
    PurgeComm(handle, PURGE_RXABORT);
    PurgeComm(handle, PURGE_TXABORT);
    GetCommState(handle, &dcb1);
    dcb1.fOutxCtsFlow = FALSE;
    dcb1.fRtsControl = RTS_CONTROL_DISABLE;
    dcb1.fAbortOnError = TRUE;
    dcb1.fOutxDsrFlow = FALSE;
    SetCommState(handle, &dcb1);
    CloseHandle(handle);
  }
}


