#include "WinRSM.h"

#include <iostream>
#include <iomanip>

unsigned char toHex(char c)
{
  if ((c >= '0') && (c <= '9')) return c - '0';
  if ((c >= 'A') && (c <= 'F')) return c - 'A' + 10;
  return 0;
}

WinRSM::WinRSM(int addr, int bps, ifLine::delim delim, char *mode)
        : WinRS(addr, bps, delim, mode)
{
}

ifLine::stat WinRSM::talk(char *message)
{
   unsigned char buff[1024];
   unsigned char bcc = 0;
   int i;
   int j;

   unsigned long dLen;

   buff[0] = '\x02';
   buff[1] = 0;
   for(i = 0, j = 2; message[i] != '\0'; i += 2, j++)
   {
     buff[j] = toHex(message[i]) * 0x10 + toHex(message[i + 1]);
     bcc ^= buff[j];
   }
   buff[1] = i / 2 + 1;
   buff[j] = bcc ^ buff[1];

   dLen = j + 1;
   WriteFile(handle, buff, dLen, &dLen, NULL);

   return ifLine::normalEnd;
}

ifLine::stat WinRSM::listen(char *message, unsigned int &len)
{
  unsigned char c;
  unsigned char c1;
  int i;
  int j;


while(getc1() != '\x02');

c1 = getc1();

for(i = 0, j = 0; i < c1; i++, j += 2)
{
      c = getc1();
      message[j] = "0123456789ABCDEF"[c / 0x10];
      message[j+1] = "0123456789ABCDEF"[c % 0x10];
}

   message[j-2] = '\0';
   len = j-1;
   return ifLine::normalEnd;
}

WinRSM::~WinRSM() { }