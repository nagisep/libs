#if !defined (_BCL_RsLine_WinRSM)
#define _BCL_RsLine_WinRsM

#include <J:\dev2\IFLINE\RSLINE\winrs.h>

class WinRSM : public WinRS
{
   public:
    	  WinRSM(int addr = 1, int bps = 9600, ifLine::delim delim = ifLine::crlf, char *mode = "8N1");
	      // mode means  "(bit size(7 or 8), parity(E or O or N), stop (1 or 2)
     	  ifLine::stat talk(char *message);
     	  ifLine::stat listen(char *message, unsigned int &len);
          ~WinRSM();
};

#endif
