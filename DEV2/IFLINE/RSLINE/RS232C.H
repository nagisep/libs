#if !defined (_BCL_Rs232c)
#define _BCL_Rs232c

#include <dev2\ifline\ifline.h>
class rsLine : public ifLine
{
   private:
	 static char BuffRec[256];
	 static char BuffSend[256];
	 int    init();
	 static int  HowMany;

   public :
	  rsLine(int addr, ifLine::delim delim = ifLine::crlf);

	  ifLine::stat  talk(char *message);
	  ifLine::stat  listen(char *message, unsigned int &len);
	  ifLine::stat  setDelim(ifLine::delim delim);
	 ~rsLine();

};

#endif


