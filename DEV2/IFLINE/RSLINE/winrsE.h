#if !defined (_BCL_RsLine_WinRS)
#define _BCL_RsLine_WinRs

#include <windows.h>
#include <winbase.h>

#include <J:\DEV2\IFLINE\IFLINE.H>

#define TIMEOUT_MUL 10
#define BUFFERSIZE  256

class WinRS : public ifLine
{
   private:
	 int    init();
	 int    myBps;
	 static int  HowMany;

         HANDLE handle; 
         _DCB   dcb1;
         _COMSTAT comstat1;
         _COMMTIMEOUTS timeout1;

         unsigned long ErrorMask;

   public :
	  WinRS(int addr = 1, int bps = 9600, ifLine::delim delim = ifLine::crlf);

	  ifLine::stat talk(char *message);
          ifLine::stat putc1(char c);
	  ifLine::stat listen(char *message, unsigned int &len);
	  ifLine::stat setDelim(ifLine::delim delim)
	  {
	    myDelim = delim;
	    return ifLine::normalEnd;
	  }
	  unsigned int loc();
	  char         getc1();
	  
	 ~WinRS();

};

#endif

