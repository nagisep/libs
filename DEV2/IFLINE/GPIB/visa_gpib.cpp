
// gpib deamon

#include <cstring>
#include <cstdio>
#include <u:/dev2/ifline/gpib/visa_gpib.h>
//#include <winbase.h>

//#include <windows.h>

// visa_gpib support fuctions

int  visa_gpib::HowManyGpib = 0;
int  visa_gpib::initStat    = 0;
ViSession visa_gpib::defaultRM;


int  visa_gpib::buffAddr[];
char visa_gpib::buffData[];

int visa_gpib::init()
{
   viOpenDefaultRM(&defaultRM);
   return 1;
}

void visa_gpib::hwait()
{
  // Sleep(80);
}

visa_gpib::visa_gpib(int addr, ifLine::delim sdelim) : ifLine(addr,gpib, sdelim)
{
   char buff[64];

   if ( (HowManyGpib == 0) && ((initStat = init()) == 0));
   HowManyGpib++;
   myType = ifLine::gpib;
   setSpeed(fast);

   std::sprintf(buff, "GPIB0::%d::INSTR", addr);
   viOpen(defaultRM, buff, VI_NULL, VI_NULL, &vi);
}

ifLine::stat visa_gpib::talk(char *message)
{
  viPrintf(vi, "%s\n", message);
  hwait();
  return ifLine::normalEnd;
}


ifLine::stat visa_gpib::listen(char *message, unsigned int &len)
{

  viScanf(vi, "%t", message);
  len = std::strlen(message);
  return ifLine::normalEnd;
}


ifLine::stat visa_gpib::setDelim(ifLine::delim delim)
{  myDelim = delim;

   return  ifLine::normalEnd;
}

 visa_gpib::~visa_gpib()
{
   HowManyGpib--;
 //  viClose(vi);
 //  if (HowManyGpib == 0) viClose(defaultRM);
}


