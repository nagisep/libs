#include <dev2\ifline\gpib\gpib98dv.h>
#define Uses_MsgBox
#include <tvision\tv.h>

void gpib98dv::dispErrorMessage(char *func)
{  switch(doStat)
   {  case normalEnd : break;
	  case timeOver  : messageBox(mfError | mfOKButton,
					   "GPIB の通信が失敗しました\n"
					   "address = %d\n(function = %s[time over])",
					   myAddr, func);
					   break;
	  case illegalCommand:
					   messageBox(mfError | mfOKButton,
					   "GPIB の通信が失敗しました\n"
					   "address = %d\n(function = %s[command error])",
					   myAddr, func);
					   break;
	  case messageFull:
					   messageBox(mfError | mfOKButton,
					   "GPIB の通信が失敗しました\n"
					   "address = %d\n(function = %s[messag full])",
					   myAddr, func);
					   break;

	  default        : messageBox(mfError | mfOKButton,
					   "GPIB の通信が失敗しました\n"
					   "address = %d\n(function = %s[code = %d])",
					   myAddr, func, doStat);
					   break;
   }
}

gpib98dv::gpib98dv(int addr, ifLine::delim delim)
		 :gpib98d(addr, delim)
{  if (initStat != 0)
   {  messageBox(mfError | mfOKButton, "GPIB の開設に失敗しました\n"
								  "速やかにシステムを終了させてください");
	  doStat = cannotUse;
   }
}

ifLine::stat gpib98dv::talk(char *message)
{  if ( (doStat == cannotUse) ||
		((doStat = gpib98d::talk(message)) != ifLine::normalEnd))
	  dispErrorMessage("TALK");
   return doStat;
}

ifLine::stat gpib98dv::listen(char *message, unsigned int &len)
{  if ( (doStat == cannotUse) ||
		((doStat = gpib98d::listen(message, len)) != ifLine::normalEnd))
	  dispErrorMessage("LISTEN");
   return doStat;
}


