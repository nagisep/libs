#include <windows.h>
#define GPIB_MAX_ADDR 30			// GPIBアドレスの上限値
#define TRUE 1
#define FALSE 0
//=================================< エラー番号(戻り値) >==================================//

#define FAIL 0						// 内容不明のエラー
#define SUCCESS 1					// 正常終了

#define UNEXPECTED_ERROR 1			// 予期しないエラー
#define ARGMENT_INVALID 2			// 引数が不正
#define OUT_OF_MEMORY 3				// メモリ不足
#define NO_DEVICE 4					// 指定のデバイスは存在しません
#define CANT_USE_DEVICE 5			// 指定されているデバイスは使用できません
#define SRQ_CONTROLER_INACTIVE 6	// コントローラとしてアクティブな状態にあります
#define SRQ_NOT_REMOTE 7			// リモート状態にありません
#define SRQ_SET_PENDING 8			// SRQの送出待ちです
#define SRQ_SET_TIMEOUT 9			// SRQの送出タイムアウト
#define GPIB_TRANS_TIMEOUT 10		// GPIBバスデータ送信タイムアウト
#define GPIB_RCV_TIMEOUT 11			// GPIBバスデータ受信タイムアウト

#define USB_TRANS_TOOBIG -1			// 送信バッファーサイズを超えるデータ
#define USB_TRANS_NODEVICE -2		// 送信デバイスがない
#define USB_TRANS_TIMEOUT -3		// 送信タイムアウト
#define USB_RCV_TOOBIG -4			// 受信バッファーサイズを超えるデータ
#define USB_RCV_NODEVICE -5			// 受信デバイスがない
#define USB_RCV_TIMEOUT -6			// 受信タイムアウト

//=================================< UV-10制御関数の定義 >=================================//

extern "C" int GetErrorCode(void);												// 制御関数実行時のｴﾗｰｺｰﾄﾞを返します
extern "C" int GpibDeviceSelect(BYTE dn);										// UV10を選択します									
extern "C" int GetInfomation(BYTE info, LPSTR dat);								// UV10の情報を読取ります

extern "C" int GpibDeviceClear(void);											// ﾃﾞﾊﾞｲｽｸﾘｱ
extern "C" int GpibDeviceTrigger(BYTE addr);									// ﾃﾞﾊﾞｲｽﾄﾘｶﾞｰ
extern "C" int GpibGetAddressMode(BYTE *mode);									// UV-10のアドレスに関する状態を返します
extern "C" int GpibGotoLocal(BYTE addr);										// ﾛｰｶﾙﾓｰﾄﾞへの移行
extern "C" int GpibInterfaceClear(void);										// ｲﾝﾀｰﾌｪｰｽｸﾘｱ
extern "C" int GpibLocalLockOut(void);											// ﾛｰｶﾙｱｸｾｽを遮断
extern "C" int GpibSystemControler(BYTE sel);									// ｼｽﾃﾑｺﾝﾄﾛｰﾗON/OFF
extern "C" int GpibPassControl(BYTE addr);										// ｺﾝﾛｰﾗ権を渡す
extern "C" int GpibNoListener(void);											// 全ﾘｽﾅｰ指定解除
extern "C" int GpibNoTalker(void);												// 全ﾄｰｶｰ指定解除
extern "C" int GetGpibMyAddress(void);											// UV10のGPIBｱﾄﾞﾚｽを得る
extern "C" int SetGpibMyAddress(BYTE myaddr);									// UV10のGPIBｱﾄﾞﾚｽを指定
extern "C" int GpibRemoteEnable(void);											// ﾘﾓｰﾄ有功
extern "C" int GpibSelectDeviceClear(BYTE addr);								// ｾﾚｸﾄﾃﾞﾊﾞｲｽｸﾘｱ
extern "C" int GpibSetReceiveDelimiter(BYTE delimiter = 0);						// 受信ﾃﾞﾘﾐﾀｰの指定
extern "C" int GpibSetSendDelimiter(LPSTR delimiter = "");						// 送信ﾃﾞﾘﾐﾀｰの指定
extern "C" int GpibSetListener(BYTE addr, BOOL ul = TRUE);						// ﾘｽﾅｰの指定
extern "C" int GpibSetTalker(BYTE addr);										// ﾄｰｶｰの指定
extern "C" int GpibSerialPoll(BYTE addr, BYTE *stb);							// ｼﾘｱﾙﾎﾟｰﾘﾝｸﾞの実施
extern "C" int GpibParallelPoll(BYTE *stb);										// ﾊﾟﾗﾚﾙﾎﾟｰﾘﾝｸﾞの実施
extern "C" int GpibSetSRQStatus(BYTE stb, BYTE pend, DWORD time = 2000);		// SRQの発行
extern "C" int GpibTimeOut(DWORD time);											// GPIBﾊﾞｽｱｸｾｽﾀｲﾑｱｳﾄ時間の設定

// データの送信
extern "C" int GpibSendString(BYTE addr, LPCSTR str, BYTE wait = TRUE);			// ﾘｽﾅｰ指定付き文字列の送信
extern "C" int GpibSendData(BYTE addr, BYTE *data, DWORD len, BYTE wait = TRUE);// ﾘｽﾅｰ指定付きﾃﾞｰﾀの送信

// データの受信
extern "C" int GpibReceiveString(BYTE addr, LPSTR buf, DWORD len);				// ﾄｰｶｰ指定付き文字列の受信
extern "C" int GpibReceiveData(BYTE addr, BYTE *data, DWORD len);				// ﾄｰｶｰ指定付きﾃﾞｰﾀの受信
