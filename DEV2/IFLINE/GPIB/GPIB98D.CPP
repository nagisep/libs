// gpib deamon

#include <string.h>
#include <dev2\ifline\gpib\gpib98d.h>

// gpib98d support fuctions
extern "C" int  g_ini(int, int, int, unsigned int);
extern "C" int  g_ifc(int);
extern "C" int  g_ren(void);
extern "C" int  g_resetren(void);
extern "C" int  g_talk(int *, unsigned int, char *);
extern "C" int  g_listen(int *, unsigned int *, char *);
extern "C" int  g_poll(int *, int *);
extern "C" int  g_srq(int);
extern "C" int  g_stb(int);
extern "C" int  g_delim(int, int);
extern "C" int  g_timeout(int);
extern "C" int  g_chkstb(int *, int *);
extern "C" int  g_readreg(int, int *);
extern "C" int  g_dma(int, int);
extern "C" int  g_exit(void);
extern "C" int  g_comand(int *);
extern "C" int  g_dmainuse(void);

int  gpib98d::HowManyGpib = 0;
int  gpib98d::initStat    = 0;
int  gpib98d::buffAddr[];
char gpib98d::buffData[];

int gpib98d::init()
{   int p  = g_ini(0, 1, 0, 0x02d0);
	hwait();
	p += g_ifc(100);
	hwait();
	p += g_ren();
	hwait();
	return p;
}

void gpib98d::hwait()
{  int i, w;
   switch(hWait)
   {  case ifLine::fast   : w =    10; break;
	  case ifLine::middle : w =   100; break;
	  case ifLine::slow   : w =  1000; break;
   }
   for (i = 0; i < w; i++, outp(0x005f, 0));
}

gpib98d::gpib98d(int addr, ifLine::delim sdelim) : ifLine(addr,gpib, sdelim)
{  if ( (HowManyGpib == 0) && ((initStat = init()) == 0));
   HowManyGpib++;
   myType = ifLine::gpib;
   setSpeed(fast);
}

ifLine::stat gpib98d::talk(char *message)
{
  int stat;
  int len = strlen(message);
  setDelim(myDelim);
  buffAddr[0] = 2;                    // 1 listen + 1 talk
  buffAddr[1] = 0;					  // Talker == 0
  buffAddr[2] = myAddr;
  strcpy(buffData, message);
  stat = g_talk(buffAddr, len, buffData);
  hwait();
  return (stat == 0) ? ifLine::normalEnd : ifLine::error;
}

ifLine::stat gpib98d::listen(char *message, unsigned int &len)
{ ifLine::stat stat;

  setDelim(myDelim);
  buffAddr[0] = 2;
  buffAddr[1] = myAddr;
  buffAddr[2] = 0;
  int p = g_listen(buffAddr, &len, buffData);
  hwait();
  strncpy(message, buffData, len);
  message[len] = '\0';

  switch (p)
  {  case   2:
	 case   1:
	 case   0:stat = normalEnd; break;
	 case 128:stat = messageFull; break;
	 case 254:stat = timeOver; break;
	 default :stat = error;
   }
   return stat;
}

ifLine::stat gpib98d::setDelim(ifLine::delim delim)
{  myDelim = delim;
   int is_eoi = (delim >= 16) ? 1 : 0;
   int delim_stat;
   switch(myDelim & 7)
   {  case ifLine::cr : delim_stat = 2; break;
	  case ifLine::lf : delim_stat = 3; break;
	  case ifLine::cr+ifLine::lf : delim_stat = 1; break;
   }
   int p = g_delim(delim_stat, is_eoi);
   hwait();
   return (p == 0) ? ifLine::normalEnd : ifLine::error;
}

 gpib98d::~gpib98d()
{  int p = 0;
   HowManyGpib--;
   if (HowManyGpib == 0)
   { p  = g_resetren();
	 hwait();
	 p += g_exit();
	 hwait();
   }
}

