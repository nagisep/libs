
#if !defined (_BCL_ifLine_VISA_GPIB)
#define       _BCL_ifLine_VISA_GPIB

#include <u:/dev2/ifline/ifline.h>
#include <u:/DEV2/IFLINE/GPIB/visa.h>
#include <conio.h>
#include <string.h>

// gpib demon

class visa_gpib :public ifLine
{
  protected:

	int         init();
	void        hwait();

	static int  initStat;
	static int  HowManyGpib;
	static int  buffAddr[8];
	static char buffData[128];
	static int  init(int MyAddr);

    static ViSession defaultRM;
    ViSession vi;

  public:
	visa_gpib(int addr, ifLine::delim sdelim = crlf);
	virtual ifLine::stat  talk(char *message);
	virtual ifLine::stat  listen(char *message, unsigned int &len);
	virtual ifLine::stat  setDelim(ifLine::delim delim);
	virtual ~visa_gpib();
};


#endif

