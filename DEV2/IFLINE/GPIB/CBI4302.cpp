// gpib deamon

#include <string.h>
#include <j:/dev2/ifline/gpib/cbi4302.h>
#include "GPC43042.H"

int  cbi4302::HowManyGpib = 0;
int  cbi4302::initStat    = 0;
long cbi4302::buffAddr[];
char cbi4302::buffData[];

int cbi4302::init()
{   int p  = GpibOpen(0);
	p += GpibSetIfc(0);
	p += GpibSetRen(0);
	return p;
}

void cbi4302::hwait()
{  int i, w;
   switch(hWait)
   {  case ifLine::fast   : w =    1000; break;
	  case ifLine::middle : w =   10000; break;
	  case ifLine::slow   : w =  100000; break;
   }
   for (i = 0; i < w; i++);
}

cbi4302::cbi4302(int addr, ifLine::delim sdelim) : ifLine(addr,gpib, sdelim)
{  if ( (HowManyGpib == 0) && ((initStat = init()) == 0));
   HowManyGpib++;
   myType = ifLine::gpib;
   setSpeed(fast);
   setDelim(sdelim);
}

ifLine::stat cbi4302::talk(char *message)
{
  int stat;
  int len = strlen(message);
  setDelim(myDelim);
  buffAddr[0] = myAddr;               // send address
  buffAddr[1] = -1;					  // Talker == 0
  strcpy(buffData, message);
  stat = GpibSend(0, buffAddr, len, buffData);
  return (stat == 0) ? ifLine::normalEnd : ifLine::error;
}

ifLine::stat cbi4302::listen(char *message, unsigned int &len)
{
  unsigned long lLen = static_cast<unsigned long>(len);

  setDelim(myDelim);
  buffAddr[0] = myAddr;
  buffAddr[1] = -1;
  int p = GpibReceive(0, buffAddr, &lLen, buffData);
  len = static_cast<int>(len);
  strncpy(message, buffData, len);
  message[len] = '\0';
  return (p == 0) ? ifLine::normalEnd : ifLine::error;
}

ifLine::stat cbi4302::setDelim(ifLine::delim delim)
{ 
    myDelim = delim;
    int p = 0;

   if (delim >= 16) // EOI 設定あり
     switch(myDelim & 7)
     {  case ifLine::cr : 
                          p = GpibSetConfig(0, "/SDELIM=CR+EOI");
                          p = GpibSetConfig(0, "/RDELIM=CR+EOI");
                          break;
	    case ifLine::lf : 
                          p = GpibSetConfig(0, "/SDELIM=LF+EOI");
                          p = GpibSetConfig(0, "/RDELIM=LF+EOI");
                          break;
	    case ifLine::cr+ifLine::lf : 
                          p = GpibSetConfig(0, "/SDELIM=CRLF+EOI");
                          p = GpibSetConfig(0, "/RDELIM=CRLF+EOI");
                          break;
     }
   else
     switch(myDelim & 7)
     {  case ifLine::cr : 
                          p = GpibSetConfig(0, "/SDELIM=CR");
                          p = GpibSetConfig(0, "/RDELIM=CR");
                          break;
	    case ifLine::lf : 
                          p = GpibSetConfig(0, "/SDELIM=LF");
                          p = GpibSetConfig(0, "/RDELIM=LF");
                          break;
	    case ifLine::cr+ifLine::lf : 
                          p = GpibSetConfig(0, "/SDELIM=CRLF");
                          p = GpibSetConfig(0, "/RDELIM=CRLF");
                          break;
     }
   return (p == 0) ? ifLine::normalEnd : ifLine::error;
}

 cbi4302::~cbi4302()
{  int p = 0;
   return;
   // delete で例外が起こるため、とりあえず、キャンセル


   HowManyGpib--;
   if (HowManyGpib == 0)
     GpibClose(0);
}

