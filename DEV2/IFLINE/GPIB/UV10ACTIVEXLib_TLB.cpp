// ************************************************************************ //
//  警告                                                                    //
// -------                                                                  //
// このファイルで定義されている型宣言はタイプライブラリから読み出した       //
// 値を元に作成されています。タイプライブラリエディタで作業中に [ソー       //
// スコードの更新] ボタンを押したときや，ほかのタイプライブラリが直接       //
// または間接にこのタイプライブラリを参照している場合に，このファイル       //
// はタイプライブラリの値を元に再生成されます。                             //
// この場合，このファイル自身に対する変更は失われてしまいます。             //
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.96.1.40  $
// このファイルは以下のタイプライブラリから 00/02/04 15:25:09 に生成されました。

// ************************************************************************ //
// Type Lib: C:\BCB\USER\DEV2\IFLINE\GPIB\UV10.OCX
// IID\LCID: {2F8E04F3-17B0-11D2-BB1D-0080C8638894}\0
// Helpfile: C:\BCB\USER\DEV2\IFLINE\GPIB\UV10ActiveX.hlp
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\SYSTEM\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINDOWS\SYSTEM\STDVCL40.DLL)
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#include "UV10ACTIVEXLib_TLB.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Uv10activexlib_tlb
{


// *********************************************************************//
// GUIDS declared in the TypeLibrary                                      
// *********************************************************************//
const GUID LIBID_UV10ACTIVEXLib = {0x2F8E04F3, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} };
const GUID DIID__DUV10 = {0x2F8E04F4, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} };
const GUID DIID__DUV10Events = {0x2F8E04F5, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} };
const GUID CLSID_UV10 = {0x2F8E04F6, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} };
};     // namespace Uv10activexlib_tlb
