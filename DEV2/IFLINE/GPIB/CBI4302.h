
#if !defined (_BCL_ifLine_CBI4302)
#define       _BCL_ifLine_CBI4302

#include <j:/dev2/ifline/ifline.h>

// gpib demon

class cbi4302 :public ifLine
{
  protected:

	int         init();
	void        hwait();

	static int  initStat;
	static int  HowManyGpib;
	static long buffAddr[8];
	static char buffData[128];
	static int  init(int MyAddr);

  public:
	cbi4302(int addr, ifLine::delim sdelim = crlf);

	virtual ifLine::stat  talk(char *message);
	virtual ifLine::stat  listen(char *message, unsigned int &len);
	virtual ifLine::stat  setDelim(ifLine::delim delim);
	virtual ~cbi4302();
};


#endif

