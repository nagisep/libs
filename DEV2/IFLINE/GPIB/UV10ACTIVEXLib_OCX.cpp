// ************************************************************************ //
//  警告                                                                    //
// -------                                                                  //
// このファイルで定義されている型宣言はタイプライブラリから読み出した       //
// 値を元に作成されています。タイプライブラリエディタで作業中に [ソー       //
// スコードの更新] ボタンを押したときや，ほかのタイプライブラリが直接       //
// または間接にこのタイプライブラリを参照している場合に，このファイル       //
// はタイプライブラリの値を元に再生成されます。                             //
// この場合，このファイル自身に対する変更は失われてしまいます。             //
// ************************************************************************ //

// C++ TLBWRTR : $Revision:   1.96.1.40  $
// このファイルは以下のタイプライブラリから 00/02/04 15:25:09 に生成されました。

// ************************************************************************ //
// Type Lib: C:\BCB\USER\DEV2\IFLINE\GPIB\UV10.OCX
// IID\LCID: {2F8E04F3-17B0-11D2-BB1D-0080C8638894}\0
// Helpfile: C:\BCB\USER\DEV2\IFLINE\GPIB\UV10ActiveX.hlp
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\SYSTEM\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINDOWS\SYSTEM\STDVCL40.DLL)
// ************************************************************************ //

#include <vcl.h>
#pragma hdrstop

#if defined(USING_ATL)
#include <atl\atlvcl.h>
#endif

#include "UV10ACTIVEXLib_OCX.h"

#if !defined(__PRAGMA_PACKAGE_SMART_INIT)
#define      __PRAGMA_PACKAGE_SMART_INIT
#pragma package(smart_init)
#endif

namespace Uv10activexlib_tlb
{



// *********************************************************************//
// OCX PROXY CLASS IMPLEMENTATION
// (The following variables/methods implement the class TUV10 which
// allows "UV10 Control" to be hosted in CBuilder IDE/apps).
// *********************************************************************//
int   TUV10::EventDispIDs[1] = {
    0x00000001};

TControlData TUV10::CControlData =
{
  // GUID of CoClass and Event Interface of Control
  {0x2F8E04F6, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} }, // CoClass
  {0x2F8E04F5, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} }, // Events

  // Count of Events and array of their DISPIDs
  1, &EventDispIDs,

  // Pointer to Runtime License string
  NULL,  // HRESULT(0x80004005)

  // Flags for OnChanged PropertyNotification
  0x00000000,
  300,// (IDE Version)

  // Count of Font Prop and array of their DISPIDs
  0, NULL,

  // Count of Pict Prop and array of their DISPIDs
  0, NULL,
  0, // Reserved
  0, // Instance count (used internally)
  0, // List of Enum descriptions (internal)
};

GUID     TUV10::DEF_CTL_INTF = {0x2F8E04F4, 0x17B0, 0x11D2,{ 0xBB, 0x1D, 0x00, 0x80, 0xC8, 0x63, 0x88, 0x94} };
TNoParam TUV10::OptParam;

static inline void ValidCtrCheck(TUV10 *)
{
   delete new TUV10((TComponent*)(0));
};

void __fastcall TUV10::InitControlData(void)
{
  ControlData = &CControlData;
};

void __fastcall TUV10::CreateControl(void)
{
  if (!m_OCXIntf)
  {
    _ASSERTE(DefaultDispatch);
    DefaultDispatch->QueryInterface(DEF_CTL_INTF, (LPVOID*)&m_OCXIntf);
  }
};

_DUV10Disp __fastcall TUV10::GetControlInterface(void)
{
  CreateControl();
  return m_OCXIntf;
};

long __fastcall TUV10::GpibDeviceSelect(short DeviceNo)
{
  return GetControlInterface().GpibDeviceSelect(DeviceNo);
}

long __fastcall TUV10::GetInfomation(short InfomationNo, TVariant* Val)
{
  return GetControlInterface().GetInfomation(InfomationNo, Val);
}

long __fastcall TUV10::GpibDeviceClear(void)
{
  return GetControlInterface().GpibDeviceClear();
}

long __fastcall TUV10::GpibDeviceTrigger(short address)
{
  return GetControlInterface().GpibDeviceTrigger(address);
}

long __fastcall TUV10::GpibGotoLocal(short address)
{
  return GetControlInterface().GpibGotoLocal(address);
}

long __fastcall TUV10::GpibInterfaceClear(void)
{
  return GetControlInterface().GpibInterfaceClear();
}

long __fastcall TUV10::GpibLocalLockOut(void)
{
  return GetControlInterface().GpibLocalLockOut();
}

long __fastcall TUV10::GpibSystemControler(short Select)
{
  return GetControlInterface().GpibSystemControler(Select);
}

long __fastcall TUV10::GpibPassControl(short address)
{
  return GetControlInterface().GpibPassControl(address);
}

long __fastcall TUV10::GpibNoListener(void)
{
  return GetControlInterface().GpibNoListener();
}

long __fastcall TUV10::GpibSetListener(short address, TOLEBOOL ul)
{
  return GetControlInterface().GpibSetListener(address, ul);
}

long __fastcall TUV10::GpibNoTalker(void)
{
  return GetControlInterface().GpibNoTalker();
}

long __fastcall TUV10::GpibSetTalker(short address)
{
  return GetControlInterface().GpibSetTalker(address);
}

long __fastcall TUV10::GpibRemoteEnable(void)
{
  return GetControlInterface().GpibRemoteEnable();
}

long __fastcall TUV10::GpibSelectDeviceClear(short address)
{
  return GetControlInterface().GpibSelectDeviceClear(address);
}

long __fastcall TUV10::GpibSetReceiveDelimiter(short delimiter)
{
  return GetControlInterface().GpibSetReceiveDelimiter(delimiter);
}

long __fastcall TUV10::GpibSetSendDelimiter(BSTR delimiter)
{
  return GetControlInterface().GpibSetSendDelimiter(delimiter);
}

long __fastcall TUV10::GpibBusTimeOut(long time)
{
  return GetControlInterface().GpibBusTimeOut(time);
}

long __fastcall TUV10::GpibSetSRQStatus(short StatusByte, short Pending, long time)
{
  return GetControlInterface().GpibSetSRQStatus(StatusByte, Pending, time);
}

long __fastcall TUV10::GpibSerialPoll(short address, TVariant* StatusByte)
{
  return GetControlInterface().GpibSerialPoll(address, StatusByte);
}

long __fastcall TUV10::GpibParallelPoll(TVariant* StatusByte)
{
  return GetControlInterface().GpibParallelPoll(StatusByte);
}

long __fastcall TUV10::GpibSendStringN(BSTR String, short wait)
{
  return GetControlInterface().GpibSendStringN(String, wait);
}

long __fastcall TUV10::GpibSendString(short address, BSTR String, short wait)
{
  return GetControlInterface().GpibSendString(address, String, wait);
}

long __fastcall TUV10::GpibSendDataN(TVariant* data, long length, short wait)
{
  return GetControlInterface().GpibSendDataN(data, length, wait);
}

long __fastcall TUV10::GpibSendData(short address, TVariant* data, long length, short wait)
{
  return GetControlInterface().GpibSendData(address, data, length, wait);
}

long __fastcall TUV10::GpibReceiveString(short address, TVariant* String, long length)
{
  return GetControlInterface().GpibReceiveString(address, String, length);
}

long __fastcall TUV10::GpibReceiveData(short address, TVariant* data, long length)
{
  return GetControlInterface().GpibReceiveData(address, data, length);
}

long __fastcall TUV10::GpibGetAddressMode(TVariant* mode)
{
  return GetControlInterface().GpibGetAddressMode(mode);
}

void __fastcall TUV10::AboutBox(void)
{
  GetControlInterface().AboutBox();
}

};     // namespace Uv10activexlib_tlb


// *********************************************************************//
// The Register function is invoked by the IDE when this module is 
// installed in a Package. It provides the list of Components (including
// OCXes) implemented by this module. The following implementation
// informs the IDE of the OCX proxy classes implemented here.
// *********************************************************************//
namespace Uv10activexlib_ocx
{

void __fastcall PACKAGE Register()
{
  // [1]
  TComponentClass classes[] = {
                              __classid(Uv10activexlib_tlb::TUV10)
                             };
  RegisterComponents("ActiveX", classes,
                     sizeof(classes)/sizeof(classes[0])-1);
}
};     // namespace Uv10activexlib_ocx
