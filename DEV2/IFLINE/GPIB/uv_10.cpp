
// gpib deamon

#include <string.h>
#include <dev2\ifline\gpib\uv_10.h>
#include <winbase.h>

// uv_10 support fuctions

int  uv_10::HowManyGpib = 0;
int  uv_10::initStat    = 0;
int  uv_10::buffAddr[];
char uv_10::buffData[];

int uv_10::init()
{  
   int p; 
   p = GpibTimeOut(500);
   hwait();
   p &= GpibSystemControler(1);
   hwait();
   p &= SetGpibMyAddress(0);
   hwait();
   p &= GpibSetSendDelimiter("\x0d\x0a");
   hwait();
   p &= GpibSetReceiveDelimiter(0x0a);
   hwait();
   p &= GpibInterfaceClear();
   hwait();
   p &= GpibRemoteEnable();
   return ( !p );
   // ライブラリは 1 = 正常だが、GPIB class では、０＝正常としているため
}

void uv_10::hwait()
{ 
  Sleep(80);
}

uv_10::uv_10(int addr, ifLine::delim sdelim) : ifLine(addr,gpib, sdelim)
{  if ( (HowManyGpib == 0) && ((initStat = init()) == 0));
   HowManyGpib++;
   myType = ifLine::gpib;
   setSpeed(fast);
}

ifLine::stat uv_10::talk(char *message)
{
  int stat;
  int len = strlen(message);
  setDelim(myDelim);
  strcpy(buffData, message);
  stat = GpibSendData(myAddr, buffData, len);
  hwait();
  return (stat == 1) ? ifLine::normalEnd : ifLine::error;
}


ifLine::stat uv_10::listen(char *message, unsigned int &len)
{ 

  int stat;
  
  setDelim(myDelim);
  stat = GpibReceiveData(myAddr, buffData, len);
  hwait();
  strncpy(message, buffData, strlen(buffData));
  message[len] = '\0';

  return (stat == 1) ? ifLine::normalEnd : ifLine::error;
}

ifLine::stat uv_10::setDelim(ifLine::delim delim)
{  myDelim = delim;

   switch(myDelim & 7)
   {  case ifLine::cr : 
              GpibSetSendDelimiter("\x0d");
              GpibSetReceiveDelimiter(0x0d);
              break;

      case ifLine::lf :
              GpibSetSendDelimiter("\x0a");
              GpibSetReceiveDelimiter(0x0a);
              break;

      case ifLine::cr+ifLine::lf : 
              GpibSetSendDelimiter("\x0d\x0a");
              GpibSetReceiveDelimiter(0x0a);
              break;
   }

   hwait();
   return  ifLine::normalEnd;
}

 uv_10::~uv_10()
{
   HowManyGpib--;
}


