
#if !defined (_BCL_ifLine_UV10)
#define       _BCL_ifLine_UV10

#include <dev2/ifline/ifline.h>
#include <dev2/ifline/gpib/uv10f2.h>
#include <conio.h>
#include <string.h>

// gpib demon

class uv_10 :public ifLine
{
  protected:

	int         init();
	void        hwait();

	static int  initStat;
	static int  HowManyGpib;
	static int  buffAddr[8];
	static char buffData[128];
	static int  init(int MyAddr);

  public:
	uv_10(int addr, ifLine::delim sdelim = crlf);
	virtual ifLine::stat  talk(char *message);
	virtual ifLine::stat  listen(char *message, unsigned int &len);
	virtual ifLine::stat  setDelim(ifLine::delim delim);
	virtual ~uv_10();
};


#endif

