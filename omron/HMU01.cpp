
#include <iostream>

#include "HMU01.h"

HMU01::HMU01(int portNo)
{
  char buff[16];
  unsigned int len = 16;
  
  port = new WinRS(portNo, 9600, ifLine::cr, "8E1", false);
  if ((*port) != 0)
  {
    do
    {
      // 最初に一度だけ通信しておく（むしろ、１発取りこぼし対策）
      len = 16;
      port->talk("1012");
      port->listen(buff, len);
    } while(len != 5);
  }
}

HMU01::~HMU01()
{
  delete port;
}

#include <cstdio>

void HMU01::makePage(int bank, int page, char *buff)
{
  unsigned long v = 1;
  v <<= page;
  std::sprintf(buff, "0020%02d%04X", bank, v);
}

unsigned long HMU01::read(int page) throw (pageOverRange)
{
   int bank = page / 16;
   page %= 16;
   return read(bank, page);
}

unsigned long HMU01::read(int bank, int page) throw (pageOverRange)
{
   char buff[128];
   if (! paramCheck(bank, page)) throw pageOverRange(bank, page);
   makePage(bank, page, buff);
   buff[0] = '3';
   buff[1] = '1';
   buff[2] = '2';
   buff[3] = '0'; // コマンドと交信パラメータに差し替え
   return getPageData(buff);
}

unsigned long HMU01::readWait(int page) throw (pageOverRange)
{
   int bank = page / 16;
   page %= 16;
   return readWait(bank, page);
}

unsigned long HMU01::readWait(int bank, int page) throw (pageOverRange)
{
   char buff[128];
   if (! paramCheck(bank, page)) throw pageOverRange(bank, page);
   makePage(bank, page, buff);
   buff[0] = '3';
   buff[1] = '1';
   buff[2] = '2';
   buff[3] = '1'; // コマンドと交信パラメータに差し替え
   return getPageData(buff);
}

bool HMU01::paramCheck(int bank, int page)
{
  if (bank > 1) return false;
  else if (bank == 1 && page > 11) return false;
  else if (page > 15) return false;
  
  return true;
}

#include <cstdlib>

unsigned long HMU01::getPageData(char *buff)
{
   char recvBuff[16];
   unsigned int len = 16;

   port->talk(buff);
   while(! port->loc());
   port->listen(recvBuff, len);

   if (len != 11) // エラー発生
   {
      recvBuff[2] = '\0';
      throw tagError(std::strtoul(recvBuff, 0, 16));
   }

   return std::strtoul(recvBuff + 2, 0, 16);
}

void HMU01::write(int page, unsigned long data) throw (pageOverRange)
{
   int bank = page / 16;
   page %= 16;
   write(bank, page, data);
}

void HMU01::write(int bank, int page, unsigned long data) throw (pageOverRange)
{
   char buff[128];
   if (! paramCheck(bank, page)) throw pageOverRange(bank, page);
   makePage(bank, page, buff);
   buff[0] = '3';
   buff[1] = '2';
   buff[2] = '2';
   buff[3] = '0'; // コマンドと交信パラメータに差し替え
   putPageData(buff, data);
}

void HMU01::writeWait(int page, unsigned long data) throw (pageOverRange)
{
   int bank = page / 16;
   page %= 16;
   writeWait(bank, page, data);
}

void HMU01::writeWait(int bank, int page, unsigned long data) throw (pageOverRange)
{
   char buff[128];
   if (! paramCheck(bank, page)) throw pageOverRange(bank, page);
   makePage(bank, page, buff);
   buff[0] = '3';
   buff[1] = '2';
   buff[2] = '2';
   buff[3] = '1'; // コマンドと交信パラメータに差し替え
   putPageData(buff, data);
}

void HMU01::putPageData(char *buff, unsigned long data)
{
   char recvBuff[16];
   unsigned int len = 16;
   std::sprintf(buff + 10, "%08X", data);
   port->talk(buff);
   while(! port->loc());
   port->listen(recvBuff, len);
   recvBuff[2] = '\0';
   int result = std::strtol(recvBuff, 0, 16);
   if (result != 0) throw(tagError(result));
}

void HMU01::stop()
{
   char buff[16];
   unsigned int len = 16;
   port->talk("13");
   while(! port->loc());
   port->listen(buff, len);
}

