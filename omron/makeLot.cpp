
#include "makeLot.h"
#include <cstdio>

char mStr[] = "0123456789XYZ";

AnsiString makeLot(TDateTime date)
{
  char buff[1024];

  unsigned short year;
  unsigned short month;
  unsigned short day;
  date.DecodeDate(&year, &month, &day);
  std::sprintf(buff, "%02d%c%01d", day, mStr[month], (year % 10));
  return static_cast<AnsiString>(buff);
}

