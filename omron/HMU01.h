#if !defined(_BCL_HMU01)
#define      _BCL_HMU01

#include <j:/dev2/ifline/rsline/winrs.h>

class tagError
{
   private:
     int myCode;

   public:
     tagError(int errorCode) : myCode(errorCode) {}
     int getError() { return myCode; }
};

class pageOverRange
{
  private:
    int myBank;
    int myPage;

  public:
    pageOverRange(int bank, int page) : myBank(bank), myPage(page) { }
    int getPage() { return myPage; }
    int getBank() { return myBank; }
};

class HMU01
{
  private:
    WinRS *port;
    void makePage(int bank, int page, char *buff);
    bool paramCheck(int bank, int page);
    unsigned long getPageData(char *buff);
    void putPageData(char *buff, unsigned long data);

  public:
   HMU01(int portNo);
   ~HMU01();

   bool isValid() { return ((*port) != 0); }

   unsigned long read(int bank, int page) throw (pageOverRange);
   unsigned long read(int page) throw (pageOverRange);
   void          write(int bank, int page, unsigned long data) throw (pageOverRange);
   void          write(int page, unsigned long data) throw (pageOverRange);

   unsigned long readWait(int bank, int page) throw (pageOverRange);
   unsigned long readWait(int page) throw (pageOverRange);
   void          writeWait(int bank, int page, unsigned long data) throw (pageOverRange);
   void          writeWait(int page, unsigned long data) throw (pageOverRange);

   void stop();

};

#endif

