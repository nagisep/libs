
#include <iostream>

#include "HMU01.h"

int main()
{
  HMU01 rfid(3);

  try
  {

      for (int page = 0; page < 20; page ++)
      {
         std::cout << "write : " << page << "\n";
         rfid.writeWait(page, page + 1);
      }

      for (int page = 0; page < 20; page ++)
      {
         std::cout << "read : " << page << "\n";
         std::cout << (rfid.readWait(page)) << "\n";
      }

  }
 catch(tagError &tag)
 {
    std::cout << "TagError : " << (tag.getError());
 }
 catch(pageOverRange &page)
 {
    std::cout << "PageRange : " << (page.getPage());
 }
 catch(...)
 {
    std::cout << "???";
 }

  return 0;
}
