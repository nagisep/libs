
// オムロン形式のロット表示を作成して返す関数

#if !defined(_BCL_OMRON_LOT)
#define      _BCL_OMRON_LOT

#include <system.hpp>
AnsiString makeLot(TDateTime date = TDateTime().CurrentDate());

#endif

